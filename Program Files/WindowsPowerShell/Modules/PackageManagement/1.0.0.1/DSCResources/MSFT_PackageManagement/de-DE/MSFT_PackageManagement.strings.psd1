# Localized	12/07/2019 11:47 AM (GMT)	303:6.40.20520 	MSFT_PackageManagement.strings.psd1
#########################################################################################
#
# Copyright (c) Microsoft Corporation.
#
#########################################################################################
ConvertFrom-StringData @'
###PSLOC
StartGetPackage=Startet den Aufruf von Get-package {0} unter Verwendung von PSModulePath {1}.
PackageFound=Das Paket {0} wurde gefunden.
PackageNotFound=Das Paket {0} wurde nicht gefunden.
MultiplePackagesFound=Für das Paket {0} wurde mehr als ein Paket gefunden.
StartTestPackage=Get-TargetResource wird von Test-TargetResource unter Verwendung von {0} aufgerufen.
InDesiredState=Die Ressource {0} weist den gewünschten Zustand auf. Der erforderliche Ensure-Wert ist {1} und der tatsächliche Ensure-Wert {2}.
NotInDesiredState=Die Ressource {0} weist nicht den gewünschten Zustand auf. Der erforderliche Ensure-Wert ist {1} und der tatsächliche Ensure-Wert {2}.
StartSetPackage=Test-TargetResource wird von Set-TargetResource unter Verwendung von {0} aufgerufen.
InstallPackageInSet=Install-Package wird unter Verwendung von {0} aufgerufen.
###PSLOC

'@

