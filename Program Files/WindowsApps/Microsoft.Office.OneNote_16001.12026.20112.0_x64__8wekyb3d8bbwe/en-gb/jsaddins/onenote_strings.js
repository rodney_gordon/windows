var JSADDINS = {};

JSADDINS.Strings = function JSADDINS_Strings() {
}


 
JSADDINS.Strings.IDS_NOTETAGS_NAME_THIS_TAG = 'Name this tag';
JSADDINS.Strings.IDS_NOTETAGS_CREATE_TAG_TITLE = 'Create a Tag';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_CURRENT_NOTEBOOK_VIEW_WITH_COUNT = 'Notebook (|0)';
JSADDINS.Strings.Generic_Retry = 'Try Again';
JSADDINS.Strings.IDS_NOTETAGS_CREATING = 'Creating';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_FISHBOWL_NO_VIOLATIONS_TITLE = 'No issues found!';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_FISHBOWL_CHECK_ALL_NOTEBOOK = 'Check entire notebook';
JSADDINS.Strings.Invalid_Agave = 'Invalid Add-in';
JSADDINS.Strings.LT_NoInternet = 'Learning Tools are not available when offline. Please connect to the Internet and try again.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_CURRENT_PAGE_VIEW = 'Page';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_LOCKED_SECTION_TITLE = 'This page is in a password-protected section.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_UNNAMED_SECTION_HOWFIX_PATTERN = 'To add a custom name for a section, |0, right-click the section name, and then select Rename.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_FISHBOWL_NO_VIOLATIONS_SUBTITLE_PAGE = 'Your page can easily be read by people with disabilities.\u200b';
JSADDINS.Strings.IDS_NOTETAGS_FAILED = 'Failed to create custom tag';
JSADDINS.Strings.Generic_NoInternet = 'This feature is not available when offline. Please connect to the Internet and try again.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_INVALID_LOCATION_TITLE = 'Not available';
JSADDINS.Strings.IDS_NOTETAGS_MESSAGE_ERROR = 'We\u2019re having trouble saving your tag. We\u2019ll keep trying, but your changes may be lost if you exit OneNote.';
JSADDINS.Strings.IDS_NOTETAGS_CHOOSE_ICON = 'Choose an icon';
JSADDINS.Strings.Learn_More = 'Learn more';
JSADDINS.Strings.IDS_NOTETAGS_TAG_NAME = 'Tag name';
JSADDINS.Strings.IDS_NOTETAGS_SELECTED = 'Selected';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_CURRENT_PAGE_VIEW_WITH_COUNT = 'Page (|0)';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_INVALID_LOCATION_DESCRIPTION = 'This location doesn\'t contain any valid pages. Wait for your notes to fully sync, or try another location.';
JSADDINS.Strings.IDS_NOTETAGS_MESSAGE_INVALID_TAGNAME = 'Only use alphanumeric characters for tag names.';
JSADDINS.Strings.MD_NoInternet = 'Meeting Details are not available when offline. Please connect to the Internet and try again.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_FISHBOWL_NO_VIOLATIONS_SUBTITLE_NOTEBOOK = 'Your notebook can easily be read by people with disabilities.\u200b';
JSADDINS.Strings.IDS_NOTETAGS_MESSAGE_INVALID_IDENTITY = 'OneNote needs your account credentials before you can create new tags. Please sign in and then try again.';
JSADDINS.Strings.IDS_NOTETAGS_CREATED = 'Custom tag created';
JSADDINS.Strings.IDS_COMMA_LIST_PATTERN = '|0, |1';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_LOCKED_SECTION_DESCRIPTION = 'Unlock the section to check its pages for accessibility issues.';
JSADDINS.Strings.IDS_NOTETAGS_ICON_LIST = 'Icon List';
JSADDINS.Strings.IDS_NOTETAGS_MESSAGE_ALREADY_EXISTS = 'The tag you\u2019re trying to create already exists.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_UNNAMED_SECTION_HOWFIX_GO_TO_STEP = 'go to the section';
JSADDINS.Strings.IDS_NOTETAGS_MESSAGE_SUCCESS = 'Your tag was created successfully.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_CURRENT_NOTEBOOK_VIEW = 'Notebook';
JSADDINS.Strings.IDS_NOTETAGS_MESSAGE_INVALID_LABEL = 'The tag name you\u2019re trying to use contains invalid characters. Use only alphanumeric characters.';
JSADDINS.Strings.IDS_NOTETAGS_ALL_ICONS = 'All Icons';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_UNNAMED_SECTION_GROUP_HOWFIX_PATTERN = 'To add a custom name for a section group, |0, right-click the section group name, and then select Rename.';
JSADDINS.Strings.IDS_NOTETAGS_CREATE = 'Create';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_UNNAMED_SECTION_GROUP_HOWFIX_GO_TO_STEP = 'go to the section group';
JSADDINS.Strings.IDS_NOTETAGS_SELECTED_ICON = 'Selected icon:';
JSADDINS.Strings.IDS_NOTETAGS_NOT_SELECTED = 'Not selected';
JSADDINS.Strings.IDS_NOTETAGS_FEATURED = 'Featured';
