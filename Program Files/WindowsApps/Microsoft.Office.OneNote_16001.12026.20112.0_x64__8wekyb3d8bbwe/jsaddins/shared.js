var Localization;
(function (Localization) {
    const defaultLang = "en-us";
    function setLanguageStringsAsync(lang) {
        let anyWindow = window;
        if (anyWindow["JSADDINS"] && anyWindow["JSADDINS"].Strings) {
            return Promise.resolve("ok");
        }
        if (!lang) {
            lang = defaultLang;
        }
        var promise = new Promise(function (resolve, reject) {
            let script = document.createElement('script');
            script.type = 'text/javascript';
            script.onload = resolve;
            script.onerror = reject;
            script.src = "ms-appx-web:///" + lang + "/jsaddins/onenote_strings.js";
            document.head.appendChild(script);
        });
        if (lang != defaultLang) {
            promise = promise.catch(() => setLanguageStringsAsync(defaultLang));
        }
        return promise;
    }
    Localization.setLanguageStringsAsync = setLanguageStringsAsync;
    function stringFromPattern(pattern, ...params) {
        for (let i = 0; i < params.length; ++i) {
            pattern = pattern.replace(new RegExp("\\|" + i, "g"), params[i]);
        }
        return pattern;
    }
    Localization.stringFromPattern = stringFromPattern;
})(Localization || (Localization = {}));
