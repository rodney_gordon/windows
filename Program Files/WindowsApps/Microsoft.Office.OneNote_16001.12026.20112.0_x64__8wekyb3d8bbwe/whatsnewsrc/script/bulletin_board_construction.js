
// Hierarchy Class Constants
containerDivId = "main-board";
containerDivClass = "bulletin-board";
itemSectionPrefix = "bulletin-";
contentPrefix = "content-";
itemSectionClass = "bulletin";
itemHeaderClass = "header";
iconBackgroundClass = "iconBackground";
iconClass = "icon";
itemTitleClass = "title";
contentClass = "content"
collapsiblePrefix = "collapsible-";
buttonPrefix = "button";
bulletinItemTag = "SECTION";

visualClassSuffix = "-visual";

errorEncountered = false;
currentItemValid = false;

// A key code "enum" for better readability
keyCode = {
	TAB: 9,
	ENTER: 13,
	ESC: 27,
	LEFT: 37,
	UP: 38,
	RIGHT: 39,
	DOWN: 40
}

// Disable ctrl+wheel and pinch from zooming
document.addEventListener("wheel", function(event){
    if (event.ctrlKey){
        event.preventDefault();
    }
});

// Keyboard event handler
document.addEventListener("keydown", function(event){
    event.stopPropagation();

    if (event.keyCode === keyCode["TAB"]) {
        document.body.classList.add("show-outline");
        return;
    }

    var activeElement = document.activeElement;
    if (activeElement.tagName === bulletinItemTag) {
        var targetElement = null;

        if (event.keyCode === keyCode["UP"]) {
            var targetElement = activeElement.previousElementSibling;
        } else if (event.keyCode === keyCode["DOWN"]) {
            targetElement = activeElement.nextElementSibling;
        }

        if (targetElement !== null) {
            document.body.classList.add("show-outline");
            targetElement.setAttribute("tabindex", 0);
            targetElement.focus();
            activeElement.removeAttribute("tabindex");
        }
    }
});

// Whenever pointer is down, remove focus outlines from all object
document.addEventListener("pointerdown", function(event){
    document.body.classList.remove("show-outline");
});

function onBulletinListFocused() {
    var item = document.querySelector(bulletinItemTag + "[tabindex='0']");
    if (item != null) {
        item.focus();
        document.body.removeAttribute("tabindex");
    }
}

function applyDarkTheme() {
    let s = document.getElementById('theme-styles');
    s.href = 'bulletin_board_dark.css';
}
function applyLightTheme() {
    let s = document.getElementById('theme-styles');
    s.href = 'bulletin_board_light.css';
}

function addBreakToElement(parent)
{
    parent.appendChild(document.createElement("br"));
}

var collapsibleDivCount = 1;
function addCollapsibleContent(bodyInfo, parent) {
    if (errorEncountered)
        return;

    var collapsingContainer = document.createElement("div");
    collapsingContainer.className = collapsiblePrefix + contentClass;
    collapsingContainer.id = collapsiblePrefix + collapsibleDivCount;
    collapsingContainer.hidden = true;
    var button = document.createElement("a");
    var message = document.createTextNode(bodyInfo.collapsedText);
    button.otherLabel = bodyInfo.expandedText;
    button.appendChild(message);
    button.class = collapsiblePrefix + buttonPrefix;
    button.id = button.class + "-" + collapsibleDivCount;
    button.href = "#" + button.id;

    button.onclick = function() {
        collapsingContainer.hidden = !collapsingContainer.hidden;
        var swapLabel = button.otherLabel;
        button.otherLabel = button.innerHTML;
        button.innerHTML = swapLabel;
    };

    collapsibleDivCount++;
    addBodyItems(bodyInfo.data, collapsingContainer);

    if (!errorEncountered) {
        parent.appendChild(collapsingContainer);
        addBreakToElement(parent);
        parent.appendChild(button);
    }
    else {
        errorEncountered = false;
    }
}

var animationCount = 1;
function animationAnchorDiv() {
    var anchorDiv = document.createElement("div");
    anchorDiv.className = "animation-anchor";
    anchorDiv.id = "animation_" + animationCount;
    animationCount++;

    return anchorDiv;
}

function createAnimationFromConfig(animationInfo, animationAnchorDiv) {
    var shouldAutoplay = true;

    if ("autoplay" in animationInfo) {
        shouldAutoplay = animationInfo.autoplay;
    }

    var shouldLoop = true;
    if ("loop" in animationInfo) {
        shouldLoop = animationInfo.loop;
    }

    var animationParameters = JSON.parse(animationInfo.data);

    var params = {
        container: animationAnchorDiv,
		renderer: 'svg',
		loop: shouldLoop,
		autoplay: shouldAutoplay,
		animationData: animationParameters
    }
    var anim;
    if (bodymovin)
      anim = bodymovin.loadAnimation(params);
}

function addAnimationContent(bodyInfo, parent) {
    var anchorDiv = animationAnchorDiv();

    createAnimationFromConfig(bodyInfo, anchorDiv);

    var classes = " ";
    if ("size" in bodyInfo) {
        classes += bodyInfo.size.toLowerCase() + visualClassSuffix;
    }
    anchorDiv.className += classes;

    parent.appendChild(anchorDiv);
    currentItemValid = true;
}

function addImageContent(bodyInfo, parent) {
    var imageTag = document.createElement("img");
    imageTag.setAttribute("aria-hidden", "true");

    if ("size" in bodyInfo) {
        imageTag.className = bodyInfo.size.toLowerCase() + visualClassSuffix;
    }

    imageTag.src = bodyInfo.data;
    parent.appendChild(imageTag);
    addBreakToElement(parent);
    currentItemValid = true;
}

function addTextContent(bodyInfo, parent) {
    var paragraphTag = document.createElement("span");
    paragraphTag.setAttribute("role", "text");
    var message = document.createTextNode(bodyInfo.data);
    paragraphTag.appendChild(message);
    parent.appendChild(paragraphTag);
    addBreakToElement(parent);
    currentItemValid = true;
}

var ContentType  = {
    Text  : 'Text',
    Image     : 'ImageUrl',
    Animation : 'AnimationData',
    Collapsible : 'Collapsible'
};

function addBodyItems(bodyArray, parent) {
    for (var i in bodyArray) {
        var content = bodyArray[i];
        
        if (content == null) {
            errorEncountered = true;
            // TODO: (MaAllen) - add error reporting for null items

            return;
        }

        if (!content.type ||
            !content.data ) 
        {
            // TODO: (MaAllen) - add error reporting for invalid body items

            continue;
        }

        switch (content.type)
        {
            case ContentType.Text:
                addTextContent(content, parent);
                break;
            case ContentType.Image:
                addImageContent(content, parent);
                break;
            case ContentType.Animation:
                addAnimationContent(content, parent);
                break;
            case ContentType.Collapsible:
                addCollapsibleContent(content, parent);
                break;
            default:
                // TODO: (MaAllen) - Add Error Handling for Unexpected Type
                break;
        }
    }
}

function addHeader(titleInfo, iconInfo, parent) {
    
    if (!iconInfo ||
        !"data" in iconInfo || 
        !"type" in iconInfo) {
        errorEncountered = true;
        return;
    }

    var headerTag = document.createElement("div");
    headerTag.className = itemHeaderClass;
    headerTag.setAttribute("aria-hidden", "true");

    var iconTag;
    switch (iconInfo.type)
    {
    case ContentType.Image:
        iconTag = document.createElement("img");
        iconTag.src = iconInfo.data;
        iconTag.setAttribute("aria-hidden", "true");
        break;
    case ContentType.Animation:
        iconTag = animationAnchorDiv();
        createAnimationFromConfig(iconInfo, iconTag);
        break;
    default:
        // TODO: (MaAllen) - Add Error Handling for icon type
        return;
    }

    iconTag.className = iconClass;

    var iconDiv;
    iconDiv = document.createElement("div");
    iconDiv.className = iconBackgroundClass;
    iconDiv.appendChild(iconTag);

    headerTag.appendChild(iconDiv);
    var titleTag = document.createElement("h1");
    titleTag.className = itemTitleClass;
    var titleMsg = document.createTextNode(titleInfo.data);
    titleTag.appendChild(titleMsg);

    headerTag.appendChild(titleTag);
    parent.appendChild(headerTag);
}

function addBulletin(itemData, isFirstItem) {
    if (itemData.version != 1) {
        // TODO: (MaAllen) - Add Error handling unexpected item schema
        // TODO: (MaAllen) - Add more schema validation
        return;
    }

    // Extract Title, Icon Type & Info, and Body Components
    var bulletin = document.createElement(bulletinItemTag);
    bulletin.id = itemSectionPrefix+itemData.id;
    bulletin.className = "bulletin";
    addHeader(itemData.title, itemData.icon, bulletin);
    bulletin.setAttribute("aria-label", itemData.title.data);
    bulletin.setAttribute("role", "listitem");
    if (isFirstItem) {
        bulletin.setAttribute("tabindex", 0);
    }

    var itemContentDiv = document.createElement("div");
    itemContentDiv.className = contentClass;
    itemContentDiv.setAttribute("role", "presentation");
    addBodyItems(itemData.body, itemContentDiv);

    if (currentItemValid && !errorEncountered) {
        if (itemData.body[0].type === "Text") {
            itemContentDiv.getElementsByTagName("span")[0].setAttribute("id", contentPrefix + itemData.id);
            bulletin.setAttribute("aria-describedby", contentPrefix + itemData.id);
        }
        bulletin.appendChild(itemContentDiv);
        document.getElementById(containerDivId).appendChild(bulletin);
    }
}

function setup(bulletinItems, documentDir, title) {
    if (!bulletinItems)
        return;

    document.dir = documentDir;
    document.title = title;
    document.getElementById("main-board").setAttribute("aria-label", title);

    for (bulletinNumber = 0; bulletinNumber < bulletinItems.length; bulletinNumber++) {
        currentItemValid = false;
        addBulletin(bulletinItems[bulletinNumber], bulletinNumber == 0 /*isFirstItem*/);
    }

    location.hash = "Loaded";
}

function getDocumentHeight() {
    html = document.documentElement;
    Debug.writeln(html.scrollHeight);
    return html.scrollHeight.toString();
}

window.onload = function () {
    document.addEventListener('keydown', function (event) {
        if (event.keyCode === 27) {
            location.hash = "Close";
        }
    }, false);

    let onHeightChange = (callback) => {
        let html = document.documentElement;
        let lastHeight = html.scrollHeight;
        let newHeight;
        (function work() {
            newHeight = html.scrollHeight;
            if (lastHeight !== newHeight) {
                callback();
                lastHeight = newHeight;
            }

            if (html.onHeightChangeTimer)
                clearTimeout(html.onHeightChangeTimer);

            html.onHeightChangeTimer = setTimeout(work, 200);
        })();
    }

    onHeightChange(() => {
        location.hash = "Layout";
    })
} 