/* WinRT host page JavaScript library */

/*
	Copyright (c) Microsoft Corporation.  All rights reserved.
*/


/*
    Your use of this file is governed by the Microsoft Services Agreement http://go.microsoft.com/fwlink/?LinkId=266419.

    This file also contains the following Promise implementation (with a few small modifications):
        * @overview es6-promise - a tiny implementation of Promises/A+.
        * @copyright Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors (Conversion to ES6 API by Jake Archibald)
        * @license   Licensed under MIT license
        *            See https://raw.githubusercontent.com/jakearchibald/es6-promise/master/LICENSE
        * @version   2.3.0
*/
var CrossIFrameCommon;(function(a){(function(a){var c="EventCallback",b="MethodCallback";a[a[b]=0]=b;a[a[c]=1]=c})(a.CallbackType||(a.CallbackType={}));var c=a.CallbackType,b=function(){function a(a,b,c){this.callbackType=a;this.callbackId=b;this.params=c}return a}();a.CallbackData=b})(CrossIFrameCommon||(CrossIFrameCommon={}));var WinRT;(function(b){var a="addInFrame";function g(){window.addEventListener("message",b.OnReceiveMessage)}b.Init=g;function f(c){var b=document.getElementById(a);b.setAttribute("src",c)}b.LoadAddIn=f;function d(b,d){var c=new CrossIFrameCommon.CallbackData(CrossIFrameCommon.CallbackType.MethodCallback,b,d),e=document.getElementById(a);e.contentWindow.postMessage(JSON.stringify(c),"*")}b.agaveHostCallback=d;function c(b,d){var c=new CrossIFrameCommon.CallbackData(CrossIFrameCommon.CallbackType.EventCallback,b,d),e=document.getElementById(a);e.contentWindow.postMessage(JSON.stringify(c),"*")}b.agaveHostEventCallback=c;function e(b){var c=document.getElementById(a);if(b.source!=c.contentWindow)return;window.external.notify(b.data)}b.OnReceiveMessage=e})(WinRT||(WinRT={}));function agaveHostCallback(a,b){WinRT.agaveHostCallback(a,b)}function agaveHostEventCallback(a,b){WinRT.agaveHostEventCallback(a,b)}function LoadAddIn(a){WinRT.LoadAddIn(a)}window.addEventListener("keydown",function(a){a.preventDefault=a.preventDefault||function(){a.returnValue=false};if(a.keyCode==117){a.preventDefault();a.stopPropagation();var b=5;if(a.shiftKey)b=6;var c={methodId:6,params:{actionId:b},callbackId:-1,callbackFunction:"agaveHostCallback"};window.external.notify(JSON.stringify(c))}})