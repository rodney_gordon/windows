(function(){

/****************************************************************************
	Global Variables and Event Registration
****************************************************************************/
// Maximum height of the flyout
var maxHeight = 700;
// Accessibility string read by screen reader when each category is on focus
var categoryA11YString;
// Accessibility string read by screen reader when each notification item is on focus
var notificationItemUnreadA11YString;
var notificationItemReadA11YString;
// Accessibility string read by screen reader when the delete button is on focus
var deleteButtonA11YString;
// Accessibility string read by screen reader after refreshed
var refreshedA11YString;
// Accessibility string read by screen reader after deleted
var deletedA11YString;
// Accessibility string read by screen reader after navigated to another notebook
var navigatedA11YString;
// Accessibility string read by screen reader after the setting list is on focus
var settingsListA11YString;
// Accessibility string read by screen reader after a setting is enabled
var settingsEnabledA11YString;
// Accessibility string read by screen reader after a setting is disabled
var settingsDisabledA11YString;

// Current navigation location: notifications or settings
var navigation = "navigation-notifications";

var isDarkMode = false;

// Setting items
var settingItems;
// Representation of App side hero notifications
var heroNotificationIDApp = new Set();
// Representation of UI side hero notifications
var heroNotificationIDUI = new Set();

// A key code "enum" for better readability
var keyCode = {
	TAB: 9,
	ENTER: 13,
	ESC: 27,
	SPACE: 32,
	LEFT: 37,
	UP: 38,
	RIGHT: 39,
	DOWN: 40
}

// Keyboard event handler
document.addEventListener("keydown", NotificationItemKeyBoardEventHandler);
// Whenever pointer is down, remove focus outlines from all object
document.addEventListener("pointerdown", function(event){
	document.getElementsByTagName("body")[0].classList.remove("show-outline");
});
// Disable ctrl+wheel and pinch from zooming
document.addEventListener("wheel", function(event){
	if (event.ctrlKey){
		event.preventDefault();
	}
});


/****************************************************************************
	Functions that interact with XAML

	These functions are exposed to outside with window.fn = function fn()
****************************************************************************/
/*---------------------------------------------------------------------------
	RaiseEvent

	Notify XAML an event has happened.
---------------------------------------------------------------------------*/
window.RaiseEvent = function RaiseEvent(event, id,  fChecked, heroItemData){
	eventArg = {"event": event, "id": id, "height": document.body.clientHeight.toString(), "activated": fChecked, "heroitemdata": JSON.stringify(heroItemData)}
	console.log(eventArg);
	window.external.notify(JSON.stringify(eventArg));
}

/*---------------------------------------------------------------------------
	SetLocalizedStrings

	Called by XAML to provide all the localized strings used in UI
---------------------------------------------------------------------------*/
window.SetLocalizedStrings = function SetLocalizedStrings(jsonStrings, isRTL){
	if (isRTL == "true"){
		document.dir = "rtl";
	}

	var strings = JSON.parse(jsonStrings);

	document.getElementById("title-notifications").textContent = strings[0];
	document.title = strings[0];

	document.getElementById("tips").getElementsByClassName("section-title")[0].textContent = strings[1];
	document.getElementById("recentnotebooks").getElementsByClassName("section-title")[0].textContent = strings[2];
	document.getElementById("activity").getElementsByClassName("section-title")[0].textContent = strings[3];
	document.getElementById("atmentions").getElementsByClassName("section-title")[0].textContent = strings[20];

	document.getElementById("fishbowl").textContent = strings[4];
	document.getElementById("fishbowl").setAttribute("aria-label", strings[4]);
	
	document.getElementById("refresh").setAttribute("aria-label", strings[5]);
	document.getElementById("settings").setAttribute("aria-label", strings[6]);

	categoryA11YString = strings[7];
	notificationItemUnreadA11YString = strings[8];
	notificationItemReadA11YString = strings[9];
	deleteButtonA11YString = strings[10];
	refreshedA11YString = strings[11];
	deletedA11YString = strings[12];
	navigatedA11YString = strings[13];

	document.getElementById("title-settings").textContent = strings[14];
	document.getElementById("settings-text").textContent = strings[15];
	document.getElementById("return").setAttribute("aria-label", strings[16]);

	settingsListA11YString = strings[17];
	settingsEnabledA11YString = strings[18];
	settingsDisabledA11YString = strings[19];
}

/*---------------------------------------------------------------------------
	ClearNotifications

	Clear all non hero notifications and the heroNotificationIDApp
---------------------------------------------------------------------------*/
window.ClearNotifications = function ClearNotifications(){
	var notifications = document.getElementsByClassName("notification-item");

	while (notifications.length > 0){
		notifications[0].remove();
	}

	// App clears all notifications, clear the App representation heroNotificationIDApp
	heroNotificationIDApp.clear();
}

/*---------------------------------------------------------------------------
	RefreshNotifications

	- Set the notificationDeleteLeftOffset based on if there's scroll bar
	- Check and hide any empty category, show fishbowl if there's no notification
	- Set accessibility strings to notifications and delete buttons

	This function needs to be called each time the DOM structure is changed.
	For example, when a notificationItem is deleted, or after all 
	notificationItems have been added (by XAML)
---------------------------------------------------------------------------*/
window.RefreshNotifications = function RefreshNotifications(isRefresh){
	var heroNotifications = document.getElementById("hero-notifications");

	RefreshHeroNotifications(heroNotifications);
	RefreshNormalNotifications();

	// Show the no notification string if there's no notification
	var body = document.getElementsByTagName("body")[0];
	var itemCount = document.getElementsByClassName("notification-item").length + heroNotifications.children.length;
	if (itemCount == 0){
		body.classList.add("show-fishbowl");
	} else {
		if (document.activeElement && document.activeElement.id === "fishbowl") {
			document.getElementById("refresh").focus();
		}
		body.classList.remove("show-fishbowl");
	}

	// Let screen reader announce the Refreshed event
	if (isRefresh == "true"){
		AnnounceEvent(refreshedA11YString.replace("|0", itemCount));
	}
}

/*---------------------------------------------------------------------------
	RefreshHeroNotifications
---------------------------------------------------------------------------*/
function RefreshHeroNotifications(heroNotifications){
	for (var i = 0; i < heroNotifications.children.length; i++){
		var heroNotification = heroNotifications.children[i];
		var id = heroNotification["notification-id"];
		heroNotification.setAttribute("tabindex", 0);

		if (heroNotificationIDUI.has(id) && !heroNotificationIDApp.has(id)){
			heroNotifications.children[i].remove();
			heroNotificationIDUI.delete(id);
			i--;
		} else {
			if (heroNotification.classList.contains("heroItem-TextBox")){
				heroNotification.getElementsByTagName("input")[0].setAttribute("tabindex", 0);
				heroNotification.getElementsByTagName("button")[0].setAttribute("tabindex", 0);
				heroNotification.getElementsByClassName("message")[0].setAttribute("tabindex", 0);
				heroNotification.getElementsByTagName("footer")[0].setAttribute("tabindex", 0);
			} else if (heroNotification.classList.contains("heroItem-Button")){
				heroNotification.getElementsByTagName("button")[0].setAttribute("tabindex", 0);
			}
		}
	}
}

/*---------------------------------------------------------------------------
	RefreshNormalNotifications
---------------------------------------------------------------------------*/
function RefreshNormalNotifications(){
	// Move the delete button 13px to the left when there's a scroll bar
	var notificationDeleteLeftOffset = 284;
	if (document.body.clientHeight > maxHeight){
		notificationDeleteLeftOffset -= 13;
	}

	// Loop through each category and notificationItem
	for (var i = 0; i < document.getElementsByClassName("category").length; i++){
		var categoryDiv = document.getElementsByClassName("category")[i];
		var notificationList = categoryDiv.getElementsByClassName("NotificationList")[0];
		var childrenCount = notificationList.children.length;

		// Hide the category if it's empty
		if (childrenCount == 0){
			categoryDiv.style.display = "none";
		} else {
			categoryDiv.style.display = "block";

			// Accessibility strings for the category
			var unread = notificationList.getElementsByClassName("unread").length;
			categoryDiv.setAttribute("aria-label", categoryA11YString.replace("|0", categoryDiv.getElementsByClassName("section-title")[0].textContent).replace("|1", unread));

			for (var j = 0; j < childrenCount; j++){
				var notificationItem = notificationList.children[j];
				var notificationDelete = notificationItem.getElementsByClassName("notification-delete")[0];

				// Move the delete button notificationDeleteLeftOffset left when there's scroll bar
				if (document.dir == "rtl"){
					notificationDelete.style.right = notificationDeleteLeftOffset + "px";
				} else {
					notificationDelete.style.left = notificationDeleteLeftOffset + "px";
				}

				var notificationItemA11YString = notificationItemReadA11YString;
				if (notificationItem.getElementsByClassName("notification-content")[0].classList.contains("unread")){
					notificationItemA11YString = notificationItemUnreadA11YString;
				}

				// Accessibility strings for notifications under this category and their delete buttons
				notificationItem.setAttribute("aria-label", notificationItemA11YString.replace("|0", notificationItem["notification-content"]));
				notificationDelete.setAttribute("aria-label", deleteButtonA11YString);
			}

			notificationList.children[0].setAttribute("tabindex", 0);
		}
	}
}

/*---------------------------------------------------------------------------
	RefreshSettings
---------------------------------------------------------------------------*/
window.RefreshSettings = function RefreshSettings(){
	var settingsList = document.getElementById("settings-container");
	settingsList.setAttribute("aria-label", settingsListA11YString);

	// Add A11Y string for each setting item
	for (var i = 0; i < settingsList.children.length; i++){
		var settingItem = settingsList.children[i];
		var fEnabled = settingItem.getElementsByTagName("input")[0].checked;

		var enabledA11YString = settingsEnabledA11YString;
		if (!fEnabled){
			enabledA11YString = settingsDisabledA11YString;
		}
		enabledA11YString = enabledA11YString.replace("|0", settingItem.getElementsByClassName("settings-content")[0].textContent);

		settingItem.setAttribute("aria-label", enabledA11YString);
		settingItem["settings-index"] = i;
	}

	// Hide the settings icon if the setting list is empty
	if (settingsList.children.length == 0){
		document.getElementById("settings").style.display = "none";
		if (document.dir == "rtl"){
			document.getElementById("refresh").style.left = "0";
		} else {
			document.getElementById("refresh").style.right = "0";
		}
	} else {
		document.getElementById("settings").style.display = "block";
		if (document.dir == "rtl"){
			document.getElementById("refresh").style.left = "32px";
		} else {
			document.getElementById("refresh").style.right = "32px";
		}

		settingsList.children[0].setAttribute("tabindex", 0);
	}
}

/*---------------------------------------------------------------------------
	GetHeight

	Return the height of current window. The max height is capped at maxHeight,
	meaning that the scroll bar will appear after 700px
---------------------------------------------------------------------------*/
window.GetHeight = function GetHeight(){
	if (document.body.clientHeight > maxHeight){
		return maxHeight.toString();
	} else {
		return document.body.clientHeight.toString();
	}
}

/*---------------------------------------------------------------------------
	applyDarkTheme
---------------------------------------------------------------------------*/
window.applyDarkTheme = function applyDarkTheme() {
	let s = document.getElementById('theme-styles');
	s.href = 'notificationCenter_dark.css';
	isDarkMode = true;
}

/*---------------------------------------------------------------------------
	applyLightTheme
---------------------------------------------------------------------------*/
window.applyLightTheme = function applyLightTheme() {
	let s = document.getElementById('theme-styles');
	s.href = 'notificationCenter_light.css';
	isDarkMode = false;
}

/*---------------------------------------------------------------------------
	AddNotificationItem

	Add a notificationItem to the UI
---------------------------------------------------------------------------*/
window.AddNotificationItem = function AddNotificationItem(id, read, body, detail, heroItemType, heroItemData, providerId, base64icon){
	if (heroItemType == "HeroItemNone"){
		AddNormalNotificationItem(id, read, body, detail, providerId, base64icon);
	} else {
		AddHeroNotificationItem(id, heroItemType, JSON.parse(heroItemData), body);
	}
}

/*---------------------------------------------------------------------------
	AddHeroNotificationItem

	Add a hero notificationItem to the UI
---------------------------------------------------------------------------*/
function AddHeroNotificationItem(id, heroItemType, heroItemData, body){
	// App wants to add this hero notification, record its ID in heroNotificationIDApp
	heroNotificationIDApp.add(id);

	// If the notification is already in the UI, don't add it again
	if (heroNotificationIDUI.has(id)){
		return;
	}

	var heroNotification;
	if (heroItemType == "HeroItemButton"){
		heroNotification = CreateHeroItemButton(id, heroItemData, body);
	} else if (heroItemType == "HeroItemTextBox"){
		heroNotification = CreateHeroItemTextBox(id, heroItemData, body);
	}
	heroNotification.classList.add("hero-notification");

	var heroNotifications = document.getElementById("hero-notifications");
	heroNotifications.appendChild(heroNotification);

	// We just added the notification in the UI
	heroNotificationIDUI.add(id);
}

/*---------------------------------------------------------------------------
	CreateHeroItemButton

	Add a hero notificationItem, which contains text contents and an action
	button.
---------------------------------------------------------------------------*/
function CreateHeroItemButton(id, heroItemData, body){
	var heroNotification = document.createElement("div");
	heroNotification["notification-id"] = id;
	heroNotification.className = "heroItem-Button tabFocusable";
	heroNotification.setAttribute("aria-label", body);
	heroNotification.setAttribute("role", "presentation");

	// Text content
	var heroContent = document.createElement("div");
	heroContent.className = "hero-content";
	heroContent.setAttribute("aria-hidden", "true");
	heroContent.textContent = body;

	// Button
	var heroButton = document.createElement("button");
	heroButton.className = "button-purple tabFocusable";
	heroButton.setAttribute("aria-hidden", "true");
	heroButton.textContent = heroItemData["button"]["label"];
	heroButton.addEventListener('click', function(){
		RaiseEvent("heroitem", this.parentNode["notification-id"]);
	}, true);

	heroNotification.appendChild(heroContent);
	heroNotification.appendChild(heroButton);
	return heroNotification;
}

/*---------------------------------------------------------------------------
	CreateHeroItemTextBox

	Add a hero notificationItem, which contains a text input box and an 
	action button.
---------------------------------------------------------------------------*/
function CreateHeroItemTextBox(id, heroItemData, body){
	var heroNotification = document.createElement("div");
	heroNotification["notification-id"] = id;
	heroNotification.setAttribute("id", id);
	heroNotification.className = "heroItem-TextBox tabFocusable";
	heroNotification.setAttribute("aria-label", body);
	heroNotification.setAttribute("role", "presentation");

	// Text content
	var heroContent = document.createElement("div");
	heroContent.className = "hero-content";
	heroContent.setAttribute("aria-hidden", "true");
	heroContent.textContent = body;

	// Text input
	var textInput = document.createElement("div");
	textInput.className = "text-input";
	textInput["input-type"] = heroItemData["textbox"]["type"];
	var input = document.createElement("input");
	input.className = "tabFocusable";
	input.setAttribute("placeholder", heroItemData["textbox"]["label"]);
	input.addEventListener("input", function(event){
		OnHeroInputEvent(this);
	}, true);

	// Invoke button
	var invokeButton = document.createElement("button");
	invokeButton.className = "button-purple tabFocusable";
	invokeButton.setAttribute("aria-label", heroItemData["button"]["accessibility"]);
	invokeButton.innerHTML = '<i class="ms-Icon ms-Icon--' + heroItemData["button"]["icon"] + '" aria-hidden="true"></i>';
	invokeButton.addEventListener('click', function(event){
		OnHeroItemTextBoxInvoked(this.parentNode);
	}, true);

	// Message
	var message = document.createElement("div");
	message.className = "message tabFocusable";
	message["invalidinput"] = heroItemData["message"]["invalidinput"];
	message["success"] = heroItemData["message"]["success"];
	message["fail"] = heroItemData["message"]["fail"];

	textInput.appendChild(input);
	textInput.appendChild(invokeButton);
	textInput.appendChild(message);

	// Footer
	var footer = document.createElement("footer");
	footer.className = "tabFocusable";
	footer.setAttribute("role", "presentation");
	footer.setAttribute("aria-label", heroItemData["footertext"]);
	var span = document.createElement("span");
	span.setAttribute("aria-hidden", "true");
	span.textContent = heroItemData["footertext"];
	footer.appendChild(span);

	heroNotification.appendChild(heroContent);
	heroNotification.appendChild(textInput);
	heroNotification.appendChild(footer);
	return heroNotification;
}

/*---------------------------------------------------------------------------
	AddNormalNotificationItem

	Add a normal notificationItem to the UI
---------------------------------------------------------------------------*/
function AddNormalNotificationItem(id, read, body, detail, providerId, base64icon){
	// notificationItem is the outter most container of each notification. It contains
	// the icon, contents (body + detail) and a delete button.
	var notificationItem = document.createElement("div");
	notificationItem.className = "notification-item tabFocusable";
	notificationItem["notification-id"] = id;
	notificationItem["providerId"] = providerId;
	notificationItem.setAttribute("tabindex", "-1");
	notificationItem.setAttribute("role", "listitem");

	// Store the plain text content of the notification, which will be used in the accessibility label
	notificationItem["notification-content"] = GetTextContent(body, detail, providerId);
	// Store the notebook title for recent notebook notifications, which will be used in the accessibility string
	if (providerId == "recentnotebooks"){
		notificationItem["recent-notification-title"] = GetNotebookNameFromRecentNotebooksNotifications(body);
	}

	// Click Event Handler - Handle in bubbling phase to give delete_button a chance
	// to handle the event first
	notificationItem.addEventListener('click', OnNotificationItemClicked, false);

	// The circle indicating the read/unread status
	var circle = document.createElement("i");
	circle.className = "ms-Icon ms-Icon--CircleFill notification-circle";
	circle.setAttribute("aria-hidden", "true");
	if (read == "read"){
		circle.style.visibility = "hidden";
	} else {
		circle.style.visibility = "visible";
	}

	// Icon of the notification
	var icon = document.createElement("div");
	icon.setAttribute("aria-hidden", "true");
	icon.className = "notification-icon";
	var sectionId;
	if (providerId === "AtMentionsProvider") 
	{
		if (base64icon == null || base64icon === "") {
			// This is the base64 encoded icon of http://officeui/Icon.aspx?name=Doughboy
			if (isDarkMode)
			{
				base64icon = "iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAACXBIWXMAAC4jAAAuIwF4pT92AAATvklEQVR4nO2df5BeVXnHH2hRNyOE9I+ibTWxA/FHlWz4Ibi8ZDcIA4k4bCRgHG3ZIgokSHZKgkIQN06ho0HdjQIKipuq01aBbKomqJDs6svKQIHdDiWEgGZt0yhjC6s1q3+l831z7u6559x73/u+773vvefe72dmJ5u9b/Y95+R83+d5znnOc445cuSIkGSpVLpOFJFOEVmkvry/i/p+SZNvOCkir6jvJ9T3B9TXRLU6/or1L0hLUCAtUql09ajJ7wmiO+MmjXmCUaIZtV5BYkOBNECl0gUB9KivzhYsQbuZVIKBWEar1fEDjrQ7cyiQCJSrBDH0qj8Xhr/aKaaUWEaUYOiahUCBGCgr0au+snaX2sWYEssIrYsfCmTOUvSpL1fcprSAOzaML1qWkgukUunqVaK4xHpIwA4llJGyjkbpBKJcKM9aFCWmSJspzaqUygUrjUAqlS6sOvWLyBXWQ9II20RksFodnyjDqBVeIGqfYqBEAXe7QGA/UPR9lsIKhMJoG4UWSuEEQmFkRiGFUhiBqOB7gDFG5mxTQilEMO+8QNQeBoLvT1kPSZZsVsG803spTgtE7WMMcrk2t2B5uN/lfRQnBaLcqWHGGc6A+KTPRbfrWOsnOadS6epXmakUhzvg/2pC/d85hTMWhFajMDhlTZywICrWoNUoBp416XWhN7m2IGqFCku3662HpAgMqSXh3K505VYgyqUaYfp54UF6fW9eXa5culiaS0VxFJ8leXa5cieQSqULLtV2EZlvPSRFBf/X29X/fa7IlYtVqXQNM1Wk9GyrVsf78jIIuRCICsZH6VIRBeKSnjwE75m7WCoYpziIDubCqJobmZKpBVGn/EYZb5AQppUlyez0YmYWhOIgMZivLElnVoOViUAoDtIAmYqk7QKhOEgTZCaStgqE4iAtkIlI2iYQioMkQNtF0pZVLLVcN0FxkITA6lZnO/K3UrcgahNwhOIgCYK5NKLmVqq0w8XiJiBJgyVqbqVKqgJRuVUUB0mLJWqOpUZqAlGZmUw8JGlzRZpZwKkE6Sq3f7v1gJD0WJVGeaHEBcIVK5IRqaxsJepiccWKZEgqK1tJxyADDMpJhixRczAxEnOxGHeQHJFYPJKIQBh3kJyRWDySlIs1THGQHDFfzcmWaVkgqt4qKx6SvNGdRC3gllwsulYk57TsarVqQehakTzTsqvVtEDUqhVdK5J3ulup2tiUi6U2YyZ4sxNxhCnlajVcZ6tZC9JPcRCHWKjmbMM0bEFUYP5z6wFpidf/6UnS0dFR+xUzMzNy6KVfcUCT502NBux/bP2kPrkrMOwSEMJb3vxm6Vy6VP78L94gZ559dmTrn3jsMTn4X/8pE08/Lc/t20fhtAbmbkN1fxuyIOqS/j3WAxIJRNG9rFsuWLFSTj7l5KiX1uWF/S/Ij3btlLEfj1EszbG8Wh2PfRKxUYGMcuUqPu887XS5aMVKefeFF6by+x/5wQ/koV075fGnnrSekVDGqtXxnrCHJrEFQusRn79cuEjWrruurvskyoX6zfS0/Pa3v5H9zz9f+9kpixfL8cefICfMnx/7d9x155fkZ1PO3bKcFbGtSCMCofWow2te/Wq59uprpHf1ZaEvxGQe3f2IPLt3b+wJDcG97a1vlZ7z3h0pmJH7vyN3f+XL8vs//MF6RnzEtiKxBELrUR9M4s8PfVEW/MkC67Uv/+/L8tUv3yWP/nRcXp6etp43woL58+Wcd3XJVdesDX2vv1v/MVqT+sSyInEFQusRAWKNLYND1gs8YTy8Z3fin+qwVucvPy9UKBv71zM2iSaWFakrEFXm8WnrAanx4b4r5W+uvNIajHa5O1Fu3T/ed598bfg+6+dklqX17h6Js5PecspwUQkSB6zGuo9+RL6wdagtsQDeA++F98R766BtaCMJpe7cjrQg3DUP5+KLVsjGmzf5niMA/9wdWzLbn8B+yw0bNlqB/Jbbb5PvPbTLej2pEbm7Xs+C5Oa20TyBmCNIHLd8clOmm3d4b7QBbdFBW9FmEkjkHKdAGgSf0jff6s+28cSRh+VVtCFIJGgz2k4sIud4qEBUDj0zdg3gwuirRkj9yIs4PDyRoG0eaDPaTiwWRp0XCRUIrYcN4g7dv0dQfMtNH8/lxlxNJDd93Be4o+3oA7EIneuBAlEHoi6xHpQYbNBhz0Fn6HPZBeRxQNvQRp3avsl8npI2uCSsImOgQKIUVVbet+pSn2uFRME9P/lx7kcDbURbPdAH9IVYBM55CiQG+MQ19zvuvfeenLbWxmwr+kIrYhE45y2BqL0P1tfVuPACf7o6dqhdOouBtqLNOmafSO0ynkXmMFgCEZHQiL6srPnQX/t6/uD2B5wbCbPNZp9IDWvuUyB1wAabHnsgx6rVjNwsQJvRdg/0iZuHFlbyok8gKpJn1q7GO071e5s/+uEPrde4gtl2s2/EXs0yLYiloLLz3t5VsyOAPYVnntvr7Iig7fq+iN43MotPA6ZA6F5pIDVDd6/Gdj9svcY19D6gb0w/sfBpgBYkApTn0UHpHdcx+2D2kYRYELXExdwrjZNOep3v71O/+IX1Gtcw+2D2kdRys2aXe3ULQuthsNj4dC3COW+zD2YfSY1ZLVAgMTFP67lMkfqSEoEC6SxK75JCL/j21BOPu96dWfS+pFXUznFmtaALhIvihBxlVgs1gai6V4QQhacJz4LQvSLET00TFEgEZQhmGbCH4hOIleZLihvMFnXxIWFqmvAEwgTFGKCKISkNNU0cG3YWl4g89eS/+Ubhz173eudHBUW2dcw+kjmgjWMZf4Tz0ksv+Z7hCgLXMftg9pH46DyW8Uc4+198wffstNPPCH2tK5h9MPtIfCyiQCLAKTy9+BqCW5fjELRdD9DRNxdPR7aRmkAYg0QwbpT2OfWv3h7+4pxjtt3sG7FgDFKPPXt2+15x+ZoP1PkX+cVsu9k3YtEZVLSBaCA9XHezUL7TXAlyAbRZL5uKPvGatvrQxYrB9u/8i+9FH3KwZI7ZZrNPJJCai8Us3jrgjkE9JQOBrktWBG3Vg3P05WG6V3FYQhcrBqiUjss4dTbdOuDEihbauMm4zwR94VXR8aBAYoJPXD0WOfmUk6X3vfkvgI82oq0e6AOtR3wokJjULsvc8hnfi6+9/vpcVydE29BGHfSB1iM+FEgDoPCaXr5Tcny1WdBVcWi7y4XvsoACaRDcfW5ebXb3PV/NlUjQFrTJvCoObSeNQYE0SNDVZnkSSZA48nxVXN6hQJqgdt3yJ270/UNPJG9/S3YZv3hvUxwAbXXpPpM8QYE0CXz5jf3rff8YE/POe+6VNasvb3t78J54b1McaCPjjub5oze+8Q0Daf3yonPw0CF5dnJSznzn2dLR0THb2zPPOkvOrSyTZyYn5OXpV1IdBWwC3vH5Qblwpf/2WrhVt970CXn8qSetf0Pic8w557zrCMerNYL8fg9coPnNb34j8bwnCAPpI0Fn5SGOaz96Fd2qBKBAEgI71jduuDFwwooSykO7dsq//8czTQfLeA+krF+0YmXk+3z2js8yIE8ICiRhlp+7TNbfsDHQmnhgP2L/88/Ls3v3yn//8lDoZIYgcA4ex2RPWbxYeldfZr3GA1YDd6K7cDW1S1AgKYCJ/cEPfNC6OjqKmZkZ+f3hw7VXvGbePF9MUw/cYPutf/pWqNBI80AgE8zoTQfcRX755Wuk99LVMm9e/Akfh8OHZ2Tkgfvl29/+Zx6bTY9JrGKt4bn0ZEHQfvGK98i1110v3cuXy3HHHZf4e+B3vmPJEjnjzLNk3qteJf/z61/L//3ud9brSEvsgwUZZeG41vEC6KvXfcyXPdtOkE7ylTu/2NJCAPExBoEMish6jktzQBjnLz9PrrpmbWRg7oFVpoMHD8qvfnmoFqSLij/MJVm4ZwtOPPr7EKS/9rXH126DClu90kHAjjMfSGunUFpiCALBRuGnHO5EZlx80Yq6wnjiscdkdPcjNTEktRcCFw6Xb567rDtSMJ5QvvfQLusZicVmCKRPRL7O8YoPNulwSi/MlYKrgzPfj/50PPUA2nPtULFEL8pgtue2Tw+wSEPj/C0EgotC9rjW8izAZMQJPfMQkoe3GZhVegcsy8qV7wldXr5761YZ+e4Oul3xWQ6BoKoJL4moAybf3//DZwKtBtyou+78Um4+oRG/vG/VpYFCgTVB6jvTUGKx4JgjR46gijU3CyPA0VWczjNjDfj4t396ILcJgXAF1667znK98t7uvFCtjh/jpbuPlX0wwkAa+ZbBIUscSBdZ8/7VuZ5ksGgbbtwgW26/zfdz9AV9yiIt3yFqmvAEwugtgA/3XWnFG/j0xRmLL2wdcsaXxyrWmtWX+o4Kiyo6gT6SQGqa8AQyEfSKMoOJY/rwmGBII3fRNUHMsW7t1VbRCfSRIgmkpgkKJIAgcSAQxwRzObitlS7aOlRLbtShSAKZE0i1Oj4a9Ioygs2/IHHc8slNhVke/drwfYEiQd/JUTxN6GfSJ8s+Nlit2njzJt/PiiYOjyCRoO95LoTXRma1oAuk1G5WUKG1oorDI0gkeS2E12ZmtaALpLRuFnbIb9iw0Sq0VmRxeEAkyADwwBhgLEp+5fWsFigQVeBZ30wrW6E1nGE3LwlyoTB3itgCqVbHse47VbCO1gW7zeZeB852lykVw6sWqYMxcfEmrQSYUlqoYRaOK50VQSqGDvYJylj4AB8I5o67OTYlwacBUyAjZRoJrNiYrlWZCzxjxx0LEx4YmxKuavk0UFoLgiDUXLWCa1X2VHBkJevc7MhNWgkSbkGq1fFXypK4iGOy+qoVPjlZU+pogqO+9IsxwliVhDGlgVmCileXws1addn7fX83PznLzIPbH/D1HseKS4I190spEPjV+sEn7APwOOocOCZsWpGSxCLW3LcEopa4Cp12gvPbOg8+cL/1mrKzc+f3I8esgEzqy7selkAUw9ZPCgLSKPSVK2yQ8f4MGyz76jvsGLOCp6AEzvnSCaR7mb9GHqqPkGBQgCJq7ApG4JwPFIiK5HdYDwrABStW+jqB0jwkGFRo1DHHrkDsMFevIgWiCFSUy6Dahx6cY2mXhZ/DwZ6QfgIRY4cxLCChcz1UINXq+EjRcrM6T/UXsf/+d//Veg3x82i1GjmGBWBKzfVAQgWiCFWWi3QuXepr9XP79lEOddj/or/QgzmGBSByjpdKIN3nnT/7PfKuWDytPnBB9VR4fQwLQuQcjxSIWhfeZj1wkFq1dC21ZGz3w0X7j06NZyafnv3VGMMCxSHbgvY+dCIFohi0fuIg3lUCHrgjkMTDHCtzLB2m7tyuK5BqdXyiCAmMuGND58CByA8OouHdYxI2lo4ypuZ2JHEsCBiwfuI4h2dmqIGYzBRzrGLN6VgCUTWCnLYip51+hu/vTE6Mj7mYYY6lg4zFrQUX14JIEa0IKS2x53JsgRTBiniYRZxJqcYstvWQBi2IuGxFTtCWJqd+9qL1nESjj9kJbi/zNjSHGxKIUp6T+yJh9/eRUo3ltkbrUNdumGqESqULxZJ+nlIHUkOv8RR07TKJBmdBOjo6Zl/j6CLHm+ptDJo0LBA5KhJeHU1cY3O1Ot5wiNBoDOIxWMYqjMRZpprNCGlKIOpwSb/1gJB80h92IKoeTblYHpVKFwKeQp/DJM6D04K9zXaiWRfLo09EeCSP5JXpVj2dlgSiVgS4w07yykCjq1YmLblYHnS1SA7BjnlPq81q1cXyoKtF8sS0mpMtk4hAlBlLpEGEJEBfq66VR1IWxKuCMmQ9IKS9DEVVKWmUxASiGOB10iRDJpNeNEokSNdRuVo4yljICmMktyDu6EzKtfJI2oIwHiFZkVjcoZO4QGQuHtlsPSAkHTYnGXfoJO5i6VQqXSjKdYX1gJDkwBmP1DyWVAUiR0WCeKRwBV1JLsClN51pNiQVF8ughytbJAUm1dxKldQtiHBliyRPKitWQbTDgngrWz1MRyEJgDnU0w5xSLsEInMlTCkS0gqeOOqWDE2KtglEKBLSGm0Xh7RbIEKRkObIRByShUCEIiGNkZk4JCuBCEVC4pGpOCRLgcicSDq5T0ICmFRLuZmJQ9q1D1KPSqXrRBEZ5Y47UUwqy9FUqZ4kyYVAPJi7RdLOrWqUTF0sEzUwzAIuL5vzJA7JmwXxqFS6etX1vExNKQfT6jxHKinrrZBLgchc/tYI45LCg3ijt12pI42SKxdLR8vfYiGI4jLUzryqZsitBdGhy1U4cutSmeTWguiogewsyh2JJWdM7W/kXhziigXRqVS6+lVpF1oTt5hWtXKbuqcjK5wTiMwF8MOsB+wMO9QdHc7d2+akQDxUbIJPpIXWQ5IHppQwnHCngnAiBglDi024uZg/NrsUa4ThtAXRUW7XAFNVMmdbEvdy5IXCCMSjUunqUUJhfNJexpQwGrqHPO8UTiAeFErbKKQwPAorEA8KJTUKLQyPwgvEo1Lp6lQXOjJGaQ3EGINZH2RqF6URiIcK5vvUF5eH4zGl9p2GixJ8x6V0AtFR+ygQyiXWQwJ2KFE4vVTbCqUWiIc68turXLCyp9dPatYi8yOvWUOBGCgXrFd9lSWw36FqAoyUzYWqBwUSgbIsPUosPQWKWaY8QeBPWopwKJAGUNalR311OuSOTarq+qNKELQSMaFAWkTts3Sqr0U5cMuwP3FACWKi6PsUaUOBpIByzTzB4Mv7u6jvm7U8sASeOzShvj/gCYKuUsKIyP8DYrFUlJQaiTYAAAAASUVORK5CYII=";
			}
			else
			{
				base64icon = "iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABGdBTUEAALGPC/xhBQAAAvpJREFUaEPVmjty4kAQhrnIHmSv4ZCMjIyUckJGQEBGAiEBAaFSyikHMOlmQG2+B9D2J/fYKrktz0vI6qq/TKkf83fPW/Ioh1yv19+3220lKASvgr/3+72sg2eqw2aFj7r3I0LglxDbCpk/TbK+UN8tsTRs96LVfmmSSQUxO+8VaeggDf1rNp4LGvugzeUTCfqk3W02nBva1pM2nyYS7LnLqn8F2qRtpREnEmBlBX8k4KB0wuQnkHcIToKuswL1CTgpvXYRYybsw8f8d1BO309sMXzYahMKuClNW8To0HSKwfl8Lne7XbnZbCrwm2eWbQTsfUJ32KShc7lcyuVyWY7HYxOLxSI5ETiaO7Yoko4Hp9OpnEwmFah6nSi/eeb02NZ9QwFXpf0mHKZSqk/lfcjVk8THsvEFnJV+Nfa3TYMQuGHjU1lssMXH0gdgq/TTVx4qGkIIW3wsnS/gXJFnQlgGviiKoqro8Xg09RawxQdfS++LajJLJklHBpdACJkYHwtwJ4HCUvqi5wQKEni1lL5wk3K/35t6C9ji4zPp2wB3Evh0AQ/FdDot5/O5qbOALT6WLgRwZwk1lSFwk5Ijg6WvA5vQHmtDlgSA2wvYcS09QIdNhj3gHdkSAC4Jhsd6va6qDPjNs9zkQdYEWFUcUQvocg0dhyyTmMMaE9ORZKiQTB08c8lhm+N4XU1iliJL6Yv6Ac2nutg4+1zLaPRGRhUhMpvNgshgiw++KT0BdxKIPkowFGIr6XouZP9oAu7RhzmGAuM5ZVKmxqgOc4hkEnyczrWbEiOmF+BckUfkQdCFhtsUlWvbtHzhNreIG9rHhYbrmWTkfaVkWaRR/lr6EMTGgrPSfxNJwPtS33cCcFXaHxLaC30Bju+TtylikOXFVsdo/wAiGQ731SIihsN+uYuI8XBfrzsRh+F+4HDyE5KIJu+ErhM8fE7QJm0rjTSRgMP9zFoXCTrMD9110R37JWcixCLmlztsF0Ii0vjw/tnDEionZHr4d5vR6D+ZDCPXFX0n9AAAAABJRU5ErkJggg==";
			}
		}

		var img = '<img class="ms-Persona-image" src="data:image/jpeg;base64,' + base64icon + '" aria-hidden="true">';
		var element = '\
			<div class="ms-Persona ms-Persona--xs"> \
				<div class="ms-Persona-imageArea"> \
					<i class="ms-Persona-placeholder ms-Icon ms-Icon--person"></i>' +
					img + '\
				</div> \
			</div>'
		icon.innerHTML = element;
		sectionId = "atmentions";
	}
	else if (!providerId.startsWith("ActivityProvider"))
	{
		icon.classList.add("notification-icon-background");

		if (providerId == "TeachingProvider"){
			icon.style.backgroundPosition = "0 -64px";
			sectionId = "tips";
		} else if (providerId == "recentnotebooks"){
			icon.style.backgroundPosition = "0 " + GetNotebookIconColorOffset(GetNotebookNameFromRecentNotebooksNotifications(body)) + "px";
			sectionId = "recentnotebooks";
		}
	} else {
		if (base64icon == null || base64icon === "") {
			// This is the base64 encoded icon of http://officeui/Icon.aspx?name=Doughboy
			if (isDarkMode)
			{
				base64icon = "iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAACXBIWXMAAC4jAAAuIwF4pT92AAATvklEQVR4nO2df5BeVXnHH2hRNyOE9I+ibTWxA/FHlWz4Ibi8ZDcIA4k4bCRgHG3ZIgokSHZKgkIQN06ho0HdjQIKipuq01aBbKomqJDs6svKQIHdDiWEgGZt0yhjC6s1q3+l831z7u6559x73/u+773vvefe72dmJ5u9b/Y95+R83+d5znnOc445cuSIkGSpVLpOFJFOEVmkvry/i/p+SZNvOCkir6jvJ9T3B9TXRLU6/or1L0hLUCAtUql09ajJ7wmiO+MmjXmCUaIZtV5BYkOBNECl0gUB9KivzhYsQbuZVIKBWEar1fEDjrQ7cyiQCJSrBDH0qj8Xhr/aKaaUWEaUYOiahUCBGCgr0au+snaX2sWYEssIrYsfCmTOUvSpL1fcprSAOzaML1qWkgukUunqVaK4xHpIwA4llJGyjkbpBKJcKM9aFCWmSJspzaqUygUrjUAqlS6sOvWLyBXWQ9II20RksFodnyjDqBVeIGqfYqBEAXe7QGA/UPR9lsIKhMJoG4UWSuEEQmFkRiGFUhiBqOB7gDFG5mxTQilEMO+8QNQeBoLvT1kPSZZsVsG803spTgtE7WMMcrk2t2B5uN/lfRQnBaLcqWHGGc6A+KTPRbfrWOsnOadS6epXmakUhzvg/2pC/d85hTMWhFajMDhlTZywICrWoNUoBp416XWhN7m2IGqFCku3662HpAgMqSXh3K505VYgyqUaYfp54UF6fW9eXa5culiaS0VxFJ8leXa5cieQSqULLtV2EZlvPSRFBf/X29X/fa7IlYtVqXQNM1Wk9GyrVsf78jIIuRCICsZH6VIRBeKSnjwE75m7WCoYpziIDubCqJobmZKpBVGn/EYZb5AQppUlyez0YmYWhOIgMZivLElnVoOViUAoDtIAmYqk7QKhOEgTZCaStgqE4iAtkIlI2iYQioMkQNtF0pZVLLVcN0FxkITA6lZnO/K3UrcgahNwhOIgCYK5NKLmVqq0w8XiJiBJgyVqbqVKqgJRuVUUB0mLJWqOpUZqAlGZmUw8JGlzRZpZwKkE6Sq3f7v1gJD0WJVGeaHEBcIVK5IRqaxsJepiccWKZEgqK1tJxyADDMpJhixRczAxEnOxGHeQHJFYPJKIQBh3kJyRWDySlIs1THGQHDFfzcmWaVkgqt4qKx6SvNGdRC3gllwsulYk57TsarVqQehakTzTsqvVtEDUqhVdK5J3ulup2tiUi6U2YyZ4sxNxhCnlajVcZ6tZC9JPcRCHWKjmbMM0bEFUYP5z6wFpidf/6UnS0dFR+xUzMzNy6KVfcUCT502NBux/bP2kPrkrMOwSEMJb3vxm6Vy6VP78L94gZ559dmTrn3jsMTn4X/8pE08/Lc/t20fhtAbmbkN1fxuyIOqS/j3WAxIJRNG9rFsuWLFSTj7l5KiX1uWF/S/Ij3btlLEfj1EszbG8Wh2PfRKxUYGMcuUqPu887XS5aMVKefeFF6by+x/5wQ/koV075fGnnrSekVDGqtXxnrCHJrEFQusRn79cuEjWrruurvskyoX6zfS0/Pa3v5H9zz9f+9kpixfL8cefICfMnx/7d9x155fkZ1PO3bKcFbGtSCMCofWow2te/Wq59uprpHf1ZaEvxGQe3f2IPLt3b+wJDcG97a1vlZ7z3h0pmJH7vyN3f+XL8vs//MF6RnzEtiKxBELrUR9M4s8PfVEW/MkC67Uv/+/L8tUv3yWP/nRcXp6etp43woL58+Wcd3XJVdesDX2vv1v/MVqT+sSyInEFQusRAWKNLYND1gs8YTy8Z3fin+qwVucvPy9UKBv71zM2iSaWFakrEFXm8WnrAanx4b4r5W+uvNIajHa5O1Fu3T/ed598bfg+6+dklqX17h6Js5PecspwUQkSB6zGuo9+RL6wdagtsQDeA++F98R766BtaCMJpe7cjrQg3DUP5+KLVsjGmzf5niMA/9wdWzLbn8B+yw0bNlqB/Jbbb5PvPbTLej2pEbm7Xs+C5Oa20TyBmCNIHLd8clOmm3d4b7QBbdFBW9FmEkjkHKdAGgSf0jff6s+28cSRh+VVtCFIJGgz2k4sIud4qEBUDj0zdg3gwuirRkj9yIs4PDyRoG0eaDPaTiwWRp0XCRUIrYcN4g7dv0dQfMtNH8/lxlxNJDd93Be4o+3oA7EIneuBAlEHoi6xHpQYbNBhz0Fn6HPZBeRxQNvQRp3avsl8npI2uCSsImOgQKIUVVbet+pSn2uFRME9P/lx7kcDbURbPdAH9IVYBM55CiQG+MQ19zvuvfeenLbWxmwr+kIrYhE45y2BqL0P1tfVuPACf7o6dqhdOouBtqLNOmafSO0ynkXmMFgCEZHQiL6srPnQX/t6/uD2B5wbCbPNZp9IDWvuUyB1wAabHnsgx6rVjNwsQJvRdg/0iZuHFlbyok8gKpJn1q7GO071e5s/+uEPrde4gtl2s2/EXs0yLYiloLLz3t5VsyOAPYVnntvr7Iig7fq+iN43MotPA6ZA6F5pIDVDd6/Gdj9svcY19D6gb0w/sfBpgBYkApTn0UHpHdcx+2D2kYRYELXExdwrjZNOep3v71O/+IX1Gtcw+2D2kdRys2aXe3ULQuthsNj4dC3COW+zD2YfSY1ZLVAgMTFP67lMkfqSEoEC6SxK75JCL/j21BOPu96dWfS+pFXUznFmtaALhIvihBxlVgs1gai6V4QQhacJz4LQvSLET00TFEgEZQhmGbCH4hOIleZLihvMFnXxIWFqmvAEwgTFGKCKISkNNU0cG3YWl4g89eS/+Ubhz173eudHBUW2dcw+kjmgjWMZf4Tz0ksv+Z7hCgLXMftg9pH46DyW8Uc4+198wffstNPPCH2tK5h9MPtIfCyiQCLAKTy9+BqCW5fjELRdD9DRNxdPR7aRmkAYg0QwbpT2OfWv3h7+4pxjtt3sG7FgDFKPPXt2+15x+ZoP1PkX+cVsu9k3YtEZVLSBaCA9XHezUL7TXAlyAbRZL5uKPvGatvrQxYrB9u/8i+9FH3KwZI7ZZrNPJJCai8Us3jrgjkE9JQOBrktWBG3Vg3P05WG6V3FYQhcrBqiUjss4dTbdOuDEihbauMm4zwR94VXR8aBAYoJPXD0WOfmUk6X3vfkvgI82oq0e6AOtR3wokJjULsvc8hnfi6+9/vpcVydE29BGHfSB1iM+FEgDoPCaXr5Tcny1WdBVcWi7y4XvsoACaRDcfW5ebXb3PV/NlUjQFrTJvCoObSeNQYE0SNDVZnkSSZA48nxVXN6hQJqgdt3yJ270/UNPJG9/S3YZv3hvUxwAbXXpPpM8QYE0CXz5jf3rff8YE/POe+6VNasvb3t78J54b1McaCPjjub5oze+8Q0Daf3yonPw0CF5dnJSznzn2dLR0THb2zPPOkvOrSyTZyYn5OXpV1IdBWwC3vH5Qblwpf/2WrhVt970CXn8qSetf0Pic8w557zrCMerNYL8fg9coPnNb34j8bwnCAPpI0Fn5SGOaz96Fd2qBKBAEgI71jduuDFwwooSykO7dsq//8czTQfLeA+krF+0YmXk+3z2js8yIE8ICiRhlp+7TNbfsDHQmnhgP2L/88/Ls3v3yn//8lDoZIYgcA4ex2RPWbxYeldfZr3GA1YDd6K7cDW1S1AgKYCJ/cEPfNC6OjqKmZkZ+f3hw7VXvGbePF9MUw/cYPutf/pWqNBI80AgE8zoTQfcRX755Wuk99LVMm9e/Akfh8OHZ2Tkgfvl29/+Zx6bTY9JrGKt4bn0ZEHQfvGK98i1110v3cuXy3HHHZf4e+B3vmPJEjnjzLNk3qteJf/z61/L//3ud9brSEvsgwUZZeG41vEC6KvXfcyXPdtOkE7ylTu/2NJCAPExBoEMish6jktzQBjnLz9PrrpmbWRg7oFVpoMHD8qvfnmoFqSLij/MJVm4ZwtOPPr7EKS/9rXH126DClu90kHAjjMfSGunUFpiCALBRuGnHO5EZlx80Yq6wnjiscdkdPcjNTEktRcCFw6Xb567rDtSMJ5QvvfQLusZicVmCKRPRL7O8YoPNulwSi/MlYKrgzPfj/50PPUA2nPtULFEL8pgtue2Tw+wSEPj/C0EgotC9rjW8izAZMQJPfMQkoe3GZhVegcsy8qV7wldXr5761YZ+e4Oul3xWQ6BoKoJL4moAybf3//DZwKtBtyou+78Um4+oRG/vG/VpYFCgTVB6jvTUGKx4JgjR46gijU3CyPA0VWczjNjDfj4t396ILcJgXAF1667znK98t7uvFCtjh/jpbuPlX0wwkAa+ZbBIUscSBdZ8/7VuZ5ksGgbbtwgW26/zfdz9AV9yiIt3yFqmvAEwugtgA/3XWnFG/j0xRmLL2wdcsaXxyrWmtWX+o4Kiyo6gT6SQGqa8AQyEfSKMoOJY/rwmGBII3fRNUHMsW7t1VbRCfSRIgmkpgkKJIAgcSAQxwRzObitlS7aOlRLbtShSAKZE0i1Oj4a9Ioygs2/IHHc8slNhVke/drwfYEiQd/JUTxN6GfSJ8s+Nlit2njzJt/PiiYOjyCRoO95LoTXRma1oAuk1G5WUKG1oorDI0gkeS2E12ZmtaALpLRuFnbIb9iw0Sq0VmRxeEAkyADwwBhgLEp+5fWsFigQVeBZ30wrW6E1nGE3LwlyoTB3itgCqVbHse47VbCO1gW7zeZeB852lykVw6sWqYMxcfEmrQSYUlqoYRaOK50VQSqGDvYJylj4AB8I5o67OTYlwacBUyAjZRoJrNiYrlWZCzxjxx0LEx4YmxKuavk0UFoLgiDUXLWCa1X2VHBkJevc7MhNWgkSbkGq1fFXypK4iGOy+qoVPjlZU+pogqO+9IsxwliVhDGlgVmCileXws1addn7fX83PznLzIPbH/D1HseKS4I190spEPjV+sEn7APwOOocOCZsWpGSxCLW3LcEopa4Cp12gvPbOg8+cL/1mrKzc+f3I8esgEzqy7selkAUw9ZPCgLSKPSVK2yQ8f4MGyz76jvsGLOCp6AEzvnSCaR7mb9GHqqPkGBQgCJq7ApG4JwPFIiK5HdYDwrABStW+jqB0jwkGFRo1DHHrkDsMFevIgWiCFSUy6Dahx6cY2mXhZ/DwZ6QfgIRY4cxLCChcz1UINXq+EjRcrM6T/UXsf/+d//Veg3x82i1GjmGBWBKzfVAQgWiCFWWi3QuXepr9XP79lEOddj/or/QgzmGBSByjpdKIN3nnT/7PfKuWDytPnBB9VR4fQwLQuQcjxSIWhfeZj1wkFq1dC21ZGz3w0X7j06NZyafnv3VGMMCxSHbgvY+dCIFohi0fuIg3lUCHrgjkMTDHCtzLB2m7tyuK5BqdXyiCAmMuGND58CByA8OouHdYxI2lo4ypuZ2JHEsCBiwfuI4h2dmqIGYzBRzrGLN6VgCUTWCnLYip51+hu/vTE6Mj7mYYY6lg4zFrQUX14JIEa0IKS2x53JsgRTBiniYRZxJqcYstvWQBi2IuGxFTtCWJqd+9qL1nESjj9kJbi/zNjSHGxKIUp6T+yJh9/eRUo3ltkbrUNdumGqESqULxZJ+nlIHUkOv8RR07TKJBmdBOjo6Zl/j6CLHm+ptDJo0LBA5KhJeHU1cY3O1Ot5wiNBoDOIxWMYqjMRZpprNCGlKIOpwSb/1gJB80h92IKoeTblYHpVKFwKeQp/DJM6D04K9zXaiWRfLo09EeCSP5JXpVj2dlgSiVgS4w07yykCjq1YmLblYHnS1SA7BjnlPq81q1cXyoKtF8sS0mpMtk4hAlBlLpEGEJEBfq66VR1IWxKuCMmQ9IKS9DEVVKWmUxASiGOB10iRDJpNeNEokSNdRuVo4yljICmMktyDu6EzKtfJI2oIwHiFZkVjcoZO4QGQuHtlsPSAkHTYnGXfoJO5i6VQqXSjKdYX1gJDkwBmP1DyWVAUiR0WCeKRwBV1JLsClN51pNiQVF8ughytbJAUm1dxKldQtiHBliyRPKitWQbTDgngrWz1MRyEJgDnU0w5xSLsEInMlTCkS0gqeOOqWDE2KtglEKBLSGm0Xh7RbIEKRkObIRByShUCEIiGNkZk4JCuBCEVC4pGpOCRLgcicSDq5T0ICmFRLuZmJQ9q1D1KPSqXrRBEZ5Y47UUwqy9FUqZ4kyYVAPJi7RdLOrWqUTF0sEzUwzAIuL5vzJA7JmwXxqFS6etX1vExNKQfT6jxHKinrrZBLgchc/tYI45LCg3ijt12pI42SKxdLR8vfYiGI4jLUzryqZsitBdGhy1U4cutSmeTWguiogewsyh2JJWdM7W/kXhziigXRqVS6+lVpF1oTt5hWtXKbuqcjK5wTiMwF8MOsB+wMO9QdHc7d2+akQDxUbIJPpIXWQ5IHppQwnHCngnAiBglDi024uZg/NrsUa4ThtAXRUW7XAFNVMmdbEvdy5IXCCMSjUunqUUJhfNJexpQwGrqHPO8UTiAeFErbKKQwPAorEA8KJTUKLQyPwgvEo1Lp6lQXOjJGaQ3EGINZH2RqF6URiIcK5vvUF5eH4zGl9p2GixJ8x6V0AtFR+ygQyiXWQwJ2KFE4vVTbCqUWiIc68turXLCyp9dPatYi8yOvWUOBGCgXrFd9lSWw36FqAoyUzYWqBwUSgbIsPUosPQWKWaY8QeBPWopwKJAGUNalR311OuSOTarq+qNKELQSMaFAWkTts3Sqr0U5cMuwP3FACWKi6PsUaUOBpIByzTzB4Mv7u6jvm7U8sASeOzShvj/gCYKuUsKIyP8DYrFUlJQaiTYAAAAASUVORK5CYII=";
			}
			else
			{
				base64icon = "iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABGdBTUEAALGPC/xhBQAAAvpJREFUaEPVmjty4kAQhrnIHmSv4ZCMjIyUckJGQEBGAiEBAaFSyikHMOlmQG2+B9D2J/fYKrktz0vI6qq/TKkf83fPW/Ioh1yv19+3220lKASvgr/3+72sg2eqw2aFj7r3I0LglxDbCpk/TbK+UN8tsTRs96LVfmmSSQUxO+8VaeggDf1rNp4LGvugzeUTCfqk3W02nBva1pM2nyYS7LnLqn8F2qRtpREnEmBlBX8k4KB0wuQnkHcIToKuswL1CTgpvXYRYybsw8f8d1BO309sMXzYahMKuClNW8To0HSKwfl8Lne7XbnZbCrwm2eWbQTsfUJ32KShc7lcyuVyWY7HYxOLxSI5ETiaO7Yoko4Hp9OpnEwmFah6nSi/eeb02NZ9QwFXpf0mHKZSqk/lfcjVk8THsvEFnJV+Nfa3TYMQuGHjU1lssMXH0gdgq/TTVx4qGkIIW3wsnS/gXJFnQlgGviiKoqro8Xg09RawxQdfS++LajJLJklHBpdACJkYHwtwJ4HCUvqi5wQKEni1lL5wk3K/35t6C9ji4zPp2wB3Evh0AQ/FdDot5/O5qbOALT6WLgRwZwk1lSFwk5Ijg6WvA5vQHmtDlgSA2wvYcS09QIdNhj3gHdkSAC4Jhsd6va6qDPjNs9zkQdYEWFUcUQvocg0dhyyTmMMaE9ORZKiQTB08c8lhm+N4XU1iliJL6Yv6Ac2nutg4+1zLaPRGRhUhMpvNgshgiw++KT0BdxKIPkowFGIr6XouZP9oAu7RhzmGAuM5ZVKmxqgOc4hkEnyczrWbEiOmF+BckUfkQdCFhtsUlWvbtHzhNreIG9rHhYbrmWTkfaVkWaRR/lr6EMTGgrPSfxNJwPtS33cCcFXaHxLaC30Bju+TtylikOXFVsdo/wAiGQ731SIihsN+uYuI8XBfrzsRh+F+4HDyE5KIJu+ErhM8fE7QJm0rjTSRgMP9zFoXCTrMD9110R37JWcixCLmlztsF0Ii0vjw/tnDEionZHr4d5vR6D+ZDCPXFX0n9AAAAABJRU5ErkJggg==";
			}
		}

		var img = '<img class="ms-Persona-image" src="data:image/jpeg;base64,' + base64icon + '" aria-hidden="true">';
		var element = '\
			<div class="ms-Persona ms-Persona--xs"> \
				<div class="ms-Persona-imageArea"> \
					<i class="ms-Persona-placeholder ms-Icon ms-Icon--person"></i>' +
					img + '\
				</div> \
			</div>'
		icon.innerHTML = element;
		sectionId = "activity";
	}

	// Contents of the notification
	var content = document.createElement("div");
	content.innerHTML = "<span>" + body + " <span class='notification-content-detail'>" + detail + "</span></span>";
	content.className = "notification-content " + read;
	content.setAttribute("aria-hidden", "true");

	// The wrapper wraps everything in the notifcation item except the delete button.
	// So that the wrapper and the delete button can have background-color independently for mouse hover events
	var wrapper = document.createElement("div");
	wrapper.className = "notification-wrapper";
	wrapper.setAttribute("aria-hidden", "true");

	wrapper.appendChild(circle);
	wrapper.appendChild(icon);
	wrapper.appendChild(content);

	// Delete buton of the notification
	var delete_button = document.createElement("button");
	delete_button.className = "button-white notification-delete tabFocusable ms-Icon ms-Icon--Delete";
	delete_button.setAttribute("tabindex", "-1");
	delete_button["notificaiton-id"] = id;
	// Handle delete on bubbling phase so that we make sure notificationItem doesn't handle the click event
	delete_button.addEventListener('click', OnDeleteButtonClicked, false);

	notificationItem.appendChild(wrapper);
	notificationItem.appendChild(delete_button);

	document.getElementById(sectionId).getElementsByClassName("NotificationList")[0].appendChild(notificationItem);
	document.getElementById(sectionId).style.display = "block";
}

/*---------------------------------------------------------------------------
	AddSettingItem

	Add a settingItem to the UI
---------------------------------------------------------------------------*/
window.AddSettingItem = function AddSettingItem(id, notebookName, fActivate){
	// settingItem is the outter most container of each setting item. It contains
	// the icon, notebook name and a check box.
	var settingItem = document.createElement("div");
	settingItem.className = "settings-item tabFocusable";
	settingItem["settings-id"] = id;
	settingItem.setAttribute("role", "listitem");
	settingItem.setAttribute("tabindex", "-1");

	// Icon of the setting item
	var icon = document.createElement("div");
	icon.className = "settings-icon notification-icon-background";
	icon.setAttribute("aria-hidden", "true");
	icon.style.backgroundPosition = "0 " + GetNotebookIconColorOffset(notebookName) + "px";

	// Content name of the setting item
	var content = document.createElement("div");
	content.textContent = notebookName;
	content.className = "settings-content";
	content.setAttribute("aria-hidden", "true");

	// Check box - A label containing an input and a span
	var label = document.createElement("label");
	label.className = "checkbox-container";
	// input tag as the checkbox function base
	var input = document.createElement("input");
	input.type = "checkbox";
	input.checked = fActivate;
	input.setAttribute("tabindex", "-1");
	label.appendChild(input);
	// span tag used to customize the apperance of the checkbox
	var span = document.createElement("span");
	span.className = "checkmark";
	label.appendChild(span);

	settingItem.appendChild(icon);
	settingItem.appendChild(content);
	settingItem.appendChild(label);

	// Click Event Handler
	settingItem.addEventListener('click', OnSettingItemClicked, true);
	
	document.getElementById("settings-container").appendChild(settingItem);
}

/*---------------------------------------------------------------------------
	InitSettingItems

	Init Setting Items
---------------------------------------------------------------------------*/
window.InitSettingItems = function InitSettingItems(items){
	settingItems = JSON.parse(items);
	for (var i = 0, len = settingItems.length; i < len; ++i) {
		item = settingItems[i];
		AddSettingItem(item.resourceId, item.name, item.recvNotification);
	}

	RefreshSettings();
}

/*---------------------------------------------------------------------------
	NotifyHeroItemResult

	Called to notify the result ("succeeded" or "failed") of a hero item invocation
---------------------------------------------------------------------------*/
window.NotifyHeroItemResult = function NotifyHeroItemResult(id, result) {
	var heroNotification = document.getElementById(id);

	if (heroNotification != null){
		if (id == "TextYourself"){
			var message = heroNotification.getElementsByClassName("message")[0];
			message.style.display = "block";
			if (result == "succeeded"){
				message.classList.remove("fail");
				message.textContent = message["success"];
				AnnounceEvent(message["success"]);
			} else {
				message.classList.add("fail");
				message.textContent = message["fail"];
				AnnounceEvent(message["fail"]);
			}
		}
	}
}


/****************************************************************************
	Event Handler
****************************************************************************/
/*---------------------------------------------------------------------------
	OnSettingsButtonClicked

	Navigate to the Settings pane
---------------------------------------------------------------------------*/
window.OnSettingsButtonClicked = function OnSettingsButtonClicked(event){
	var body = document.getElementsByTagName("body")[0];
	navigation = "navigation-settings";
	body.classList.remove("navigation-notifications");
	body.classList.add(navigation);

	RaiseEvent("updateheight", "");

	// Focus on the setting list automatically if navigated through keyboard
	if (body.classList.contains("show-outline")){
		const settingContainer = document.getElementById("settings-container");
		if (settingContainer.children.length > 0) {
			settingContainer.children[0].focus();
		} else {
			document.getElementById("return").focus();
		}
	}
}

/*---------------------------------------------------------------------------
	OnReturnButtonClicked

	Navigate to the Notifications pane
---------------------------------------------------------------------------*/
window.OnReturnButtonClicked = function OnReturnButtonClicked(event){
	var body = document.getElementsByTagName("body")[0];
	navigation = "navigation-notifications";
	body.classList.remove("navigation-settings");
	body.classList.add(navigation);

	RaiseEvent("updateheight", "");

	// Focus on the settings button automatically if navigated through keyboard
	if (body.classList.contains("show-outline")){
		document.getElementById("settings").focus();
	}
}

/*---------------------------------------------------------------------------
	OnNotificationItemClicked

	Notify XAML a notificationItem is clicked
---------------------------------------------------------------------------*/
function OnNotificationItemClicked(event){
	event.stopPropagation();

	// The coordinate of the pointer position relative to the viewport
	// We can treat x as the relative position to the notification item as well because:
	// 1. The whole width of the notification center is always visible; so viewport width = page width
	// 2. Notification item actually occupies the whole width
	var x = event.clientX;
	var y = event.clientY;

	// If the clicked position is within the boundary of the delete button, raise
	// the delete button clicked event instead. This is to handle the case of deleting
	// multiple notifications quickly. When there's no mouse move between two clicks,
	// the mouse hover event is not fired in Edge, so the delete button of the second
	// notification won't receive the clikcing event (because it depends on .notification-item:hover).
	// The click event is instead received by the notification item, so we need to pass
	// the event to the delete button manually.
	var deleteButton = this.getElementsByTagName("button")[0];
	if (x > deleteButton.offsetLeft
		&& x < deleteButton.offsetLeft + deleteButton.offsetWidth
		&& y > deleteButton.getBoundingClientRect().top
		&& y < deleteButton.getBoundingClientRect().top + deleteButton.offsetHeight){
		deleteButton.click();
		return;
	}

	RaiseEvent("click", this["notification-id"]);

	// If it's unread, change it to read state
	var content = this.getElementsByClassName("unread")[0];
	if (content != null){
		content.classList.remove("unread");
		this.getElementsByClassName("notification-circle")[0].style.visibility = "hidden";
	}

	// Let screen reader announce the Navigated to event
	if (this["providerId"] == "recentnotebooks"){
		AnnounceEvent(navigatedA11YString.replace("|0", GetNotebookNameFromRecentNotebooksNotifications(this["recent-notification-title"])));
	}
}

/*---------------------------------------------------------------------------
	OnSettingItemClicked

	Check or uncheck the check box, and notify XAML the event
---------------------------------------------------------------------------*/
function OnSettingItemClicked(event){
	event.stopPropagation();

	var input = this.getElementsByClassName("checkbox-container")[0].getElementsByTagName("input")[0];
	input.checked = !input.checked;

	RaiseEvent("setting", this["settings-id"], input.checked);

	var enabledA11YString = settingsEnabledA11YString;
	if (!input.checked){
		enabledA11YString = settingsDisabledA11YString;
	}
	enabledA11YString = enabledA11YString.replace("|0", this.getElementsByClassName("settings-content")[0].textContent);

	AnnounceEvent(enabledA11YString);
	this.setAttribute("aria-label", enabledA11YString);
}

/*---------------------------------------------------------------------------
	OnDeleteButtonClicked

	Notify UI a notificationItem is deleted, remove that item from UI.
	Call RefreshNotifications to update tabindex and id of notificationItems
---------------------------------------------------------------------------*/
function OnDeleteButtonClicked(event){
	event.stopPropagation();

	var id = this.parentNode["notification-id"];
	this.parentNode.remove();
	RefreshNotifications("false");
	RaiseEvent("delete", id);
	AnnounceEvent(deletedA11YString);
}

/*---------------------------------------------------------------------------
	OnHeroItemTextBoxInvoked
---------------------------------------------------------------------------*/
function OnHeroItemTextBoxInvoked(inputForm){
	var input = inputForm.getElementsByTagName("input")[0];
	var message = inputForm.getElementsByClassName("message")[0];

	if (inputForm["input-type"] == "USPhone"){
		var value = input.value.replace(/\s/g, "");

		if (value != ""){
			if (value.length == 10){
				input.classList.remove("input-error");
				message.textContent = "";
				message.style.display = "none";
				RaiseEvent("heroitem", inputForm.parentNode["notification-id"], false, {"textbox": value});
			} else {
				input.classList.add("input-error");
				message.style.display = "block";
				message.className = "message fail";
				message.textContent = message["invalidinput"];
				AnnounceEvent(message["invalidinput"]);
			}
		}
	}
}

/*---------------------------------------------------------------------------
	OnHeroInputEvent
---------------------------------------------------------------------------*/
function OnHeroInputEvent(self){
	var value = self.value;
	var selectionStart = self.selectionStart;

	if (self.parentNode["input-type"] == "USPhone"){
		// Remove all non digit characters
		value = value.replace(/\D/ig, "");

		// Remove digits after 10th digit
		value = value.substr(0, 10);

		// Format number into xxx xxx xxxx
		if (value.length > 3){
			value = value.substr(0, 3) + " " + value.substr(3);

			if (value.length > 7){
				value = value.substr(0, 7) + " " + value.substr(7);
			}
		}

		// Move the selectionStart forward since we appended a space at certain digit
		if (selectionStart == 4 || selectionStart == 8){
			selectionStart++;
		}
	}

	self.value = value;

	// Since we change self.value manually, the selectionStart will jump to the end of the string by default.
	// We need to handle the case where users select some characters in the middle and keep selectionStart at its original place.
	if (selectionStart < value.length){
		self.selectionStart = selectionStart;
		self.selectionEnd = selectionStart;
	}
}

/*---------------------------------------------------------------------------
	GetLastValidFocusableElement
---------------------------------------------------------------------------*/
function GetLastValidFocusableElement() {
	let tabableObjs = document.querySelectorAll("[tabIndex='0']");

	for (let i = tabableObjs.length - 1; i >= 0; i--) {
		let obj = tabableObjs[i];
		if (obj.offsetParent !== null) {
			return obj;
		}
	}

	return null;
}

/*---------------------------------------------------------------------------
	GetFirstValidFocusableElement
---------------------------------------------------------------------------*/
function GetFirstValidFocusableElement() {
	let tabableObjs = document.querySelectorAll("[tabIndex='0']");

	for (let i = 0; i < tabableObjs.length; i++) {
		let obj = tabableObjs[i];
		if (obj.offsetParent !== null) {
			return obj;
		}
	}

	return null;
}

/*---------------------------------------------------------------------------
	OnKeyboardTabPressed

	Focus the first element if tab pressed and last tabable object is on focus.
	Focus the last element if shift tab pressed and first tabable object is on
	focus.
---------------------------------------------------------------------------*/
function OnKeyboardTabPressed(event){
	var lastFocusableElement = GetLastValidFocusableElement();
	if (lastFocusableElement === null) {
		return;
	}

	var firstFocusableElement = GetFirstValidFocusableElement();
	if (firstFocusableElement === null) {
		return;
	}

	// If shift tab at the beginning of a tab loop, focus the last tabable object
	if (event.shiftKey) {
		if (event.target === firstFocusableElement) {
			lastFocusableElement.focus();
			event.preventDefault();
		}
	// If tab at the last tabable object, focus the first object
	} else {
		if (event.target === lastFocusableElement) {
			firstFocusableElement.focus();
			event.preventDefault();
		}
	}
}

/*---------------------------------------------------------------------------
	NotificationItemKeyBoardEventHandler

	Keyboard event handler.
---------------------------------------------------------------------------*/
function NotificationItemKeyBoardEventHandler(event){
	if (event.keyCode === keyCode["TAB"]){
		// Add the "show-outline" class to body, so that objects on focus can
		// display black outline
		document.body.classList.add("show-outline");

		// While most tab navigation is done by tabIndex automatically, there's a bug in webview
		// that between each tab loop, there's a state where nothing is onfocus.
		// So we need to handle the special case where users tab at the end of a tab loop, or shift tab
		// at the beginning of a tab loop
		OnKeyboardTabPressed(event);

		return;
	}

	// Prevent default actions except for input object or Esc key
	if (document.activeElement.nodeName != "INPUT" && event.keyCode != keyCode["ESC"]){
		event.preventDefault();
	}

	// Click the object
	if (event.keyCode === keyCode["ENTER"] || event.keyCode === keyCode["SPACE"]){
		var nextFocusElement = null;
		if (event.target.classList.contains("notification-delete")){
			if (event.target.parentNode.nextElementSibling !== null){
				nextFocusElement = event.target.parentNode.nextElementSibling;
			} else if (event.target.parentNode.previousElementSibling !== null){
				nextFocusElement = event.target.parentNode.previousElementSibling;
			} else {
				nextFocusElement = document.getElementById("refresh");
			}
		}

		if (event.target.tagName == "INPUT"){
			event.target.nextElementSibling.click();
		} else {
			event.target.click();
		}

		if (nextFocusElement != null){
			nextFocusElement.focus();
		}

		return;
	}

	if (event.target.getAttribute("id") == "refresh"){
		return;
	}

	// Move to next notification
	if (event.keyCode === keyCode["DOWN"]){
		if (event.target.classList.contains("notification-item")){
			FocusNextItemIfExists(event.target);
		} else if (event.target.classList.contains("notification-delete")){
			FocusNextItemIfExists(event.target.parentNode);
		} else if (event.target.classList.contains("settings-item")){
			FocusNextItemIfExists(event.target);
		}

	// Move to previous notification
	} else if (event.keyCode === keyCode["UP"]){
		if (event.target.classList.contains("notification-item")){
			FocusPrevItemIfExists(event.target);
		} else if (event.target.classList.contains("notification-delete")){
			FocusPrevItemIfExists(event.target.parentNode);
		} else if (event.target.classList.contains("settings-item")){
			FocusPrevItemIfExists(event.target);
		}

	// Move to the delete button or next notification
	} else if ((event.keyCode === keyCode["LEFT"] && document.dir == "rtl")
			|| (event.keyCode === keyCode["RIGHT"] && document.dir != "rtl")){
		if (event.target.classList.contains("notification-item")){
			event.target.getElementsByClassName("notification-delete")[0].focus();
		} else if (event.target.classList.contains("notification-delete")){
			FocusNextItemIfExists(event.target.parentNode);
		} else if (event.target.classList.contains("settings-item")){
			FocusNextItemIfExists(event.target);
		}

	// Move to the notification or the delete button of previous notification
	} else if ((event.keyCode === keyCode["RIGHT"] && document.dir == "rtl")
			|| (event.keyCode === keyCode["LEFT"] && document.dir != "rtl")){
		if (event.target.classList.contains("notification-item")){
			if (event.target.previousElementSibling != null){
				event.target.previousElementSibling.setAttribute("tabindex", 0);
				event.target.previousElementSibling.getElementsByClassName("notification-delete")[0].focus();
				event.target.setAttribute("tabindex", -1);
			}
		} else if (event.target.classList.contains("notification-delete")){
			event.target.parentNode.focus();
		} else if (event.target.classList.contains("settings-item")){
			FocusPrevItemIfExists(event.target);
		}
	}
}


/****************************************************************************
	Helper functions
****************************************************************************/
/*---------------------------------------------------------------------------
	FocusNextItemIfExists

	Move the focus to next element sibling if it exists
---------------------------------------------------------------------------*/
function FocusNextItemIfExists(current){
	if (current.nextElementSibling !== null){
		current.nextElementSibling.setAttribute("tabindex", 0);
		current.nextElementSibling.focus();
		current.setAttribute("tabindex", -1);
	}
}

/*---------------------------------------------------------------------------
	FocusPrevItemIfExists

	Move the focus to previous sibling if it exists
---------------------------------------------------------------------------*/
function FocusPrevItemIfExists(current){
	if (current.previousElementSibling !== null){
		current.previousElementSibling.setAttribute("tabindex", 0);
		current.previousElementSibling.focus();
		current.setAttribute("tabindex", -1);
	}
}

/*---------------------------------------------------------------------------
	GetNotebookIconColorOffset

	Hash the notebookName to get the offset index to choose the color of
	the notebook icon for this notebook. There are in total 16 notebook icon
	colors, so that the hash function first gets the sum of the notebookName
	characters ASCII values, then mod it by 16, then get corresponding offset.
---------------------------------------------------------------------------*/
function GetNotebookIconColorOffset(notebookName){
	var sum = 0;
	for (var i = 0; i < notebookName.length; i++){
		sum += notebookName.charCodeAt(i);
	}
	
	return (((sum % 16) + 7) * -32);
}

/*---------------------------------------------------------------------------
	GetNotebookNameFromRecentNotebooksNotifications

	Get the notebook name from the body of Recentnotebooks notifications,
	which is the bolded string (wrapped by <strong>)
---------------------------------------------------------------------------*/
function GetNotebookNameFromRecentNotebooksNotifications(body){
	var start = body.search("<strong class='recent-notification-title'>");
	var end = body.search("</strong>");
	return body.substring(start + 42, end);
}

/*---------------------------------------------------------------------------
	GetTextContent

	Get the plain text content of a notification based on its content
---------------------------------------------------------------------------*/
function GetTextContent(body, detail, providerId){
	var content = body.replace("<strong>", "").replace("</strong>", "") + ", " + detail;

	if (providerId == "recentnotebooks"){
		var content = content.replace("<strong class='recent-notification-title'>", "")
	}

	return content;
}

/*---------------------------------------------------------------------------
	AnnounceEvent

	Let screen reader announce announcement
---------------------------------------------------------------------------*/
function AnnounceEvent(announcement){
	var accessibilityEvent = document.getElementById("AccessibilityEvent");
	accessibilityEvent.setAttribute("aria-label", announcement);
	accessibilityEvent.textContent = accessibilityEvent.textContent + "1";
}

})();