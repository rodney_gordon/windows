 -- ----------------------------------------------------------------------------
-- Copyright (C) 2012 Nokia Gate5 GmbH Berlin
--
-- These coded instructions, statements, and computer programs contain
-- unpublished proprietary information of Nokia Gate5 GmbH Berlin, and
-- are copy protected by law. They may not be disclosed to third parties
-- or copied or duplicated in any form, in whole or in part, without the
-- specific, prior written permission of Nokia Gate5 GmbH Berlin.
-- ----------------------------------------------------------------------------
--   Author: Fabian TP Riek
-- ----------------------------------------------------------------------------
--       Voice Skin: de-DE  German TTS

street_article = { 
	["ziel"] = {
		["in der"] = {"stra[sß]-e", "gasse" },
		["im"] = {"weg","gang","pfad","tunnel"},
		["auf dem"] = {"damm","wall"},
		["auf der"] = {"allee"} },

	["waypoint"] = {
		["in der"] = {"stra[sß]-e", "gasse" },
		["im"] = {"weg","gang","pfad","tunnel"},
		["auf dem"] = {"damm","wall"},
		["auf der"] = {"allee"} },

	[" folgen"] = {
		["der"] = { "stra[sß]-e", "gasse", "allee" },
		["dem"] = { "weg","gang","pfad","damm","wall","tunnel"}, },

	["richtung"] = { [" "] = { ".+" }, },

	["baseArticle"] = {
		["in die"] = {"stra[sß]-e", "gasse" },
		["in den"] = {"weg","gang","pfad","tunnel"},
		["auf den"] = {"damm","wall"},
		["auf die"] = {"allee"} },
}

nG_prepositions = {	
	["GERBVORX"] = "Vor",
	["GERBVRDR"] = "Vor der",
	["GERBVRDM"] = "Vor dem",
	["GERBVRDN"] = "Vor den",
	["GERBUNTR"] = "Unter",
	["GERBUNDR"] = "Unter der",
	["GERBUNDM"] = "Unter dem",
	["GERBUNDN"] = "Unter den",
	["GERBUNDI"] = "Unter die",
	["GERBHINT"] = "Hinter",
	["GERBHNDR"] = "Hinter der",
	["GERBHNDM"] = "Hinter dem",
	["GERBHNDN"] = "Hinter den",
	["GERBANXX"] = "An",
	["GERBANDR"] = "An der",
	["GERBANDM"] = "An dem",
	["GERBANDN"] = "An den",
	["GERBDURC"] = "Durch",
	["GERBDRDI"] = "Durch die",
	["GERBDRDN"] = "Durch den",
	["GERBDRDS"] = "Durch das",
	["GERBAUFX"] = "Auf",
	["GERBAUDR"] = "Auf der",
	["GERBAUDM"] = "Auf dem",
	["GERBAUDN"] = "Auf den",
	["GERBAUDI"] = "Auf die",
	["GERBUBER"] = "Über",
	["GERBUBDR"] = "Über der",
	["GERBUBDM"] = "Über dem",
	["GERBUBDI"] = "Über die",
	["GERBVORB"] = "Vorbei",
	["GERBVBAN"] = "Vorbei an",
	["GERBVBDR"] = "Vorbei an der",
	["GERBVBDM"] = "Vorbei an dem",
	["GERBVBDN"] = "Vorbei an den",
	["NONE"] = "", 
}

nG_elements = { 
	[1] = "an der nächsten Ampel", 
	[2] = "an der zweiten Ampel", 
	[3] = "an der dritten Ampel", 
	["UNDEFINED"] = "",
}

unit_after = { 
	["MILE"] = "Meile", 
	["YARDS"] = "Yards", 
	["FEET"] = "Fuß", 
	["KILOMETER"] = "Kilometer", 
	["METERS"] = "Metern", 
	["METER"] = "Meter", 
	["KILOMETERS"] = "Kilometern", 
	["MILES"] = "Meilen", 
	["UNDEFINED"] = "",
}

unit_follow = { 
	["MILE"] = "Meile", 
	["YARDS"] = "Yards", 
	["FEET"] = "Fuß", 
	["KILOMETER"] = "Kilometer", 
	["METERS"] = "Meter", 
	["METER"] = "Meter", 
	["KILOMETERS"] = "Kilometer", 
	["MILES"] = "Meilen", 
	["UNDEFINED"] = "",
}

dist = { 
	["a"] = "einem Kilometer", 
	["b"] = "einer Meile", 
	["c"] = "einer viertel Meile", 
	["d"] = "einer halben Meile", 
	["e"] = "einer dreiviertel Meile", 
	["UNDEFINED"] = "",
}

exit_number_roundabout = { 
	[1] = "die erste Ausfahrt nehmen", 
	[2] = "die zweite Ausfahrt nehmen", 
	[3] = "die dritte Ausfahrt nehmen", 
	[4] = "die vierte Ausfahrt nehmen", 
	[5] = "die fünfte Ausfahrt nehmen", 
	[6] = "die sechste Ausfahrt nehmen", 
	[7] = "die siebte Ausfahrt nehmen", 
	[8] = "die achte Ausfahrt nehmen", 
	[9] = "die neunte Ausfahrt nehmen", 
	[10] = "die zehnte Ausfahrt nehmen", 
	[11] = "die elfte Ausfahrt nehmen", 
	[12] = "die zwölfte Ausfahrt nehmen", 
	["UNDEFINED"] = "",
}

orientation = { 
	["NORTH"] = "Norden", 
	["NORTH_EAST"] = "Nordosten", 
	["EAST"] = "Osten", 
	["SOUTH_EAST"] = "Südosten", 
	["SOUTH"] = "Süden", 
	["SOUTH_WEST"] = "Südwesten", 
	["WEST"] = "Westen", 
	["NORTH_WEST"] = "Nordwesten", 
	["UNDEFINED"] = "",
}

turn_number_ped = { 
	[1] = "und an der ersten Straße abbiegen !STREET!", 
	[2] = "und an der zweiten Straße abbiegen !STREET!", 
	[3] = "und an der dritten Straße abbiegen !STREET!", 
	["UNDEFINED"] = "",
}

commands_common = { 
	
	["00000000"] = " ",
	
	["c00c0zc0"] = "Am Ende der Straße im Kreisverkehr !EXIT_NO_ROUNDABOUT! Richtung !SIGNPOST!",
	
	["c00c0zb0"] = "Am Ende der Straße im Kreisverkehr !EXIT_NO_ROUNDABOUT! !STREET!",
	
	["c00c0z00"] = "Am Ende der Straße im Kreisverkehr !EXIT_NO_ROUNDABOUT!",
	
	["h00d000x"] = "und dann !NG_COMMAND_2! die Ausfahrt Richtung !STREET_2! !SIGNPOST_2! nehmen",
	
	["h00f000x"] = "und dann !NG_COMMAND_2! auf die Autobahn !STREET_2! Richtung !SIGNPOST_2! fahren ",
	
	["h00j000x"] = "und dann !NG_COMMAND_2! die Ausfahrt Richtung !STREET_2! !SIGNPOST_2! nehmen",
	
	["h00e000x"] = "und dann !NG_COMMAND_2! wenden Richtung !STREET_2! !SIGNPOST_2!",
	
	["h00i000x"] = "und dann !NG_COMMAND_2! auf die Stadtautobahn !STREET_2! Richtung !SIGNPOST_2! fahren",
	
	["h00g000x"] = "und dann !NG_COMMAND_2! die Ausfahrt Richtung !STREET_2! !SIGNPOST_2! nehmen",
	
	["h00o000x"] = "und dann !NG_COMMAND_2! die mittlere Spur Richtung !STREET_2! !SIGNPOST_2! nehmen",
	
	["h00v000x"] = "und dann !NG_COMMAND_2! links abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["h00w000x"] = "und dann !NG_COMMAND_2! leicht links abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["h00u000x"] = "und dann !NG_COMMAND_2! scharf links abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["h00s000x"] = "und dann !NG_COMMAND_2! scharf rechts abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["h00r000x"] = "und dann !NG_COMMAND_2! rechts abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["h00p000x"] = "und dann !NG_COMMAND_2! rechts halten Richtung !STREET_2! !SIGNPOST_2!",
	
	["h00t000x"] = "und dann !NG_COMMAND_2! wenden Richtung !STREET_2! !SIGNPOST_2!",
	
	["h00x000x"] = "und dann !NG_COMMAND_2! links halten Richtung !STREET_2! !SIGNPOST_2!",
	
	["h00q000x"] = "und dann !NG_COMMAND_2! leicht rechts abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["h000cz0x"] = "und dann im Kreisverkehr !EXIT_NO_ROUNDABOUT! Richtung !STREET_2! !SIGNPOST_2! ",
	
	["bl0o000j"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, die Ausfahrt nehmen",
	
	["bl0x000j"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, die Ausfahrt nehmen",
	
	["bl0p000j"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, die Ausfahrt nehmen",
	
	["bl0n000j"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren, die Ausfahrt nehmen",
	
	["bl0o00fj"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen und die Ausfahrt nehmen ",
	
	["bl00000j"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die Ausfahrt nehmen",
	
	["bl0x00fj"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten und die Ausfahrt nehmen",
	
	["bl0p00fj"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten und die Ausfahrt nehmen",
	
	["bl0n00fj"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren und die Ausfahrt nehmen",
	
	["a000000g"] = "Jetzt !NG_COMMAND_1! die Ausfahrt nehmen",
	
	["bl000e0x"] = "Nach !DIST! !UNIT! Ausfahrt !EXIT_NUMBER! Richtung !STREET_2! !SIGNPOST_2! nehmen",
	
	["a00ni000"] = "Jetzt !NG_COMMAND_1! geradeaus auf die Stadtautobahn fahren",
	
	["h00n000x"] = "und dann geradeaus fahren Richtung !STREET_2! !SIGNPOST_2!",
	
	["a00o000j"] = "Jetzt !NG_COMMAND_1! die mittlere Spur nehmen, die Ausfahrt nehmen",
	
	["a00x000j"] = "Jetzt !NG_COMMAND_1! links halten, die Ausfahrt nehmen",
	
	["a00p000j"] = "Jetzt !NG_COMMAND_1! rechts halten, die Ausfahrt nehmen",
	
	["a00n00fj"] = "Jetzt !NG_COMMAND_1! geradeaus fahren und die Ausfahrt nehmen",
	
	["a00n000j"] = "Jetzt !NG_COMMAND_1! geradeaus fahren, die Ausfahrt nehmen ",
	
	["a00o00fj"] = "Jetzt !NG_COMMAND_1! die mittlere Spur nehmen und die Ausfahrt nehmen",
	
	["a000000j"] = "Jetzt !NG_COMMAND_1! die Ausfahrt nehmen",
	
	["a00x00fj"] = "Jetzt !NG_COMMAND_1! links halten und die Ausfahrt nehmen",
	
	["a00p00fj"] = "Jetzt !NG_COMMAND_1! rechts halten und die Ausfahrt nehmen",
	
	["a00n00fj"] = "Jetzt !NG_COMMAND_1! geradeaus fahren und die Ausfahrt nehmen",
	
	["h000ab00"] = "und dann !NG_COMMAND_2! haben Sie Ihr Ziel !STREET! erreicht",
	
	["bl0v0d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen !STREET!",
	
	["a00pi000"] = "Jetzt !NG_COMMAND_1! rechts halten, auf die Stadtautobahn fahren",
	
	["bl0nf000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus auf die Autobahn fahren",
	
	["bl0a0b00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! haben Sie Ihr Ziel !STREET! erreicht",
	
	["j00q0ac0"] = "und dann sofort leicht rechts abbiegen !STREET! Richtung !SIGNPOST!",
	
	["bl0o0edz"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen und !STREET_2! Richtung !SIGNPOST_2! folgen",
	
	["h000ed00"] = "und dann !NG_COMMAND_2! wenden !STREET!",
	
	["bl0r0c00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen Richtung !STREET!",
	
	["j000ed00"] = "und dann sofort !NG_COMMAND_2! wenden !STREET!",
	
	["bl0x0edz"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen und !STREET_2! Richtung !SIGNPOST_2! folgen",
	
	["a00rf000"] = "Jetzt !NG_COMMAND_1! rechts abbiegen, auf die Autobahn fahren",
	
	["bl0v0c00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen Richtung !STREET!",
	
	["bl0q0c00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts Richtung !STREET! abbiegen",
	
	["c00rf000"] = "Am Ende der Straße !NG_COMMAND_1! rechts abbiegen, auf die Autobahn fahren",
	
	["bl0b0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! haben Sie Ihr Zwischenziel erreicht",
	
	["bl0x0e0z"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten Ausfahrt !EXIT_NUMBER! nehmen und !STREET_2! Richtung !SIGNPOST_2! folgen",
	
	["a00d0000"] = "Jetzt !NG_COMMAND_1! die Ausfahrt nehmen",
	
	["bl00f000"] = "Nach !DIST! !UNIT! auf die Autobahn fahren",
	
	["h00d0000"] = "und dann !NG_COMMAND_2! die Ausfahrt nehmen",
	
	["bl0qf0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["h00v0000"] = "und dann !NG_COMMAND_2! links abbiegen",
	
	["j00v0000"] = "und dann sofort !NG_COMMAND_2! links abbiegen",
	
	["j000e000"] = "und dann sofort !NG_COMMAND_2! wenden",
	
	["bl0uf0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["c00ui0c0"] = "Am Ende der Straße !NG_COMMAND_1! scharf links abbiegen, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["h000e000"] = "und dann !NG_COMMAND_2! wenden",
	
	["c00r0d00"] = "Am Ende der Straße !NG_COMMAND_1! rechts abbiegen !STREET!",
	
	["h00r0a00"] = "und dann rechts abbiegen !STREET!",
	
	["j000dc00"] = "und dann sofort !NG_COMMAND_2! die Ausfahrt nehmen Richtung !STREET!",
	
	["a00v0000"] = "Jetzt !NG_COMMAND_1! links abbiegen",
	
	["c00v0000"] = "Am Ende der Straße !NG_COMMAND_1! links abbiegen",
	
	["c00m0c00"] = "Am Ende der Straße !NG_COMMAND_1! die Fähre Richtung !STREET! nehmen",
	
	["bl0o0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen",
	
	["bl0x00fg"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten und die Ausfahrt nehmen",
	
	["bl0o0e0x"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, Ausfahrt !EXIT_NUMBER! Richtung !STREET_2! !SIGNPOST_2! nehmen",
	
	["bl0ofac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["a00n00fg"] = "Jetzt !NG_COMMAND_1! geradeaus fahren und die Ausfahrt nehmen",
	
	["a00d0c00"] = "Jetzt !NG_COMMAND_1! die Ausfahrt Richtung !STREET! nehmen",
	
	["a00x000g"] = "Jetzt !NG_COMMAND_1! links halten, die Ausfahrt nehmen",
	
	["h000b000"] = "und dann !NG_COMMAND_2! haben Sie Ihr Zwischenziel erreicht",
	
	["c00qi0c0"] = "Am Ende der Straße !NG_COMMAND_1! leicht rechts abbiegen, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["bl0sf0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["bl0piac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0x0ed0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen",
	
	["bl0p0edy"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen und !STREET_2! folgen",
	
	["bl0ti0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["bl0w0d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen !STREET! ",
	
	["c00wi0c0"] = "Am Ende der Straße !NG_COMMAND_1! leicht links abbiegen, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["bl0c0z00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! im Kreisverkehr !EXIT_NO_ROUNDABOUT! ",
	
	["bl0o0edx"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, Ausfahrt !EXIT_NUMBER! !SIGNPOST! Richtung !STREET_2! !SIGNPOST_2! nehmen",
	
	["c00t0000"] = "Am Ende der Straße !NG_COMMAND_1! wenden",
	
	["bl0ni0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["bz0p0000"] = "Nach der Kreuzung !NG_COMMAND_1! rechts halten",
	
	["bz0n0000"] = "Nach der Kreuzung !NG_COMMAND_1! geradeaus fahren",
	
	["h00t0000"] = "und dann !NG_COMMAND_2! wenden",
	
	["bl0n00fg"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren und die Ausfahrt nehmen",
	
	["bl0uiac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0n000x"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren Richtung !STREET_2! !SIGNPOST_2!",
	
	["bl0n0edy"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren, Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen und !STREET_2! folgen",
	
	["j00s0ac0"] = "und dann sofort scharf rechts !STREET! Richtung !SIGNPOST! abbiegen",
	
	["bl0m0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die Fähre nehmen",
	
	["bl0oi0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["c00via00"] = "Am Ende der Straße !NG_COMMAND_1! links abbiegen, auf die Stadtautobahn !STREET! fahren",
	
	["h00s00c0"] = "und dann !NG_COMMAND_2! scharf rechts abbiegen Richtung !SIGNPOST! ",
	
	["a00o0000"] = "Jetzt !NG_COMMAND_1! die mittlere Spur nehmen",
	
	["bl0o0edy"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen und !STREET_2! folgen",
	
	["c00q0000"] = "Am Ende der Straße !NG_COMMAND_1! leicht rechts abbiegen",
	
	["h00o0000"] = "und dann !NG_COMMAND_2! die mittlere Spur nehmen",
	
	["a00p00fg"] = "Jetzt !NG_COMMAND_1! rechts halten und die Ausfahrt nehmen ",
	
	["j00o0000"] = "und dann sofort !NG_COMMAND_2! die mittlere Spur nehmen",
	
	["bl0wfac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0t0c00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden Richtung !STREET!",
	
	["h00m0000"] = "und dann !NG_COMMAND_2! die Fähre nehmen",
	
	["a00q0000"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen",
	
	["bl0ri000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen, auf die Stadtautobahn fahren",
	
	["h00x0c00"] = "und dann !NG_COMMAND_2! links halten Richtung !STREET!",
	
	["j00q0d00"] = "und dann sofort !NG_COMMAND_2! leicht rechts abbiegen !STREET!",
	
	["bl0rf000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen, auf die Autobahn fahren",
	
	["bl0e0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden",
	
	["h00i00c0"] = "und dann !NG_COMMAND_2! auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["c00m0000"] = "Am Ende der Straße !NG_COMMAND_1! die Fähre nehmen",
	
	["a00m0000"] = "Jetzt !NG_COMMAND_1! die Fähre nehmen",
	
	["bl0s0c00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen Richtung !STREET!",
	
	["c00ufa00"] = "Am Ende der Straße !NG_COMMAND_1! scharf links abbiegen, auf die Autobahn !STREET! fahren",
	
	["c00tfa00"] = "Am Ende der Straße !NG_COMMAND_1! wenden, auf die Autobahn !STREET! fahren",
	
	["bl00fac0"] = "Nach !DIST! !UNIT! auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["h00i0ac0"] = "und dann auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["j00o00c0"] = "und dann sofort !NG_COMMAND_2! die mittlere Spur Richtung !SIGNPOST! nehmen",
	
	["h00o0d00"] = "und dann !NG_COMMAND_2! die mittlere Spur nehmen !STREET!",
	
	["bl0o0e00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, Ausfahrt !EXIT_NUMBER! nehmen",
	
	["bl0via00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen, auf die Stadtautobahn !STREET! fahren",
	
	["c00uf000"] = "Am Ende der Straße !NG_COMMAND_1! scharf links abbiegen, auf die Autobahn fahren",
	
	["a00uf000"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen, auf die Autobahn fahren",
	
	["bl0p0ed0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen",
	
	["j000i000"] = "und dann sofort !NG_COMMAND_2! auf die Stadtautobahn fahren",
	
	["c00vfa00"] = "Am Ende der Straße !NG_COMMAND_1! links abbiegen, auf die Autobahn !STREET! fahren",
	
	["h000i000"] = "und dann !NG_COMMAND_2! auf die Stadtautobahn fahren",
	
	["bl0qfac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0w0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen",
	
	["a000i000"] = "Jetzt auf die Stadtautobahn fahren",
	
	["bl0xia00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, auf die Stadtautobahn !STREET! fahren",
	
	["bl0n0ed0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren, Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen",
	
	["j00u00c0"] = "und dann sofort !NG_COMMAND_2! scharf links abbiegen Richtung !SIGNPOST!",
	
	["a00n000g"] = "Jetzt !NG_COMMAND_1! geradeaus fahren, die Ausfahrt nehmen",
	
	["c00uia00"] = "Am Ende der Straße !NG_COMMAND_1! scharf links abbiegen, auf die Stadtautobahn !STREET! fahren",
	
	["h00u00c0"] = "und dann !NG_COMMAND_2! scharf links abbiegen Richtung !SIGNPOST!",
	
	["bl0s0d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen !STREET!",
	
	["bl0tf000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden, auf die Autobahn fahren",
	
	["bl0q0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen",
	
	["h00g0000"] = "und dann !NG_COMMAND_2! die Ausfahrt nehmen",
	
	["c00s0000"] = "Am Ende der Straße !NG_COMMAND_1! scharf rechts abbiegen",
	
	["h00e0000"] = "und dann !NG_COMMAND_2! wenden",
	
	["a00s0000"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen",
	
	["j00p0d00"] = "und dann sofort !NG_COMMAND_2! rechts halten !STREET!",
	
	["c00e0000"] = "Am Ende der Straße !NG_COMMAND_1! wenden",
	
	["h00s0000"] = "und dann !NG_COMMAND_2! scharf rechts abbiegen",
	
	["c00u0c00"] = "Am Ende der Straße !NG_COMMAND_1! scharf links abbiegen Richtung !STREET!",
	
	["bl0tf0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["c00wfac0"] = "Am Ende der Straße !NG_COMMAND_1! leicht links abbiegen, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["c00wi000"] = "Am Ende der Straße !NG_COMMAND_1! leicht links abbiegen, auf die Stadtautobahn fahren",
	
	["bl0ofa00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, auf die Autobahn !STREET! fahren",
	
	["bl0ti000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden, auf die Stadtautobahn fahren",
	
	["c00qiac0"] = "Am Ende der Straße !NG_COMMAND_1! leicht rechts abbiegen, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["a00r000g"] = "Jetzt !NG_COMMAND_1! rechts abbiegen, die Ausfahrt nehmen ",
	
	["bl0qi000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen, auf die Stadtautobahn fahren",
	
	["bl0q0d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts !STREET! abbiegen",
	
	["bl0p00fg"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten und die Ausfahrt nehmen",
	
	["c00s0c00"] = "Am Ende der Straße !NG_COMMAND_1! scharf rechts abbiegen Richtung !STREET!",
	
	["a00vi000"] = "Jetzt !NG_COMMAND_1! links abbiegen, auf die Stadtautobahn fahren",
	
	["c00vi000"] = "Am Ende der Straße !NG_COMMAND_1! links abbiegen, auf die Stadtautobahn fahren",
	
	["a00v00fg"] = "Jetzt !NG_COMMAND_1! links abbiegen und die Ausfahrt nehmen",
	
	["bl0xfa00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, auf die Autobahn !STREET! fahren",
	
	["bl0c0zc0"] = "Nach !DIST! !UNIT! im Kreisverkehr !EXIT_NO_ROUNDABOUT! Richtung !SIGNPOST!",
	
	["c00tf0c0"] = "Am Ende der Straße !NG_COMMAND_1! wenden, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["bl0si0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["c00b0b00"] = "Am Ende der Straße !NG_COMMAND_1! haben Sie Ihr Zwischenziel !STREET! erreicht",
	
	["bz0w0000"] = "Nach der Kreuzung !NG_COMMAND_1! leicht links abbiegen",
	
	["c00qfac0"] = "Am Ende der Straße !NG_COMMAND_1! leicht rechts abbiegen, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0s0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen",
	
	["h00v0ac0"] = "und dann links abbiegen !STREET! Richtung !SIGNPOST!",
	
	["h00p0a00"] = "und dann rechts halten !STREET!",
	
	["c00siac0"] = "Am Ende der Straße !NG_COMMAND_1! scharf rechts abbiegen, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["c00vf0c0"] = "Am Ende der Straße !NG_COMMAND_1! links abbiegen, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["bl0tfa00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden, auf die Autobahn !STREET! fahren",
	
	["c00viac0"] = "Am Ende der Straße !NG_COMMAND_1! links abbiegen, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["h000ec00"] = "und dann !NG_COMMAND_2! wenden Richtung !STREET!",
	
	["j000ec00"] = "und dann sofort !NG_COMMAND_2! wenden Richtung !STREET!",
	
	["a00d0d00"] = "Jetzt !NG_COMMAND_1! die Ausfahrt nehmen !STREET!",
	
	["h000g000"] = "und dann !NG_COMMAND_2! die Ausfahrt nehmen",
	
	["j00o0ac0"] = "und dann sofort die mittlere Spur nehmen !STREET! Richtung !SIGNPOST!",
	
	["j000g000"] = "und dann sofort !NG_COMMAND_2! die Ausfahrt nehmen",
	
	["c00qia00"] = "Am Ende der Straße !NG_COMMAND_1! leicht rechts abbiegen, auf die Autobahn !STREET! fahren",
	
	["h000fac0"] = "und dann auf die Autobahn !STREET! Richtung !SIGNPOST! fahren ",
	
	["bl0c0zb0"] = "Nach !DIST! !UNIT! im Kreisverkehr !EXIT_NO_ROUNDABOUT! !STREET!",
	
	["bl0d0d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die Ausfahrt nehmen !STREET!",
	
	["a00ri000"] = "Jetzt !NG_COMMAND_1! rechts abbiegen, auf die Stadtautobahn fahren",
	
	["c00ri000"] = "Am Ende der Straße !NG_COMMAND_1! rechts abbiegen, auf die Stadtautobahn fahren",
	
	["y0000000"] = "Sie haben Ihr Ziel erreicht",
	
	["yp000000"] = "Sie haben Ihr Ziel erreicht. Das Ziel befindet sich auf der rechten Seite",
	
	["yq000000"] = "Sie haben Ihr Ziel erreicht. Das Ziel befindet sich auf der linken Seite",
	
	["x0000000"] = "!SET_AUDIO!beep16K.wav!SET_AUDIO!",
	
	-- ["w0000000"] = "Routenneuberechnung",
	
	["v0000000"] = "!SET_AUDIO!beep16K.wav!SET_AUDIO! Blitzgerät voraus",
	
	["000c0z00"] = "Im Kreisverkehr !EXIT_NO_ROUNDABOUT!",
	
	["a00nf000"] = "Jetzt !NG_COMMAND_1! geradeaus auf die Autobahn fahren",
	
	["zr000000"] = "Sie haben ein Zwischenziel erreicht. Das Zwischenziel befindet sich auf der rechten Seite",
	
	["zs000000"] = "Sie haben ein Zwischenziel erreicht. Das Zwischenziel befindet sich auf der linken Seite",
	
	["z0000000"] = "Sie haben ein Zwischenziel erreicht",
	
	["q0000000"] = "Die GPS Verbindung wurde wiederhergestellt",
	
	["p0000000"] = "GPS Signal verloren",
	
	["c00sfa00"] = "Am Ende der Straße !NG_COMMAND_1! scharf rechts abbiegen, auf die Autobahn !STREET! fahren",
	
	["u0000000"] = "Keine Umleitung für Verkehrsereignis gefunden",
	
	["t0000000"] = "Verkehrsereignis auf der Route, wünschen Sie eine Umleitung?",
	
	["r0000000"] = "!SET_AUDIO!speeding_beep16K.wav!SET_AUDIO!",
	
	["c00rfa00"] = "Am Ende der Straße !NG_COMMAND_1! rechts abbiegen, auf die Autobahn !STREET! fahren",
	
	["j00n0a00"] = "und dann sofort geradeaus fahren !STREET!",
	
	["c00qf0c0"] = "Am Ende der Straße !NG_COMMAND_1! leicht rechts abbiegen, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["bl0riac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0pfac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["c00tf000"] = "Am Ende der Straße !NG_COMMAND_1! wenden, auf die Autobahn fahren",
	
	["a00ti000"] = "Jetzt !NG_COMMAND_1! wenden, auf die Stadtautobahn fahren",
	
	["a00tf000"] = "Jetzt !NG_COMMAND_1! wenden, auf die Autobahn fahren",
	
	["bl0b0b00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! haben Sie Ihr Zwischenziel erreicht !STREET!",
	
	["j00y0000"] = "und dann sofort !NG_COMMAND_2! die Zweite rechts abbiegen",
	
	["h00y0000"] = "und dann !NG_COMMAND_2! die Zweite rechts abbiegen",
	
	["bl00i000"] = "Nach !DIST! !UNIT! auf die Stadtautobahn fahren",
	
	["bl0pia00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, auf die Stadtautobahn !STREET! fahren",
	
	["a000f000"] = "Jetzt auf die Autobahn fahren",
	
	["c00si000"] = "Am Ende der Straße !NG_COMMAND_1! scharf rechts abbiegen, auf die Stadtautobahn fahren",
	
	["bl0oia00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, auf die Stadtautobahn !STREET! fahren",
	
	["c00w0d00"] = "Am Ende der Straße !NG_COMMAND_1! leicht links abbiegen !STREET!",
	
	["h00w0000"] = "und dann !NG_COMMAND_2! leicht links abbiegen",
	
	["j00w0000"] = "und dann sofort !NG_COMMAND_2! leicht links abbiegen",
	
	["j00y0d00"] = "und dann sofort !NG_COMMAND_2! die Zweite rechts abbiegen !STREET!",
	
	["h00y0d00"] = "und dann !NG_COMMAND_2! die Zweite rechts abbiegen !STREET! ",
	
	["j000f000"] = "und dann sofort !NG_COMMAND_2! auf die Autobahn fahren",
	
	["a00si000"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen, auf die Stadtautobahn fahren",
	
	["a00w0000"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen",
	
	["bz0s0000"] = "Nach der Kreuzung !NG_COMMAND_1! scharf rechts abbiegen",
	
	["c00w0000"] = "Am Ende der Straße !NG_COMMAND_1! leicht links abbiegen",
	
	["h000f000"] = "und dann !NG_COMMAND_2! auf die Autobahn fahren",
	
	["bl0wi000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen, auf die Stadtautobahn fahren",
	
	["a00p0000"] = "Jetzt !NG_COMMAND_1! rechts halten",
	
	["j00x0d00"] = "und dann sofort !NG_COMMAND_2! links halten !STREET!",
	
	["j00r0ac0"] = "und dann sofort rechts abbiegen !STREET! Richtung !SIGNPOST!",
	
	["bl0wfa00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen, auf die Autobahn !STREET! fahren",
	
	["h00p0000"] = "und dann !NG_COMMAND_2! rechts halten",
	
	["bl0xf000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, auf die Autobahn fahren",
	
	["h000gc00"] = "und dann !NG_COMMAND_2! die Ausfahrt Richtung !STREET! nehmen",
	
	["c00ti0c0"] = "Am Ende der Straße !NG_COMMAND_1! wenden, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["j00z0d00"] = "und dann sofort !NG_COMMAND_2! die Zweite links abbiegen !STREET!",
	
	["bl0p0edz"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen und !STREET_2! Richtung !SIGNPOST_2! folgen",
	
	["h00o0c00"] = "und dann !NG_COMMAND_2! die mittlere Spur nehmen Richtung !STREET!",
	
	["bl0nf0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["bl0of0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["bl0w0c00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen Richtung !STREET!",
	
	["h00z0d00"] = "und dann !NG_COMMAND_2! die Zweite links abbiegen !STREET!",
	
	["bl0r0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen",
	
	["bl0p0e0x"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, Ausfahrt !EXIT_NUMBER! Richtung !STREET_2! !SIGNPOST_2! nehmen",
	
	["bz0v0000"] = "Nach der Kreuzung !NG_COMMAND_1! links abbiegen",
	
	["bl0q000x"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["c00wia00"] = "Am Ende der Straße !NG_COMMAND_1! leicht links abbiegen, auf die Stadtautobahn !STREET! fahren",
	
	["h000d000"] = "und dann !NG_COMMAND_2! die Ausfahrt nehmen",
	
	["c00t0d00"] = "Am Ende der Straße !NG_COMMAND_1! wenden !STREET!",
	
	["h00m000x"] = "und dann die Fähre Richtung !STREET_2! !SIGNPOST_2! nehmen",
	
	["h000mc00"] = "und dann !NG_COMMAND_2! die Fähre Richtung !STREET! nehmen",
	
	["h000a000"] = "und dann !NG_COMMAND_2! haben Sie Ihr Ziel erreicht",
	
	["h000dc00"] = "und dann !NG_COMMAND_2! die Ausfahrt Richtung !STREET! nehmen ",
	
	["h00p00c0"] = "und dann !NG_COMMAND_2! rechts halten Richtung !SIGNPOST!",
	
	["j00q00c0"] = "und dann sofort !NG_COMMAND_2! leicht rechts abbiegen Richtung !SIGNPOST!",
	
	["bl0wf000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen, auf die Autobahn fahren",
	
	["j00n0000"] = "und dann sofort !NG_COMMAND_2! geradeaus fahren",
	
	["j00t00c0"] = "und dann sofort !NG_COMMAND_2! wenden Richtung !SIGNPOST!",
	
	["bl0siac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["el000000"] = "Folgen Sie dem Verlauf der Straße für !DIST! !UNIT!",
	
	["dl000000"] = "Folgen Sie der Autobahn !STREET! für !DIST! !UNIT!",
	
	["j00p00c0"] = "und dann sofort !NG_COMMAND_2! rechts halten Richtung !SIGNPOST!",
	
	["j00x00c0"] = "und dann sofort !NG_COMMAND_2! links halten Richtung !SIGNPOST!",
	
	["h00f0ac0"] = "und dann auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["j00s00c0"] = "und dann sofort !NG_COMMAND_2! scharf rechts abbiegen Richtung !SIGNPOST!",
	
	["j00w00c0"] = "und dann sofort !NG_COMMAND_2! leicht links abbiegen Richtung !SIGNPOST!",
	
	["bl0n0edz"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren, Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen und !STREET_2! Richtung !SIGNPOST_2! folgen",
	
	["j00v00c0"] = "und dann sofort !NG_COMMAND_2! links abbiegen Richtung !SIGNPOST!",
	
	["j000i0c0"] = "und dann sofort !NG_COMMAND_2! auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["j000f0c0"] = "und dann sofort !NG_COMMAND_2! auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["h00z0000"] = "und dann !NG_COMMAND_2! die Zweite links abbiegen",
	
	["c00w0c00"] = "Am Ende der Straße !NG_COMMAND_1! leicht links abbiegen Richtung !STREET!",
	
	["j00z0000"] = "und dann sofort !NG_COMMAND_2! die Zweite links abbiegen",
	
	["a00xi000"] = "Jetzt !NG_COMMAND_1! links halten, auf die Stadtautobahn fahren",
	
	["j000m000"] = "und dann sofort !NG_COMMAND_2! die Fähre nehmen",
	
	["bl0r0d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen !STREET!",
	
	["h00w0c00"] = "und dann !NG_COMMAND_2! leicht links abbiegen Richtung !STREET!",
	
	["j000d000"] = "und dann sofort !NG_COMMAND_2! die Ausfahrt nehmen",
	
	["h00x00c0"] = "und dann !NG_COMMAND_2! links halten Richtung !SIGNPOST!",
	
	["j000mc00"] = "und dann sofort !NG_COMMAND_2! die Fähre Richtung !STREET! nehmen",
	
	["h000j000"] = "und dann !NG_COMMAND_2! die Ausfahrt nehmen",
	
	["j000dd00"] = "und dann sofort !NG_COMMAND_2! die Ausfahrt nehmen !STREET!",
	
	["j000j000"] = "und dann sofort !NG_COMMAND_2! die Ausfahrt nehmen",
	
	["bl0vfa00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen, auf die Autobahn !STREET! fahren",
	
	["bl0ni000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus auf die Stadtautobahn fahren",
	
	["h00n0000"] = "und dann !NG_COMMAND_2! geradeaus fahren",
	
	["bz0x0000"] = "Nach der Kreuzung !NG_COMMAND_1! links halten",
	
	["000l0000"] = "Fahren Sie zur nächsten Straße",
	
	["j00x0c00"] = "und dann sofort !NG_COMMAND_2! links halten Richtung !STREET!",
	
	["j000fac0"] = "und dann sofort auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["j000fa00"] = "und dann sofort auf die Autobahn !STREET! fahren",
	
	["j00z0c00"] = "und dann sofort !NG_COMMAND_2! die Zweite links abbiegen !STREET!",
	
	["a00n0000"] = "Jetzt !NG_COMMAND_1! geradeaus fahren",
	
	["j00w0c00"] = "und dann sofort !NG_COMMAND_2! leicht links abbiegen !STREET!",
	
	["j00z0a00"] = "und dann sofort die Zweite links nehmen !STREET!",
	
	["j00t0ac0"] = "und dann sofort wenden !STREET! Richtung !SIGNPOST!",
	
	["bl0u0c00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen Richtung !STREET!",
	
	["j00p0ac0"] = "und dann sofort rechts halten !STREET! Richtung !SIGNPOST!",
	
	["j00v0ac0"] = "und dann sofort links abbiegen !STREET! Richtung !SIGNPOST!",
	
	["j00u0d00"] = "und dann sofort !NG_COMMAND_2! scharf links abbiegen !STREET!",
	
	["j00n0c00"] = "und dann sofort !NG_COMMAND_2! geradeaus fahren !STREET!",
	
	["bl0tfac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["c00ti000"] = "Am Ende der Straße !NG_COMMAND_1! wenden, auf die Stadtautobahn fahren ",
	
	["bl0vfac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["c00u0d00"] = "Am Ende der Straße !NG_COMMAND_1! scharf links abbiegen !STREET!",
	
	["h00z0c00"] = "und dann !NG_COMMAND_2! die Zweite links abbiegen Richtung !STREET!",
	
	["j00q0000"] = "und dann sofort !NG_COMMAND_2! leicht rechts abbiegen",
	
	["h00n00c0"] = "und dann !NG_COMMAND_2! geradeaus fahren Richtung !SIGNPOST! ",
	
	["bz0r0000"] = "Nach der Kreuzung !NG_COMMAND_1! rechts abbiegen",
	
	["j00n00c0"] = "und dann sofort !NG_COMMAND_2! geradeaus fahren Richtung !SIGNPOST!",
	
	["j00t0d00"] = "und dann sofort !NG_COMMAND_2! wenden !STREET!",
	
	["j00t0000"] = "und dann sofort !NG_COMMAND_2! wenden",
	
	["bl0p0e00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, Ausfahrt !EXIT_NUMBER! nehmen",
	
	["j000ia00"] = "und dann sofort auf die Stadtautobahn !STREET! fahren",
	
	["h00x0ac0"] = "und dann !NG_COMMAND_2! links halten !STREET! Richtung !SIGNPOST!",
	
	["j00r0c00"] = "und dann sofort !NG_COMMAND_2! rechts abbiegen Richtung !STREET!",
	
	["j00r0d00"] = "und dann sofort !NG_COMMAND_2! rechts abbiegen !STREET! ",
	
	["bl0wia00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen, auf die Stadtautobahn !STREET! fahren",
	
	["bl0si000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen, auf die Stadtautobahn fahren ",
	
	["j00r00c0"] = "und dann sofort !NG_COMMAND_2! rechts abbiegen Richtung !SIGNPOST!",
	
	["j00s0c00"] = "und dann sofort !NG_COMMAND_2! scharf rechts abbiegen Richtung !STREET! ",
	
	["h00r00c0"] = "und dann !NG_COMMAND_2! rechts abbiegen Richtung !SIGNPOST!",
	
	["h000ia00"] = "und dann auf die Stadtautobahn !STREET! fahren",
	
	["a00xf000"] = "Jetzt !NG_COMMAND_1! links halten, auf die Autobahn fahren",
	
	["bl0ui000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen, auf die Stadtautobahn fahren",
	
	["j00s0000"] = "und dann sofort !NG_COMMAND_2! scharf rechts abbiegen",
	
	["j00u0c00"] = "und dann sofort !NG_COMMAND_2! scharf links abbiegen Richtung !STREET!",
	
	["j00u0000"] = "und dann sofort !NG_COMMAND_2! scharf links abbiegen",
	
	["bl0x0edx"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen Richtung !STREET_2! !SIGNPOST_2! ",
	
	["j00y0c00"] = "und dann sofort !NG_COMMAND_2! die Zweite rechts nehmen Richtung !STREET!",
	
	["j00w0d00"] = "und dann sofort !NG_COMMAND_2! leicht links abbiegen !STREET!",
	
	["h00y0c00"] = "und dann !NG_COMMAND_2! die Zweite rechts nehmen Richtung !STREET!",
	
	["j00v0c00"] = "und dann sofort !NG_COMMAND_2! links abbiegen Richtung !STREET!",
	
	["j00x0a00"] = "und dann sofort links halten !STREET!",
	
	["j00o0c00"] = "und dann sofort !NG_COMMAND_2! die mittlere Spur nehmen Richtung !STREET! ",
	
	["bz0o0000"] = "Nach der Kreuzung !NG_COMMAND_1! die mittlere Spur nehmen",
	
	["j00o0d00"] = "und dann sofort !NG_COMMAND_2! die mittlere Spur nehmen !STREET! ",
	
	["c00u0000"] = "Am Ende der Straße !NG_COMMAND_1! scharf links abbiegen",
	
	["h00u0000"] = "und dann !NG_COMMAND_2! scharf links abbiegen",
	
	["j00v0d00"] = "und dann sofort !NG_COMMAND_2! links abbiegen !STREET!",
	
	["j00t0a00"] = "und dann sofort wenden !STREET! ",
	
	["j00p0a00"] = "und dann sofort rechts halten !STREET!",
	
	["j00n0ac0"] = "und dann sofort geradeaus fahren !STREET! Richtung !SIGNPOST!",
	
	["bl0o00fg"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen und die Ausfahrt nehmen ",
	
	["j00r0a00"] = "und dann sofort rechts abbiegen !STREET!",
	
	["bl0rf0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["bl0xf0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["a00u0000"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen",
	
	["c00v0d00"] = "Am Ende der Straße !NG_COMMAND_1! links abbiegen !STREET!",
	
	["h00o0ac0"] = "und dann die mittlere Spur nehmen !STREET! Richtung !SIGNPOST!",
	
	["bl0d0c00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die Ausfahrt Richtung !STREET! nehmen",
	
	["bl0vi0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["j00w0a00"] = "und dann sofort leicht links abbiegen !STREET! ",
	
	["bl0n0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren",
	
	["h000bb00"] = "und dann !NG_COMMAND_2! haben Sie Ihr Zwischenziel !STREET! erreicht",
	
	["h00n0ac0"] = "und dann geradeaus fahren !STREET! Richtung !SIGNPOST!",
	
	["c00wfa00"] = "Am Ende der Straße !NG_COMMAND_1! leicht links abbiegen, auf die Autobahn !STREET! fahren",
	
	["h00x0000"] = "und dann !NG_COMMAND_2! links halten",
	
	["h00q0000"] = "und dann !NG_COMMAND_2! leicht rechts abbiegen",
	
	["h00r0000"] = "und dann !NG_COMMAND_2! rechts abbiegen",
	
	["h00a0000"] = "und dann !NG_COMMAND_2! haben Sie Ihr Ziel erreicht",
	
	["bl0o000x"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen Richtung !STREET_2! !SIGNPOST_2!",
	
	["h000jc00"] = "und dann !NG_COMMAND_2! die Ausfahrt Richtung !STREET! nehmen",
	
	["bl0qf000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen, auf die Autobahn fahren",
	
	["h00x0d00"] = "und dann !NG_COMMAND_2! links halten !STREET!",
	
	["bl0n000g"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren, die Ausfahrt nehmen",
	
	["h000gd00"] = "und dann !NG_COMMAND_2! die Ausfahrt nehmen !STREET! ",
	
	["h00w0d00"] = "und dann !NG_COMMAND_2! leicht links abbiegen !STREET!",
	
	["h00s0d00"] = "und dann !NG_COMMAND_2! scharf rechts abbiegen !STREET!",
	
	["a00o00fg"] = "Jetzt !NG_COMMAND_1! die mittlere Spur nehmen und die Ausfahrt nehmen",
	
	["h000jd00"] = "und dann !NG_COMMAND_2! die Ausfahrt nehmen !STREET! ",
	
	["c00rfac0"] = "Am Ende der Straße !NG_COMMAND_1! rechts abbiegen, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["h00n0d00"] = "und dann !NG_COMMAND_2! geradeaus fahren !STREET! ",
	
	["c00v000x"] = "Am Ende der Straße !NG_COMMAND_1! links abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["h00n0c00"] = "und dann !NG_COMMAND_2! geradeaus fahren Richtung !STREET!",
	
	["h00t0d00"] = "und dann !NG_COMMAND_2! wenden !STREET! ",
	
	["bl0vf000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen, auf die Autobahn fahren",
	
	["bl0pf0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["h00u0d00"] = "und dann !NG_COMMAND_2! scharf links abbiegen !STREET!",
	
	["h00u0c00"] = "und dann !NG_COMMAND_2! scharf links abbiegen Richtung !STREET! ",
	
	["h00v0d00"] = "und dann !NG_COMMAND_2! links abbiegen !STREET!",
	
	["bl0nfa00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus auf die Autobahn !STREET! fahren",
	
	["bl0ria00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen, auf die Stadtautobahn !STREET! fahren",
	
	["bl0of000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, auf die Autobahn fahren",
	
	["h00p0d00"] = "und dann !NG_COMMAND_2! rechts halten !STREET! ",
	
	["bl0n0d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren !STREET!",
	
	["h00p0c00"] = "und dann !NG_COMMAND_2! rechts halten Richtung !STREET!",
	
	["h00q0d00"] = "und dann !NG_COMMAND_2! leicht rechts abbiegen !STREET!",
	
	["bl0n0e0x"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren, Ausfahrt !EXIT_NUMBER! Richtung !STREET_2! !SIGNPOST_2! nehmen",
	
	["c00rf0c0"] = "Am Ende der Straße !NG_COMMAND_1! rechts abbiegen, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["h00s0c00"] = "und dann !NG_COMMAND_2! scharf rechts abbiegen Richtung !STREET!",
	
	["h00r0d00"] = "und dann !NG_COMMAND_2! rechts abbiegen !STREET!",
	
	["bl0vf0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["c00wf000"] = "Am Ende der Straße !NG_COMMAND_1! leicht links abbiegen, auf die Autobahn fahren",
	
	["h000iac0"] = "und dann auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["a00x0000"] = "Jetzt !NG_COMMAND_1! links halten",
	
	["c00wiac0"] = "Am Ende der Straße !NG_COMMAND_1! leicht links abbiegen, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["h000f0c0"] = "und dann !NG_COMMAND_2! auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["bl0xi0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["c00ri0c0"] = "Am Ende der Straße !NG_COMMAND_1! rechts abbiegen, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["h000fa00"] = "und dann auf die Autobahn !STREET! fahren",
	
	["h00n0a00"] = "und dann geradeaus fahren !STREET!",
	
	["j00x0000"] = "und dann sofort !NG_COMMAND_2! links halten",
	
	["c00w000x"] = "Am Ende der Straße !NG_COMMAND_1! leicht links abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["h00t0ac0"] = "und dann wenden !STREET! Richtung !SIGNPOST!",
	
	["h00o00c0"] = "und dann !NG_COMMAND_2! die mittlere Spur nehmen Richtung !SIGNPOST!",
	
	["j00s0a00"] = "und dann sofort scharf rechts abbiegen !STREET!",
	
	["c00sia00"] = "Am Ende der Straße !NG_COMMAND_1! scharf rechts abbiegen, auf die Stadtautobahn !STREET! fahren",
	
	["h00x0a00"] = "und dann links halten !STREET!",
	
	["a00sf000"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen, auf die Autobahn fahren",
	
	["h00t00c0"] = "und dann !NG_COMMAND_2! wenden Richtung !SIGNPOST!",
	
	["c00sf000"] = "Am Ende der Straße !NG_COMMAND_1! scharf rechts abbiegen, auf die Autobahn fahren",
	
	["bl0u0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen",
	
	["c00tia00"] = "Am Ende der Straße !NG_COMMAND_1! wenden, auf die Stadtautobahn !STREET! fahren",
	
	["h00w00c0"] = "und dann !NG_COMMAND_2! leicht links abbiegen Richtung !SIGNPOST!",
	
	["h00u0a00"] = "und dann scharf links abbiegen !STREET!",
	
	["bl0o000g"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, die Ausfahrt nehmen",
	
	["c00si0c0"] = "Am Ende der Straße !NG_COMMAND_1! scharf rechts abbiegen, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["c00riac0"] = "Am Ende der Straße !NG_COMMAND_1! rechts abbiegen, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["h00q00c0"] = "und dann !NG_COMMAND_2! leicht rechts abbiegen Richtung !SIGNPOST! ",
	
	["h00s0a00"] = "und dann scharf rechts abbiegen !STREET!",
	
	["bl0v0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen",
	
	["bl0o0e0z"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, Ausfahrt !EXIT_NUMBER! nehmen und !STREET_2! Richtung !SIGNPOST_2! folgen",
	
	["h00s0ac0"] = "und dann scharf rechts abbiegen !STREET! Richtung !SIGNPOST!",
	
	["h00r0ac0"] = "und dann rechts abbiegen !STREET! Richtung !SIGNPOST!",
	
	["h000czb0"] = "und dann im Kreisverkehr !EXIT_NO_ROUNDABOUT! Richtung !STREET! ",
	
	["h00j0000"] = "und dann !NG_COMMAND_2! die Ausfahrt nehmen",
	
	["h00c0z00"] = "und dann im Kreisverkehr !EXIT_NO_ROUNDABOUT!",
	
	["bl0x0c00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten Richtung !STREET!",
	
	["bl0x000x"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten Richtung !STREET_2! !SIGNPOST_2!",
	
	["bl0v000x"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["bz0t0000"] = "Nach der Kreuzung !NG_COMMAND_1! wenden",
	
	["bl0pfa00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, auf die Autobahn !STREET! fahren",
	
	["h00f0000"] = "und dann !NG_COMMAND_2! auf die Autobahn fahren",
	
	["bl0a0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! haben Sie Ihr Ziel erreicht",
	
	["h000dd00"] = "und dann !NG_COMMAND_2! die Ausfahrt nehmen !STREET!",
	
	["bl0wf0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["h000cz00"] = "und dann im Kreisverkehr !EXIT_NO_ROUNDABOUT!",
	
	["h000m000"] = "und dann !NG_COMMAND_2! die Fähre nehmen",
	
	["h00i0a00"] = "und dann auf die Stadtautobahn !STREET! fahren",
	
	["bl0xi000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, auf die Stadtautobahn fahren",
	
	["h00f00c0"] = "und dann auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["h00f0a00"] = "und dann auf die Autobahn !STREET! fahren",
	
	["j00r0000"] = "und dann sofort !NG_COMMAND_2! rechts abbiegen",
	
	["a00x00fg"] = "Jetzt !NG_COMMAND_1! links halten und die Ausfahrt nehmen",
	
	["bl0r000x"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["bl0o0c00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen Richtung !STREET!",
	
	["bl0x000g"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, die Ausfahrt nehmen",
	
	["bl0p000g"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, die Ausfahrt nehmen",
	
	["h00t0a00"] = "und dann wenden !STREET!",
	
	["a00r0000"] = "Jetzt !NG_COMMAND_1! rechts abbiegen",
	
	["h00v00c0"] = "und dann !NG_COMMAND_2! links abbiegen Richtung !SIGNPOST!",
	
	["c00r0000"] = "Am Ende der Straße !NG_COMMAND_1! rechts abbiegen",
	
	["bl0p000x"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten Richtung !STREET_2! !SIGNPOST_2!",
	
	["bl0s000x"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["bl0u000x"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["c00qfa00"] = "Am Ende der Straße !NG_COMMAND_1! leicht rechts abbiegen, auf die Autobahn !STREET! fahren",
	
	["bl0x0d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten !STREET!",
	
	["h00i0000"] = "und dann !NG_COMMAND_2! auf die Stadtautobahn fahren",
	
	["bl0n0e0y"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren, Ausfahrt !EXIT_NUMBER! nehmen und !STREET_2! folgen",
	
	["bl0rfa00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen, auf die Autobahn !STREET! fahren",
	
	["bl00iac0"] = "Nach !DIST! !UNIT! auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0xiac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["a00wi000"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen, auf die Stadtautobahn fahren",
	
	["bl0x0e0y"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, Ausfahrt !EXIT_NUMBER! nehmen und !STREET_2! folgen",
	
	["c00vf000"] = "Am Ende der Straße !NG_COMMAND_1! links abbiegen, auf die Autobahn fahren",
	
	["c00sf0c0"] = "Am Ende der Straße !NG_COMMAND_1! scharf rechts abbiegen, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["bl0w000x"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["h000czc0"] = "und dann im Kreisverkehr !EXIT_NO_ROUNDABOUT! Richtung !SIGNPOST!",
	
	["c00uiac0"] = "Am Ende der Straße !NG_COMMAND_1! scharf links abbiegen, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0x0e00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, Ausfahrt !EXIT_NUMBER! nehmen",
	
	["j00p0c00"] = "und dann sofort !NG_COMMAND_2! rechts halten Richtung !STREET!",
	
	["bl0p0e0z"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, Ausfahrt !EXIT_NUMBER! nehmen und !STREET_2! Richtung !SIGNPOST_2! folgen",
	
	["bl000e0z"] = "Nach !DIST! !UNIT! Ausfahrt !EXIT_NUMBER! nehmen und !STREET_2! Richtung !SIGNPOST_2! folgen",
	
	["bl000e00"] = "Nach !DIST! !UNIT! Ausfahrt !EXIT_NUMBER! nehmen",
	
	["bl00000g"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die Ausfahrt nehmen",
	
	["bl0sfa00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen, auf die Autobahn !STREET! fahren",
	
	["a00p000g"] = "Jetzt !NG_COMMAND_1! rechts halten, die Ausfahrt nehmen",
	
	["bl0u0d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen !STREET!",
	
	["a00oi000"] = "Jetzt !NG_COMMAND_1! die mittlere Spur nehmen, auf die Stadtautobahn fahren",
	
	["bl0p0d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten !STREET!",
	
	["bl0t0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden",
	
	["bl0ufa00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen, auf die Autobahn !STREET! fahren",
	
	["c00vfac0"] = "Am Ende der Straße !NG_COMMAND_1! links abbiegen, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0qia00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen, auf die Stadtautobahn !STREET! fahren",
	
	["a00pf000"] = "Jetzt !NG_COMMAND_1! rechts halten, auf die Autobahn fahren",
	
	["bl0x0e0x"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, Ausfahrt !EXIT_NUMBER! Richtung !STREET_2! !SIGNPOST_2! nehmen",
	
	["j000iac0"] = "und dann sofort auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["j00s0d00"] = "und dann sofort !NG_COMMAND_2! scharf rechts abbiegen !STREET!",
	
	["bl0p0e0y"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, Ausfahrt !EXIT_NUMBER! nehmen und !STREET_2! folgen",
	
	["a00wf000"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen, auf die Autobahn fahren",
	
	["a00vf000"] = "Jetzt !NG_COMMAND_1! links abbiegen, auf die Autobahn fahren",
	
	["a00o000g"] = "Jetzt !NG_COMMAND_1! die mittlere Spur nehmen, die Ausfahrt nehmen",
	
	["bl0ui0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["c00s0d00"] = "Am Ende der Straße !NG_COMMAND_1! scharf rechts abbiegen !STREET!",
	
	["a00of000"] = "Jetzt !NG_COMMAND_1! die mittlere Spur nehmen, auf die Autobahn fahren",
	
	["c00tiac0"] = "Am Ende der Straße !NG_COMMAND_1! wenden, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["c00t0c00"] = "Am Ende der Straße !NG_COMMAND_1! wenden Richtung !STREET! ",
	
	["c00tfac0"] = "Am Ende der Straße !NG_COMMAND_1! wenden, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["a00qf000"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen, auf die Autobahn fahren",
	
	["c00q0d00"] = "Am Ende der Straße !NG_COMMAND_1! leicht rechts abbiegen !STREET! ",
	
	["bl0niac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["c00v0c00"] = "Am Ende der Straße !NG_COMMAND_1! links abbiegen Richtung !STREET!",
	
	["bl0nia00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus auf die Stadtautobahn !STREET! fahren",
	
	["bl0tiac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["j00t0c00"] = "und dann sofort !NG_COMMAND_2! wenden Richtung !STREET!",
	
	["bl0pi0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["h00t0c00"] = "und dann !NG_COMMAND_2! wenden Richtung !STREET!",
	
	["bl0o0ed0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen",
	
	["h00v0c00"] = "und dann !NG_COMMAND_2! links abbiegen !STREET!",
	
	["bl0ri0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["bl0sfac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["c00u000x"] = "Am Ende der Straße !NG_COMMAND_1! scharf links abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["bl0wiac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0wi0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["bl0viac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0vi000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen, auf die Stadtautobahn fahren",
	
	["bl0oiac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0p0edx"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, Ausfahrt !EXIT_NUMBER! !SIGNPOST! Richtung !STREET_2! !SIGNPOST_2! nehmen",
	
	["bl0m0c00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die Fähre Richtung !STREET! nehmen",
	
	["bl0oi000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, auf die Stadtautobahn fahren",
	
	["bl0qfa00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen, auf die Autobahn !STREET! fahren",
	
	["bl0nfac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0pf000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, auf die Autobahn fahren",
	
	["bl0xfac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0rfac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["a00t0000"] = "Jetzt !NG_COMMAND_1! wenden",
	
	["c00qi000"] = "Am Ende der Straße !NG_COMMAND_1! leicht rechts abbiegen, auf die Stadtautobahn fahren",
	
	["bl0ufac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["a00qi000"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen, auf die Stadtautobahn fahren",
	
	["h00r0c00"] = "und dann !NG_COMMAND_2! rechts abbiegen Richtung !STREET!",
	
	["bl0p0c00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten Richtung !STREET!",
	
	["bl0pi000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten, auf die Stadtautobahn fahren",
	
	["c00t000x"] = "Am Ende der Straße !NG_COMMAND_1! wenden Richtung !STREET_2! !SIGNPOST_2!",
	
	["c00r000x"] = "Am Ende der Straße !NG_COMMAND_1! rechts abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["c00s000x"] = "Am Ende der Straße !NG_COMMAND_1! scharf rechts abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["bl0o0e0y"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen, Ausfahrt !EXIT_NUMBER! nehmen und !STREET_2! folgen",
	
	["bl0uf000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen, auf die Autobahn fahren",
	
	["bl0uia00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen, auf die Stadtautobahn !STREET! fahren",
	
	["c00ui000"] = "Am Ende der Straße !NG_COMMAND_1! scharf links abbiegen, auf die Stadtautobahn fahren",
	
	["a00ui000"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen, auf die Stadtautobahn fahren",
	
	["h00b0000"] = "und dann !NG_COMMAND_2! haben Sie Ihr Zwischenziel erreicht",
	
	["bl0qiac0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen, auf die Stadtautobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["c00ria00"] = "Am Ende der Straße !NG_COMMAND_1! rechts abbiegen, auf die Stadtautobahn !STREET! fahren",
	
	["bl0n0c00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren Richtung !STREET!",
	
	["c00r0c00"] = "Am Ende der Straße !NG_COMMAND_1! rechts abbiegen Richtung !STREET!",
	
	["c00vi0c0"] = "Am Ende der Straße !NG_COMMAND_1! links abbiegen, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["h00p0ac0"] = "und dann rechts halten !STREET! Richtung !SIGNPOST!",
	
	["bl0t0d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden !STREET! ",
	
	["j00q0c00"] = "und dann sofort !NG_COMMAND_2! leicht rechts abbiegen Richtung !STREET!",
	
	["c00ufac0"] = "Am Ende der Straße !NG_COMMAND_1! scharf links abbiegen, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["c00sfac0"] = "Am Ende der Straße !NG_COMMAND_1! scharf rechts abbiegen, auf die Autobahn !STREET! Richtung !SIGNPOST! fahren",
	
	["bl0n0e00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren, Ausfahrt !EXIT_NUMBER! nehmen",
	
	["c00a0b00"] = "Am Ende der Straße !NG_COMMAND_1! haben Sie Ihr Ziel !STREET! erreicht",
	
	["c00q000x"] = "Am Ende der Straße !NG_COMMAND_1! leicht rechts abbiegen Richtung !STREET_2! !SIGNPOST_2!",
	
	["c00q0c00"] = "Am Ende der Straße !NG_COMMAND_1! leicht rechts abbiegen Richtung !STREET!",
	
	["bl0x0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten",
	
	["c00uf0c0"] = "Am Ende der Straße !NG_COMMAND_1! scharf links abbiegen, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["j00n0d00"] = "und dann sofort !NG_COMMAND_2! geradeaus fahren !STREET!",
	
	["h000i0c0"] = "und dann !NG_COMMAND_2! auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["c00qf000"] = "Am Ende der Straße !NG_COMMAND_1! leicht rechts abbiegen, auf die Autobahn fahren",
	
	["h00q0c00"] = "und dann !NG_COMMAND_2! leicht rechts abbiegen Richtung !STREET!",
	
	["bl0n0e0z"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren, Ausfahrt !EXIT_NUMBER! nehmen und !STREET_2! Richtung !SIGNPOST_2! folgen",
	
	["bl0d0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die Ausfahrt nehmen",
	
	["s0000000"] = "Verkehrsereignis auf der Route. Sie werden umgeleitet",
	
	["bz0u0000"] = "Nach der Kreuzung !NG_COMMAND_1! scharf links abbiegen",
	
	["bl0sf000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen, auf die Autobahn fahren",
	
	["c00wf0c0"] = "Am Ende der Straße !NG_COMMAND_1! leicht links abbiegen, auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["bz0q0000"] = "Nach der Kreuzung !NG_COMMAND_1! leicht rechts abbiegen",
	
	["a00e0000"] = "Jetzt !NG_COMMAND_1! wenden",
	
	["bl0sia00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen, auf die Stadtautobahn !STREET! fahren",
	
	["bl0x0edy"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten, Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen und !STREET_2! folgen",
	
	["bl0tia00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden, auf die Stadtautobahn !STREET! fahren",
	
	["bl0n0edx"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus fahren, Ausfahrt !EXIT_NUMBER! !SIGNPOST! Richtung !STREET_2! !SIGNPOST_2! nehmen",
	
	["bl0p0000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten",
	
	["bl0o0d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen !STREET!",
	
	["bl0qi0c0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen, auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["j00u0a00"] = "und dann sofort scharf links abbiegen !STREET!",
	
	["blt00000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden",
	
	["blt00d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden !STREET!",
	
	["blt00f00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! wenden und !STREET! folgen",
	
	["a0t00d00"] = "Jetzt !NG_COMMAND_1! wenden !STREET!",
	
	["a0t00f00"] = "Jetzt !NG_COMMAND_1! wenden und !STREET! folgen",
	
	["ll0a0000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! haben Sie Ihr Ziel erreicht",
	
	["ll0e0000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! wenden",
	
	["ll0b0000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! haben Sie Ihr Zwischenziel erreicht",
	
	["llt00000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! wenden",
	
	["bl00fa00"] = "Nach !DIST! !UNIT! auf die Autobahn !STREET! fahren",
	
	["j00x0ac0"] = "und dann sofort !NG_COMMAND_2! links halten, !STREET! Richtung !SIGNPOST!",
	
	["bl000e0y"] = "Nach !DIST! !UNIT! Ausfahrt !EXIT_NUMBER! nehmen und !STREET_2! folgen",
	
	["bl000ed0"] = "Nach !DIST! !UNIT! Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen",
	
	["bl000edx"] = "Nach !DIST! !UNIT! Ausfahrt !EXIT_NUMBER! !SIGNPOST! Richtung !STREET_2! !SIGNPOST_2! nehmen",
	
	["bl000edz"] = "Nach !DIST! !UNIT! Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen und !STREET_2! Richtung !SIGNPOST_2! folgen",
	
	["bl000edy"] = "Nach !DIST! !UNIT! Ausfahrt !EXIT_NUMBER! !SIGNPOST! nehmen und !STREET_2! folgen",
	
	["bl0a000l"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! haben Sie Ihr Ziel erreicht. Das Ziel befindet sich auf der linken Seite",
	
	["bl0a000r"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! haben Sie Ihr Ziel erreicht. Das Ziel befindet sich auf der rechten Seite",
	
	["bl0a0b0l"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! haben Sie Ihr Ziel !STREET! erreicht. Das Ziel befindet sich auf der linken Seite",
	
	["bl0a0b0r"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! haben Sie Ihr Ziel !STREET! erreicht. Das Ziel befindet sich auf der rechten Seite",
	
	["bl00f0c0"] = "Nach !DIST! !UNIT! auf die Autobahn Richtung !SIGNPOST! fahren",
	
	["bl00i0c0"] = "Nach !DIST! !UNIT! auf die Stadtautobahn Richtung !SIGNPOST! fahren",
	
	["bl00ia00"] = "Nach !DIST! !UNIT! auf die Stadtautobahn !STREET! fahren",
	
	["000c0y00"] = "In den Kreisverkehr einbiegen",
	
	["bl0c0y00"] = "Nach !DIST! !UNIT! in den Kreisverkehr einbiegen",
	
	["c00c0y00"] = "Am Ende der Straße in den Kreisverkehr einbiegen",
	
	["h000cy00"] = "und dann in den Kreisverkehr einbiegen",
	
	["bl0x000y"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten und !STREET_2! folgen",
	
	["bl0x000z"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links halten und !STREET_2! Richtung !SIGNPOST_2! folgen",
	
	["bl0o000y"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen und !STREET_2! folgen",
	
	["bl0o000z"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Spur nehmen und !STREET_2! Richtung !SIGNPOST_2! folgen",
	
	["bl0p000y"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten und !STREET_2! folgen",
	
	["bl0p000z"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts halten und !STREET_2! Richtung !SIGNPOST_2! folgen",
}

commands_ped = { 
	
	["00000000"] = " ",
	
	["blo00000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Strasse nehmen",
	
	["blw00000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen",
	
	["blu00000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen",
	
	["bls00000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen",
	
	["blq00000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen",
	
	["blo00a00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Strasse nehmen !STREET!",
	
	["blv00f00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen und !STREET! folgen",
	
	["blw00d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen !STREET!",
	
	["blw00f00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen und !STREET! folgen",
	
	["blu00d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen !STREET!",
	
	["blu00f00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen und !STREET! folgen",
	
	["bls00d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen !STREET!",
	
	["bls00f00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen und !STREET! folgen",
	
	["blr00f00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen und !STREET! folgen",
	
	["blq00d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen !STREET!",
	
	["blq00f00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen und !STREET! folgen",
	
	["a0q00d00"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen !STREET!",
	
	["a0s00d00"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen !STREET!",
	
	["a0w00d00"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen !STREET!",
	
	["a0u00d00"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen !STREET!",
	
	["a0o00a00"] = "Jetzt !NG_COMMAND_1! die mittlere Strasse nehmen !STREET!",
	
	["a0o00000"] = "Jetzt !NG_COMMAND_1! die mittlere Strasse nehmen",
	
	["a0v00f00"] = "Jetzt !NG_COMMAND_1! links abbiegen und !STREET! folgen",
	
	["a0w00f00"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen und !STREET! folgen",
	
	["a0u00f00"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen und !STREET! folgen",
	
	["a0s00f00"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen und !STREET! folgen",
	
	["a0r00f00"] = "Jetzt !NG_COMMAND_1! rechts abbiegen und !STREET! folgen",
	
	["a0n00f00"] = "Jetzt geradeaus weitergehen und !STREET! folgen",
	
	["a0q00f00"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen und !STREET! folgen",
	
	["a0c00h00"] = "Jetzt den Kreisverkehr entlanggehen und !STREET! abbiegen",
	
	["blr00o00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen und durch das Gebäude gehen",
	
	["blb00000"] = "Nach !DIST! !UNIT! links den Kreisverkehr entlanggehen",
	
	["a0r00000"] = "Jetzt !NG_COMMAND_1! rechts abbiegen",
	
	["a0v00000"] = "Jetzt !NG_COMMAND_1! links abbiegen",
	
	["blp00a00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Strasse nehmen !STREET!",
	
	["a0p00a00"] = "Jetzt !NG_COMMAND_1! die rechte Strasse nehmen !STREET!",
	
	["blp00000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Strasse nehmen",
	
	["a0p00000"] = "Jetzt !NG_COMMAND_1! die rechte Strasse nehmen",
	
	["blx00a00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die linke Strasse nehmen !STREET!",
	
	["a0x00a00"] = "Jetzt !NG_COMMAND_1! die linke Strasse nehmen !STREET!",
	
	["blx00000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die linke Strasse nehmen",
	
	["a0x00000"] = "Jetzt !NG_COMMAND_1! die linke Strasse nehmen",
	
	["blv00000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen",
	
	["blr00000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen",
	
	["blv00d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen !STREET!",
	
	["blr00d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen !STREET!",
	
	["a0v00d00"] = "Jetzt links abbiegen !STREET!",
	
	["a0r00d00"] = "Jetzt rechts abbiegen !STREET!",
	
	["a0c00j00"] = "Jetzt den Kreisverkehr entlanggehen !PED_TURN_NO!",
	
	["ll0m0000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! die Fähre nehmen",
	
	["llb00000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! links den Kreisverkehr entlanggehen",
	
	["llc00000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! den Kreisverkehr entlanggehen",
	
	["lla00000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! rechts den Kreisverkehr entlanggehen",
	
	["llo00000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Straße nehmen",
	
	["llv00000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen",
	
	["llw00000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen",
	
	["llu00000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen",
	
	["lls00000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen",
	
	["llr00000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen",
	
	["llx00000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! die linke Straße nehmen",
	
	["llp00000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Straße nehmen",
	
	["lln00000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen",
	
	["llq00000"] = "und dann nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen",
	
	["bln000s0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen und den Aufzug nehmen",
	
	["a0q00g00"] = "Jetzt leicht rechts abbiegen auf den Fußgängerweg",
	
	["a0r00g00"] = "Jetzt !NG_COMMAND_1! rechts abbiegen auf den Fußgängerweg",
	
	["bln000r0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen und die Treppe nehmen",
	
	["a0a00h00"] = "Jetzt rechts den Kreisverkehr entlanggehen und !STREET! abbiegen",
	
	["blr00q00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen und den Park durchqueren",
	
	["bln00r00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen und die Treppe nehmen",
	
	["a0v000t0"] = "Jetzt !NG_COMMAND_1! links abbiegen und die Rolltreppe nehmen",
	
	["blo000s0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Strasse nehmen und den Aufzug nehmen",
	
	["bls00o00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen, durch das Gebäude gehen",
	
	["a0o00q00"] = "Jetzt !NG_COMMAND_1! die mittlere Strasse nehmen, den Park durchqueren",
	
	["blw00t00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen, die Rolltreppe nehmen",
	
	["a0w000t0"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen und die Rolltreppe nehmen",
	
	["blw000s0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen und den Aufzug nehmen",
	
	["a0b00j00"] = "Jetzt links den Kreisverkehr entlanggehen !PED_TURN_NO!",
	
	["a0b00h00"] = "Jetzt !NG_COMMAND_1! links den Kreisverkehr entlanggehen und !STREET! abbiegen",
	
	["a0q00p00"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen, den Platz überqueren",
	
	["f0000000"] = "Gehen Sie Richtung !ORIENTATION!",
	
	["a0q00q00"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen, den Park durchqueren",
	
	["blq000q0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen und den Park durchqueren",
	
	["blw000o0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen und durch das Gebäude gehen",
	
	["blw00s00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen, den Aufzug nehmen",
	
	["a0u000t0"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen und die Rolltreppe nehmen",
	
	["a0n000p0"] = "Jetzt !NG_COMMAND_1! geradeaus weitergehen und den Platz überqueren",
	
	["a0p000o0"] = "Jetzt !NG_COMMAND_1! die rechte Strasse nehmen und durch das Gebäude gehen",
	
	["blw000t0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen und die Rolltreppe nehmen",
	
	["a0v00r00"] = "Jetzt !NG_COMMAND_1! links abbiegen, die Treppe nehmen",
	
	["blo000q0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Strasse nehmen und den Park durchqueren",
	
	["a0o000p0"] = "Jetzt !NG_COMMAND_1! die mittlere Strasse nehmen und den Platz überqueren",
	
	["a0q000o0"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen und durch das Gebäude gehen",
	
	["bln00q00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen, den Park durchqueren",
	
	["a0a00j00"] = "Jetzt !NG_COMMAND_1! rechts den Kreisverkehr entlanggehen !PED_TURN_NO!",
	
	["blq000t0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen und die Rolltreppe nehmen",
	
	["a0s000t0"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen und die Rolltreppe nehmen",
	
	["a0o000o0"] = "Jetzt !NG_COMMAND_1! die mittlere Strasse nehmen und durch das Gebäude gehen",
	
	["bln000o0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen und durch das Gebäude gehen",
	
	["a0n00p00"] = "Jetzt !NG_COMMAND_1! geradeaus weitergehen, den Platz überqueren",
	
	["bln000q0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen und den Park durchqueren",
	
	["blx000s0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die linke Strasse nehmen und den Aufzug nehmen",
	
	["blx00t00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die linke Strasse nehmen, die Rolltreppe nehmen",
	
	["a0q00t00"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen, die Rolltreppe nehmen",
	
	["blr000s0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen und den Aufzug nehmen",
	
	["blw000q0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen und den Park durchqueren",
	
	["a0q00r00"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen, die Treppe nehmen",
	
	["blq000r0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen und die Treppe nehmen",
	
	["blq00r00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen, die Treppe nehmen",
	
	["a0y00g00"] = "Jetzt  !NG_COMMAND_1! abbiegen auf den Fußgängerweg",
	
	["blu000q0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen und den Park durchqueren",
	
	["a0r000o0"] = "Jetzt !NG_COMMAND_1! rechts abbiegen und durch das Gebäude gehen",
	
	["blu000o0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen und durch das Gebäude gehen",
	
	["a0s00o00"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen, durch das Gebäude gehen",
	
	["blu000t0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen und die Rolltreppe nehmen",
	
	["blo000r0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Strasse nehmen und die Treppe nehmen",
	
	["bls000r0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen und die Treppe nehmen",
	
	["blv000p0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen und den Platz überqueren",
	
	["blr000q0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen und den Park durchqueren",
	
	["a0v000r0"] = "Jetzt !NG_COMMAND_1! links abbiegen und die Treppe nehmen",
	
	["a0x00t00"] = "Jetzt !NG_COMMAND_1! die linke Strasse nehmen, die Rolltreppe nehmen",
	
	["a0w00p00"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen, den Platz überqueren",
	
	["a0s000q0"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen, den Park durchqueren",
	
	["blr000t0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen und die Rolltreppe nehmen",
	
	["blr000o0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen und durch das Gebäude gehen",
	
	["blp000q0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Strasse nehmen und den Park durchqueren",
	
	["bla00000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts den Kreisverkehr entlanggehen",
	
	["a0p000p0"] = "Jetzt !NG_COMMAND_1! die rechte Strasse nehmen und den Platz überqueren",
	
	["bls000o0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen und durch das Gebäude gehen",
	
	["a0n000r0"] = "Jetzt !NG_COMMAND_1! geradeaus weitergehen und die Treppe nehmen",
	
	["a0s000o0"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen und durch das Gebäude gehen",
	
	["a0r000s0"] = "Jetzt !NG_COMMAND_1! rechts abbiegen und den Aufzug nehmen",
	
	["a0q000s0"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen und den Aufzug nehmen",
	
	["blo00s00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Strasse nehmen, den Aufzug nehmen",
	
	["a0n00r00"] = "Jetzt !NG_COMMAND_1! geradeaus weitergehen, die Treppe nehmen",
	
	["blr000r0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen und die Treppe nehmen",
	
	["a0x000t0"] = "Jetzt !NG_COMMAND_1! die linke Strasse nehmen und die Rolltreppe nehmen",
	
	["blv000q0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen und den Park durchqueren",
	
	["bln00t00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen, die Rolltreppe nehmen",
	
	["a0u000r0"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen und die Treppe nehmen",
	
	["blp00q00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Strasse nehmen, den Park durchqueren",
	
	["blp00p00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Strasse nehmen, den Platz überqueren",
	
	["bln00d00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen !STREET!",
	
	["a0p00r00"] = "Jetzt !NG_COMMAND_1! die rechte Strasse nehmen, die Treppe nehmen",
	
	["blq00p00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen, den Platz überqueren",
	
	["blq00q00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen, den Park durchqueren",
	
	["a0s00r00"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen, die Treppe nehmen",
	
	["a0y00000"] = "Jetzt !NG_COMMAND_1! abbiegen",
	
	["a0v00p00"] = "Jetzt !NG_COMMAND_1! links abbiegen, den Platz überqueren",
	
	["blv000o0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen und durch das Gebäude gehen",
	
	["a0s000s0"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen und den Aufzug nehmen",
	
	["a0x00r00"] = "Jetzt !NG_COMMAND_1! die linke Strasse nehmen, die Treppe nehmen",
	
	["a0q000p0"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen und den Platz überqueren",
	
	["bln00000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen",
	
	["blx000p0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die linke Strasse nehmen und den Platz überqueren",
	
	["a0v000q0"] = "Jetzt !NG_COMMAND_1! links abbiegen und den Park durchqueren",
	
	["blu00r00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen, die Treppe nehmen",
	
	["a0o000t0"] = "Jetzt !NG_COMMAND_1! die mittlere Strasse nehmen und die Rolltreppe nehmen",
	
	["blu00g00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen auf den Fußgängerweg",
	
	["bln00p00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen, den Platz überqueren",
	
	["a0r000t0"] = "Jetzt !NG_COMMAND_1! rechts abbiegen und die Rolltreppe nehmen",
	
	["a0x00p00"] = "Jetzt !NG_COMMAND_1! die linke Strasse nehmen, den Platz überqueren",
	
	["blx000q0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die linke Strasse nehmen und den Park durchqueren",
	
	["blr00r00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen, die Treppe nehmen",
	
	["a0s00q00"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen, den Park durchqueren",
	
	["a0o000q0"] = "Jetzt !NG_COMMAND_1! die mittlere Strasse nehmen und den Park durchqueren",
	
	["bls000t0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen und die Rolltreppe nehmen",
	
	["bls00p00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen, den Platz überqueren",
	
	["bls000s0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen und den Aufzug nehmen",
	
	["bln000t0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen und die Rolltreppe nehmen",
	
	["blx000o0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die linke Strasse nehmen und durch das Gebäude gehen",
	
	["a0p00p00"] = "Jetzt !NG_COMMAND_1! die rechte Strasse nehmen, den Platz überqueren",
	
	["a0p00s00"] = "Jetzt !NG_COMMAND_1! die rechte Strasse nehmen, den Aufzug nehmen",
	
	["blv00p00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen, den Platz überqueren",
	
	["blp00r00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Strasse nehmen, die Treppe nehmen",
	
	["bln00f00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen und !STREET! folgen",
	
	["a0w00r00"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen, die Treppe nehmen",
	
	["a0v00o00"] = "Jetzt !NG_COMMAND_1! links abbiegen, durch das Gebäude gehen",
	
	["blo00t00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Strasse nehmen, die Rolltreppe nehmen",
	
	["blo00p00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Strasse nehmen, den Platz überqueren",
	
	["a0n000s0"] = "Jetzt !NG_COMMAND_1! geradeaus weitergehen und den Aufzug nehmen",
	
	["bls00r00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen, die Treppe nehmen",
	
	["blu000p0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen und den Platz überqueren",
	
	["blx00q00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die linke Strasse nehmen, den Park durchqueren",
	
	["bls00q00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen, den Park durchqueren",
	
	["blu00q00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen, den Park durchqueren",
	
	["a0u000q0"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen und den Park durchqueren",
	
	["blp000t0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Strasse nehmen und die Rolltreppe nehmen",
	
	["a0w00q00"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen, den Park durchqueren",
	
	["a0o00t00"] = "Jetzt !NG_COMMAND_1! die mittlere Strasse nehmen, die Rolltreppe nehmen",
	
	["a0q000q0"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen und den Park durchqueren",
	
	["blo00q00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Strasse nehmen, den Park durchqueren",
	
	["a0w000p0"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen und den Platz überqueren",
	
	["blx00r00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die linke Strasse nehmen, die Treppe nehmen",
	
	["a0o00p00"] = "Jetzt !NG_COMMAND_1! die mittlere Strasse nehmen, den Platz überqueren",
	
	["blv00r00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen, die Treppe nehmen",
	
	["blo00r00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Strasse nehmen, die Treppe nehmen",
	
	["f00000c0"] = "Gehen Sie Richtung !ORIENTATION! , Richtung !SIGNPOST!",
	
	["a0w00o00"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen, durch das Gebäude gehen",
	
	["blv00t00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen, die Rolltreppe nehmen",
	
	["a0v000p0"] = "Jetzt !NG_COMMAND_1! links abbiegen und den Platz überqueren",
	
	["blq00s00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen, den Aufzug nehmen",
	
	["bls000p0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen und den Platz überqueren",
	
	["blq00g00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen auf den Fußgängerweg",
	
	["a0u000o0"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen und durch das Gebäude gehen",
	
	["blx00s00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die linke Strasse nehmen, den Aufzug nehmen",
	
	["blr00s00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen, den Aufzug nehmen",
	
	["bls00s00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen, den Aufzug nehmen",
	
	["blu00s00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen, den Aufzug nehmen",
	
	["blp00t00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Strasse nehmen, die Rolltreppe nehmen",
	
	["a0v00s00"] = "Jetzt !NG_COMMAND_1! links abbiegen, den Aufzug nehmen",
	
	["a0x000o0"] = "Jetzt !NG_COMMAND_1! die linke Strasse nehmen und durch das Gebäude gehen",
	
	["a0p000q0"] = "Jetzt !NG_COMMAND_1! die rechte Strasse nehmen und den Park durchqueren",
	
	["blu00t00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen, die Rolltreppe nehmen",
	
	["blq00o00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen, durch das Gebäude gehen",
	
	["blp00o00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Strasse nehmen, durch das Gebäude gehen",
	
	["blo000t0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Strasse nehmen und die Rolltreppe nehmen",
	
	["a0u00s00"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen, den Aufzug nehmen",
	
	["blx00o00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die linke Strasse nehmen, durch das Gebäude gehen",
	
	["blu00o00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen, durch das Gebäude gehen",
	
	["blw00o00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen, durch das Gebäude gehen",
	
	["blv00o00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen, durch das Gebäude gehen",
	
	["blo00o00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Strasse nehmen, durch das Gebäude gehen",
	
	["blu00p00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen, den Platz überqueren",
	
	["a0s000r0"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen und die Treppe nehmen",
	
	["a0r00p00"] = "Jetzt !NG_COMMAND_1! rechts abbiegen, den Platz überqueren",
	
	["a0o000r0"] = "Jetzt !NG_COMMAND_1! die mittlere Strasse nehmen und die Treppe nehmen",
	
	["a0s00p00"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen, den Platz überqueren",
	
	["blv000r0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen und die Treppe nehmen",
	
	["a0r000q0"] = "Jetzt !NG_COMMAND_1! rechts abbiegen und den Park durchqueren",
	
	["a0r000r0"] = "Jetzt !NG_COMMAND_1! rechts abbiegen und die Treppe nehmen",
	
	["bls000q0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen und den Park durchqueren",
	
	["a0u00p00"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen, den Platz überqueren",
	
	["blw00r00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen, die Treppe nehmen",
	
	["a0n00q00"] = "Jetzt !NG_COMMAND_1! geradeaus weitergehen, den Park durchqueren",
	
	["blw000r0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen und die Treppe nehmen",
	
	["a0p00q00"] = "Jetzt !NG_COMMAND_1! die rechte Strasse nehmen, den Park durchqueren",
	
	["blp000p0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Strasse nehmen und den Platz überqueren",
	
	["a0q000t0"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen und die Rolltreppe nehmen",
	
	["a0v000s0"] = "Jetzt !NG_COMMAND_1! links abbiegen und den Aufzug nehmen",
	
	["a0x00q00"] = "Jetzt !NG_COMMAND_1! die linke Strasse nehmen, den Park durchqueren",
	
	["a0r00q00"] = "Jetzt !NG_COMMAND_1! rechts abbiegen, den Park durchqueren",
	
	["blq000p0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen und den Platz überqueren",
	
	["a0u00q00"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen, den Park durchqueren",
	
	["blw00q00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen, den Park durchqueren",
	
	["a0v00q00"] = "Jetzt !NG_COMMAND_1! links abbiegen, den Park durchqueren",
	
	["blp000o0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Strasse nehmen und durch das Gebäude gehen",
	
	["a0r00r00"] = "Jetzt !NG_COMMAND_1! rechts abbiegen, die Treppe nehmen",
	
	["a0v00g00"] = "Jetzt !NG_COMMAND_1! links abbiegen auf den Fußgängerweg",
	
	["a0u00r00"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen, die Treppe nehmen",
	
	["a0n000t0"] = "Jetzt !NG_COMMAND_1! geradeaus weitergehen und die Rolltreppe nehmen",
	
	["a0x000r0"] = "Jetzt !NG_COMMAND_1! die linke Strasse nehmen und die Treppe nehmen",
	
	["blr000p0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen und den Platz überqueren",
	
	["a0o00r00"] = "Jetzt !NG_COMMAND_1! die mittlere Strasse nehmen, die Treppe nehmen",
	
	["blq000o0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen und durch das Gebäude gehen",
	
	["a0q00s00"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen, den Aufzug nehmen",
	
	["a0r00s00"] = "Jetzt !NG_COMMAND_1! rechts abbiegen, den Aufzug nehmen",
	
	["a0q000r0"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen und die Treppe nehmen",
	
	["blo000p0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Strasse nehmen und den Platz überqueren",
	
	["blw00p00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen, den Platz überqueren",
	
	["a0x00s00"] = "Jetzt !NG_COMMAND_1! die linke Strasse nehmen, den Aufzug nehmen",
	
	["a0n00d00"] = "Jetzt !NG_COMMAND_1! geradeaus weitergehen !STREET!",
	
	["a0s00s00"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen, den Aufzug nehmen",
	
	["a0w00s00"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen, den Aufzug nehmen",
	
	["blq00t00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen, die Rolltreppe nehmen",
	
	["a0o00s00"] = "Jetzt !NG_COMMAND_1! die mittlere Strasse nehmen, den Aufzug nehmen",
	
	["a0n00t00"] = "Jetzt !NG_COMMAND_1! geradeaus weitergehen, die Rolltreppe nehmen",
	
	["bls00g00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf rechts abbiegen auf den Fußgängerweg ",
	
	["blo000o0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die mittlere Strasse nehmen und durch das Gebäude gehen",
	
	["a0r00o00"] = "Jetzt !NG_COMMAND_1! rechts abbiegen, durch das Gebäude gehen",
	
	["blv000t0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen und die Rolltreppe nehmen",
	
	["a0w000q0"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen und den Park durchqueren",
	
	["a0r00t00"] = "Jetzt !NG_COMMAND_1! rechts abbiegen, die Rolltreppe nehmen",
	
	["a0s00t00"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen, die Rolltreppe nehmen",
	
	["a0u00t00"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen, die Rolltreppe nehmen",
	
	["a0v00t00"] = "Jetzt !NG_COMMAND_1! links abbiegen, die Rolltreppe nehmen",
	
	["a0q00o00"] = "Jetzt !NG_COMMAND_1! leicht rechts abbiegen, durch das Gebäude gehen",
	
	["a0n00o00"] = "Jetzt !NG_COMMAND_1! geradeaus weitergehen, durch das Gebäude gehen",
	
	["a0p00o00"] = "Jetzt !NG_COMMAND_1! die rechte Strasse nehmen, durch das Gebäude gehen",
	
	["blq000s0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht rechts abbiegen und den Aufzug nehmen",
	
	["a0p000s0"] = "Jetzt !NG_COMMAND_1! die rechte Strasse nehmen und den Aufzug nehmen",
	
	["blu000s0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen und den Aufzug nehmen",
	
	["a0x00o00"] = "Jetzt !NG_COMMAND_1! die linke Strasse nehmen, durch das Gebäude gehen",
	
	["a0p00t00"] = "Jetzt !NG_COMMAND_1! die rechte Strasse nehmen, die Rolltreppe nehmen",
	
	["f0000bc0"] = "Gehen Sie auf !STREET! Richtung !ORIENTATION! , Richtung !SIGNPOST!",
	
	["a0o00o00"] = "Jetzt !NG_COMMAND_1! die mittlere Strasse nehmen, durch das Gebäude gehen",
	
	["a0x000p0"] = "Jetzt !NG_COMMAND_1! die linke Strasse nehmen und den Platz überqueren",
	
	["a0r000p0"] = "Jetzt !NG_COMMAND_1! rechts abbiegen und den Platz überqueren",
	
	["a0s000p0"] = "Jetzt !NG_COMMAND_1! scharf rechts abbiegen und den Platz überqueren",
	
	["a0u000p0"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen und den Platz überqueren",
	
	["blv00q00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen, den Park durchqueren",
	
	["a0n000q0"] = "Jetzt !NG_COMMAND_1! geradeaus weitergehen und den Park durchqueren",
	
	["a0u00g00"] = "Jetzt scharf links abbiegen auf den Fußgängerweg",
	
	["a0w000o0"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen und durch das Gebäude gehen",
	
	["blr00t00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen, die Rolltreppe nehmen",
	
	["a0u000s0"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen und den Aufzug nehmen",
	
	["a0p000r0"] = "Jetzt !NG_COMMAND_1! die rechte Strasse nehmen und die Treppe nehmen",
	
	["a0v000o0"] = "Jetzt !NG_COMMAND_1! links abbiegen und durch das Gebäude gehen",
	
	["blr00p00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen, den Platz überqueren",
	
	["a0n00s00"] = "Jetzt !NG_COMMAND_1! geradeaus weitergehen, den Aufzug nehmen",
	
	["a0w000r0"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen und die Treppe nehmen",
	
	["a0x000s0"] = "Jetzt !NG_COMMAND_1! die linke Strasse nehmen und den Aufzug nehmen",
	
	["a0w00g00"] = "Jetzt leicht links abbiegen auf den Fußgängerweg",
	
	["a0o000s0"] = "Jetzt !NG_COMMAND_1! die mittlere Strasse nehmen und den Aufzug nehmen",
	
	["blv000s0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen und den Aufzug nehmen",
	
	["blr00g00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts abbiegen auf den Fußgängerweg",
	
	["blp000r0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Strasse nehmen und die Treppe nehmen",
	
	["f0000b00"] = "Gehen Sie auf !STREET! Richtung !ORIENTATION!",
	
	["blx000r0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die linke Strasse nehmen und die Treppe nehmen",
	
	["blw00g00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen auf den Fußgängerweg",
	
	["a0n000o0"] = "Jetzt !NG_COMMAND_1! geradeaus weitergehen und durch das Gebäude gehen",
	
	["bln000p0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen und den Platz überqueren",
	
	["blc00000"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! den Kreisverkehr entlanggehen",
	
	["bln00s00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen, den Aufzug nehmen",
	
	["blw000p0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! leicht links abbiegen und den Platz überqueren",
	
	["blx00p00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die linke Strasse nehmen, den Platz überqueren",
	
	["a0p000t0"] = "Jetzt !NG_COMMAND_1! die rechte Strasse nehmen und die Rolltreppe nehmen",
	
	["blv00s00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen, den Aufzug nehmen",
	
	["blu000r0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! scharf links abbiegen und die Treppe nehmen",
	
	["blp000s0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Strasse nehmen und den Aufzug nehmen",
	
	["bln00o00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! geradeaus weitergehen, durch das Gebäude gehen",
	
	["a0x000q0"] = "Jetzt !NG_COMMAND_1! die linke Strasse nehmen und den Park durchqueren",
	
	["blp00s00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die rechte Strasse nehmen, den Aufzug nehmen",
	
	["blv00g00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links abbiegen auf den Fußgängerweg",
	
	["a0w000s0"] = "Jetzt !NG_COMMAND_1! leicht links abbiegen und den Aufzug nehmen",
	
	["a0n00000"] = "Jetzt !NG_COMMAND_1! geradeaus weitergehen",
	
	["a0s00g00"] = "Jetzt scharf rechts abbiegen auf den Fußgängerweg",
	
	["a0y00d00"] = "Jetzt !NG_COMMAND_1! abbiegen !STREET!",
	
	["a0u00o00"] = "Jetzt !NG_COMMAND_1! scharf links abbiegen, durch das Gebäude gehen",
	
	["bl000o00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! durch das Gebäude gehen",
	
	["bl000p00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! den Platz überqueren",
	
	["bl000q00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! den Park durchqueren",
	
	["bl000r00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die Treppen nehmen",
	
	["bl000s00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! den Aufzug nehmen",
	
	["bl000t00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die Rolltreppe nehmen",
	
	["bla00h00"] = "Nach !DIST! !UNIT! rechts um den Kreisverkehr gehen und !STREET! abbiegen",
	
	["bla00j00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! rechts um den Kreisverkehr gehen !PED_TURN_NO!",
	
	["blb00h00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links um den Kreisverkehr gehen und !STREET! abbiegen",
	
	["blb00j00"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! links um den Kreisverkehr gehen !PED_TURN_NO!",
	
	["blc00h00"] = "Nach !DIST! !UNIT! um den Kreisverkehr gehen und !STREET! abbiegen",
	
	["blc00j00"] = "Nach !DIST! !UNIT! um den Kreisverkehr gehen !PED_TURN_NO!",
	
	["blx000t0"] = "Nach !DIST! !UNIT! !NG_COMMAND_1! die linke Straße nehmen und die Rolltreppe nehmen",
}

