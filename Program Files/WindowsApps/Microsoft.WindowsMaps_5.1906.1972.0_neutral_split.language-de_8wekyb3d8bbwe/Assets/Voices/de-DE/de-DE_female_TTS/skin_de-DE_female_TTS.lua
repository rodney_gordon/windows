-- ----------------------------------------------------------------------------
-- Copyright (C) 2012 Nokia Gate5 GmbH Berlin
--
-- These coded instructions, statements, and computer programs contain
-- unpublished proprietary information of Nokia Gate5 GmbH Berlin, and
-- are copy protected by law. They may not be disclosed to third parties
-- or copied or duplicated in any form, in whole or in part, without the
-- specific, prior written permission of Nokia Gate5 GmbH Berlin.
-- ----------------------------------------------------------------------------
--      Author: Fabian TP Riek
-- ----------------------------------------------------------------------------
--      Header-Data for WindowsPhone 8.0 with MSFT TTS
--      v4 packages
--      Language: de-DE
--      Technology: TTS
--      Gender: female


-- Platform Independent
description = "TTS"
id = "3002"
audio_files_version = "0.4.0.1305061308"
client_range = "[ client >= 1.7.0.0 ]"
network_provider_support_list = {
	"all",
}
application_support_list = {
	'IN "WP8_TTS_Core":1.0',
}
feature_list = { "metric", "imperial_uk", "imperial_us" }
travel_mode = "0"
output_type = "tts"
language_id = "3"
marc_code = "ger"
language = "German"
language_loc = "Deutsch"
gender = "f"
config_file = "de-DE_female_TTS/common.lua"

main_attribute_array = {
	["language_code"] = "de-DE",
	["ngLangCode"] = "ger",
	["ms_lcid"] = "407",
	["LocalizedType"] = "angesagte Straßennamen",
	["LocalizedGender"] = "weiblich",
	["VoiceFeatures"] = "drive;walk;metric;imperialuk;imperialus;naturalguidance;trafficlights;tts",
}

-- Platform Specific
rulesets_file = ""
userdictionary_file = ""
audio_files_path = "de-DE_female_TTS\\de-DE_female_TTS"
apdb = ""
speaker = ""
platform_support_list = {
--	"all",
	'= "Windows Phone OS":>= 8.0',
}
tts_engine_type = { "msft:8.0" }


-- Load main LUA files
require("ruleset_de-DE_female_TTS")
require("prompts_de-DE_female_TTS")
require("platform_format")
