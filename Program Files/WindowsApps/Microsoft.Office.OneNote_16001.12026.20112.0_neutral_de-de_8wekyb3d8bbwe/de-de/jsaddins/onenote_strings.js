var JSADDINS = {};

JSADDINS.Strings = function JSADDINS_Strings() {
}


 
JSADDINS.Strings.IDS_NOTETAGS_NAME_THIS_TAG = 'Dieses Tag benennen';
JSADDINS.Strings.IDS_NOTETAGS_CREATE_TAG_TITLE = 'Tag erstellen';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_CURRENT_NOTEBOOK_VIEW_WITH_COUNT = 'Notizbuch (|0)';
JSADDINS.Strings.Generic_Retry = 'Wiederholen';
JSADDINS.Strings.IDS_NOTETAGS_CREATING = 'Wird erstellt';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_FISHBOWL_NO_VIOLATIONS_TITLE = 'Keine Probleme gefunden.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_FISHBOWL_CHECK_ALL_NOTEBOOK = 'Gesamtes Notizbuch pr\u00fcfen';
JSADDINS.Strings.Invalid_Agave = 'Ung\u00fcltiges Add-In';
JSADDINS.Strings.LT_NoInternet = 'Lerntools sind nicht verf\u00fcgbar, wenn Sie offline sind. Stellen Sie eine Internetverbindung her, und versuchen Sie es noch mal.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_CURRENT_PAGE_VIEW = 'Seite';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_LOCKED_SECTION_TITLE = 'Diese Seite befindet sich in einem kennwortgesch\u00fctzten Abschnitt.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_UNNAMED_SECTION_HOWFIX_PATTERN = 'Um einen benutzerdefinierten Namen f\u00fcr einen Abschnitt hinzuzuf\u00fcgen, |0, klicken Sie mit der rechten Maustaste auf den Abschnittsnamen, und w\u00e4hlen Sie dann \"Umbenennen\" aus.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_FISHBOWL_NO_VIOLATIONS_SUBTITLE_PAGE = 'Ihre Seite kann von Personen mit Behinderungen problemlos gelesen werden.';
JSADDINS.Strings.IDS_NOTETAGS_FAILED = 'Fehler beim Erstellen des benutzerdefinierten Tags.';
JSADDINS.Strings.Generic_NoInternet = 'Diese Funktion ist nicht verf\u00fcgbar, wenn Sie offline sind. Stellen Sie eine Internetverbindung her, und versuchen Sie es noch mal.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_INVALID_LOCATION_TITLE = 'Nicht verf\u00fcgbar';
JSADDINS.Strings.IDS_NOTETAGS_MESSAGE_ERROR = 'Wir haben ein Problem beim Speichern Ihres Tags. Wir versuchen es weiter, aber Ihre \u00c4nderungen gehen eventuell verloren, wenn Sie OneNote verlassen.';
JSADDINS.Strings.IDS_NOTETAGS_CHOOSE_ICON = 'Symbol ausw\u00e4hlen';
JSADDINS.Strings.Learn_More = 'Weitere Informationen';
JSADDINS.Strings.IDS_NOTETAGS_TAG_NAME = 'Kategoriename';
JSADDINS.Strings.IDS_NOTETAGS_SELECTED = 'Ausgew\u00e4hlt';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_CURRENT_PAGE_VIEW_WITH_COUNT = 'Seite (|0)';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_INVALID_LOCATION_DESCRIPTION = 'Dieser Ort enth\u00e4lt keine g\u00fcltigen Seiten. Warten Sie die vollst\u00e4ndige Synchronisierung Ihrer Notizen ab, oder versuchen Sie es mit einem anderen Ort.';
JSADDINS.Strings.IDS_NOTETAGS_MESSAGE_INVALID_TAGNAME = 'Verwenden Sie nur alphanumerische Zeichen f\u00fcr Tagnamen.';
JSADDINS.Strings.MD_NoInternet = 'Besprechungsdetails sind nicht verf\u00fcgbar, wenn Sie offline sind. Stellen Sie eine Internetverbindung her, und versuchen Sie es noch mal.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_FISHBOWL_NO_VIOLATIONS_SUBTITLE_NOTEBOOK = 'Ihr Notizbuch kann von Personen mit Behinderungen problemlos gelesen werden.';
JSADDINS.Strings.IDS_NOTETAGS_MESSAGE_INVALID_IDENTITY = 'OneNote ben\u00f6tigte Ihre Kontoanmeldeinformationen, bevor Sie neue Tags erstellen k\u00f6nnen. Melden Sie sich an, und versuchen Sie es dann noch mal.';
JSADDINS.Strings.IDS_NOTETAGS_CREATED = 'Benutzerdefiniertes Tag erstellt.';
JSADDINS.Strings.IDS_COMMA_LIST_PATTERN = '|0, |1';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_LOCKED_SECTION_DESCRIPTION = 'Entsperren Sie den Abschnitt, um seine Seiten auf Barrierefreiheitsprobleme zu \u00fcberpr\u00fcfen.';
JSADDINS.Strings.IDS_NOTETAGS_ICON_LIST = 'Symbolliste';
JSADDINS.Strings.IDS_NOTETAGS_MESSAGE_ALREADY_EXISTS = 'Das Tag, das Sie erstellen m\u00f6chten, ist bereits vorhanden.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_UNNAMED_SECTION_HOWFIX_GO_TO_STEP = 'Zum Abschnitt wechseln';
JSADDINS.Strings.IDS_NOTETAGS_MESSAGE_SUCCESS = 'Ihr Tag wurde erfolgreich erstellt.';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_CURRENT_NOTEBOOK_VIEW = 'Notizbuch';
JSADDINS.Strings.IDS_NOTETAGS_MESSAGE_INVALID_LABEL = 'Das Tag, das Sie verwenden m\u00f6chten, enth\u00e4lt ung\u00fcltige Zeichen. Verwenden Sie nur alphanumerische Zeichen.';
JSADDINS.Strings.IDS_NOTETAGS_ALL_ICONS = 'Alle Symbole';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_UNNAMED_SECTION_GROUP_HOWFIX_PATTERN = 'Um einen benutzerdefinierten Namen f\u00fcr eine Abschnittsgruppe hinzuzuf\u00fcgen, |0, klicken Sie mit der rechten Maustaste auf den Namen der Abschnittsgruppe, und w\u00e4hlen Sie dann \"Umbenennen\" aus.';
JSADDINS.Strings.IDS_NOTETAGS_CREATE = 'Erstellen';
JSADDINS.Strings.IDS_ACCESSIBILITYCHECKER_UNNAMED_SECTION_GROUP_HOWFIX_GO_TO_STEP = 'Zur Abschnittsgruppe wechseln';
JSADDINS.Strings.IDS_NOTETAGS_SELECTED_ICON = 'Ausgew\u00e4hltes Symbol:';
JSADDINS.Strings.IDS_NOTETAGS_NOT_SELECTED = 'Nicht ausgew\u00e4hlt';
JSADDINS.Strings.IDS_NOTETAGS_FEATURED = 'Empfohlen';
