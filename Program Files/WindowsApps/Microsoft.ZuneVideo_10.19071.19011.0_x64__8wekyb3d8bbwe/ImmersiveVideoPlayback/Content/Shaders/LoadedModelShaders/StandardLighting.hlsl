/********************************************************
*                                                       *
*   Copyright (C) Microsoft. All rights reserved.       *
*                                                       *
********************************************************/
#if !defined(STANDARDLIGHTING_HLSL_INCLUDED)
#define STANDARDLIGHTING_HLSL_INCLUDED

float SmoothnessToSpecularPower(float smoothness)
{
    return Square(10.0f / log2(smoothness * 0.968f + 0.03f));
}

float RoughnessToSpecularPower(float roughness)
{
    float smoothness = 1.0f - roughness;
    return SmoothnessToSpecularPower(SATURATE_1D(smoothness));
}

// Relationship between roughness (variance=stddev^2 in rad^2 or sr) and traditional specular power exponent (shininess)
// Roughness variance is assumed/required to be in [0,1].
// RoughnessVariance = 2 / (SpecularPower + 2)
// SpecularPower = (2 / RoughnessVariance) - 2
// float specularPower = 2.0 / roughness - 2.0;


// Mathematica comparison of:
//  a) Marmoset (RoughnessToSpecPower 1st implementation)
//  b) ThisRenderer
//  c) Unity (RoughnessToSpecPower 2nd implementation)
// plot log2((10 / log2((1 - x)*0.968 + 0.03)) ^ 2), log2(0.5*(2 / ((1 - 0.99*(1 - x)) ^ 4) - 2)), log2(2 / (x ^ 3 + 1.0e-4) - 2) for x = 0..1

float UnitySmoothnessToSpecularPower(float smoothness)
{
    // NOTE: Unity has now switched away from this as the default conversion function, and now
    // uses academic roughness conversion:
    //      half m = max(1e-4f, roughness * roughness);
    //      half n = (2.0 / (m*m)) - 2.0;
    //      specularPower = max(n, 1e-4f);
    // We should consider switching across as well, for wider compatibility and fewer magic numbers.


    // Convert perceptual smoothness [0,1] into cosine specular power using the transfer function used by 
    // Marmoset Toolbag and Unity 5. For further information see: Marmoset documentation, Unity documentation
    // and standard shaders, and Knald (normal map generator) documentation: https://s3.amazonaws.com/docs.knaldtech.com/knald/1.0.0/lys_power_drops.html
    return pow(10.0f / log2(smoothness * 0.968f + 0.03f), 2.0f);
}

//-----------------------------------------------------------------------------
float SpecularPowerToUnitySmoothness(float specularPower)
{
    // Inverse of UnitySmoothnessToSpecularPower()
    float x = sqrt(specularPower);
    x = -x;
    x = 10.0f / x;
    x = exp2(x);
    x -= 0.03f;
    x /= 0.968f;
    return x;
}

// Generic Smith-Schlick visibility term
inline half SmithVisibilityTerm(half NdotL, half NdotV, half k)
{
    half gL = NdotL * (1 - k) + k;
    half gV = NdotV * (1 - k) + k;
    return 1.0 / (gL * gV + 1e-4f);
}

// From Microfacet Models for Refraction through Rough Surfaces, Walter et al. 2007
float SmithVisibilityG1_TrowbridgeReitzGGX(float Ndotv, float alphaG)
{
    float tanSquared = (1.0 - Ndotv*Ndotv) / (Ndotv*Ndotv);
    return 2.0 / (1.0 + sqrt(1.0 + alphaG*alphaG * tanSquared));
}

inline float SmithVisibility_BlinnPhong()
{
    return 1.0f;
}

inline half SmithVisibilityG_TrowbridgeReitzGGX_Walter(half NdotL, half NdotV, half alphaG)
{
    return SmithVisibilityG1_TrowbridgeReitzGGX(NdotL, alphaG) * SmithVisibilityG1_TrowbridgeReitzGGX(NdotV, alphaG);
}

// Smith-Schlick derived for GGX 
inline half SmithVisibility_TrowbridgeReitzGGX_Unity(half NdotL, half NdotV, half roughness)
{
    half k = (roughness * roughness) / 2; // derived by B. Karis, http://graphicrants.blogspot.se/2013/08/specular-brdf-reference.html
    return SmithVisibilityTerm(NdotL, NdotV, k);
}


// Trowbridge-Reitz (GGX)
// Generalised Trowbridge-Reitz with gamma power=2.0
float NormalDistributionFunction_TrowbridgeReitzGGX(float NdotH, float alphaG)
{
    // Note: alphaG is average slope (gradient) of the normals in slope-space.
    // It is also the (trigonometric) tangent of the median distribution value, i.e. 50% of normals have
    // a tangent (gradient) closer to the macrosurface than this slope.

    half a = alphaG;
    half a2 = a * a;
    half d = NdotH * NdotH * (a2 - 1.0f) + 1.0f;
    return a2 / (kPi * d * d);
}


float NormalDistributionFunction_BlinnPhongForPower(float NdotH, float specularPower)
{
    float D = pow(NdotH, specularPower);
    D *= (specularPower + 2.0f) / (2.0f * kPi);
    return D;
}

float ConvertRoughnessToAverageSlope(float roughness)
{
    // Calculate AlphaG as square of roughness; add epsilon to avoid numerical issues
    const float kMinimumVariance = 0.0005f;
    float alphaG = Square(roughness) + kMinimumVariance;
    return alphaG;
}

#endif // STANDARDLIGHTING_HLSL_INCLUDED
