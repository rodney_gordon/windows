/********************************************************
*                                                       *
*   Copyright (C) Microsoft. All rights reserved.   *
*                                                       *
********************************************************/

#if !defined(GLOBALS_HLSL_INCLUDED)
#define GLOBALS_HLSL_INCLUDED

#include "Platform.hlsl"

struct SurfaceProperties
{
    float3 N; // normal for the macrosurface

    float3 diffuse; // diffuse reflectance
    float3 specular; // specular reflectance at normal incidence
    float3 emissive;

    // microsurface smoothness: similar to inverse of roughness variance, with non-linear scaling to be perceptually linear
    float smoothness; 
    float opacity;
    float3 occlusion;
};


STATIC_CONST(float3, kHDTVRec709_RGBLuminanceCoefficients, float3(0.2126, 0.7152, 0.0722));

// Power 2.2 approximation to full sRGB gamma encoding (which uses power 2.4 with linear toe).
STATIC_CONST(float, kHDTVRec709_GammaDecodePowerApprox, 2.2f);
STATIC_CONST(float, kHDTVRec709_GammaEncodePowerApprox, 1.0 / 2.2f);

STATIC_CONST(float2, kOne_Float2, float2(1.0, 1.0));
STATIC_CONST(float3, kOne_Float3, float3(1.0, 1.0, 1.0));
STATIC_CONST(float4, kOne_Float4, float4(1.0, 1.0, 1.0, 1.0));

STATIC_CONST(float, kPi, 3.141592653f);

STATIC_CONST(float4, defaultCameraPosition, float4(0.0f, 0.0f, 0.f, 1.0f));

float RGBToLuminance(float3 color)
{
    return dot(color, kHDTVRec709_RGBLuminanceCoefficients);
}

float Power(float x, float y)
{
    return pow(abs(x), y);
}

float2 Power(float2 x, float y)
{
    return pow(abs(x), y);
}

float3 Power(float3 x, float y)
{
    return pow(abs(x), y);
}

float4 Power(float4 x, float y)
{
    return pow(abs(x), y);
}

float Power5(float x)
{
    float x2 = x*x;
    return x2*x2*x;
}

float Square(float x)
{
    return x * x;
}

float VectorSum(float3 value)
{
    return dot(value, float3(1, 1, 1));
}


float3 ApplyEaseInOut(float3 x)
{
    return x * x * (3.0 - 2.0 * x);
}

float MaxChannel(float3 color)
{
    return max(max(color.r, color.g), color.b);
}

float3 QuaternionVectorRotation(float4 Q, float3 V)
{
    float3 T = cross(Q.xyz, V);         // mul mad
    T += Q.www * V;                     // mad
    T = cross(Q.xyz, T);                // mul mad
    return 2.0 * T + V;                 // mad
}

float3 QuaternionVectorRotation_ScaledSqrtTwo(float4 Q /* quaternion prescaled by sqrt(2) */, float3 V)
{
    float3 T = cross(Q.xyz, V);         // mul mad
    T += Q.www * V;                     // mad
    return cross(Q.xyz, T) + V;         // mad mad
}

void TestPattern_RotatedGrid(float2 screenPos, inout float4 color)
{
    // Useful skewed grid test pattern for verifying render target, texture and pixel sampling alignment and filtering quality and detecting aliasing.
    // The caller should provide the SV_Position (screen-space position) as input from pixel shader.

    float2 pos = floor(screenPos);

    if (fmod(floor(pos.x - pos.y / 8.0), 64.0) == 0)
        color = float4(1.0f, 1.0f, 1.0f, 1.0f);

    if (fmod(floor(pos.y + pos.x / 8.0), 64.0) == 0)
        color = float4(1.0f, 1.0f, 1.0f, 1.0f);
}

void TestPattern_Stripes(float2 screenPos, inout float4 color)
{
    // Useful test pattern that provides horizontal and vertical stripes with precise pixel exact alignment.
    // This is useful for verifying texture filtering and checking for resampling artifacts or issues with display output
    // that occur even after we have finished processing, e.g. later in the Windows/XAML composition engine when
    // it presents our swap chain to the user.

    // The caller should provide the SV_Position (screen-space position) as input from pixel shader.

    float2 d = frac(screenPos * 0.5) - 0.5;

    if (screenPos.x < 12)
        color = d.y > 0.0 ? float4(1.0f, 1.0f, 1.0f, 1.0f) : float4(0.0f, 0.0f, 0.0f, 1.0f);

    if (screenPos.y < 12)
        color = d.x > 0.0 ? float4(1.0f, 1.0f, 1.0f, 1.0f) : float4(0.0f, 0.0f, 0.0f, 1.0f);
}

float TestPattern_Checkerboard(float2 p)
{
    float2 f = frac(p);
    f -= 0.5f;
    return (f.x * f.y) > 0.0f ? 1.0f : 0.0f;
}

#endif //GLOBALS_HLSL_INCLUDED
