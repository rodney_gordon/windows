/********************************************************
*                                                       *
*   Copyright (C) Microsoft. All rights reserved.   *
*                                                       *
********************************************************/

#if !defined(PLATFORM_HLSL_INCLUDED)
#define PLATFORM_HLSL_INCLUDED

// Default to D3D11 mode if nothing is specified
#if !defined(SHADER_PLATFORM_DIRECT3D11) && !defined(SHADER_PLATFORM_OPENGL4)
#define SHADER_PLATFORM_DIRECT3D11
#endif 

/**
* Use this macro to define global constants that are compiled into the shader.
* @param type       Type of constant (float, bool, int, float4, etc...)
* @param var        Name of the global constant (tip: prefix with g_, eg. g_test)
* @param value      Constant value.
*
* Example:
* STATIC_CONST(float, kPi, 3.1415926f)
* 
*/
#define STATIC_CONST(type, var, value)

/**
* Use this macro to define global constant arrays that are compiled into the shader.
* @param type       Type of constant array elements (float, bool, int, float4, etc...)
* @param var        Name of the global constant (tip: prefix with g_, eg. g_test)
* @param count      Count of elements.
*
* Example:
* STATIC_CONST_ARRAY(float, kValues, 3) = BEGIN_ARRAY(float, 3) 1, 2, 3 END_ARRAY;
*
*/
#define STATIC_CONST_ARRAY(type, var, count)

#define BEGIN_ARRAY(type, count)
#define END_ARRAY

/**
* Used to mark the beginning of a struct in the shader. Must be paired with DECL_END_STRUCT. A struct
* allows logical grouping of a set of variables.
* Once inside a DECL_BEGIN_STRUCT, you can use the DECL_FIELD* commands to declare members of the structure.
*
* NOTE: Structs cannot be used as VertexShader outputs - see DECL_BEGIN_VS_OUTPUT for that
* NOTE: Structs cannot be used as PixelShader inputs - see DECL_BEGIN_PS_INPUT for that
* NOTE: Structs cannot be for constant buffers or uniforms. - see DECL_BEGIN_CONSTANT_BUFFER for that
*
* @param name       Name of the structure
*
* Example:
* DECL_BEGIN_STRUCT(myStruct)
*    DECL_FIELD(float4, Position) 
* DECL_END_STRUCT
*/
#define DECL_BEGIN_STRUCT(name)

/**
* Used to mark the end of a struct. Must be paired with DECL_BEGIN_STRUCT
*/
#define DECL_END_STRUCT

/**
* Used to mark the beginning of a Constant Buffer in the shader. Must be paired with DECL_END_CONSTANT_BUFFER. 
* A constant buffer, also known as a uniform buffer, is a well buffer that is set per-drawcall by the CPU, and is
* read only for the shader.
* Once inside a DECL_BEGIN_CONSTANT_BUFFER, you can use the DECL_FIELD* commands to declare members of the structure.
* Constant buffers must specify a binding slot, which is an integer from 0 to 32 (and beyond). The binding slot
* can only be used by 1 constant buffer at a time within a shader. A constant buffer is usually available to both the pixel
* and the vertex shader, and must use the same binding slot in both (portability)
*
* @param name       Name of the constant buffer
*
* Example:
* DECL_BEGIN_CONSTANT_BUFFER(myStruct)
*    DECL_FIELD(float4, Position)
* DECL_END_CONSTANT_BUFFER
*/
#define DECL_BEGIN_CONSTANT_BUFFER(name, bindingSlot)

/**
* Used to mark the end of a constant buffer. Must be paired with DECL_BEGIN_CONSTANT_BUFFER
*/
#define DECL_END_CONSTANT_BUFFER

/**
* Used to mark the start of the Vertex Shader Output Declaration. For portability reasons, this cannot be
* declared as normal STRUCTS. You must have exactly 1 of these declared in your vertex shader. You must also
* have an equivalent structure defined in the Pixel Shader as input, and this must use DECL_BEGIN_PS_INPUT.
* NOTE: Do not specify explicit Position in the Vertex Shader Output or Pixel Shader Input declaraction. The Macros
* take care of this automatically. You should not read or write to the position field directly either. Instead use the
* VS_WRITE_OUTPUT_POSITION to write the position field in the vertex shader. This is due to the fact that in HLSL, Position can
* be specified and bound to an interpolator, whereas in GLSL one must write to the built in gl_Position output.
*
* @param name       Name of the Vertex Shader Output
*
* Example:
* DECL_BEGIN_VS_OUTPUT
*    DECL_FIELD(float2, UV)   // NOTE: We only declare UV, but there is also an implicit float4 Position field.
* DECL_BEGIN_VS_OUTPUT
*/#define DECL_BEGIN_VS_OUTPUT (name)

/**
* Used to mark the end of the Vertex Shader Output Declaration. Must be paired with DECL_BEGIN_VS_OUTPUT
*/
#define DECL_END_VS_OUTPUT

/**
* Must be used between a DECL_BEGIN_* and DECL_END_* macro. This macro defines a member of a struct/buffer. The member
* will be of the specified type and will have the specified name. Inside vertex or pixel shader code, you can access
* the member directly (see example below).
*
* Example:
* DECL_BEGIN_STRUCT(myStruct)
*    DECL_FIELD(float4, g_MyValue)
* DECL_END_STRUCT
* // Inside code
* float4 value = g_MyValue; // Direct access
*/
#define DECL_FIELD(type, name) 

/**
* To be removed - added to aid initial port. Do not use in your structures.
*/
#define DECL_FIELD_WITHBIND(type, name, binding) 

/**
* To be removed - added to aid initial port. Do not use in your structures.
*/
#define DECL_FIELD_WITHOFFSET(type, name, packOffset)

/*
* Use this macro inside the vertex shader main function, to write the vertex position out. 
* This must be called at least once.
*/
#define VS_WRITE_OUTPUT_POSITION(pos)

/**
* Use this macro to mark the start of the Vertex Shader entry point. This must be pared with VS_FUNC_MAIN_END.
* The macro will automatically setup the inputs and outputs of the vertex shader. These can be accessed via the
* implicitly created vsInput and vsOutput variables. The Types of the input and output are specified in the arguments to this
* macro.
*
* @param outName       Type name of the output of the vertex shader. This is the name as specified in the DECL_BEGIN_VS_OUTPUT macro
* @param inName        Type name of the input of the vertex shader.
*
* Example:
* VS_FUNC_MAIN_BEGIN(VsOut, VsIn)
*   VS_WRITE_OUTPUT_POSITION( vsInput.Position );
* VS_FUNC_MAIN_END
*/
#define VS_FUNC_MAIN_BEGIN(outName, inName) 

/*
* Marks the end of the Vertex Shader entry point.
*/
#define VS_FUNC_MAIN_END  

/*
* Use this macro inside the pixel shader main function, to write the fragment color out.
* This must be called at least once.
*/
#define PS_WRITE_OUTPUT_COLOR(col) 

/**
* Use this macro to mark the start of the Pixel Shader entry point. This must be pared with PS_FUNC_MAIN_END.
* The macro will automatically setup the inputs of the pixel shader. These can be accessed via the
* implicitly created psInput variable. The Types of the input is specified in the arguments to this
* macro.
*
* @param inName        Type name of the input of the pixel shader.
*
* Example:
* PS_FUNC_MAIN_BEGIN(PsIn)
*   PS_WRITE_OUTPUT_COLOR( normalize(psInput.Normal) );
* PS_FUNC_MAIN_END
*/
#define PS_FUNC_MAIN_BEGIN(inName)

/*
* Marks the end of the Pixel Shader entry point.
*/
#define PS_FUNC_MAIN_END 

#define DECL_TEXTURE_2D(textureName, binding)      
#define DECL_TEXTURE_2D_NO_SAMPLER(textureName, binding)      
#define DECL_TEXTURE_3D(textureName, binding)      
#define DECL_TEXTURE_CUBE(textureName, binding)    
#define DECL_TEXTURE_2D_COMP(textureName, binding) 

#define SAMPLE_TEXTURE_2D(textureName, uv)
#define SAMPLE_TEXTURE_2D_COMP(textureName, uv, comp)       
#define SAMPLE_TEXTURE_CUBE(textureName, uvw)               
#define SAMPLE_TEXTURE_CUBE_LOD(textureName, uvw, lod)      
#define SAMPLE_TEXTURE_3D(textureName, uvw)                 

#define LOAD_TEXTURE_2D(textureName, uv, lod)

#define ddx(arg)                                            
#define ddy(arg)             

#define lerp(a, b, t)                                          
#define mul(a, b)

#define GREATER_THAN(a, b)
#define GREATER_THAN_OR_EQUAL(a, b)
#define LESS_THAN_OR_EQUAL(a, b)
#define LESS_THAN(a, b)
#define EQUAL(a, b)
#define NOT_EQUAL(a, b)

#define SATURATE_1D(a)                                      
#define SATURATE_2D(a)                                      
#define SATURATE_3D(a)                                      
#define SATURATE_4D(a)                                      
#define RCP_1D(a)

#define FLOAT_TO_UINT(a)
#define FLOAT_TO_INT(a)
#define INT_TO_FLOAT(a)
#define UINT_TO_FLOAT(a)

/*****************************************************************************
* Platform specific code below
******************************************************************************/

#undef STATIC_CONST
#undef STATIC_CONST_ARRAY
#undef BEGIN_ARRAY
#undef END_ARRAY
#undef DECL_BEGIN_STRUCT
#undef DECL_END_STRUCT
#undef DECL_BEGIN_CONSTANT_BUFFER
#undef DECL_END_CONSTANT_BUFFER
#undef DECL_BEGIN_VS_OUTPUT
#undef DECL_END_VS_OUTPUT
#undef DECL_FIELD_WITHBIND
#undef DECL_FIELD_WITHOFFSET
#undef DECL_FIELD

#undef VS_WRITE_OUTPUT_POSITION
#undef VS_FUNC_MAIN_BEGIN
#undef VS_FUNC_MAIN_END

#undef PS_WRITE_OUTPUT_COLOR
#undef PS_FUNC_MAIN_BEGIN
#undef PS_FUNC_MAIN_END

#undef DECL_TEXTURE_2D
#undef DECL_TEXTURE_2D_NO_SAMPLER
#undef DECL_TEXTURE_3D
#undef DECL_TEXTURE_CUBE
#undef DECL_TEXTURE_2D_COMP

#undef SAMPLE_TEXTURE_2D
#undef SAMPLE_TEXTURE_2D_COMP
#undef SAMPLE_TEXTURE_CUBE
#undef SAMPLE_TEXTURE_CUBE_LOD
#undef SAMPLE_TEXTURE_3D

#undef LOAD_TEXTURE_2D

#undef ddx
#undef ddy

#undef lerp
#undef mul

#undef GREATER_THAN
#undef GREATER_THAN_OR_EQUAL
#undef LESS_THAN_OR_EQUAL
#undef LESS_THAN
#undef EQUAL
#undef NOT_EQUAL

#undef SATURATE_1D
#undef SATURATE_2D
#undef SATURATE_3D
#undef SATURATE_4D
#undef RCP_1D

#undef FLOAT_TO_UINT
#undef FLOAT_TO_INT
#undef INT_TO_FLOAT
#undef UINT_TO_FLOAT

/*****************************************************************************
* Macros for use when compiling the shader for Direct3D 11 - HLSL
******************************************************************************/
#ifdef SHADER_PLATFORM_DIRECT3D11

#define STATIC_CONST(type, var, value)                      static const type var = value
#define STATIC_CONST_ARRAY(type, var, count)                static const type var[count]
#define BEGIN_ARRAY(type, count)                            {
#define END_ARRAY                                           }

#define DECL_BEGIN_STRUCT(name)                             struct name {
#define DECL_END_STRUCT                                     };

#define DECL_BEGIN_CONSTANT_BUFFER(name, bindingSlot)       cbuffer name : register(b ## bindingSlot) {
#define DECL_END_CONSTANT_BUFFER                            };

#define DECL_BEGIN_VS_OUTPUT(name)                          struct name { float4 Position : SV_POSITION;
#define DECL_END_VS_OUTPUT                                  };

#define DECL_BEGIN_VS_INPUT(name)                           struct name {
#define DECL_END_VS_INPUT                                   };

#define DECL_FIELD_WITHBIND(type, name, binding)            type name : binding;
#define DECL_FIELD_WITHOFFSET(type, name, packOffset)       type name : packoffset(c ## packOffset);
#define DECL_FIELD(type, name)                              type name;
#define VS_DECL_FIELD_WITHBIND(type, name, binding)         type name : binding;
#define VS_INPUT_FIELD(name)                                vsInput.name

#define VS_WRITE_OUTPUT_POSITION(pos)                       vsOutput.Position = pos;
#define VS_FUNC_MAIN_BEGIN(outName, inName)                 outName RenderSceneVS(inName vsInput) { outName vsOutput;
#define VS_FUNC_MAIN_END                                    return vsOutput; }

#define PS_WRITE_OUTPUT_COLOR(col)                          float4 psFinalOutput = col;
#ifdef ENABLE_DUAL_SOURCE_BLENDING
#define PS_FUNC_MAIN_BEGIN(inName)                          float4 RenderScenePS(inName psInput, out float4 outputTarget1 : SV_TARGET1 ) : SV_TARGET0 {
#define PS_WRITE_OUTPUT_COLOR2(col)                         outputTarget1 = col;
#else
#define PS_FUNC_MAIN_BEGIN(inName)                          float4 RenderScenePS(inName psInput ) : SV_TARGET0 {
#define PS_WRITE_OUTPUT_COLOR2(col)                         
#endif
#define PS_FUNC_MAIN_END                                    return psFinalOutput; }
#define PS_GET_INPUT_POSITION                               psInput.Position 

#define DECL_TEXTURE_2D_NO_SAMPLER(textureName, binding)    Texture2D textureName ## texture : register(t ## binding);
#define DECL_TEXTURE_2D(textureName, binding)               Texture2D textureName ## texture : register(t ## binding); SamplerState textureName ## sampler : register(s ## binding);
#define DECL_TEXTURE_3D(textureName, binding)               Texture3D textureName ## texture : register(t ## binding); SamplerState textureName ## sampler : register(s ## binding);
#define DECL_TEXTURE_CUBE(textureName, binding)             TextureCube textureName ## texture : register(t ## binding); SamplerState textureName ## sampler : register(s ## binding);
#define DECL_TEXTURE_2D_COMP(textureName, binding)          Texture2D textureName ## texture : register(t ## binding); SamplerComparisonState textureName ## sampler : register(s ## binding);

#define SAMPLE_TEXTURE_2D(textureName, uv)                  textureName ## texture.Sample(textureName ## sampler, uv)
#define SAMPLE_TEXTURE_2D_LOD(textureName, uv, lod)         textureName ## texture.SampleLevel(textureName ## sampler, uv, lod)
#define SAMPLE_TEXTURE_2D_COMP(textureName, uv, comp)       textureName ## texture.SampleCmp(textureName ## sampler, uv, comp)
#define SAMPLE_TEXTURE_CUBE(textureName, uvw)               textureName ## texture.Sample(textureName ## sampler, uvw)
#define SAMPLE_TEXTURE_CUBE_LOD(textureName, uvw, lod)      textureName ## texture.SampleLevel(textureName ## sampler, uvw, lod)
#define SAMPLE_TEXTURE_3D(textureName, uvw)                 textureName ## texture.Sample(textureName ## sampler, uvw)

#ifdef SHADER_PLATFORM_DIRECT3D11
// Special stuff only supported on D3D11.
// TODO: Port this concept of having separate texture and sampler state to OpenGL.
// NOTE: This ability to have "naked" samplers and textures is currently unused but left here for reference.
#define DECL_SAMPLER(samplerName, binding)                  SamplerState samplerName ## sampler : register(s ## binding);
#define DECL_SAMPLER_COMP(samplerName, binding)             SamplerComparisonState samplerName ## sampler : register(s ## binding);

#define DECL_TEXTURE_2D_NAKED(textureName, binding)         Texture2D textureName ## texture : register(t ## binding);
#define SAMPLE_TEXTURE_2D_USING_SAMPLER(textureName, samplerName, uv) textureName ## texture.Sample(samplerName ## sampler, uv)
#define SAMPLE_TEXTURE_2D_COMP_USING_SAMPLER(textureName, samplerName, uv, comp) textureName ## texture.SampleCmp(samplerName ## sampler, uv, comp)
#endif // SHADER_PLATFORM_DIRECT3D11
#define LOAD_TEXTURE_2D(textureName, uv, lod)               textureName ## texture.Load(int3(uv, lod))

#define ddx(arg)                                            ddx(arg)
#define ddy(arg)                                            ddy(arg)

#define lerp(a, b, t)                                       lerp(a, b, t)
#define mul(a, b)                                           mul(a, b)

#define GREATER_THAN(a, b)                                  (a > b)
#define GREATER_THAN_OR_EQUAL(a, b)                         (a >= b)
#define LESS_THAN_OR_EQUAL(a, b)                            (a <= b)
#define LESS_THAN(a, b)                                     (a < b)
#define EQUAL(a, b)                                         (a == b)
#define NOT_EQUAL(a, b)                                     (a != b)

#define SATURATE_1D(a)                                      saturate(a)
#define SATURATE_2D(a)                                      saturate(a)
#define SATURATE_3D(a)                                      saturate(a)
#define SATURATE_4D(a)                                      saturate(a)

#define UNROLL                                              [unroll]

#define RCP_1D(a)                                           rcp(a)

#define FLOAT_TO_UINT(a)                                    asuint(a)
#define FLOAT_TO_INT(a)                                     asint(a)
#define INT_TO_FLOAT(a)                                     asfloat(a)
#define UINT_TO_FLOAT(a)                                    asfloat(a)

#endif // SHADER_PLATFORM_DIRECT3D11

// Undefine types to prevent developers using them directly.

#ifdef SHADER_PLATFORM_DIRECT3D11
//#define Texture2D
//#define Texture3D
//#define TextureCube
//#define SamplerState
//#define SamplerComparisonState
//#define packoffset
//#define cbuffer
#endif

#endif // PLATFORM_HLSL_INCLUDED