/********************************************************
*                                                       *
*   Copyright (C) Microsoft. All rights reserved.   *
*                                                       *
********************************************************/

#if !defined(STANDARD_SHADER_COMMON_IO_HLSL_INCLUDED)
#define STANDARD_SHADER_COMMON_IO_HLSL_INCLUDED

//--------------------------------------------------------------------------------------
// Shader Input/Output Definitions
//--------------------------------------------------------------------------------------

/**
* Standard Vertex Shader Output & Pixel Shader Input
*/
DECL_BEGIN_VS_OUTPUT(StandardShaderVertexOutput)
    DECL_FIELD_WITHBIND(float2, TextureUV,      TEXCOORD0)
    DECL_FIELD_WITHBIND(float3, WorldPos,       TEXCOORD1)
    DECL_FIELD_WITHBIND(float3, Normal,         NORMAL0)
    DECL_FIELD_WITHBIND(float4, Tangent,        TEXCOORD2)

    DECL_FIELD_WITHBIND(uint, viewId,           TEXCOORD8)  // SV_InstanceID % 2
DECL_END_VS_OUTPUT

DECL_BEGIN_STRUCT(StandardShaderGeometryOutput)
    DECL_FIELD_WITHBIND(float4, Position, SV_POSITION)
    DECL_FIELD_WITHBIND(float2, TextureUV, TEXCOORD0)
    DECL_FIELD_WITHBIND(float3, WorldPos, TEXCOORD1)

    DECL_FIELD_WITHBIND(float3, Normal, NORMAL0)
    DECL_FIELD_WITHBIND(float4, Tangent, TEXCOORD2)
    DECL_FIELD_WITHBIND(uint, rtvId, SV_RenderTargetArrayIndex)
DECL_END_STRUCT

#endif // STANDARD_SHADER_COMMON_IO_HLSL_INCLUDED
