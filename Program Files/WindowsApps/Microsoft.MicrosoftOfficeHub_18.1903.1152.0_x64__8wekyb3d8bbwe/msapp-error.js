﻿(function () {
    function parseQueryParameters() {
        var query = location.search.slice(1);
        return query.split("&").reduce(function (queryParameters, rawPair) {
            var pair = rawPair.split("=").map(decodeURIComponent);
            queryParameters[pair[0]] = pair[1];
            return queryParameters;
        }, {});
    }

    function createCID() {
        return `${createCIDHelper(8)}-${createCIDHelper(4)}-${createCIDHelper(4)}-${createCIDHelper(4)}-${createCIDHelper()}`;
    }

    function createCIDHelper(count) {
        return count ? Math.random().toString(16).slice(2, 2 + count) : Math.random().toString(16).slice(2);
    }

    function initialize() {
        const resourceLoader = Windows.ApplicationModel.Resources.ResourceLoader.getForCurrentView();
        const correlationId = createCID();
        var errorDetails = {};
        if (!navigator.onLine) {
            document.getElementById("ErrorTitle").textContent = resourceLoader.getString("InternetConnectionRequired");
            document.getElementById("ErrorMessage").textContent = resourceLoader.getString("InternetConnectionRequiredMessage");
            Object.assign(errorDetails, { 'title': 'noInternetConnection'});
        }
        else {
            var queryParameters = parseQueryParameters();
            document.getElementById("ErrorTitle").textContent = resourceLoader.getString("GenericError");
            const noErrorString = resourceLoader.getString("NoErrorName");
            document.getElementById("ErrorMessage").textContent = `${resourceLoader.getString("GenericErrorMessage")}`;
            document.getElementById("HttpStatus").textContent = `${resourceLoader.getString("HttpStatus")} ${queryParameters["httpStatus"] || noErrorString}`;
            document.getElementById("FailureUrl").textContent = `${resourceLoader.getString("FailureUrl")} ${queryParameters["failureUrl"] || noErrorString}`;
            document.getElementById("FailureName").textContent = `${resourceLoader.getString("FailureName")} ${queryParameters["failureName"] || noErrorString}`;
            document.getElementById("CorrelationId").textContent = `${resourceLoader.getString("CorrelationId")} ${correlationId || noErrorString}`;
            Object.assign(errorDetails, { 'title': 'loadFailed'}, queryParameters);
        }
        // TODO (bepan): determine when to call this and what parameters should be logged
        // I'd leave it here but apparently this page somehow gets loaded (and thus logs the error)
        // even when the real page is able to load
        logErrorTelemetry(errorDetails, correlationId);
    }


    const appService = Windows.ApplicationModel.AppService;
    var connection = null;

    function getAppServiceConnection() {
        return new Promise((resolve, reject) => {

            if (connection !== null
                && typeof connection !== 'Windows.ApplicationModel.AppService.AppServiceConnection'
                && connection.status !== appService.AppServiceConnectionStatus.success) {
                return resolve(connection);
            }

            connection = new appService.AppServiceConnection();
            connection.appServiceName = 'com.microsoft.myoffice.client.bridge';
            connection.packageFamilyName = Windows.ApplicationModel.Package.current.id.familyName;

            connection.openAsync().done(status => {
                if (status === appService.AppServiceConnectionStatus.success) {
                    connection.onserviceclosed = () => {
                        connection = null;
                    };

                    return resolve(connection);
                }

                connection = null;
                reject('AppService connection failed');
            });
        });
    }

    function logErrorTelemetry(errorDetails, correlationId) {
        if (!errorDetails) {
            return;
        }

        return getAppServiceConnection().then(connection => {
            var message = new Windows.Foundation.Collections.ValueSet();

            var errorMessage = {
                'Telemetry.LogFailure': {
                    'type': 'LoadFailure',
                    'feature': 'ErrorPage',
                    'details': 'Failed to load start page',
                    'properties': errorDetails
                },
                'cid': correlationId
            };

            message.insert('error', JSON.stringify(errorMessage));
            return connection.sendMessageAsync(message);
        });
    }

    document.addEventListener("DOMContentLoaded", initialize);
}());