
-- ----------------------------------------------------------------------------
-- Copyright (C) 2008 Nokia Gate5 GmbH Berlin
--
-- These coded instructions, statements, and computer programs contain
-- unpublished proprietary information of Nokia Gate5 GmbH Berlin, and
-- are copy protected by law. They may not be disclosed to third parties
-- or copied or duplicated in any form, in whole or in part, without the
-- specific, prior written permission of Nokia Gate5 GmbH Berlin.
-- ----------------------------------------------------------------------------
--      Authors: Raul Ferrandez
-- ----------------------------------------------------------------------------
--             Voice Skin: French Canadian

require("uncommon")

function get_text_output()
end

function get_voicefiles()
	if maneuver_2 ~= nil then
		if maneuver_2.id == "STOPOVER" then
			maneuver_2 = {}
			maneuver_set_count = 1
			get_common_code()
		elseif maneuver_1.id ~= "STOPOVER" then
			get_common_code()
		end
    elseif maneuver_1.id ~= "STOPOVER" then
        get_common_code()
    end
end

function process_maneuver_triggers( )

    if trigger == "COMMAND" then
        
        if maneuver_1.id ~= "END" and maneuver_1.id ~= "ROUNDABOUT" and maneuver_1.id ~= "STOPOVER" and maneuver_1.id ~= "DRIVE_TO_NEAREST_ROAD" and maneuver_1.id ~= "PASS_JUNCTION" then
            -- "now"
            table.insert(result_list, misc[ "now_wav" ] )
        end
        
        if maneuver_1.id == "END" then
            table.insert(result_list, misc[ "destination_wav" ] )
  --    elseif maneuver_1.id == "STOPOVER" then
  --        table.insert(result_list, misc[ "stopover_wav" ] )  -- missing prompts
        elseif maneuver_1.id == "PASS_JUNCTION" then
            table.insert(result_list, misc[ "after_wav" ] )
  --          table.insert( result_list, misc[ "the_junction_wav" ] )  !!change next line to add_maneuver_to_table( false )
            add_maneuver_to_table( true )
        else
            add_maneuver_to_table( false )
        end

    elseif trigger == "REMINDER1" then
        -- If maneuver is "at the end of the street"
        if maneuver_1.extra_string == "EOS" then
            -- "At the end of the street"
            table.insert(result_list, misc[ "end_of_street_wav" ] )
            add_maneuver_to_table( false )
        else
            add_maneuver_to_table( true )
        end

    elseif trigger == "REMINDER2" then
        add_maneuver_to_table( true )

    elseif trigger == "ANNOUNCEMENT" then
        
        get_distance_and_unit()
        
        -- if Highway and dist_to_maneuver > 10 km or dist_to_maneuver > 10 miles
        if ( road_class == "HIGHWAY" or road_class == "URBAN_HIGHWAY" )
        and (   maneuver_1.dist_unit == "UNDEFINED"  )
        then
            -- "follow highway."
            table.insert(result_list, misc[ "follow_hwy_wav" ] )

        elseif ( road_class == "STREET" )
        and (   maneuver_1.dist_unit == "UNDEFINED"  )
        then
            -- "follow the course of the road."
            table.insert(result_list, misc[ "follow_road_e_wav" ] )

        elseif road_class == "STREET"
            and maneuver_1.dist_to >= 3000
            then
            -- "follow the course of the road."
            table.insert(result_list, misc[ "follow_road_wav" ] )
            -- "for"
            table.insert(result_list, misc[ "for_wav" ] )
            -- insert distance to maneuver
            table.insert(result_list, distance_to_maneuver )
            -- insert unit to maneuver
            table.insert(result_list, unit_to_maneuver_down )

        else
            add_maneuver_to_table( true )
        end
    end
end

function get_distance_and_unit()

    -- read and save distance to maneuver
    if tonumber(maneuver_1.dist_to_unit) == 1 and maneuver_1.dist_unit == "KILOMETER" then
        -- un
        distance_to_maneuver = misc[ "un_wav" ]
    elseif tonumber(maneuver_1.dist_to_unit) == 1 and maneuver_1.dist_unit == "MILE" then
        -- une
        distance_to_maneuver = misc[ "une_wav" ]
    else
        distance_to_maneuver = distances[ tonumber(maneuver_1.dist_to_unit) ]
    end

    -- read and save unit intonation with "up" intonation
    unit_to_maneuver_up = unit[ maneuver_1.dist_unit ] [ up ]
    -- read and save unit intonation with "down" intonation
    unit_to_maneuver_down = unit[ maneuver_1.dist_unit ] [ down ]

end

function get_maneuver( index, pitch )

    if index == 1 then
        maneuver = maneuver_1
    elseif index == 2 then
        maneuver = maneuver_2
    else
        maneuver = nil
    end

    if maneuver ~= nil then
        -- By roundabouts
        if maneuver.id == "ROUNDABOUT" then    
            -- "at the roundabout"
            table.insert(result_list, misc[ "roundabout_wav" ] )
            exit_roundabout = exit_number_check[ maneuver.extra_integer ] [ pitch ]
            table.insert(result_list, exit_roundabout )
            junction_turn = nil
            maneuver_command = nil
        else    -- By rest of maneuvers
            junction_turn = maneuver_turns[ maneuver.turn ] [ pitch ]
            maneuver_command = maneuver_check[ maneuver.id ] [ pitch ]
        end

        return junction_turn, maneuver_command
    end
end

function add_maneuver_to_table( include_distance )
    -- add_voice is needed in this function due to grammar issues (and then take the second right/left)
    add_voice = nil
    if include_distance == true then
        get_distance_and_unit()
        table.insert( result_list, misc[ "after_wav" ] ) -- "after"
        
        -- insert distance to maneuver
        table.insert( result_list, distance_to_maneuver )
        -- insert unit to maneuver
        table.insert( result_list, unit[ maneuver_1.dist_unit ] [ up ] )
    end

    if double_command == false then
        -- if maneuver ENTER/LEAVE/CHANGE HWY, then junction atribute with upper intonation 
        if maneuver_1.id == "ENTER_HIGHWAY" or maneuver_1.id == "ENTER_URBAN_HIGHWAY" or maneuver_1.id == "LEAVE_HIGHWAY" or maneuver_1.id == "LEAVE_URBAN_HIGHWAY" or maneuver_1.id == "CHANGE_HIGHWAY" or maneuver_1.id == "CHANGE_URBAN_HIGHWAY" then 
            junction_turn_back, dummy_var = get_maneuver( 1, up )
            dummy_var, maneuver_command_back = get_maneuver( 1, down )
        else
            junction_turn_back, maneuver_command_back = get_maneuver( 1, down )
        end
        
        table.insert( result_list, junction_turn_back )
        
        if maneuver_command_back ~= nil and junction_turn_back ~= nil then
            table.insert( result_list, misc[ "and_wav" ] ) -- "and"
        end
        
        table.insert( result_list, maneuver_command_back )
        
    else
        junction_turn_up, maneuver_command_up = get_maneuver( 1, up )
        table.insert( result_list, junction_turn_up )
        table.insert( result_list, maneuver_command_up )
  --      if maneuver_2.dist_to <= 30 and maneuver_2.id ~= "END" and maneuver_2.id ~= "ROUNDABOUT" and maneuver_2.id ~= "STOPOVER" then
  --          table.insert( result_list, "g5con_002" )     -- "and"
  --          table.insert( result_list, "g5immediately" ) -- "immediately" (missing)
  --      else
            table.insert( result_list, misc[ "and_then_wav" ] ) -- "and then"
  --      end

        if maneuver_2.id == "JUNCTION" then    -- By junctions
            if right[ maneuver_2.turn ] ~= nil and maneuver_2.exits_right == 1 then
                -- at the second possibility / (missing take the second right)
                add_voice = misc[ "second_possibility_wav" ]
                -- turn right
                junction_turn_down = maneuver_turns [ "QUITE_RIGHT" ][1]
                maneuver_command_down = nil
            elseif left[ maneuver_2.turn ] ~= nil and maneuver_2.exits_left == 1 then
                -- at the second possibility / (missing take the second left)
                add_voice = misc[ "second_possibility_wav" ]
                -- turn left
                junction_turn_down = maneuver_turns [ "QUITE_LEFT" ][1]
                maneuver_command_down = nil
            else
                junction_turn_down, maneuver_command_down = get_maneuver( 2, down )
            end
        -- if second maneuver is enter/leave/change hwy, then we just read the junction attribute     
        elseif maneuver_2.id == "ENTER_HIGHWAY" or maneuver_2.id == "ENTER_URBAN_HIGHWAY" or maneuver_2.id == "LEAVE_HIGHWAY" or maneuver_2.id == "LEAVE_URBAN_HIGHWAY" or maneuver_2.id == "CHANGE_HIGHWAY" or maneuver_2.id == "CHANGE_URBAN_HIGHWAY" then 
            junction_turn_down, maneuver_command_down = get_maneuver( 2, down )
            if junction_turn_down ~= nil then
                maneuver_command_down = nil
            end
        else
            junction_turn_down, maneuver_command_down = get_maneuver( 2, down )
        end
           
        table.insert( result_list, add_voice )
        table.insert( result_list, junction_turn_down )
        table.insert( result_list, maneuver_command_down )
    end
end

