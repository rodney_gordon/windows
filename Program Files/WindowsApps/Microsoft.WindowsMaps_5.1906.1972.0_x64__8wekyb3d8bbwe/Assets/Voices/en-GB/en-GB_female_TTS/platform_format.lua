-- ----------------------------------------------------------------------------
-- Copyright (C) 2012 Nokia Gate5 GmbH Berlin
--
-- These coded instructions, statements, and computer programs contain
-- unpublished proprietary information of Nokia Gate5 GmbH Berlin, and
-- are copy protected by law. They may not be disclosed to third parties
-- or copied or duplicated in any form, in whole or in part, without the
-- specific, prior written permission of Nokia Gate5 GmbH Berlin.
-- ----------------------------------------------------------------------------
--      Author: Fabian TP Riek
-- ----------------------------------------------------------------------------
--      Output format for Windows Phone w/ MSFT Engine
--      0.4.0.XXX

Platform = {}

Platform.take_phoneme_if_exits = function( article, name, phoneme )
	if use_phoneticdata
		and ( string.find( name, "%d") == nil )
		and phoneme ~= nil
		and phoneme ~= ""
	then
		return article.." <break time=\"0.15s\"/><phoneme alphabet=\"ipa\" ph=\""..phoneme.."\">"..name.."</phoneme> "
	else
		return article.." <break time=\"0.15s\"/><say-as interpret-as=\"address\">"..name.."</say-as> "
	end
end


Platform.build_streetname = function( street_name, r_sentence )
	if street_name == "" or street_name == nil then
		return ""
	end

	article = add_street_article( street_name, r_sentence )
	phoneme = add_phonetic_data( street_name )
	name = expand_dgttsp( street_name, r_sentence )

	return Platform.take_phoneme_if_exits( article, name, phoneme )
end


Platform.build_signpost = function( signpost_name, r_sentence )
	if signpost_name == "" or signpost_name == nil then
		return ""
	end

	phoneme = add_phonetic_data( signpost_name )
	name = expand_dgttsp( signpost_name, r_sentence )

	return Platform.take_phoneme_if_exits( "", name, phoneme )
end


function set_result( id )

	if nonTTS and double_command and id == command_id_1 and commands_double[ id ] ~= nil and commands_double[ id ] ~= "" then
		result_sentence = commands_double[ id ]
	elseif guidance_mode == "pedestrian" and commands_ped[ id ] ~= nil and commands_ped[ id ] ~= "" then
		result_sentence = commands_ped[ id ]
	else
		result_sentence = commands_common[ id ]
	end

	if result_sentence == nil or result_sentence == "" then
		result_sentence = " "
	end

	if result_sentence ~= nil and result_sentence ~= " " and not nonTTS then

		tts_street_1 = Platform.build_streetname(tts_street_1, result_sentence)
		tts_street_2 = Platform.build_streetname(tts_street_2, result_sentence)

		tts_signpost_1 = Platform.build_signpost(tts_signpost_1, result_sentence)
		tts_signpost_2 = Platform.build_signpost(tts_signpost_2, result_sentence)


		if exit_number ~= "" and exit_number ~= nil then
			exit_number = " <say-as interpret-as=\"address\">"..exit_number.."</say-as> "
		end

		tts_street_1 = SSML_avoid_break_in_say_as( tts_street_1 )
		tts_street_2 = SSML_avoid_break_in_say_as( tts_street_2 )
		tts_signpost_1 = SSML_avoid_break_in_say_as( tts_signpost_1 )
		tts_signpost_2 = SSML_avoid_break_in_say_as( tts_signpost_2 )

		--audio_mode = '\\audio="'..audio_files_path_absolute..'\\'


		
		if (maneuver.ng_name == "TRAFFIC_LIGHT") and (maneuver.traffic_light_int > 0) and use_trafficlights then
			nG_cmd_1 = ", "..nG_elements[maneuver.traffic_light_int]..", "
		elseif maneuver.nguidance ~= " " and  maneuver.nguidance ~= "" and use_nguidance then
			nG_cmd_1 = ", "..maneuver.nguidance..", "
		else
			nG_cmd_1 = ""
		end
		if (nextManeuver.ng_name == "TRAFFIC_LIGHT") and (nextManeuver.traffic_light_int > 0) and use_trafficlights then
			nG_cmd_2 = ", "..nG_elements[nextManeuver.traffic_light_int]..", "
		elseif nextManeuver.nguidance ~= " " and nextManeuver.nguidance ~= "" and use_nguidance then
			nG_cmd_2 = ", "..nextManeuver.nguidance..", "
		else
			nG_cmd_2 = ""
		end

		
		if string.find( result_sentence, "!SET_AUDIO!" ) then
			audio_start = "<audio src=\""..audio_files_path_absolute.."\\"
			audio_end = "\"/>\n<mstts:prompt domain=\"tbtdirection\"/>\n"
			result_sentence, trash = string.gsub ( result_sentence, "!SET_AUDIO!(.+)!SET_AUDIO!", audio_start.."%1"..audio_end )
		end

		result_sentence, trash = string.gsub ( result_sentence, "!PED_TURN_NO!", ped_turn_no )
		result_sentence, trash = string.gsub ( result_sentence, "!DIST!", distance_1 )
		result_sentence, trash = string.gsub ( result_sentence, "!UNIT!", usedUnit_1 )
		result_sentence, trash = string.gsub ( result_sentence, "!STREET!", tts_street_1 )
		result_sentence, trash = string.gsub ( result_sentence, "!STREET_2!", tts_street_2 )
		result_sentence, trash = string.gsub ( result_sentence, "!SIGNPOST!", tts_signpost_1 )
		result_sentence, trash = string.gsub ( result_sentence, "!SIGNPOST_2!", tts_signpost_2 )
		result_sentence, trash = string.gsub ( result_sentence, "!EXIT_NO_ROUNDABOUT!", exit_no_roundabout )
		result_sentence, trash = string.gsub ( result_sentence, "!ORIENTATION!", head_orientation )
		result_sentence, trash = string.gsub ( result_sentence, "!NG_COMMAND_1!", nG_cmd_1 )
		result_sentence, trash = string.gsub ( result_sentence, "!NG_COMMAND_2!", nG_cmd_2 )
		result_sentence, trash = string.gsub ( result_sentence, "!EXIT_NUMBER!", exit_number )
		result_sentence, trash = string.gsub ( result_sentence, "!BREAK!", "<break time=\"100ms\"\/>" )
		result_sentence, trash = string.gsub ( result_sentence, " & ", " &amp; " )

		while string.find( result_sentence, "!BREAK:" ) do
			pauseTime, trash = string.gsub(result_sentence, ".+!BREAK:(%d+).+", "%1")
			result_sentence, trash = string.gsub ( result_sentence, "!BREAK:"..pauseTime.."!", "<break time=\""..pauseTime.."ms\"\/>", 1)
		end

		while true do
			result_sentence, b_use = string.gsub ( result_sentence, "  ", " " )
			if b_use == 0 then
				break
			end
		end

		if ( string.byte(result_sentence,-1) == 32 ) then
			result_sentence = string.sub(result_sentence,1,-2)
		end

		result_sentence, trash = string.gsub( result_sentence ," ,", "," )

		
		result_sentence = languageSpecificMods( result_sentence )

	elseif nonTTS then

		result_sentence, trash = string.gsub ( result_sentence, "!DIST!", distance_1 )
		result_sentence, trash = string.gsub ( result_sentence, "!UNIT!", usedUnit_1 )
		result_sentence, trash = string.gsub ( result_sentence, "!EXIT_NO_ROUNDABOUT!", exit_no_roundabout )

	end

	return result_sentence

end

function set_result_tts( )

	if double_command then
		if sentence_1 ~= "" and sentence_1 ~= " " then
			result_sentence = sentence_1.." "..sentence_2
		else
			result_sentence = ""
		end
	else
		if sentence_1 ~= "" and sentence_1 ~= " " then
			result_sentence = sentence_1
		else
			result_sentence = ""
		end
	end

	result_sentence, b_use = string.gsub ( result_sentence, "  ", "" )

	if ( string.byte(result_sentence,-1) == 32 ) then
		result_sentence = string.sub(result_sentence,1,-2)
	end

	if result_sentence ~= "" then
		result_sentence = result_sentence.."."
	end

	
	result_sentence = string.upper( string.sub(result_sentence, 1, 1 ) )..string.sub(result_sentence, 2 )

--	result_sentence, count = string.gsub( result_sentence, '(audio=\\"[%a%d]+.wav"\\)'..'(\.)', "%1" )	
	result_sentence, count = string.gsub( result_sentence, ',%p', "\." )	

	table.insert( result_list, "<?xml version=\"1.0\"?>\n")
	table.insert( result_list, "<speak version=\"1.0\"\n")
	table.insert( result_list, " xmlns=\"http://www.w3.org/2001/10/synthesis\"\n")
	table.insert( result_list, " xmlns:mstts=\"http://www.w3.org/2001/mstts\"\n")
	table.insert( result_list, " xml:lang=\""..main_attribute_array.language_code.."\">\n")
	table.insert( result_list, "<voice gender=\""..voice_settings["gender"].."\">\n")
	table.insert( result_list, "<mstts:prompt domain=\"tbtdirection\"/>\n")
	table.insert( result_list, result_sentence.."\n")
	table.insert( result_list, "</voice>\n")
	table.insert( result_list, "</speak>\n")

end

function set_result_wave( )

	for k,v in pairs(sentence_1) do
		table.insert( result_list, v )
	end

	if double_command then
		for k,v in pairs(sentence_2) do
			table.insert( result_list, v )
		end
	end

end

function SSML_avoid_break_in_say_as( object )

	if string.find( object, "!BREAK!" ) then
		if string.find( object, " <say.as " ) then
			object, trash = string.gsub ( object, "!BREAK!", "</say-as> <break time=\"100ms\"\/> <say-as interpret-as=\"address\">" )
		else
			object, trash = string.gsub ( object, "!BREAK!", "" )
		end
	end

	return object

end
