-- ----------------------------------------------------------------------------
-- Copyright (C) 2012 Nokia Gate5 GmbH Berlin
--
-- These coded instructions, statements, and computer programs contain
-- unpublished proprietary information of Nokia Gate5 GmbH Berlin, and
-- are copy protected by law. They may not be disclosed to third parties
-- or copied or duplicated in any form, in whole or in part, without the
-- specific, prior written permission of Nokia Gate5 GmbH Berlin.
-- ----------------------------------------------------------------------------
--      Author: Fabian TP Riek
-- ----------------------------------------------------------------------------
--      Header-Data for WindowsPhone 8.0 with MSFT TTS
--      v4 packages
--      Language: en-GB
--      Technology: TTS
--      Gender: female


description = "TTS"
id = "1002"
audio_files_version = "0.4.0.1305061308"
client_range = "[ client >= 1.7.0.0 ]"
network_provider_support_list = {
	"all",
}
application_support_list = {
	'IN "WP8_TTS_Core":1.0',
}
feature_list = { "metric", "imperial_uk", "imperial_us" }
travel_mode = "0"
output_type = "tts"
language_id = "1"
marc_code = "eng"
language = "English (UK)"
language_loc = "English (UK)"
gender = "f"
config_file = "en-GB_female_TTS/common.lua"

main_attribute_array = {
	["language_code"] = "en-GB",
	["ngLangCode"] = "eng",
	["ms_lcid"] = "809",
	["LocalizedType"] = "announced street names",
	["LocalizedGender"] = "female",
	["VoiceFeatures"] = "drive;walk;metric;imperialuk;imperialus;naturalguidance;trafficlights;tts",
}


rulesets_file = ""
userdictionary_file = ""
audio_files_path = "en-GB_female_TTS\\en-GB_female_TTS"
apdb = ""
speaker = ""
platform_support_list = {
	'= "Windows Phone OS":>= 8.0',
}
tts_engine_type = { "msft:8.0.5515" }



require("ruleset_en-GB_TTS")
require("prompts_en-GB_TTS")
require("platform_format")
