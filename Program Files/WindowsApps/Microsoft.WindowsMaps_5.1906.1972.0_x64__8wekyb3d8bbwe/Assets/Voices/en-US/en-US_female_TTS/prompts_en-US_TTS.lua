-- � Microsoft. All rights reserved.
-- Voice Prompts: en-US English US TTS

street_article = nil 

nG_prepositions = {	
	["ENGBAFTR"] = "after",
	["ENGBAFTT"] = "after the",
	["ENGBATXX"] = "at",
	["ENGBATTX"] = "at the",
	["ENGBBEFR"] = "before",
	["ENGBBEFT"] = "before the",
	["ENGBONTO"] = "onto",
	["ENGBONTT"] = "onto the",
	["ENGBOVER"] = "over",
	["ENGBOVRT"] = "over the",
	["ENGBPAST"] = "past",
	["ENGBPSTT"] = "past the",
	["ENGBTHRU"] = "through",
	["ENGBTHRT"] = "through the",
	["ENGBUNDR"] = "under",
	["ENGBUNDT"] = "under the",
	["NONE"] = "", 
}
nG_elements = { 
	[1] = "at the next light", 
	[2] = "at the second light", 
	[3] = "at the third light", 
	["UNDEFINED"] = "",
}

unit_after = { 
	["MILE"] = "mile", 
	["YARDS"] = "yards", 
	["FEET"] = "feet", 
	["KILOMETER"] = "kilometer", 
	["METERS"] = "meters", 
	["METER"] = "meter", 
	["KILOMETERS"] = "kilometers", 
	["MILES"] = "miles", 
	["UNDEFINED"] = "",
}

unit_follow = { 
	["MILE"] = "mile", 
	["YARDS"] = "yards", 
	["FEET"] = "feet", 
	["KILOMETER"] = "kilometer", 
	["METERS"] = "meters", 
	["METER"] = "meter", 
	["KILOMETERS"] = "kilometers", 
	["MILES"] = "miles", 
	["UNDEFINED"] = "",
}

dist = { 
	["a"] = "one kilometer", 
	["b"] = "one mile", 
	["c"] = "a quarter of a mile", 
	["d"] = "half a mile", 
	["e"] = "three quarters of a mile", 
	["UNDEFINED"] = "",
}

exit_number_roundabout = { 
	[1] = "take the first exit", 
	[2] = "take the second exit", 
	[3] = "take the third exit", 
	[4] = "take the fourth exit", 
	[5] = "take the fifth exit", 
	[6] = "take the sixth exit", 
	[7] = "take the seventh exit", 
	[8] = "take the eighth exit", 
	[9] = "take the ninth exit", 
	[10] = "take the tenth exit", 
	[11] = "take the eleventh exit", 
	[12] = "take the twelfth exit", 
	["UNDEFINED"] = "",
}

orientation = { 
	["NORTH"] = "north", 
	["NORTH_EAST"] = "northeast", 
	["EAST"] = "east", 
	["SOUTH_EAST"] = "southeast", 
	["SOUTH"] = "south", 
	["SOUTH_WEST"] = "southwest", 
	["WEST"] = "west", 
	["NORTH_WEST"] = "northwest", 
	["UNDEFINED"] = "",
}

turn_number_ped = { 
	[1] = "and turn at the first street !STREET!", 
	[2] = "and turn at the second street !STREET!", 
	[3] = "and turn at the third street !STREET!", 
	["UNDEFINED"] = "",
}

commands_common = { 
	
	["00000000"] = " ",
	
	["c00c0zc0"] = "!EXIT_NO_ROUNDABOUT! at the end of the road at the traffic circle towards !SIGNPOST!",
	
	["c00c0zb0"] = "!EXIT_NO_ROUNDABOUT! at the end of the road at the traffic circle onto !STREET!",
	
	["c00c0z00"] = "!EXIT_NO_ROUNDABOUT! at the end of the road at the traffic circle ",
	
	["h00d000x"] = "and then take the exit !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00f000x"] = "and then enter the highway !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00j000x"] = "and then take the exit !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00e000x"] = "and then make a u turn !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00i000x"] = "and then enter the expressway !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00g000x"] = "and then take the exit !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00o000x"] = "and then take the middle lane !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00v000x"] = "and then turn left !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00w000x"] = "and then bear left !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00u000x"] = "and then make a sharp left !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00s000x"] = "and then make a sharp right !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00r000x"] = "and then turn right !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00p000x"] = "and then keep right !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00t000x"] = "and then make a u turn !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00x000x"] = "and then keep left !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h00q000x"] = "and then bear right !NG_COMMAND_2! towards !STREET_2! !SIGNPOST_2!",
	
	["h000cz0x"] = "and then !EXIT_NO_ROUNDABOUT! at the traffic circle towards !STREET_2! !SIGNPOST_2!",
	
	["bl0o000j"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , take the exit",
	
	["bl0x000j"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , take the exit ",
	
	["bl0p000j"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , take the exit ",
	
	["bl0n000j"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , take the exit",
	
	["bl0o00fj"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! and take the exit ",
	
	["bl00000j"] = "After !DIST! !UNIT! , take the exit !NG_COMMAND_1!",
	
	["bl0x00fj"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! and take the exit ",
	
	["bl0p00fj"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! and take the exit ",
	
	["bl0n00fj"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! and take the exit ",
	
	["a000000g"] = "Take the exit !NG_COMMAND_1!",
	
	["bl000e0x"] = "After !DIST! !UNIT! , take exit !EXIT_NUMBER! towards !STREET_2! !SIGNPOST_2!",
	
	["a00ni000"] = "Go straight ahead !NG_COMMAND_1! , and enter the expressway ",
	
	["h00n000x"] = "and then go straight ahead towards !STREET_2! !SIGNPOST_2!",
	
	["a00o000j"] = "Take the middle lane !NG_COMMAND_1! , take the exit ",
	
	["a00x000j"] = "Keep left !NG_COMMAND_1! , take the exit ",
	
	["a00p000j"] = "Keep right !NG_COMMAND_1!  , take the exit ",
	
	["a00n00fj"] = "Go straight ahead !NG_COMMAND_1! and take the exit ",
	
	["a00n000j"] = "Go straight ahead !NG_COMMAND_1! , take the exit",
	
	["a00o00fj"] = "Take the middle lane !NG_COMMAND_1! and take the exit ",
	
	["a000000j"] = "Take the exit !NG_COMMAND_1!",
	
	["a00x00fj"] = "Keep left !NG_COMMAND_1! and take the exit ",
	
	["a00p00fj"] = "Keep right !NG_COMMAND_1! and take the exit",
	
	["a00n00fj"] = "Go straight ahead !NG_COMMAND_1! and take the exit ",
	
	["h000ab00"] = "and then you'll arrive at your destination !NG_COMMAND_2! on !STREET!",
	
	["bl0v0d00"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! onto !STREET!",
	
	["a00pi000"] = "Keep right !NG_COMMAND_1! , and enter the expressway",
	
	["bl0nf000"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , and enter the highway",
	
	["bl0a0b00"] = "After !DIST! !UNIT! , you'll arrive at your destination !NG_COMMAND_1! on !STREET!",
	
	["j00q0ac0"] = "and then immediately bear right onto !STREET! towards !SIGNPOST!",
	
	["bl0o0edz"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST! and continue on !STREET_2! towards !SIGNPOST_2!",
	
	["h000ed00"] = "and then make a u turn !NG_COMMAND_2! onto !STREET!",
	
	["bl0r0c00"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! towards !STREET!",
	
	["j000ed00"] = "and then immediately make a u turn !NG_COMMAND_2! onto !STREET!",
	
	["bl0x0edz"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST! and continue on !STREET_2! towards !SIGNPOST_2!",
	
	["a00rf000"] = "Turn right !NG_COMMAND_1! , and enter the highway",
	
	["bl0v0c00"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! towards !STREET!",
	
	["bl0q0c00"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! towards !STREET!",
	
	["c00rf000"] = "At the end of the road turn right !NG_COMMAND_1! , and enter the highway",
	
	["bl0b0000"] = "After !DIST! !UNIT! , you'll reach a stopover !NG_COMMAND_1!",
	
	["bl0x0e0z"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , take exit !EXIT_NUMBER! and continue on !STREET_2! towards !SIGNPOST_2!",
	
	["a00d0000"] = "Take the exit !NG_COMMAND_1!",
	
	["bl00f000"] = "After !DIST! !UNIT! , and enter the highway",
	
	["h00d0000"] = "and then take the exit !NG_COMMAND_2!",
	
	["bl0qf0c0"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["h00v0000"] = "and then turn left !NG_COMMAND_2!",
	
	["j00v0000"] = "and then immediately turn left !NG_COMMAND_2!",
	
	["j000e000"] = "and then immediately make a u turn !NG_COMMAND_2!",
	
	["bl0uf0c0"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["c00ui0c0"] = "At the end of the road make a sharp left !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["h000e000"] = "and then make a u turn !NG_COMMAND_2!",
	
	["c00r0d00"] = "At the end of the road turn right !NG_COMMAND_1! onto !STREET!",
	
	["h00r0a00"] = "and then turn right, !STREET!",
	
	["j000dc00"] = "and then immediately take the exit !NG_COMMAND_2! towards !STREET!",
	
	["a00v0000"] = "Turn left !NG_COMMAND_1!",
	
	["c00v0000"] = "At the end of the road turn left !NG_COMMAND_1!",
	
	["c00m0c00"] = "At the end of the road take the ferry !NG_COMMAND_1! towards !STREET!",
	
	["bl0o0000"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1!",
	
	["bl0x00fg"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! and take the exit",
	
	["bl0o0e0x"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , take exit !EXIT_NUMBER! towards !STREET_2! !SIGNPOST_2!",
	
	["bl0ofac0"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["a00n00fg"] = "Go straight ahead !NG_COMMAND_1! and take the exit",
	
	["a00d0c00"] = "Take the exit !NG_COMMAND_1! towards !STREET!",
	
	["a00x000g"] = "Keep left !NG_COMMAND_1! , take the exit",
	
	["h000b000"] = "and then you'll reach a stopover !NG_COMMAND_2!",
	
	["c00qi0c0"] = "At the end of the road bear right !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["bl0sf0c0"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["bl0piac0"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["bl0x0ed0"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST!",
	
	["bl0p0edy"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST! and continue on !STREET_2!",
	
	["bl0ti0c0"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["bl0w0d00"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! onto !STREET!",
	
	["c00wi0c0"] = "At the end of the road bear left !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["bl0c0z00"] = "After !DIST! !UNIT! , !EXIT_NO_ROUNDABOUT! !NG_COMMAND_1! at the traffic circle",
	
	["bl0o0edx"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST! towards !STREET_2! !SIGNPOST_2!",
	
	["c00t0000"] = "At the end of the road make a u turn !NG_COMMAND_1!",
	
	["bl0ni0c0"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["bz0p0000"] = "After the junction keep right !NG_COMMAND_1!",
	
	["bz0n0000"] = "After the junction go straight ahead !NG_COMMAND_1!",
	
	["h00t0000"] = "and then make a u turn !NG_COMMAND_2!",
	
	["bl0n00fg"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! and take the exit",
	
	["bl0uiac0"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["bl0n000x"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["bl0n0edy"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST! and continue on !STREET_2!",
	
	["j00s0ac0"] = "and then immediately make a sharp right onto !STREET! towards !SIGNPOST!",
	
	["bl0m0000"] = "After !DIST! !UNIT! , take the ferry !NG_COMMAND_1!",
	
	["bl0oi0c0"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["c00via00"] = "At the end of the road turn left !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["h00s00c0"] = "and then make a sharp right !NG_COMMAND_2! towards !SIGNPOST!",
	
	["a00o0000"] = "Take the middle lane !NG_COMMAND_1!",
	
	["bl0o0edy"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST! and continue on !STREET_2!",
	
	["c00q0000"] = "At the end of the road bear right !NG_COMMAND_1!",
	
	["h00o0000"] = "and then take the middle lane !NG_COMMAND_2!",
	
	["a00p00fg"] = "Keep right !NG_COMMAND_1! and take the exit",
	
	["j00o0000"] = "and then immediately take the middle lane !NG_COMMAND_2!",
	
	["bl0wfac0"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["bl0t0c00"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1! towards !STREET!",
	
	["h00m0000"] = "and then take the ferry !NG_COMMAND_2!",
	
	["a00q0000"] = "Bear right !NG_COMMAND_1!",
	
	["bl0ri000"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! , and enter the expressway",
	
	["h00x0c00"] = "and then keep left !NG_COMMAND_2! towards !STREET!",
	
	["j00q0d00"] = "and then immediately bear right !NG_COMMAND_2! onto !STREET!",
	
	["bl0rf000"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! , and enter the highway",
	
	["bl0e0000"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1!",
	
	["h00i00c0"] = "and then enter the expressway !NG_COMMAND_2! towards !SIGNPOST!",
	
	["c00m0000"] = "At the end of the road take the ferry !NG_COMMAND_1!",
	
	["a00m0000"] = "Take the ferry !NG_COMMAND_1!",
	
	["bl0s0c00"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! towards !STREET!",
	
	["c00ufa00"] = "At the end of the road make a sharp left !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["c00tfa00"] = "At the end of the road make a u turn !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["bl00fac0"] = "After !DIST! !UNIT! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["h00i0ac0"] = "and then enter the expressway !STREET! towards !SIGNPOST!",
	
	["j00o00c0"] = "and then immediately take the middle lane !NG_COMMAND_2! towards !SIGNPOST!",
	
	["h00o0d00"] = "and then take the middle lane !NG_COMMAND_2! onto !STREET!",
	
	["bl0o0e00"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , take exit !EXIT_NUMBER!",
	
	["bl0via00"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["c00uf000"] = "At the end of the road make a sharp left !NG_COMMAND_1! , and enter the highway",
	
	["a00uf000"] = "Make a sharp left !NG_COMMAND_1! , and enter the highway",
	
	["bl0p0ed0"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST!",
	
	["j000i000"] = "and then immediately enter the expressway !NG_COMMAND_2!",
	
	["c00vfa00"] = "At the end of the road turn left !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["h000i000"] = "and then enter the expressway !NG_COMMAND_2!",
	
	["bl0qfac0"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["bl0w0000"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1!",
	
	["a000i000"] = "Enter the expressway",
	
	["bl0xia00"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["bl0n0ed0"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST!",
	
	["j00u00c0"] = "and then immediately make a sharp left !NG_COMMAND_2! towards !SIGNPOST!",
	
	["a00n000g"] = "Go straight ahead !NG_COMMAND_1! , take the exit",
	
	["c00uia00"] = "At the end of the road make a sharp left !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["h00u00c0"] = "and then make a sharp left !NG_COMMAND_2! towards !SIGNPOST!",
	
	["bl0s0d00"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! onto !STREET!",
	
	["bl0tf000"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1! , and enter the highway",
	
	["bl0q0000"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1!",
	
	["h00g0000"] = "and then take the exit !NG_COMMAND_2!",
	
	["c00s0000"] = "At the end of the road make a sharp right !NG_COMMAND_1!",
	
	["h00e0000"] = "and then make a u turn !NG_COMMAND_2!",
	
	["a00s0000"] = "Make a sharp right !NG_COMMAND_1!",
	
	["j00p0d00"] = "and then immediately keep right !NG_COMMAND_2! onto !STREET!",
	
	["c00e0000"] = "At the end of the road make a u turn !NG_COMMAND_1!",
	
	["h00s0000"] = "and then make a sharp right !NG_COMMAND_2!",
	
	["c00u0c00"] = "At the end of the road make a sharp left !NG_COMMAND_1! towards !STREET!",
	
	["bl0tf0c0"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["c00wfac0"] = "At the end of the road bear left !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["c00wi000"] = "At the end of the road bear left !NG_COMMAND_1! , and enter the expressway",
	
	["bl0ofa00"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["bl0ti000"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1! , and enter the expressway",
	
	["c00qiac0"] = "At the end of the road bear right !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["a00r000g"] = "Turn right !NG_COMMAND_1! take the exit",
	
	["bl0qi000"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! , and enter the expressway",
	
	["bl0q0d00"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! onto !STREET!",
	
	["bl0p00fg"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! and take the exit",
	
	["c00s0c00"] = "At the end of the road make a sharp right !NG_COMMAND_1! towards !STREET!",
	
	["a00vi000"] = "Turn left !NG_COMMAND_1! , and enter the expressway",
	
	["c00vi000"] = "At the end of the road turn left !NG_COMMAND_1! , and enter the expressway",
	
	["a00v00fg"] = "Turn left !NG_COMMAND_1! and take the exit",
	
	["bl0xfa00"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["bl0c0zc0"] = "After !DIST! !UNIT! , !EXIT_NO_ROUNDABOUT! at the traffic circle  towards !SIGNPOST!",
	
	["c00tf0c0"] = "At the end of the road make a u turn !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["bl0si0c0"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["c00b0b00"] = "At the end of the road you'll reach a stopover !NG_COMMAND_1! on !STREET!",
	
	["bz0w0000"] = "After the junction bear left !NG_COMMAND_1!",
	
	["c00qfac0"] = "At the end of the road bear right !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["bl0s0000"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1!",
	
	["h00v0ac0"] = "and then turn left !STREET! towards !SIGNPOST!",
	
	["h00p0a00"] = "and then keep right !STREET!",
	
	["c00siac0"] = "At the end of the road make a sharp right !NG_COMMAND_1! , and enter the expressway on !STREET! towards !SIGNPOST!",
	
	["c00vf0c0"] = "At the end of the road turn left !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["bl0tfa00"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["c00viac0"] = "At the end of the road turn left !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["h000ec00"] = "and then make a u turn !NG_COMMAND_2! towards !STREET!",
	
	["j000ec00"] = "and then immediately make a u turn !NG_COMMAND_2! towards !STREET!",
	
	["a00d0d00"] = "Take the exit !NG_COMMAND_1! onto !STREET!",
	
	["h000g000"] = "and then take the exit !NG_COMMAND_2!",
	
	["j00o0ac0"] = "and then immediately take the middle lane, !STREET! towards !SIGNPOST!",
	
	["j000g000"] = "and then immediately take the exit !NG_COMMAND_2!",
	
	["c00qia00"] = "At the end of the road bear right !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["h000fac0"] = "and then enter the highway !STREET! towards !SIGNPOST!",
	
	["bl0c0zb0"] = "After !DIST! !UNIT! , !EXIT_NO_ROUNDABOUT! at the traffic circle onto !STREET!",
	
	["bl0d0d00"] = "After !DIST! !UNIT! , take the exit !NG_COMMAND_1! onto !STREET!",
	
	["a00ri000"] = "Turn right !NG_COMMAND_1! , and enter the expressway",
	
	["c00ri000"] = "At the end of the road turn right !NG_COMMAND_1! , and enter the expressway",
	
	["y0000000"] = "You've reached your destination",
	
	["yp000000"] = "You've reached your destination. The destination is on your right",
	
	["yq000000"] = "You've reached your destination. The destination is on your left",
	
	["x0000000"] = "!SET_AUDIO!beep16K.wav!SET_AUDIO!",
	
	-- ["w0000000"] = "Route recalculation",
	
	["v0000000"] = "!SET_AUDIO!beep16K.wav!SET_AUDIO! There's a safety camera ahead",
	
	["000c0z00"] = "!EXIT_NO_ROUNDABOUT! at the traffic circle ",
	
	["a00nf000"] = "Go straight ahead !NG_COMMAND_1! enter the highway",
	
	["zr000000"] = "You've reached a stopover, on your right",
	
	["zs000000"] = "You've reached a stopover, on your left",
	
	["z0000000"] = "You've reached a stopover",
	
	["q0000000"] = "Your GPS signal has been restored",
	
	["p0000000"] = "Your GPS signal was lost",
	
	["c00sfa00"] = "At the end of the road make a sharp right !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["u0000000"] = "I couldn't find a detour around traffic",
	
	["t0000000"] = "Traffic on route, do you want to detour?",
	
	["r0000000"] = "!SET_AUDIO!speeding_beep16K.wav!SET_AUDIO!",
	
	["c00rfa00"] = "At the end of the road turn right !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["j00n0a00"] = "and then immediately go straight ahead !STREET!",
	
	["c00qf0c0"] = "At the end of the road bear right !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["bl0riac0"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["bl0pfac0"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["c00tf000"] = "At the end of the road make a u turn !NG_COMMAND_1! , and enter the highway",
	
	["a00ti000"] = "Make a u turn !NG_COMMAND_1! , and enter the expressway",
	
	["a00tf000"] = "Make a u turn !NG_COMMAND_1! , and enter the highway",
	
	["bl0b0b00"] = "After !DIST! !UNIT! , you'll reach a stopover !NG_COMMAND_1! on !STREET!",
	
	["j00y0000"] = "and then immediately take the second right !NG_COMMAND_2!",
	
	["h00y0000"] = "and then take the second right !NG_COMMAND_2!",
	
	["bl00i000"] = "After !DIST! !UNIT! , and enter the expressway",
	
	["bl0pia00"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["a000f000"] = "Enter the highway",
	
	["c00si000"] = "At the end of the road make a sharp right !NG_COMMAND_1! , and enter the expressway",
	
	["bl0oia00"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["c00w0d00"] = "At the end of the road bear left !NG_COMMAND_1! onto !STREET!",
	
	["h00w0000"] = "and then bear left !NG_COMMAND_2!",
	
	["j00w0000"] = "and then immediately bear left !NG_COMMAND_2!",
	
	["j00y0d00"] = "and then immediately take the second right !NG_COMMAND_2! onto !STREET!",
	
	["h00y0d00"] = "and then take the second right !NG_COMMAND_2! onto !STREET!",
	
	["j000f000"] = "and then immediately enter the highway !NG_COMMAND_2!",
	
	["a00si000"] = "Make a sharp right !NG_COMMAND_1! , and enter the expressway",
	
	["a00w0000"] = "Bear left !NG_COMMAND_1!",
	
	["bz0s0000"] = "After the junction make a sharp right !NG_COMMAND_1!",
	
	["c00w0000"] = "At the end of the road bear left !NG_COMMAND_1!",
	
	["h000f000"] = "and then enter the highway !NG_COMMAND_2!",
	
	["bl0wi000"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! , and enter the expressway",
	
	["a00p0000"] = "Keep right !NG_COMMAND_1!",
	
	["j00x0d00"] = "and then immediately keep left !NG_COMMAND_2! onto !STREET!",
	
	["j00r0ac0"] = "and then immediately turn right !STREET! towards !SIGNPOST!",
	
	["bl0wfa00"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["h00p0000"] = "and then keep right !NG_COMMAND_2!",
	
	["bl0xf000"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , and enter the highway",
	
	["h000gc00"] = "and then take the exit !NG_COMMAND_2! towards !STREET!",
	
	["c00ti0c0"] = "At the end of the road make a u turn !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["j00z0d00"] = "and then immediately take the second left !NG_COMMAND_2! onto !STREET!",
	
	["bl0p0edz"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST! and continue on !STREET_2! towards !SIGNPOST_2!",
	
	["h00o0c00"] = "and then take the middle lane !NG_COMMAND_2! towards !STREET!",
	
	["bl0nf0c0"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["bl0of0c0"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["bl0w0c00"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! towards !STREET!",
	
	["h00z0d00"] = "and then take the second left !NG_COMMAND_2! onto !STREET!",
	
	["bl0r0000"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1!",
	
	["bl0p0e0x"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , take exit !EXIT_NUMBER! towards !STREET_2! !SIGNPOST_2!",
	
	["bz0v0000"] = "After the junction turn left !NG_COMMAND_1!",
	
	["bl0q000x"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["c00wia00"] = "At the end of the road bear left !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["h000d000"] = "and then take the exit !NG_COMMAND_2!",
	
	["c00t0d00"] = "At the end of the road make a u turn !NG_COMMAND_1! onto !STREET!",
	
	["h00m000x"] = "and then take the ferry towards !STREET_2! !SIGNPOST_2!",
	
	["h000mc00"] = "and then take the ferry !NG_COMMAND_2! towards !STREET!",
	
	["h000a000"] = "and then you'll arrive at your destination !NG_COMMAND_2!",
	
	["h000dc00"] = "and then take the exit !NG_COMMAND_2! towards !STREET!",
	
	["h00p00c0"] = "and then keep right !NG_COMMAND_2! towards !SIGNPOST!",
	
	["j00q00c0"] = "and then immediately bear right !NG_COMMAND_2! towards !SIGNPOST!",
	
	["bl0wf000"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! , and enter the highway",
	
	["j00n0000"] = "and then immediately go straight ahead !NG_COMMAND_2!",
	
	["j00t00c0"] = "and then immediately make a u turn !NG_COMMAND_2! towards !SIGNPOST!",
	
	["bl0siac0"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["el000000"] = "Follow the course of the road for !DIST! !UNIT!",
	
	["dl000000"] = "Follow the highway !STREET! for !DIST! !UNIT!",
	
	["j00p00c0"] = "and then immediately keep right !NG_COMMAND_2! towards !SIGNPOST!",
	
	["j00x00c0"] = "and then immediately keep left !NG_COMMAND_2! towards !SIGNPOST!",
	
	["h00f0ac0"] = "and then enter the highway !STREET! towards !SIGNPOST!",
	
	["j00s00c0"] = "and then immediately make a sharp right !NG_COMMAND_2! towards !SIGNPOST!",
	
	["j00w00c0"] = "and then immediately bear left !NG_COMMAND_2! towards !SIGNPOST!",
	
	["bl0n0edz"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! take exit !EXIT_NUMBER! !SIGNPOST! and continue on !STREET_2! towards !SIGNPOST_2!",
	
	["j00v00c0"] = "and then immediately turn left !NG_COMMAND_2! towards !SIGNPOST!",
	
	["j000i0c0"] = "and then immediately enter the expressway !NG_COMMAND_2! towards !SIGNPOST!",
	
	["j000f0c0"] = "and then immediately enter the highway !NG_COMMAND_2! towards !SIGNPOST!",
	
	["h00z0000"] = "and then take the second left !NG_COMMAND_2!",
	
	["c00w0c00"] = "At the end of the road bear left !NG_COMMAND_1! towards !STREET!",
	
	["j00z0000"] = "and then immediately take the second left !NG_COMMAND_2!",
	
	["a00xi000"] = "Keep left !NG_COMMAND_1! enter the expressway",
	
	["j000m000"] = "and then immediately take the ferry !NG_COMMAND_2!",
	
	["bl0r0d00"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! onto !STREET!",
	
	["h00w0c00"] = "and then bear left !NG_COMMAND_2! towards !STREET!",
	
	["j000d000"] = "and then immediately take the exit !NG_COMMAND_2!",
	
	["h00x00c0"] = "and then keep left !NG_COMMAND_2! towards !SIGNPOST!",
	
	["j000mc00"] = "and then immediately take the ferry !NG_COMMAND_2! towards !STREET!",
	
	["h000j000"] = "and then take the exit !NG_COMMAND_2!",
	
	["j000dd00"] = "and then immediately take the exit !NG_COMMAND_2! onto !STREET!",
	
	["j000j000"] = "and then immediately take the exit !NG_COMMAND_2!",
	
	["bl0vfa00"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["bl0ni000"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , and enter the expressway",
	
	["h00n0000"] = "and then go straight ahead !NG_COMMAND_2!",
	
	["bz0x0000"] = "After the junction keep left !NG_COMMAND_1!",
	
	["000l0000"] = "drive to nearest road",
	
	["j00x0c00"] = "and then immediately keep left !NG_COMMAND_2! towards !STREET!",
	
	["j000fac0"] = "and then immediately enter the highway !STREET! towards !SIGNPOST!",
	
	["j000fa00"] = "and then immediately enter the highway !STREET!",
	
	["j00z0c00"] = "and then immediately take the second left !NG_COMMAND_2! towards !STREET!",
	
	["a00n0000"] = "Go straight ahead !NG_COMMAND_1!",
	
	["j00w0c00"] = "and then immediately bear left !NG_COMMAND_2! towards !STREET!",
	
	["j00z0a00"] = "and then immediately take the second left , !STREET!",
	
	["j00t0ac0"] = "and then immediately make a u turn, !STREET! towards !SIGNPOST!",
	
	["bl0u0c00"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! towards !STREET!",
	
	["j00p0ac0"] = "and then immediately keep right, !STREET! towards !SIGNPOST!",
	
	["j00v0ac0"] = "and then immediately turn left, !STREET! towards !SIGNPOST!",
	
	["j00u0d00"] = "and then immediately make a sharp left !NG_COMMAND_2! onto !STREET!",
	
	["j00n0c00"] = "and then immediately go straight ahead !NG_COMMAND_2! towards !STREET!",
	
	["bl0tfac0"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["c00ti000"] = "At the end of the road make a u turn !NG_COMMAND_1! , and enter the expressway",
	
	["bl0vfac0"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["c00u0d00"] = "At the end of the road make a sharp left !NG_COMMAND_1! onto !STREET!",
	
	["h00z0c00"] = "and then take the second left !NG_COMMAND_2! towards !STREET!",
	
	["j00q0000"] = "and then immediately bear right !NG_COMMAND_2!",
	
	["h00n00c0"] = "and then go straight ahead !NG_COMMAND_2! towards !SIGNPOST!",
	
	["bz0r0000"] = "After the junction turn right !NG_COMMAND_1!",
	
	["j00n00c0"] = "and then immediately go straight ahead !NG_COMMAND_2! towards !SIGNPOST!",
	
	["j00t0d00"] = "and then immediately make a u turn !NG_COMMAND_2! onto !STREET!",
	
	["j00t0000"] = "and then immediately make a u turn !NG_COMMAND_2!",
	
	["bl0p0e00"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , take exit !EXIT_NUMBER!",
	
	["j000ia00"] = "and then immediately enter the expressway !STREET!",
	
	["h00x0ac0"] = "and then keep left !NG_COMMAND_2! , !STREET! towards !SIGNPOST!",
	
	["j00r0c00"] = "and then immediately turn right !NG_COMMAND_2! towards !STREET!",
	
	["j00r0d00"] = "and then immediately turn right !NG_COMMAND_2! onto !STREET!",
	
	["bl0wia00"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["bl0si000"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! , and enter the expressway",
	
	["j00r00c0"] = "and then immediately turn right !NG_COMMAND_2! towards !SIGNPOST!",
	
	["j00s0c00"] = "and then immediately make a sharp right !NG_COMMAND_2! towards !STREET!",
	
	["h00r00c0"] = "and then turn right !NG_COMMAND_2! towards !SIGNPOST!",
	
	["h000ia00"] = "and then enter the expressway , !STREET!",
	
	["a00xf000"] = "Keep left !NG_COMMAND_1! , and enter the highway",
	
	["bl0ui000"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! , and enter the expressway",
	
	["j00s0000"] = "and then immediately make a sharp right !NG_COMMAND_2!",
	
	["j00u0c00"] = "and then immediately make a sharp left !NG_COMMAND_2! towards !STREET!",
	
	["j00u0000"] = "and then immediately make a sharp left !NG_COMMAND_2!",
	
	["bl0x0edx"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST! towards !STREET_2! !SIGNPOST_2!",
	
	["j00y0c00"] = "and then immediately take the second right !NG_COMMAND_2! towards !STREET!",
	
	["j00w0d00"] = "and then immediately bear left !NG_COMMAND_2! onto !STREET!",
	
	["h00y0c00"] = "and then take the second right !NG_COMMAND_2! towards !STREET!",
	
	["j00v0c00"] = "and then immediately turn left !NG_COMMAND_2! towards !STREET!",
	
	["j00x0a00"] = "and then immediately keep left, !STREET!",
	
	["j00o0c00"] = "and then immediately take the middle lane !NG_COMMAND_2! towards !STREET!",
	
	["bz0o0000"] = "After the junction take the middle lane !NG_COMMAND_1!",
	
	["j00o0d00"] = "and then immediately take the middle lane !NG_COMMAND_2! onto !STREET!",
	
	["c00u0000"] = "At the end of the road make a sharp left !NG_COMMAND_1!",
	
	["h00u0000"] = "and then make a sharp left !NG_COMMAND_2!",
	
	["j00v0d00"] = "and then immediately turn left !NG_COMMAND_2! onto !STREET!",
	
	["j00t0a00"] = "and then immediately make a u turn, !STREET!",
	
	["j00p0a00"] = "and then immediately keep right, !STREET!",
	
	["j00n0ac0"] = "and then immediately go straight ahead, !STREET! towards !SIGNPOST!",
	
	["bl0o00fg"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! and take the exit",
	
	["j00r0a00"] = "and then immediately turn right, !STREET!",
	
	["bl0rf0c0"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["bl0xf0c0"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["a00u0000"] = "Make a sharp left !NG_COMMAND_1!",
	
	["c00v0d00"] = "At the end of the road turn left !NG_COMMAND_1! onto !STREET!",
	
	["h00o0ac0"] = "and then take the middle lane, !STREET! towards !SIGNPOST!",
	
	["bl0d0c00"] = "After !DIST! !UNIT! , take the exit !NG_COMMAND_1! towards !STREET!",
	
	["bl0vi0c0"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["j00w0a00"] = "and then immediately bear left, !STREET!",
	
	["bl0n0000"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1!",
	
	["h000bb00"] = "and then you'll reach a stopover !NG_COMMAND_2! on !STREET!",
	
	["h00n0ac0"] = "and then go straight ahead, !STREET! towards !SIGNPOST!",
	
	["c00wfa00"] = "At the end of the road bear left !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["h00x0000"] = "and then keep left !NG_COMMAND_2!",
	
	["h00q0000"] = "and then bear right !NG_COMMAND_2!",
	
	["h00r0000"] = "and then turn right !NG_COMMAND_2!",
	
	["h00a0000"] = "and then you'll arrive at your destination !NG_COMMAND_2!",
	
	["bl0o000x"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["h000jc00"] = "and then take the exit !NG_COMMAND_2! towards !STREET!",
	
	["bl0qf000"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! , and enter the highway",
	
	["h00x0d00"] = "and then keep left !NG_COMMAND_2! onto !STREET!",
	
	["bl0n000g"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , take the exit",
	
	["h000gd00"] = "and then take the exit !NG_COMMAND_2! onto !STREET!",
	
	["h00w0d00"] = "and then bear left !NG_COMMAND_2! onto !STREET!",
	
	["h00s0d00"] = "and then make a sharp right !NG_COMMAND_2! onto !STREET!",
	
	["a00o00fg"] = "Take the middle lane !NG_COMMAND_1! and take the exit",
	
	["h000jd00"] = "and then take the exit !NG_COMMAND_2! onto !STREET!",
	
	["c00rfac0"] = "At the end of the road turn right !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["h00n0d00"] = "and then go straight ahead !NG_COMMAND_2! onto !STREET!",
	
	["c00v000x"] = "At the end of the road turn left !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["h00n0c00"] = "and then go straight ahead !NG_COMMAND_2! towards !STREET!",
	
	["h00t0d00"] = "and then make a u turn !NG_COMMAND_2! onto !STREET!",
	
	["bl0vf000"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! enter the highway",
	
	["bl0pf0c0"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["h00u0d00"] = "and then make a sharp left !NG_COMMAND_2! onto !STREET!",
	
	["h00u0c00"] = "and then make a sharp left !NG_COMMAND_2! towards !STREET!",
	
	["h00v0d00"] = "and then turn left !NG_COMMAND_2! onto !STREET!",
	
	["bl0nfa00"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["bl0ria00"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["bl0of000"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , and enter the highway",
	
	["h00p0d00"] = "and then keep right !NG_COMMAND_2! onto !STREET!",
	
	["bl0n0d00"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! onto !STREET!",
	
	["h00p0c00"] = "and then keep right !NG_COMMAND_2! towards !STREET!",
	
	["h00q0d00"] = "and then bear right !NG_COMMAND_2! onto !STREET!",
	
	["bl0n0e0x"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , take exit !EXIT_NUMBER! towards !STREET_2! !SIGNPOST_2!",
	
	["c00rf0c0"] = "At the end of the road turn right !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["h00s0c00"] = "and then make a sharp right !NG_COMMAND_2! towards !STREET!",
	
	["h00r0d00"] = "and then turn right !NG_COMMAND_2! onto !STREET!",
	
	["bl0vf0c0"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["c00wf000"] = "At the end of the road bear left !NG_COMMAND_1! , and enter the highway",
	
	["h000iac0"] = "and then enter the expressway !STREET! towards !SIGNPOST!",
	
	["a00x0000"] = "Keep left !NG_COMMAND_1!",
	
	["c00wiac0"] = "At the end of the road bear left !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["h000f0c0"] = "and then enter the highway !NG_COMMAND_2! towards !SIGNPOST!",
	
	["bl0xi0c0"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["c00ri0c0"] = "At the end of the road turn right !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["h000fa00"] = "and then enter the highway !STREET!",
	
	["h00n0a00"] = "and then go straight ahead, !STREET!",
	
	["j00x0000"] = "and then immediately keep left !NG_COMMAND_2!",
	
	["c00w000x"] = "At the end of the road bear left !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["h00t0ac0"] = "and then make a u turn, !STREET! towards !SIGNPOST!",
	
	["h00o00c0"] = "and then take the middle lane !NG_COMMAND_2! towards !SIGNPOST!",
	
	["j00s0a00"] = "and then immediately make a sharp right, !STREET!",
	
	["c00sia00"] = "At the end of the road make a sharp right !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["h00x0a00"] = "and then keep left, !STREET!",
	
	["a00sf000"] = "Make a sharp right !NG_COMMAND_1! , and enter the highway",
	
	["h00t00c0"] = "and then make a u turn !NG_COMMAND_2! towards !SIGNPOST!",
	
	["c00sf000"] = "At the end of the road make a sharp right !NG_COMMAND_1! , and enter the highway",
	
	["bl0u0000"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1!",
	
	["c00tia00"] = "At the end of the road make a u turn !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["h00w00c0"] = "and then bear left !NG_COMMAND_2! towards !SIGNPOST!",
	
	["h00u0a00"] = "and then make a sharp left, !STREET!",
	
	["bl0o000g"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , take the exit",
	
	["c00si0c0"] = "At the end of the road make a sharp right !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["c00riac0"] = "At the end of the road turn right !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["h00q00c0"] = "and then bear right !NG_COMMAND_2! towards !SIGNPOST!",
	
	["h00s0a00"] = "and then make a sharp right, !STREET!",
	
	["bl0v0000"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1!",
	
	["bl0o0e0z"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , take exit !EXIT_NUMBER! and continue on !STREET_2! towards !SIGNPOST_2!",
	
	["h00s0ac0"] = "and then make a sharp right, !STREET! towards !SIGNPOST!",
	
	["h00r0ac0"] = "and then turn right onto !STREET! towards !SIGNPOST!",
	
	["h000czb0"] = "and then !EXIT_NO_ROUNDABOUT! at the traffic circle onto !STREET!",
	
	["h00j0000"] = "and then take the exit !NG_COMMAND_2!",
	
	["h00c0z00"] = "and then !EXIT_NO_ROUNDABOUT! at the traffic circle",
	
	["bl0x0c00"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! towards !STREET!",
	
	["bl0x000x"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["bl0v000x"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["bz0t0000"] = "After the junction make a u turn !NG_COMMAND_1!",
	
	["bl0pfa00"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["h00f0000"] = "and then enter the highway !NG_COMMAND_2!",
	
	["bl0a0000"] = "After !DIST! !UNIT! , you'll arrive at your destination !NG_COMMAND_1!",
	
	["h000dd00"] = "and then take the exit !NG_COMMAND_2! onto !STREET!",
	
	["bl0wf0c0"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["h000cz00"] = "and then !EXIT_NO_ROUNDABOUT! at the traffic circle",
	
	["h000m000"] = "and then take the ferry !NG_COMMAND_2!",
	
	["h00i0a00"] = "and then enter the expressway !STREET!",
	
	["bl0xi000"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , and enter the expressway",
	
	["h00f00c0"] = "and then enter the highway towards !SIGNPOST!",
	
	["h00f0a00"] = "and then enter the highway !STREET!",
	
	["j00r0000"] = "and then immediately turn right !NG_COMMAND_2!",
	
	["a00x00fg"] = "Keep left !NG_COMMAND_1! and take the exit",
	
	["bl0r000x"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["bl0o0c00"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! towards !STREET!",
	
	["bl0x000g"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , take the exit",
	
	["bl0p000g"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , take the exit",
	
	["h00t0a00"] = "and then make a u turn, !STREET!",
	
	["a00r0000"] = "Turn right !NG_COMMAND_1!",
	
	["h00v00c0"] = "and then turn left !NG_COMMAND_2! towards !SIGNPOST!",
	
	["c00r0000"] = "At the end of the road turn right !NG_COMMAND_1!",
	
	["bl0p000x"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["bl0s000x"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["bl0u000x"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["c00qfa00"] = "At the end of the road bear right !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["bl0x0d00"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! onto !STREET!",
	
	["h00i0000"] = "and then enter the expressway !NG_COMMAND_2!",
	
	["bl0n0e0y"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , take exit !EXIT_NUMBER! and continue on !STREET_2!",
	
	["bl0rfa00"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["bl00iac0"] = "After !DIST! !UNIT! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["bl0xiac0"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["a00wi000"] = "Bear left !NG_COMMAND_1! , and enter the expressway",
	
	["bl0x0e0y"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! take exit !EXIT_NUMBER! and continue on !STREET_2!",
	
	["c00vf000"] = "At the end of the road turn left !NG_COMMAND_1! , and enter the highway",
	
	["c00sf0c0"] = "At the end of the road make a sharp right !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["bl0w000x"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["h000czc0"] = "and then !EXIT_NO_ROUNDABOUT! at the traffic circle towards !SIGNPOST!",
	
	["c00uiac0"] = "At the end of the road make a sharp left !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["bl0x0e00"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , take exit !EXIT_NUMBER!",
	
	["j00p0c00"] = "and then immediately keep right !NG_COMMAND_2! towards !STREET!",
	
	["bl0p0e0z"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , take exit !EXIT_NUMBER! and continue on !STREET_2! towards !SIGNPOST_2!",
	
	["bl000e0z"] = "After !DIST! !UNIT! , take exit !EXIT_NUMBER! and continue on !STREET_2! towards !SIGNPOST_2!",
	
	["bl000e00"] = "After !DIST! !UNIT! , take exit !EXIT_NUMBER!",
	
	["bl00000g"] = "After !DIST! !UNIT! , take the exit !NG_COMMAND_1!",
	
	["bl0sfa00"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["a00p000g"] = "Keep right !NG_COMMAND_1! , take the exit",
	
	["bl0u0d00"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! onto !STREET!",
	
	["a00oi000"] = "Take the middle lane !NG_COMMAND_1! , and enter the expressway",
	
	["bl0p0d00"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! onto !STREET!",
	
	["bl0t0000"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1!",
	
	["bl0ufa00"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["c00vfac0"] = "At the end of the road turn left !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["bl0qia00"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["a00pf000"] = "Keep right !NG_COMMAND_1! enter the highway",
	
	["bl0x0e0x"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , take exit !EXIT_NUMBER! towards !STREET_2! !SIGNPOST_2!",
	
	["j000iac0"] = "and then immediately enter the expressway !STREET! towards !SIGNPOST!",
	
	["j00s0d00"] = "and then immediately make a sharp right !NG_COMMAND_2! onto !STREET!",
	
	["bl0p0e0y"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , take exit !EXIT_NUMBER! and continue on !STREET_2!",
	
	["a00wf000"] = "Bear left !NG_COMMAND_1! , and enter the highway",
	
	["a00vf000"] = "Turn left !NG_COMMAND_1! , and enter the highway",
	
	["a00o000g"] = "Take the middle lane !NG_COMMAND_1! , take the exit",
	
	["bl0ui0c0"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["c00s0d00"] = "At the end of the road make a sharp right !NG_COMMAND_1! onto !STREET!",
	
	["a00of000"] = "Take the middle lane !NG_COMMAND_1! enter the highway",
	
	["c00tiac0"] = "At the end of the road make a u turn !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["c00t0c00"] = "At the end of the road make a u turn !NG_COMMAND_1! towards !STREET!",
	
	["c00tfac0"] = "At the end of the road make a u turn !NG_COMMAND_1! enter the highway !STREET! towards !SIGNPOST!",
	
	["a00qf000"] = "Bear right !NG_COMMAND_1! , and enter the highway",
	
	["c00q0d00"] = "At the end of the road bear right !NG_COMMAND_1! onto !STREET!",
	
	["bl0niac0"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["c00v0c00"] = "At the end of the road turn left !NG_COMMAND_1! towards !STREET!",
	
	["bl0nia00"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["bl0tiac0"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["j00t0c00"] = "and then immediately make a u turn !NG_COMMAND_2! towards !STREET!",
	
	["bl0pi0c0"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["h00t0c00"] = "and then make a u turn !NG_COMMAND_2! towards !STREET!",
	
	["bl0o0ed0"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST!",
	
	["h00v0c00"] = "and then turn left !NG_COMMAND_2! towards !STREET!",
	
	["bl0ri0c0"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["bl0sfac0"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["c00u000x"] = "At the end of the road make a sharp left !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["bl0wiac0"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["bl0wi0c0"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["bl0viac0"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["bl0vi000"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! , and enter the expressway",
	
	["bl0oiac0"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["bl0p0edx"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST! towards !STREET_2! !SIGNPOST_2!",
	
	["bl0m0c00"] = "After !DIST! !UNIT! , take the ferry !NG_COMMAND_1! towards !STREET!",
	
	["bl0oi000"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , and enter the expressway",
	
	["bl0qfa00"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! , and enter the highway !STREET!",
	
	["bl0nfac0"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["bl0pf000"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , and enter the highway",
	
	["bl0xfac0"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["bl0rfac0"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["a00t0000"] = "Make a u turn !NG_COMMAND_1!",
	
	["c00qi000"] = "At the end of the road bear right !NG_COMMAND_1! , and enter the expressway",
	
	["bl0ufac0"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["a00qi000"] = "Bear right !NG_COMMAND_1! , and enter the expressway",
	
	["h00r0c00"] = "and then turn right !NG_COMMAND_2! towards !STREET!",
	
	["bl0p0c00"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! towards !STREET!",
	
	["bl0pi000"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! , and enter the expressway",
	
	["c00t000x"] = "At the end of the road make a u turn !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["c00r000x"] = "At the end of the road turn right !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["c00s000x"] = "At the end of the road make a sharp right !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["bl0o0e0y"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! , take exit !EXIT_NUMBER! and continue on !STREET_2!",
	
	["bl0uf000"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! , and enter the highway",
	
	["bl0uia00"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["c00ui000"] = "At the end of the road make a sharp left !NG_COMMAND_1! , and enter the expressway",
	
	["a00ui000"] = "Make a sharp left !NG_COMMAND_1! , and enter the expressway",
	
	["h00b0000"] = "and then you'll reach a stopover !NG_COMMAND_2!",
	
	["bl0qiac0"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! , and enter the expressway !STREET! towards !SIGNPOST!",
	
	["c00ria00"] = "At the end of the road turn right !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["bl0n0c00"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! towards !STREET!",
	
	["c00r0c00"] = "At the end of the road turn right !NG_COMMAND_1! towards !STREET!",
	
	["c00vi0c0"] = "At the end of the road turn left !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["h00p0ac0"] = "and then keep right, !STREET! towards !SIGNPOST!",
	
	["bl0t0d00"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1! onto !STREET!",
	
	["j00q0c00"] = "and then immediately bear right !NG_COMMAND_2! towards !STREET!",
	
	["c00ufac0"] = "At the end of the road make a sharp left !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["c00sfac0"] = "At the end of the road make a sharp right !NG_COMMAND_1! , and enter the highway !STREET! towards !SIGNPOST!",
	
	["bl0n0e00"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , take exit !EXIT_NUMBER!",
	
	["c00a0b00"] = "At the end of the road you'll arrive at your destination !NG_COMMAND_1! on !STREET!",
	
	["c00q000x"] = "At the end of the road bear right !NG_COMMAND_1! towards !STREET_2! !SIGNPOST_2!",
	
	["c00q0c00"] = "At the end of the road bear right !NG_COMMAND_1! towards !STREET!",
	
	["bl0x0000"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1!",
	
	["c00uf0c0"] = "At the end of the road make a sharp left !NG_COMMAND_1! enter the highway towards !SIGNPOST!",
	
	["j00n0d00"] = "and then immediately go straight ahead !NG_COMMAND_2! onto !STREET!",
	
	["h000i0c0"] = "and then enter the expressway !NG_COMMAND_2! towards !SIGNPOST!",
	
	["c00qf000"] = "At the end of the road bear right !NG_COMMAND_1! , and enter the highway",
	
	["h00q0c00"] = "and then bear right !NG_COMMAND_2! towards !STREET!",
	
	["bl0n0e0z"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , take exit !EXIT_NUMBER! and continue on !STREET_2! towards !SIGNPOST_2!",
	
	["bl0d0000"] = "After !DIST! !UNIT! , take the exit !NG_COMMAND_1!",
	
	["s0000000"] = "Traffic on route, I'm finding a detour",
	
	["bz0u0000"] = "After the junction make a sharp left !NG_COMMAND_1!",
	
	["bl0sf000"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! , and enter the highway",
	
	["c00wf0c0"] = "At the end of the road bear left !NG_COMMAND_1! , and enter the highway towards !SIGNPOST!",
	
	["bz0q0000"] = "After the junction bear right !NG_COMMAND_1!",
	
	["a00e0000"] = "Make a u turn !NG_COMMAND_1!",
	
	["bl0sia00"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["bl0x0edy"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST! and continue on !STREET_2!",
	
	["bl0tia00"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1! , and enter the expressway !STREET!",
	
	["bl0n0edx"] = "After !DIST! !UNIT! , go straight ahead !NG_COMMAND_1! , take exit !EXIT_NUMBER! !SIGNPOST! towards !STREET_2! !SIGNPOST_2!",
	
	["bl0p0000"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1!",
	
	["bl0o0d00"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! onto !STREET!",
	
	["bl0qi0c0"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! , and enter the expressway towards !SIGNPOST!",
	
	["j00u0a00"] = "and then immediately make a sharp left onto !STREET!",
	
	["blt00000"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1!",
	
	["blt00d00"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1! onto !STREET!",
	
	["blt00f00"] = "After !DIST! !UNIT! , make a u turn !NG_COMMAND_1! and continue on !STREET!",
	
	["a0t00d00"] = "Make a u turn !NG_COMMAND_1! onto !STREET!",
	
	["a0t00f00"] = "Make a u turn !NG_COMMAND_1! and continue on !STREET!",
	
	["ll0a0000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! you'll arrive at your destination",
	
	["ll0e0000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! make a u turn",
	
	["ll0b0000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! you'll reach a stopover",
	
	["llt00000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! make a u turn",
	
	["bl00fa00"] = "After !DIST! !UNIT! , and enter the highway !STREET!",
	
	["j00x0ac0"] = "and then immediately keep left !NG_COMMAND_2!, !STREET! towards !SIGNPOST!",
	
	["bl000e0y"] = "After !DIST! !UNIT! , take exit !EXIT_NUMBER! and continue on !STREET_2!",
	
	["bl000ed0"] = "After !DIST! !UNIT! , take exit !EXIT_NUMBER! !SIGNPOST!",
	
	["bl000edx"] = "After !DIST! !UNIT! , take exit !EXIT_NUMBER! !SIGNPOST! towards !STREET_2! !SIGNPOST_2!",
	
	["bl000edz"] = "After !DIST! !UNIT! , take exit !EXIT_NUMBER! !SIGNPOST! and continue on !STREET_2! towards !SIGNPOST_2!",
	
	["bl000edy"] = "After !DIST! !UNIT! , take exit !EXIT_NUMBER! !SIGNPOST! and continue on !STREET_2!",
	
	["bl0a000l"] = "After !DIST! !UNIT! , you'll arrive at your destination !NG_COMMAND_1! . The destination is on your left",
	
	["bl0a000r"] = "After !DIST! !UNIT! , you'll arrive at your destination !NG_COMMAND_1! . The destination is on your right",
	
	["bl0a0b0l"] = "After !DIST! !UNIT! , you'll arrive at your destination !NG_COMMAND_1! on !STREET! . The destination is on your left",
	
	["bl0a0b0r"] = "After !DIST! !UNIT! , you'll arrive at your destination !NG_COMMAND_1! on !STREET! . The destination is on your right",
	
	["bl00f0c0"] = "After !DIST! !UNIT! , and enter the highway towards !SIGNPOST!",
	
	["bl00i0c0"] = "After !DIST! !UNIT! , and enter the expressway towards !SIGNPOST!",
	
	["bl00ia00"] = "After !DIST! !UNIT! , and enter the expressway !STREET!",
	
	["000c0y00"] = "Enter the traffic circle",
	
	["bl0c0y00"] = "After !DIST! !UNIT! , and enter the traffic circle",
	
	["c00c0y00"] = "At the end of the road enter the traffic circle",
	
	["h000cy00"] = "and then enter the traffic circle",
	
	["bl0x000y"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! and continue on !STREET_2!",
	
	["bl0x000z"] = "After !DIST! !UNIT! , keep left !NG_COMMAND_1! and continue on !STREET_2! towards !SIGNPOST_2!",
	
	["bl0o000y"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! and continue on !STREET_2!",
	
	["bl0o000z"] = "After !DIST! !UNIT! , take the middle lane !NG_COMMAND_1! and continue on !STREET_2! towards !SIGNPOST_2!",
	
	["bl0p000y"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! and continue on !STREET_2!",
	
	["bl0p000z"] = "After !DIST! !UNIT! , keep right !NG_COMMAND_1! and continue on !STREET_2! towards !SIGNPOST_2!",
}

commands_ped = { 
	
	["00000000"] = " ",
	
	["blo00000"] = "After !DIST! !UNIT! , take the street in the middle !NG_COMMAND_1!",
	
	["blw00000"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1!",
	
	["blu00000"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1!",
	
	["bls00000"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1!",
	
	["blq00000"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1!",
	
	["blo00a00"] = "After !DIST! !UNIT! , take the street in the middle !NG_COMMAND_1! , !STREET!",
	
	["blv00f00"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! and continue on !STREET!",
	
	["blw00d00"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! onto !STREET!",
	
	["blw00f00"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! and continue on !STREET!",
	
	["blu00d00"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! onto !STREET!",
	
	["blu00f00"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! and continue on !STREET!",
	
	["bls00d00"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! onto !STREET!",
	
	["bls00f00"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! and continue on !STREET!",
	
	["blr00f00"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! and continue on !STREET!",
	
	["blq00d00"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! onto !STREET!",
	
	["blq00f00"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! and continue on !STREET!",
	
	["a0q00d00"] = "Bear right !NG_COMMAND_1! onto !STREET!",
	
	["a0s00d00"] = "Make a sharp right !NG_COMMAND_1! onto !STREET!",
	
	["a0w00d00"] = "Bear left !NG_COMMAND_1! onto !STREET!",
	
	["a0u00d00"] = "Make a sharp left !NG_COMMAND_1! onto !STREET!",
	
	["a0o00a00"] = "Take the street in the middle !NG_COMMAND_1! , !STREET!",
	
	["a0o00000"] = "Take the street in the middle !NG_COMMAND_1!",
	
	["a0v00f00"] = "Turn left !NG_COMMAND_1! and continue on !STREET!",
	
	["a0w00f00"] = "Bear left !NG_COMMAND_1! and continue on !STREET!",
	
	["a0u00f00"] = "Make a sharp left !NG_COMMAND_1! and continue on !STREET!",
	
	["a0s00f00"] = "Make a sharp right !NG_COMMAND_1! and continue on !STREET!",
	
	["a0r00f00"] = "Turn right !NG_COMMAND_1! and continue on !STREET!",
	
	["a0n00f00"] = "Walk straight ahead and continue on !STREET!",
	
	["a0q00f00"] = "Bear right !NG_COMMAND_1! and continue on !STREET!",
	
	["a0c00h00"] = "Walk around the traffic circle and turn onto !STREET!",
	
	["blr00o00"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! , go through the building",
	
	["blb00000"] = "After !DIST! !UNIT! , walk left around the traffic circle",
	
	["a0r00000"] = "Turn right !NG_COMMAND_1!",
	
	["a0v00000"] = "Turn left !NG_COMMAND_1!",
	
	["blp00a00"] = "After !DIST! !UNIT! , take the street on the right !NG_COMMAND_1! , !STREET!",
	
	["a0p00a00"] = "Take the street on the right !NG_COMMAND_1! , !STREET!",
	
	["blp00000"] = "After !DIST! !UNIT! , take the street on the right !NG_COMMAND_1!",
	
	["a0p00000"] = "Take the street on the right !NG_COMMAND_1!",
	
	["blx00a00"] = "After !DIST! !UNIT! , take the street on the left !NG_COMMAND_1! , !STREET!",
	
	["a0x00a00"] = "Take the street on the left !NG_COMMAND_1! onto !STREET!",
	
	["blx00000"] = "After !DIST! !UNIT! , take the street on the left !NG_COMMAND_1!",
	
	["a0x00000"] = "Take the street on the left !NG_COMMAND_1!",
	
	["blv00000"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1!",
	
	["blr00000"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1!",
	
	["blv00d00"] = "After !DIST! !UNIT! , !NG_COMMAND_1! turn left onto !STREET!",
	
	["blr00d00"] = "After !DIST! !UNIT! , !NG_COMMAND_1! turn right onto !STREET!",
	
	["a0v00d00"] = "Turn left onto !STREET!",
	
	["a0r00d00"] = "Turn right onto !STREET!",
	
	["a0c00j00"] = "Walk around the traffic circle !PED_TURN_NO!",
	
	["ll0m0000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! take the ferry",
	
	["llb00000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! walk left around the traffic circle",
	
	["llc00000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! walk around the traffic circle",
	
	["lla00000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! walk right around the traffic circle",
	
	["llo00000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! take the street in the middle",
	
	["llv00000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! turn left",
	
	["llw00000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! bear left",
	
	["llu00000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! make a sharp left",
	
	["lls00000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! make a sharp right",
	
	["llr00000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! turn right",
	
	["llx00000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! take the street on the left",
	
	["llp00000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! take the street on the right",
	
	["lln00000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! walk straight ahead",
	
	["llq00000"] = "and then After !DIST! !UNIT! , !NG_COMMAND_1! bear right",
	
	["bln000s0"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1! and take the elevator",
	
	["a0q00g00"] = "Bear right onto the footpath",
	
	["a0r00g00"] = "Turn right !NG_COMMAND_1! onto the footpath",
	
	["bln000r0"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1! and take the stairs",
	
	["a0a00h00"] = "Walk right around the traffic circle and turn onto !STREET!",
	
	["blr00q00"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! , cross the park",
	
	["bln00r00"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1! , take the stairs",
	
	["a0v000t0"] = "Turn left !NG_COMMAND_1! and take the escalator",
	
	["blo000s0"] = "After !DIST! !UNIT! , take the street in the middle !NG_COMMAND_1! and take the elevator",
	
	["bls00o00"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! , go through the building",
	
	["a0o00q00"] = "Take the street in the middle !NG_COMMAND_1! , cross the park",
	
	["blw00t00"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! , take the escalator",
	
	["a0w000t0"] = "Bear left !NG_COMMAND_1! and take the escalator",
	
	["blw000s0"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! and take the elevator",
	
	["a0b00j00"] = "Walk left around the traffic circle !PED_TURN_NO!",
	
	["a0b00h00"] = "Walk left around the traffic circle !NG_COMMAND_1! and turn onto !STREET!",
	
	["a0q00p00"] = "Bear right !NG_COMMAND_1! , cross the square",
	
	["f0000000"] = "Head !ORIENTATION!",
	
	["a0q00q00"] = "Bear right !NG_COMMAND_1! , cross the park",
	
	["blq000q0"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! and cross the park",
	
	["blw000o0"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! and go through the building",
	
	["blw00s00"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! , take the elevator",
	
	["a0u000t0"] = "Make a sharp left !NG_COMMAND_1! and take the escalator",
	
	["a0n000p0"] = "Walk straight ahead !NG_COMMAND_1! and cross the square",
	
	["a0p000o0"] = "Take the street on the right !NG_COMMAND_1! and go through the building",
	
	["blw000t0"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! and take the escalator",
	
	["a0v00r00"] = "Turn left !NG_COMMAND_1! take the stairs",
	
	["blo000q0"] = "After !DIST! !UNIT! , take the street in the middle !NG_COMMAND_1! and cross the park",
	
	["a0o000p0"] = "Take the street in the middle !NG_COMMAND_1! and cross the square",
	
	["a0q000o0"] = "Bear right !NG_COMMAND_1! and go through the building",
	
	["bln00q00"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1! , cross the park",
	
	["a0a00j00"] = "Walk right around the traffic circle !NG_COMMAND_1! !PED_TURN_NO!",
	
	["blq000t0"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! and take the escalator",
	
	["a0s000t0"] = "Make a sharp right !NG_COMMAND_1! and take the escalator",
	
	["a0o000o0"] = "Take the street in the middle !NG_COMMAND_1! and go through the building",
	
	["bln000o0"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1! and go through the building",
	
	["a0n00p00"] = "Walk straight ahead !NG_COMMAND_1! , cross the square",
	
	["bln000q0"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1! and cross the park",
	
	["blx000s0"] = "After !DIST! !UNIT! , take the street on the left !NG_COMMAND_1! and take the elevator",
	
	["blx00t00"] = "After !DIST! !UNIT! , take the street on the left !NG_COMMAND_1! , take the escalator",
	
	["a0q00t00"] = "Bear right !NG_COMMAND_1! , take the escalator",
	
	["blr000s0"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! and take the elevator",
	
	["blw000q0"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! and cross the park",
	
	["a0q00r00"] = "Bear right !NG_COMMAND_1! , take the stairs",
	
	["blq000r0"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! and take the stairs",
	
	["blq00r00"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! , take the stairs",
	
	["a0y00g00"] = "Turn !NG_COMMAND_1! onto the footpath",
	
	["blu000q0"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! and cross the park",
	
	["a0r000o0"] = "Turn right !NG_COMMAND_1! and go through the building",
	
	["blu000o0"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! and go through the building",
	
	["a0s00o00"] = "Make a sharp right !NG_COMMAND_1! , go through the building",
	
	["blu000t0"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! and take the escalator",
	
	["blo000r0"] = "After !DIST! !UNIT! , take the street in the middle !NG_COMMAND_1! and take the stairs",
	
	["bls000r0"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! and take the stairs",
	
	["blv000p0"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! and cross the square",
	
	["blr000q0"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! and cross the park",
	
	["a0v000r0"] = "Turn left !NG_COMMAND_1! and take the stairs",
	
	["a0x00t00"] = "Take the street on the left !NG_COMMAND_1! , take the escalator",
	
	["a0w00p00"] = "Bear left !NG_COMMAND_1! , cross the square",
	
	["a0s000q0"] = "Make a sharp right !NG_COMMAND_1! and cross the park",
	
	["blr000t0"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! and take the escalator",
	
	["blr000o0"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! and go through the building",
	
	["blp000q0"] = "After !DIST! !UNIT! , take the street on the right !NG_COMMAND_1! and cross the park",
	
	["bla00000"] = "After !DIST! !UNIT! , walk right around the traffic circle !NG_COMMAND_1!",
	
	["a0p000p0"] = "Take the street on the right !NG_COMMAND_1! and cross the square",
	
	["bls000o0"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! and go through the building",
	
	["a0n000r0"] = "Walk straight ahead !NG_COMMAND_1! and take the stairs",
	
	["a0s000o0"] = "Make a sharp right !NG_COMMAND_1! and go through the building",
	
	["a0r000s0"] = "Turn right !NG_COMMAND_1! and take the elevator",
	
	["a0q000s0"] = "Bear right !NG_COMMAND_1! and take the elevator",
	
	["blo00s00"] = "After !DIST! !UNIT! , take the street in the middle !NG_COMMAND_1! , take the elevator",
	
	["a0n00r00"] = "Walk straight ahead !NG_COMMAND_1! , take the stairs",
	
	["blr000r0"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! and take the stairs",
	
	["a0x000t0"] = "Take the street on the left !NG_COMMAND_1! and take the escalator",
	
	["blv000q0"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! and cross the park",
	
	["bln00t00"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1! , take the escalator",
	
	["a0u000r0"] = "Make a sharp left !NG_COMMAND_1! and take the stairs",
	
	["blp00q00"] = "After !DIST! !UNIT! , take the street on the right !NG_COMMAND_1! cross the park",
	
	["blp00p00"] = "After !DIST! !UNIT! , take the street on the right !NG_COMMAND_1! , cross the square",
	
	["bln00d00"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1! onto !STREET!",
	
	["a0p00r00"] = "Take the street on the right !NG_COMMAND_1! , take the stairs",
	
	["blq00p00"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! , cross the square",
	
	["blq00q00"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! , cross the park",
	
	["a0s00r00"] = "Make a sharp right !NG_COMMAND_1! , take the stairs",
	
	["a0y00000"] = "Turn !NG_COMMAND_1!",
	
	["a0v00p00"] = "Turn left !NG_COMMAND_1! , cross the square",
	
	["blv000o0"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! and go through the building",
	
	["a0s000s0"] = "Make a sharp right !NG_COMMAND_1! and take the elevator",
	
	["a0x00r00"] = "Take the street on the left !NG_COMMAND_1! , take the stairs",
	
	["a0q000p0"] = "Bear right !NG_COMMAND_1! and cross the square",
	
	["bln00000"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1!",
	
	["blx000p0"] = "After !DIST! !UNIT! , take the street on the left !NG_COMMAND_1! and cross the square",
	
	["a0v000q0"] = "Turn left !NG_COMMAND_1! and cross the park",
	
	["blu00r00"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! , take the stairs",
	
	["a0o000t0"] = "Take the street in the middle !NG_COMMAND_1! and take the escalator",
	
	["blu00g00"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! onto the footpath",
	
	["bln00p00"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1! , cross the square",
	
	["a0r000t0"] = "Turn right !NG_COMMAND_1! and take the escalator",
	
	["a0x00p00"] = "Take the street on the left !NG_COMMAND_1! , cross the square",
	
	["blx000q0"] = "After !DIST! !UNIT! , take the street on the left !NG_COMMAND_1! and cross the park",
	
	["blr00r00"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! , take the stairs",
	
	["a0s00q00"] = "Make a sharp right !NG_COMMAND_1! , cross the park",
	
	["a0o000q0"] = "Take the street in the middle !NG_COMMAND_1! and cross the park",
	
	["bls000t0"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! and take the escalator",
	
	["bls00p00"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! , cross the square",
	
	["bls000s0"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! and take the elevator",
	
	["bln000t0"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1! and take the escalator",
	
	["blx000o0"] = "After !DIST! !UNIT! , take the street on the left !NG_COMMAND_1! and go through the building",
	
	["a0p00p00"] = "Take the street on the right !NG_COMMAND_1! , cross the square",
	
	["a0p00s00"] = "Take the street on the right !NG_COMMAND_1! , take the elevator",
	
	["blv00p00"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! , cross the square",
	
	["blp00r00"] = "After !DIST! !UNIT! , take the street on the right !NG_COMMAND_1! , take the stairs",
	
	["bln00f00"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1! and continue on !STREET!",
	
	["a0w00r00"] = "Bear left !NG_COMMAND_1! , take the stairs",
	
	["a0v00o00"] = "Turn left !NG_COMMAND_1! , go through the building",
	
	["blo00t00"] = "After !DIST! !UNIT! , take the street in the middle !NG_COMMAND_1! , take the escalator",
	
	["blo00p00"] = "After !DIST! !UNIT! , take the street in the middle !NG_COMMAND_1! , cross the square",
	
	["a0n000s0"] = "Walk straight ahead !NG_COMMAND_1! and take the elevator",
	
	["bls00r00"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! , take the stairs",
	
	["blu000p0"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! and cross the square",
	
	["blx00q00"] = "After !DIST! !UNIT! , take the street on the left !NG_COMMAND_1! , cross the park",
	
	["bls00q00"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! , cross the park",
	
	["blu00q00"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! , cross the park",
	
	["a0u000q0"] = "Make a sharp left !NG_COMMAND_1! and cross the park",
	
	["blp000t0"] = "After !DIST! !UNIT! , take the street on the right !NG_COMMAND_1! and take the escalator",
	
	["a0w00q00"] = "Bear left !NG_COMMAND_1! , cross the park",
	
	["a0o00t00"] = "Take the street in the middle !NG_COMMAND_1! , take the escalator",
	
	["a0q000q0"] = "Bear right !NG_COMMAND_1! and cross the park",
	
	["blo00q00"] = "After !DIST! !UNIT! , take the street in the middle !NG_COMMAND_1! , cross the park",
	
	["a0w000p0"] = "Bear left !NG_COMMAND_1! and cross the square",
	
	["blx00r00"] = "After !DIST! !UNIT! , take the street on the left !NG_COMMAND_1! , take the stairs",
	
	["a0o00p00"] = "Take the street in the middle !NG_COMMAND_1! , cross the square",
	
	["blv00r00"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! , take the stairs",
	
	["blo00r00"] = "After !DIST! !UNIT! , take the street in the middle !NG_COMMAND_1! , take the stairs",
	
	["f00000c0"] = "Head !ORIENTATION! towards !SIGNPOST!",
	
	["a0w00o00"] = "Bear left !NG_COMMAND_1! , go through the building",
	
	["blv00t00"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! , take the escalator",
	
	["a0v000p0"] = "Turn left !NG_COMMAND_1! and cross the square",
	
	["blq00s00"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! take the elevator",
	
	["bls000p0"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! and cross the square",
	
	["blq00g00"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! onto the footpath",
	
	["a0u000o0"] = "Make a sharp left !NG_COMMAND_1! and go through the building",
	
	["blx00s00"] = "After !DIST! !UNIT! , take the street on the left !NG_COMMAND_1! , take the elevator",
	
	["blr00s00"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! , take the elevator",
	
	["bls00s00"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! , take the elevator",
	
	["blu00s00"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! , take the elevator",
	
	["blp00t00"] = "After !DIST! !UNIT! , take the street on the right !NG_COMMAND_1! , take the escalator",
	
	["a0v00s00"] = "Turn left !NG_COMMAND_1! , take the elevator",
	
	["a0x000o0"] = "Take the street on the left !NG_COMMAND_1! and go through the building",
	
	["a0p000q0"] = "Take the street on the right !NG_COMMAND_1! and cross the park",
	
	["blu00t00"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! , take the escalator",
	
	["blq00o00"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! , go through the building",
	
	["blp00o00"] = "After !DIST! !UNIT! , take the street on the right !NG_COMMAND_1! , go through the building",
	
	["blo000t0"] = "After !DIST! !UNIT! , take the street in the middle !NG_COMMAND_1! and take the escalator",
	
	["a0u00s00"] = "Make a sharp left !NG_COMMAND_1! , take the elevator",
	
	["blx00o00"] = "After !DIST! !UNIT! , take the street on the left !NG_COMMAND_1! , go through the building",
	
	["blu00o00"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! , go through the building",
	
	["blw00o00"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! , go through the building",
	
	["blv00o00"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! , go through the building",
	
	["blo00o00"] = "After !DIST! !UNIT! , take the street in the middle !NG_COMMAND_1! , go through the building",
	
	["blu00p00"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! , cross the square",
	
	["a0s000r0"] = "Make a sharp right !NG_COMMAND_1! and take the stairs",
	
	["a0r00p00"] = "Turn right !NG_COMMAND_1! , cross the square",
	
	["a0o000r0"] = "Take the street in the middle !NG_COMMAND_1! and take the stairs",
	
	["a0s00p00"] = "Make a sharp right !NG_COMMAND_1! , cross the square",
	
	["blv000r0"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! and take the stairs",
	
	["a0r000q0"] = "Turn right !NG_COMMAND_1! and cross the park",
	
	["a0r000r0"] = "Turn right !NG_COMMAND_1! and take the stairs",
	
	["bls000q0"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! and cross the park",
	
	["a0u00p00"] = "Make a sharp left !NG_COMMAND_1! , cross the square",
	
	["blw00r00"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! , take the stairs",
	
	["a0n00q00"] = "Walk straight ahead !NG_COMMAND_1! , cross the park",
	
	["blw000r0"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! and take the stairs",
	
	["a0p00q00"] = "Take the street on the right !NG_COMMAND_1! , cross the park",
	
	["blp000p0"] = "After !DIST! !UNIT! , take the street on the right !NG_COMMAND_1! and cross the square",
	
	["a0q000t0"] = "Bear right !NG_COMMAND_1! and take the escalator",
	
	["a0v000s0"] = "Turn left !NG_COMMAND_1! and take the elevator",
	
	["a0x00q00"] = "Take the street on the left !NG_COMMAND_1! , cross the park",
	
	["a0r00q00"] = "Turn right !NG_COMMAND_1! , cross the park",
	
	["blq000p0"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! and cross the square",
	
	["a0u00q00"] = "Make a sharp left !NG_COMMAND_1! , cross the park",
	
	["blw00q00"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! , cross the park",
	
	["a0v00q00"] = "Turn left !NG_COMMAND_1! , cross the park",
	
	["blp000o0"] = "After !DIST! !UNIT! , take the street on the right !NG_COMMAND_1! and go through the building",
	
	["a0r00r00"] = "Turn right !NG_COMMAND_1! , take the stairs",
	
	["a0v00g00"] = "Turn left !NG_COMMAND_1! onto the footpath",
	
	["a0u00r00"] = "Make a sharp left !NG_COMMAND_1! , take the stairs",
	
	["a0n000t0"] = "Walk straight ahead !NG_COMMAND_1! and take the escalator",
	
	["a0x000r0"] = "Take the street on the left !NG_COMMAND_1! and take the stairs",
	
	["blr000p0"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! and cross the square",
	
	["a0o00r00"] = "Take the street in the middle !NG_COMMAND_1! , take the stairs",
	
	["blq000o0"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! and go through the building",
	
	["a0q00s00"] = "Bear right !NG_COMMAND_1! , take the elevator",
	
	["a0r00s00"] = "Turn right !NG_COMMAND_1! , take the elevator",
	
	["a0q000r0"] = "Bear right !NG_COMMAND_1! and take the stairs",
	
	["blo000p0"] = "After !DIST! !UNIT! , take the street in the middle !NG_COMMAND_1! and cross the square",
	
	["blw00p00"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! , cross the square",
	
	["a0x00s00"] = "Take the street on the left !NG_COMMAND_1! , take the elevator",
	
	["a0n00d00"] = "Walk straight ahead !NG_COMMAND_1! onto !STREET!",
	
	["a0s00s00"] = "Make a sharp right !NG_COMMAND_1! , take the elevator",
	
	["a0w00s00"] = "Bear left !NG_COMMAND_1! , take the elevator",
	
	["blq00t00"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! , take the escalator",
	
	["a0o00s00"] = "Take the street in the middle !NG_COMMAND_1! , take the elevator",
	
	["a0n00t00"] = "Walk straight ahead !NG_COMMAND_1! , take the escalator",
	
	["bls00g00"] = "After !DIST! !UNIT! , make a sharp right !NG_COMMAND_1! onto the footpath",
	
	["blo000o0"] = "After !DIST! !UNIT! , take the street in the middle !NG_COMMAND_1! and go through the building",
	
	["a0r00o00"] = "Turn right !NG_COMMAND_1! , go through the building",
	
	["blv000t0"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! and take the escalator",
	
	["a0w000q0"] = "Bear left !NG_COMMAND_1! and cross the park",
	
	["a0r00t00"] = "Turn right !NG_COMMAND_1! , take the escalator",
	
	["a0s00t00"] = "Make a sharp right !NG_COMMAND_1! , take the escalator",
	
	["a0u00t00"] = "Make a sharp left !NG_COMMAND_1! , take the escalator",
	
	["a0v00t00"] = "Turn left !NG_COMMAND_1! , take the escalator",
	
	["a0q00o00"] = "Bear right !NG_COMMAND_1! , go through the building",
	
	["a0n00o00"] = "Walk straight ahead !NG_COMMAND_1! , go through the building",
	
	["a0p00o00"] = "Take the street on the right !NG_COMMAND_1! , go through the building",
	
	["blq000s0"] = "After !DIST! !UNIT! , bear right !NG_COMMAND_1! and take the elevator",
	
	["a0p000s0"] = "Take the street on the right !NG_COMMAND_1! and take the elevator",
	
	["blu000s0"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! and take the elevator",
	
	["a0x00o00"] = "Take the street on the left !NG_COMMAND_1! , go through the building",
	
	["a0p00t00"] = "Take the street on the right !NG_COMMAND_1! , take the escalator",
	
	["f0000bc0"] = "Head !ORIENTATION! on !STREET! towards !SIGNPOST!",
	
	["a0o00o00"] = "Take the street in the middle !NG_COMMAND_1! , go through the building",
	
	["a0x000p0"] = "Take the street on the left !NG_COMMAND_1! and cross the square",
	
	["a0r000p0"] = "Turn right !NG_COMMAND_1! and cross the square",
	
	["a0s000p0"] = "Make a sharp right !NG_COMMAND_1! and cross the square",
	
	["a0u000p0"] = "Make a sharp left !NG_COMMAND_1! and cross the square",
	
	["blv00q00"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! , cross the park",
	
	["a0n000q0"] = "Walk straight ahead !NG_COMMAND_1! and cross the park",
	
	["a0u00g00"] = "Make a sharp left onto the footpath",
	
	["a0w000o0"] = "Bear left !NG_COMMAND_1! and go through the building",
	
	["blr00t00"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! , take the escalator",
	
	["a0u000s0"] = "Make a sharp left !NG_COMMAND_1! and take the elevator",
	
	["a0p000r0"] = "Take the street on the right !NG_COMMAND_1! and take the stairs",
	
	["a0v000o0"] = "Turn left !NG_COMMAND_1! and go through the building",
	
	["blr00p00"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! , cross the square",
	
	["a0n00s00"] = "Walk straight ahead !NG_COMMAND_1! , take the elevator",
	
	["a0w000r0"] = "Bear left !NG_COMMAND_1! and take the stairs",
	
	["a0x000s0"] = "Take the street on the left !NG_COMMAND_1! and take the elevator",
	
	["a0w00g00"] = "Bear left onto the footpath",
	
	["a0o000s0"] = "Take the street in the middle !NG_COMMAND_1! and take the elevator",
	
	["blv000s0"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! and take the elevator",
	
	["blr00g00"] = "After !DIST! !UNIT! , turn right !NG_COMMAND_1! onto the footpath",
	
	["blp000r0"] = "After !DIST! !UNIT! , take the street on the right !NG_COMMAND_1! and take the stairs",
	
	["f0000b00"] = "Head !ORIENTATION! on !STREET!",
	
	["blx000r0"] = "After !DIST! !UNIT! , take the street on the left !NG_COMMAND_1! and take the stairs",
	
	["blw00g00"] = "After !DIST! !UNIT! , !NG_COMMAND_1! bear left onto the footpath",
	
	["a0n000o0"] = "Walk straight ahead !NG_COMMAND_1! and go through the building",
	
	["bln000p0"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1! and cross the square",
	
	["blc00000"] = "After !DIST! !UNIT! , walk around the traffic circle !NG_COMMAND_1!",
	
	["bln00s00"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1! , take the elevator",
	
	["blw000p0"] = "After !DIST! !UNIT! , bear left !NG_COMMAND_1! and cross the square",
	
	["blx00p00"] = "After !DIST! !UNIT! , take the street on the left !NG_COMMAND_1! , cross the square",
	
	["a0p000t0"] = "Take the street on the right !NG_COMMAND_1! and take the escalator",
	
	["blv00s00"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! , take the elevator",
	
	["blu000r0"] = "After !DIST! !UNIT! , make a sharp left !NG_COMMAND_1! and take the stairs",
	
	["blp000s0"] = "After !DIST! !UNIT! , take the street on the right !NG_COMMAND_1! and take the elevator",
	
	["bln00o00"] = "After !DIST! !UNIT! , walk straight ahead !NG_COMMAND_1! , go through the building",
	
	["a0x000q0"] = "Take the street on the left !NG_COMMAND_1! and cross the park",
	
	["blp00s00"] = "After !DIST! !UNIT! , take the street on the right !NG_COMMAND_1! , take the elevator",
	
	["blv00g00"] = "After !DIST! !UNIT! , turn left !NG_COMMAND_1! onto the footpath",
	
	["a0w000s0"] = "Bear left !NG_COMMAND_1! and take the elevator",
	
	["a0n00000"] = "Walk straight ahead !NG_COMMAND_1!",
	
	["a0s00g00"] = "Make a sharp right onto the footpath",
	
	["a0y00d00"] = "Turn !NG_COMMAND_1! onto !STREET!",
	
	["a0u00o00"] = "Make a sharp left !NG_COMMAND_1! , go through the building",
	
	["bl000o00"] = "After !DIST! !UNIT! , !NG_COMMAND_1! go through the building",
	
	["bl000p00"] = "After !DIST! !UNIT! , !NG_COMMAND_1! cross the square",
	
	["bl000q00"] = "After !DIST! !UNIT! , !NG_COMMAND_1! cross the park",
	
	["bl000r00"] = "After !DIST! !UNIT! , !NG_COMMAND_1! take the stairs",
	
	["bl000s00"] = "After !DIST! !UNIT! , !NG_COMMAND_1! take the lift",
	
	["bl000t00"] = "After !DIST! !UNIT! , !NG_COMMAND_1! take the escalator",
	
	["bla00h00"] = "After !DIST! !UNIT! , walk right around the traffic circle and turn onto !STREET!",
	
	["bla00j00"] = "After !DIST! !UNIT! , walk right around the traffic circle !NG_COMMAND_1! !PED_TURN_NO!",
	
	["blb00h00"] = "After !DIST! !UNIT! , walk left around the traffic circle !NG_COMMAND_1! and turn onto !STREET!",
	
	["blb00j00"] = "After !DIST! !UNIT! , walk left around the traffic circle !PED_TURN_NO!",
	
	["blc00h00"] = "After !DIST! !UNIT! , walk around the traffic circle and turn onto !STREET!",
	
	["blc00j00"] = "After !DIST! !UNIT! , walk around the traffic circle !PED_TURN_NO!",
	
	["blx000t0"] = "After !DIST! !UNIT! , take the street on the left !NG_COMMAND_1! and take the escalator",
}

