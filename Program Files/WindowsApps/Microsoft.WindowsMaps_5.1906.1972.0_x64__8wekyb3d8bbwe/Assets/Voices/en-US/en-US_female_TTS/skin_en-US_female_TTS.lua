-- � Microsoft. All rights reserved.
--      Header-Data for WindowsPhone 8.0 with MSFT TTS
--      v4 packages
--      Language: en-US
--      Technology: TTS
--      Gender: female


description = "TTS"
id = "10002"
audio_files_version = "0.4.0.1305061308"
client_range = "[ client >= 1.7.0.0 ]"
network_provider_support_list = {
	"all",
}
application_support_list = {
	'IN "WP8_TTS_Core":1.0',
}
feature_list = { "metric", "imperial_uk", "imperial_us" }
travel_mode = "0"
output_type = "tts"
language_id = "10"
marc_code = "010"
language = "English (US)"
language_loc = "English (US)"
gender = "f"
config_file = "en-US_female_TTS/common.lua"

main_attribute_array = {
	["language_code"] = "en-US",
	["ngLangCode"] = "eng",
	["ms_lcid"] = "409",
	["LocalizedType"] = "announced street names",
	["LocalizedGender"] = "female",
	["VoiceFeatures"] = "drive;walk;metric;imperialuk;imperialus;naturalguidance;trafficlights;tts",
}


rulesets_file = ""
userdictionary_file = ""
audio_files_path = "en-US_female_TTS\\en-US_female_TTS"
apdb = ""
speaker = ""
platform_support_list = {
	'= "Windows Phone OS":>= 8.0',
}
tts_engine_type = { "msft:8.0.5515" }



require("ruleset_en-US_TTS")
require("prompts_en-US_TTS")
require("platform_format")
