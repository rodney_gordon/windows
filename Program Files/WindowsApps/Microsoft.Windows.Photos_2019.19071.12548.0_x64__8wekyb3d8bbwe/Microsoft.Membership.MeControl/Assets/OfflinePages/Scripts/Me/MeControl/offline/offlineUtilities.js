MeControlDefine('offlineUtilities', ['exports'], function (exports) { 'use strict';

    var splitSymbol = '-';
    var defaultCulture = 'en-US';
    function ResolveCulture(culture) {
        if (!culture || !culture.trim()) {
            return defaultCulture;
        }
        for (var key = culture.trim().toUpperCase(); !!key; key = key.substring(0, key.lastIndexOf(splitSymbol))) {
            if (cultureFallbackDict[key]) {
                return cultureFallbackDict[key];
            }
        }
        return defaultCulture;
    }

    exports.defaultCulture = defaultCulture;
    exports.ResolveCulture = ResolveCulture;

    Object.defineProperty(exports, '__esModule', { value: true });

});
MeControlImport('offlineUtilities')
//# sourceMappingURL=offlineUtilities.js.map
