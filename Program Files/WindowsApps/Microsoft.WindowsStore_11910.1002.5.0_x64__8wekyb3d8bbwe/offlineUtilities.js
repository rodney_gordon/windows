MeControlDefine('offlineUtilities', ['exports'], function (exports) { 'use strict';

var splitSymbol = '-';
var defaultCulture = 'en-US';
/**
 * a string culture matching algorithm to find the closest culture from the existing set.
 * starting from exact match 'AA-BB-CC', falling back one level after each try: 'AA-BB-CC' -> 'AA-BB' -> 'AA' -> Default
 * original algorithm was implemented under the following class
 * class definition: Microsoft.Osgs.Web.Culture.Validation.CultureInfoValidatorBase (link: https://aka.ms/mee.resolver)
 * culture fallback xml: https://aka.ms/defaultCultures
 * @param culture
 * the culture to find a match for
 */
function ResolveCulture(culture) {
    // return default culture if the passed in culture is undefined or an empty string
    if (!culture || !culture.trim()) {
        return defaultCulture;
    }
    for (var key = culture.trim().toUpperCase(); !!key; key = key.substring(0, key.lastIndexOf(splitSymbol))) {
        if (cultureFallbackDict[key]) {
            return cultureFallbackDict[key];
        }
    }
    return defaultCulture;
}

exports.defaultCulture = defaultCulture;
exports.ResolveCulture = ResolveCulture;

Object.defineProperty(exports, '__esModule', { value: true });

});
MeControlImport('offlineUtilities')
//# sourceMappingURL=offlineUtilities.js.map
