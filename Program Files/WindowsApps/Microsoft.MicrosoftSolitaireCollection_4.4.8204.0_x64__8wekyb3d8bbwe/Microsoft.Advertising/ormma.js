﻿/*!
  Copyright (C) Microsoft. All rights reserved.
  This library is supported for use in Windows Store apps only.
*/
/// <disable>EnableStrictMode.EnforceModulePattern</disable>
"use strict";
var ORMMA_STATE_DEFAULT = "default";
var ORMMA_STATE_EXPANDED = "expanded";
var ORMMA_STATE_HIDDEN = "hidden";
var ORMMA_STATE_LOADING = "loading";
var ORMMA_STATE_RESIZED = "resized";
var ORMMA_STATE_SUSPENDED = "suspended";
var ORMMA_STATE_UNKNOWN = "unknown";

var ORMMA_EVENT_ERROR = "error";
var ORMMA_EVENT_HEADING_CHANGE = "headingChange";
var ORMMA_EVENT_KEYBOARD_CHANGE = "keyboardChange";
var ORMMA_EVENT_LOCATION_CHANGE = "locationChange";
var ORMMA_EVENT_NETWORK_CHANGE = "networkChange";
var ORMMA_EVENT_ORIENTATION_CHANGE = "orientationChange";
var ORMMA_EVENT_READY = "ready";
var ORMMA_EVENT_RESPONSE = "response";
var ORMMA_EVENT_SCREEN_CHANGE = "screenChange";
var ORMMA_EVENT_SHAKE = "shake";
var ORMMA_EVENT_SIZE_CHANGE = "sizeChange";
var ORMMA_EVENT_STATE_CHANGE = "stateChange";
var ORMMA_EVENT_TILT_CHANGE = "tiltChange";
var ORMMA_EVENT_VIEWABLE_CHANGE = "viewableChange";
var MAPLE_EVENT_CLICK = "click";
var MAPLE_EVENT_VIEWABLEDATA_CHANGE = "viewableDataChange";

var ORMMA_FEATURE_AUDIO = "audio";
var ORMMA_FEATURE_CALENDAR = "calendar";
var ORMMA_FEATURE_CAMERA = "camera";
var ORMMA_FEATURE_EMAIL = "email";
var ORMMA_FEATURE_HEADING = "heading";
var ORMMA_FEATURE_INLINEVIDEO = "inlineVideo";
var ORMMA_FEATURE_LEVEL1 = "level-1";
var ORMMA_FEATURE_LEVEL2 = "level-2";
var ORMMA_FEATURE_LEVEL3 = "level-3";
var ORMMA_FEATURE_LOCATION = "location";
var ORMMA_FEATURE_MAP = "map";
var ORMMA_FEATURE_NETWORK = "network";
var ORMMA_FEATURE_ORIENTATION = "orientation";
var ORMMA_FEATURE_PHONE = "phone";
var ORMMA_FEATURE_SCREEN = "screen";
var ORMMA_FEATURE_SHAKE = "shake";
var ORMMA_FEATURE_SMS = "sms";
var ORMMA_FEATURE_TILT = "tilt";
var ORMMA_FEATURE_VIDEO = "video";
var ORMMA_API_VERSION = "1.1.0";
var MRAID_API_VERSION = "1.0";
var MAPLE_MAX_STATE_DATA_SIZE = 65536;

var ORMMA_PLACEMENTTYPE_INLINE = "inline";
var ORMMA_PLACEMENTTYPE_INTERSTITIAL = "interstitial";

var MSG_TYPE_ADRENDERED = "rendered";
var MSG_TYPE_CLOSE = "close";
var MSG_TYPE_ERROR = "error";
var MSG_TYPE_EXPAND = "expand";
var MSG_TYPE_GETORIENTATION = "getorientation";
var MSG_TYPE_GETTILT = "gettilt";
var MSG_TYPE_HIDE = "hide";
var MSG_TYPE_LISTENER = "listener";
var MSG_TYPE_OPEN = "web";
var MSG_TYPE_REFRESH = "refresh";
var MSG_TYPE_REQUEST = "request";
var MSG_TYPE_RESIZE = "resize";
var MSG_TYPE_SETEXPANDPROPERTIES = "setexpandproperties";
var MSG_TYPE_SETUSERENGAGED = "setuserengaged";
var MSG_TYPE_SHAKE = "shake";
var MSG_TYPE_SHOW = "show";
var MSG_TYPE_TILT = "tilt";
var MSG_TYPE_USECUSTOMCLOSE = "usecustomclose";
var MSG_TYPE_VALUESTART = "start";
var MSG_TYPE_VALUESTOP = "stop";
var MSG_TYPE_VIEWABLECHANGE = "viewableChange";
var MSG_TYPE_VIEWABLEDATACHANGE = "viewableDataChange";
var MSG_TYPE_LOGAPIUSAGE = "logapiusage";

var FUNC_NAME_ADDEVENTLISTENER = "addEventListener";
var FUNC_NAME_REMOVEEVENTLISTENER = "removeEventListener";
var FUNC_NAME_EXPAND = "expand";
var FUNC_NAME_SUPPORTS = "supports";
var FUNC_NAME_GETSIZE = "getSize";
var FUNC_NAME_OPEN = "open";
var FUNC_NAME_CLOSE = "close";
var FUNC_NAME_GETEXPANDPROPERTIES = "getExpandProperties";
var FUNC_NAME_GETPLACEMENTTYPE = "getPlacementType";
var FUNC_NAME_SETEXPANDPROPERTIES = "setExpandProperties";
var FUNC_NAME_USECUSTOMCLOSE = "useCustomClose";

var ADBROKER_PREBID_HEADER_APPID = "appId";
var ADBROKER_PREBID_HEADER_APPGUID = "appGuid";
var ADBROKER_PREBID_HEADER_DOMAIN = "domain";


// These are callbacks which will be used by the C# client when information
// is requested by rich media.
function setScreenSize(width, height) {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>
    /// <param name="width" type="String">the screen width</param>
    /// <param name="height" type="String">the screen height</param>
    var convertWidth = parseInt(width);
    convertWidth = convertWidth === NaN ? -1 : convertWidth;
    var convertHeight = parseInt(height);
    convertHeight = convertHeight === NaN ? -1 : convertHeight;

    Ormma._setScreenSize(convertWidth, convertHeight);
}

function invokeInit() {
    /// <summary>
    /// Called when the c# layer has initialized and raises the ready event/calls ORMMAReady.
    /// </summary>
    Ormma._init();
}

function reportError(action, msg) {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>
    /// <param name="action" type="String">the Ormma action that resulted in the error, empty if not known</param>
    /// <param name="msg" type="String">the msg associated with the error</param>
    Ormma._throwError(action, msg);
}

function setOrmmaState(state) {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>
    /// <param name="state" type="String">the state of the Ormma control, can be unknown, hidden, default, expanded, resized</param>
    Ormma._setState(state);
}

function setOrmmaPlacementType(placementType) {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>
    /// <param name="placementType" type="String">the placement type of the ad, can be inline or interstitial</param>
    Ormma._setPlacementType(placementType);
}

function setOrmmaLocale(locale) {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>
    /// <param name="locale" type="String">the locale of the Ormma control</param>
    Ormma._setLocale(locale);
}

function setSize(width, height) {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>
    /// <param name="width" type="String">the control width</param>
    /// <param name="height" type="String">the control height</param>
    var convertWidth = parseInt(width);
    convertWidth = convertWidth === NaN ? -1 : convertWidth;
    var convertHeight = parseInt(height);
    convertHeight = convertHeight === NaN ? -1 : convertHeight;

    Ormma._setSize(convertWidth, convertHeight);
}

function setMaxSize(width, height) {
    /// <summary>
    /// helper method for C#/Javascript interaction, forwards data onto
    /// associated ormma method
    /// </summary>
    /// <param name="width" type="String">the control width</param>
    /// <param name="height" type="String">the control height</param>
    var convertWidth = parseInt(width);
    convertWidth = convertWidth === NaN ? -1 : convertWidth;
    var convertHeight = parseInt(height);
    convertHeight = convertHeight === NaN ? -1 : convertHeight;

    Ormma._setMaxSize(convertWidth, convertHeight);
}

function setOrientation(orientation) {
    /// <summary>
    /// helper method for C#/Javascript interaction, forwards data onto
    /// associated ormma method
    /// </summary>
    /// <param name="orientation" type="Number">the orientation of the phone, 
    /// -1 - device orientation unknown
    /// 0 - portrait, 
    /// 90 - clockwise to landscape
    /// 180 - portrait upside down
    /// 270 - counter clockwise to landscape
    /// </param>   
    var convertOrientation = parseInt(orientation);
    convertOrientation = convertOrientation === NaN ? -1 : convertOrientation;
    Ormma._setOrientation(convertOrientation);
}

function fireShake() {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>
    Ormma._shake();
}

function updateTiltCoords(x, y, z) {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>
    /// <param name="x" type="Number">the x acceleration value</param>
    /// <param name="y" type="Number">the y acceleration value</param>
    /// <param name="z" type="Number">the z acceleration value</param>
    Ormma._tiltChange({ x: x, y: y, z: z });
}

function fireViewable(isViewable) {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>
    var isViewableBool = stringToBoolean(isViewable);
    Ormma._viewableChange(isViewableBool);
}

function viewableDataChange(data) {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>
    /// <param name="data" type="json">a json payload containing the various viewableDataChange properties. 
    /// e.g. "{ \"adOnScreenPercentage\":0.0, \"adXOffset\":0.0 }"
    /// </param>
    Ormma._viewableDataChange(data);
}

function setCapability(capability, value) {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>
    /// <param name="capability" type="String">the capability to enable/disable</param>
    /// <param name="value" type="String">"true" to enable "false" to disable</param>
    Ormma._setCapability(capability, stringToBoolean(value));
}

function stringToBoolean(value) {
    /// <summary>
    /// Converts a string to its boolean value.
    /// </summary>
    /// <param name="value" type="String">the value to convert</param>
    /// <returns type="Boolean">true if value is "true" (case-insensitive), otherwise false.</returns>
    if (value.toLowerCase() === "true") {
        return true;
    }
    return false;
}

function fireResponse(url, response) {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>

    // The data coming from the C# code is not escaped, so do it now.
    var data = { url: escape(url), response: escape(response) };
    Ormma._sendResponse(data);
}

function setNetwork(networkType) {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>
    Ormma._setNetwork(networkType);
}

function setSdkVersion(sdkVersion, client, runtimeType) {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>
    Ormma._setSdkVersion(sdkVersion, client, runtimeType);
}

function fireClickEvent() {
    /// <summary>
    /// Helper method for C#/Javascript interaction. Forwards data on to associated ormma method.
    /// </summary>
    Ormma._clicked();
}

(function () {

    // Wrap the AdClient's data in this object
    function AdClientData() {
        this.listeners = [];
    }

    var _adClient = new AdClientData();

    //Used to prevent function calls from being logged twice for Ormma/Mraid Telemtry. Some functions exist in both classes.
    var lastApiCall;

    // The main ad controller object
    window.ORMMA = window.ormma = window.Ormma = window.MAPLE = window.maple = window.Maple = {

        // The maximum size the control can grow to when resizing in the format { width: 480, height: 80 }. This can be modified by the developer.
        // Default sizes to 0 --- will get updated once ORMMA initialized.
        _maxSize: { width: 0, height: 0 },

        // Holds the current ad placement size.
        _dimensions: {},

        // Holds the default dimension in the format { width: 480, height: 80, x: 0, y: 0 }
        _defaultDimensions: {},

        // The available screen size for expanded ads
        _screenSize: null,

        // Set on initialization by ad control, used to determine if there is support
        // for detecting tilt.
        _tiltCapability: false,

        // Set on initialization by ad control, used to determine if there is support
        // for detecting shake.
        _shakeCapability: false,

        // Stores properties used during expanding.
        // Supported properties are width, height, useCustomClose, isModal, lockOrientation.
        // Default sizes to 0 -- will get updated once the screen size is determined
        _expandProperties: {
            width: 0,
            height: 0,
            useCustomClose: false,
            lockOrientation: false,
            isModal: true
        },

        // Holds the current shake property values, currently unsupported
        _shakeProperties: {},

        // Holds the current resize property values, currently unsupported
        _resizeProperties: {
            transition: "none"
        },

        // Holds the ad placement type
        _placementType: ORMMA_PLACEMENTTYPE_INLINE,

        // Holds the current state of the control
        _state: ORMMA_STATE_LOADING,

        // Holds the current locale of the control
        _locale: null,

        // Holds the last known location of the device as JSON.
        // Example: {lat: 32, lon: -44, acc: 10}
        _location: null,

        // -1 - device orientation unknown
        // 0 - portrait, 
        // 90 - clockwise to landscape
        // 180 - portrait upside down
        // 270 - counter clockwise to landscape
        _orientation: -1,

        // Holds the last recorded tilt coordinates
        _lastTiltCoords: { x: 0, y: 0, z: 0 },

        // Holds the last recorded network state
        _lastNetworkState: ORMMA_STATE_UNKNOWN,

        // Holds the SDK info
        _sdkInfo: null,

        // Stores the mapping of the IE11+ msOrientation property to the ormma returned values. Also stores the
        // IE11+ msOrientation property name.
        _msOrientationModes: {
            portaitPrimary: { ieProperty: "portrait-primary", orientationDegrees: 0 },
            landscapePrimary: { ieProperty: "landscape-primary", orientationDegrees: 270 },
            portaitSecondary: { ieProperty: "portrait-secondary", orientationDegrees: 180 },
            landscapeSecondary: { ieProperty: "landscape-secondary", orientationDegrees: 90 },
            unknown: { ieProperty: "", orientationDegrees: -1 }
        },

        addEventListener: function (evt, callback) {
            /// <summary>
            /// Adds an listener for an event
            /// </summary>
            /// <param name="evt" type="String">The name of the event to listen for</param>
            /// <param name="callback" type="Function">Name of function or an anonymous function to execute when the event occurs</param>
            if (_adClient.listeners[evt] === null || typeof (_adClient.listeners[evt]) === "undefined") {
                _adClient.listeners[evt] = [];
            }

            if (evt === ORMMA_EVENT_TILT_CHANGE && !this._tiltCapability) {
                this._throwError("tilt", "tilt capability is not supported, event listener not added");
                return;
            } else if (evt === ORMMA_EVENT_SHAKE && !this._shakeCapability) {
                this._throwError("shake", "shake capability is not supported, event listener not added");
                return;
            }

            // add listener before calling _notify since some events will fire immediately
            _logme("adding listener for " + evt);
            _adClient.listeners[evt].push(callback);

            if (evt === ORMMA_EVENT_TILT_CHANGE) {
                _notify(MSG_TYPE_TILT + ":" + MSG_TYPE_LISTENER + "=" + MSG_TYPE_VALUESTART);
            } else if (evt === ORMMA_EVENT_SHAKE) {
                _notify(MSG_TYPE_SHAKE + ":" + MSG_TYPE_LISTENER + "=" + MSG_TYPE_VALUESTART);
            } else if (evt === ORMMA_EVENT_ORIENTATION_CHANGE) {
                _notify(MSG_TYPE_GETORIENTATION + ":" + MSG_TYPE_LISTENER + "=" + MSG_TYPE_VALUESTART);
            } else if (evt === ORMMA_EVENT_VIEWABLE_CHANGE) {
                _notify(MSG_TYPE_VIEWABLECHANGE + ":" + MSG_TYPE_LISTENER + "=" + MSG_TYPE_VALUESTART);
            } else if (evt === MAPLE_EVENT_VIEWABLEDATA_CHANGE) {
                _notify(MSG_TYPE_VIEWABLEDATACHANGE + ":" + MSG_TYPE_LISTENER + "=" + MSG_TYPE_VALUESTART);
            }
        },

        removeEventListener: function (evt, listener) {
            /// <summary>
            /// Use this method to unsubscribe a specific handler method from a specific event. Event listeners should
            /// always be removed when they are no longer useful to avoid errors. If no listener function is provided,
            /// then all functions listening to the event will be removed.
            /// </summary>
            /// <param name="evt" type="String">name of event to unsubscribe from</param>
            /// <param name="listener" type="Function">the function to unsubscribe</param>
            _logme("removing listener for " + evt);
            if (_adClient.listeners[evt] !== null && typeof (_adClient.listeners[evt]) !== "undefined") {
                if (listener === undefined || listener === null || listener === "") {
                    // Removes all listeners if no listener function has been specified.
                    _adClient.listeners[evt].length = 0;
                }
                else {
                    var callbacks = _adClient.listeners[evt];
                    var ix;
                    for (ix = 0; ix < callbacks.length; ix++) {

                        if (callbacks[ix] === listener) {
                            callbacks.splice(ix, 1);
                            break;
                        }
                    }
                }

                // Only send the stop message if no more client listeners are present
                if (_adClient.listeners[evt] === null || typeof (_adClient.listeners[evt]) === "undefined" || _adClient.listeners[evt].length === 0) {
                    if (evt === ORMMA_EVENT_TILT_CHANGE) {
                        if (!this._tiltCapability) {
                            this._throwError("tilt", "tilt capability is not supported, no event listener");
                            return;
                        }
                        _logme("stopping tilt device for event: " + evt + " (no more client listeners)");
                        _notify(MSG_TYPE_TILT + ":" + MSG_TYPE_LISTENER + "=" + MSG_TYPE_VALUESTOP);
                    } else if (evt === ORMMA_EVENT_SHAKE) {
                        if (!this._shakeCapability) {
                            this._throwError("shake", "shake capability is not supported, no event listener");
                            return;
                        }
                        _logme("stopping shake device for event: " + evt + " (no more client listeners)");
                        _notify(MSG_TYPE_SHAKE + ":" + MSG_TYPE_LISTENER + "=" + MSG_TYPE_VALUESTOP);
                    } else if (evt === ORMMA_EVENT_ORIENTATION_CHANGE) {
                        _logme("stopping orientation changed event: " + evt + " (no more client listeners)");
                        _notify(MSG_TYPE_GETORIENTATION + ":" + MSG_TYPE_LISTENER + "=" + MSG_TYPE_VALUESTOP);
                    } else if (evt === ORMMA_EVENT_VIEWABLE_CHANGE) {
                        _logme("stopping viewable changed event: " + evt + " (no more client listeners)");
                        _notify(MSG_TYPE_VIEWABLECHANGE + ":" + MSG_TYPE_LISTENER + "=" + MSG_TYPE_VALUESTOP);
                    } else if (evt === MAPLE_EVENT_CLICK) {
                        _logme("stopping clickEvent : " + evt + " (no more client listeners)");
                    } else if (evt === MAPLE_EVENT_VIEWABLEDATA_CHANGE) {
                        _logme("stopping viewable data changed event: " + evt + " (no more client listeners)");
                        _notify(MSG_TYPE_VIEWABLEDATACHANGE + ":" + MSG_TYPE_LISTENER + "=" + MSG_TYPE_VALUESTOP);
                    }
                }
            }
        },

        close: function () {
            /// <summary>
            /// Closes the ad when it is in the expanded or resized state and issues stateChange
            /// event, hides when in the default state, and does nothing in the hidden state
            /// </summary>
            _logme("sending close request");
            _notify(MSG_TYPE_CLOSE);
        },

        expand: function (url, isLegacy) {
            /// <summary>
            /// Opens a new web view/iframe in the expanded state
            /// </summary>
            /// <param name="url" type="String">The url to navigate the expanded control to</param>
            if (typeof (url) === "undefined" || url === null) {
                url = "";
            }

            if (!(this._state === ORMMA_STATE_DEFAULT || this._state === ORMMA_STATE_RESIZED)) {
                reportError(FUNC_NAME_EXPAND, "can only expand from resized or default states");
                return;
            }

            if (typeof (isLegacy) === "undefined" || isLegacy === null) {
                isLegacy = true;
            }

            // Send expandProperties to the ad control first
            if (typeof (this._expandProperties) === "object") {
                var propStr = JSON.stringify(this._expandProperties);
                _notify(MSG_TYPE_SETEXPANDPROPERTIES + ":" + propStr);
            }

            _logme("sending expand request");

            var msg = { "url": url, "islegacy": isLegacy };
            _notify(MSG_TYPE_EXPAND + ":" + JSON.stringify(msg));
        },

        getDefaultPosition: function () {
            /// <summary>
            /// Gets the default expand position.
            /// </summary>
            /// <returns type="Object">
            /// the default position in the format {x : 0, y : 0, height : 800, width : 480}
            /// </returns>
            return this._defaultDimensions;
        },

        getExpandProperties: function () {
            /// <summary>
            /// Gets the current expand properties.
            /// </summary>
            /// <returns type="Object">the current expand properties in the format:
            /// { width : "nn", height : "nn", useCustomClose : "true|false", isModal : "true|false", lockOrientation : "true|false", useBackground : "true|false (deprecated)", backgroundColor : "#rrggbb (deprecated)", backgroundOpacity : "n.n (deprecated)" }
            /// </returns>
            if (this._expandProperties) {
                return this._expandProperties;
            }
            else {
                return Ormma._expandProperties;
            }
        },

        getNetwork: function () {
            /// <summary>
            /// returns the most recent network status of the device
            /// </summary>
            /// <returns type="String">the most recent network status of the device</returns>
            return this._lastNetworkState;
        },

        getOrientation: function () {
            /// <summary>
            /// returns the orientation of the device
            /// </summary>
            /// <returns type="Number"/>
            // convert the orientation to the ormma value, set if different.
            var newOrientation = this._msOrientationToOrmmaDegrees(window.screen.msOrientation);
            this._orientation = this._orientation === newOrientation ? this._orientation : newOrientation;
            //postToLocal("log:getOrientation called orientation:" + newOrientation);
            return this._orientation;
        },

        getPlacementType: function (none) {
            /// <summary>
            /// returns ad placement type: inline or interstitial
            /// </summary>
            return this._placementType;
        },

        getScreenSize: function () {
            /// <summary>
            /// Gets the device screen size
            /// </summary>
            /// <returns type="Object">the device screen size in the format {width: 480, height: 800} or {} if not known</returns>
            if (this._screenSize === null) {
                this._screenSize = { width: screen.availWidth, height: screen.availHeight };
            }
            return this._screenSize;
        },

        getShakeProperties: function () {
            /// <summary>
            /// Currently not supported
            /// </summary>
            /// <returns type="Object"/>
            return this._shakeProperties;
        },

        getSize: function () {
            /// <summary>
            /// Gets the current size of the ad
            /// </summary>
            /// <returns type="Object">the ad size in the format {width: 480, height: 800} or {} if not known</returns>
            if (typeof (this._dimensions) === "undefined") {
                this._dimensions = {};
            }
            return this._dimensions;
        },

        getState: function () {
            /// <summary>
            /// Gets the current state of the ad, can be
            /// </summary>
            /// <returns type="String">the current state of the ad, can be hidden, default, expanded, resized</returns>
            return this._state;
        },

        getTilt: function () {
            /// <summary>
            /// Returns the last known tilt coordinates.
            /// </summary>
            /// <returns type="Object"/>
            _logme("calling getTilt");
            
            if (!this._tiltCapability) {
                this._throwError("tilt", "tilt capability is not supported");
                return;
            }
            _notify(MSG_TYPE_TILT + ":" + MSG_TYPE_GETTILT + "=" + MSG_TYPE_REFRESH);
            return this._lastTiltCoords;
        },

        getVersion: function () {
            ///	<summary>
            /// Returns version number of API specification
            ///	</summary>
            /// <returns type="String">version number of the ORMMA API specification</returns>
            return ORMMA_API_VERSION;
        },

        hide: function () {
            /// <summary>
            /// moves control from the default state to the hidden state
            /// </summary>
            _logme("calling hide");
            _notify(MSG_TYPE_HIDE);
        },

        isViewable: function () {
            /// <summary>
            /// Currently not supported.
            /// Logic is implemented but this function should not return anything since we cannot guarantee an up-to-date viewable state.
            /// Use the viewableChange event instead.
            /// </summary>
            _logme("[not impl] isViewable called");
        },

        open: function (url) {
            /// <summary>
            /// Navigates to a web page in an external browser ie the devices default web page
            /// </summary>
            /// <param name="url" type="String">The url to navigate to</param>
            _logme("sending website request: " + url);
            var msg = JSON.stringify({ url: url });
            _notify(MSG_TYPE_OPEN + ":" + msg);
        },

        request: function (url, display) {
            /// <summary>
            /// Requests a URI resource through the Ormma API
            /// </summary>
            var requestParams = {
                url: url,
                display: (typeof (display) !== "undefined" && display !== null ? display : "ignore")
            };
            var msg = JSON.stringify(requestParams);
            _notify(MSG_TYPE_REQUEST + ":" + msg);
            return false;
        },

        resize: function (width, height) {
            /// <summary>
            /// Resizes the adControl to the spcified height and width.
            /// <param name="width" type="Number">the max width that can be used on resize</param>
            /// <param name="height" type="Number">the max height that can be used on resize</param>
            /// </summary>
            if (width > this._maxSize.width || width < 0) {
                this._throwError("resize", "width is greater than max allowed width (" + this._maxSize.width + ") or less than zero.");
                return;
            }

            if (height > this._maxSize.height || height < 0) {
                this._throwError("resize", "height is greater than max allowed height  (" + this._maxSize.height + ") or less than zero.");
                return;
            }

            _logme("calling resize:width=" + width + "&height=" + height);

            var msg = { width: width, height: height };
            _notify(MSG_TYPE_RESIZE + ":" + JSON.stringify(msg));
        },

        setExpandProperties: function (properties) {
            /// <summary>
            /// Sets the expand properties
            /// </summary>
            /// <param name="properties" type="Object">
            /// contains the properties to set, if a property is not present defaults are used:
            /// { width: "nn", height: "nn", useCustomClose:  "true|false",  isModal: "true|false", lockOrientation: "true|false", useBackground: "true|false (deprecated)", backgroundColor: "#rrggbb (deprecated)", backgroundOpacity: "n.n (deprecated)" }
            /// </param>
            this._expandProperties = typeof (properties) !== "object" ? {} : properties;

            // MRAID doesn't have getScreenSize so borrowing from Ormma object is needed
            var screenSize = (typeof (this.getScreenSize) === "function" ? this.getScreenSize() : window.ormma.getScreenSize());

            this._expandProperties.width = (_isValidExpandPropertiesDimension(this._expandProperties.width) && this._expandProperties.width < screenSize.width) ? this._expandProperties.width : screenSize.width;
            this._expandProperties.height = (_isValidExpandPropertiesDimension(this._expandProperties.height) && this._expandProperties.height < screenSize.height) ? this._expandProperties.height : screenSize.height;
            this._expandProperties.useCustomClose = typeof (this._expandProperties.useCustomClose) === "undefined" ? false : this._expandProperties.useCustomClose;
            this._expandProperties.lockOrientation = typeof (this._expandProperties.lockOrientation) === "undefined" ? false : this._expandProperties.lockOrientation;
            this._expandProperties.isModal = true;

            var propStr = JSON.stringify(this._expandProperties);
            _logme("setting expand properties: " + propStr);
            _notify(MSG_TYPE_SETEXPANDPROPERTIES + ":" + propStr);
        },

        setShakeProperties: function (properties) {
            /// <summary>
            /// Sets the properties that control how shake detection works.
            /// </summary>
            this._shakeProperties = typeof (properties) !== "object" ? {} : properties;
            var msg = JSON.stringify(this._shakeProperties);
            _logme("setting shake properties: " + msg);
        },

        show: function () {
            /// <summary>
            /// Moves control from the hidden to the default state
            /// </summary>
            _logme("calling show");
            _notify(MSG_TYPE_SHOW);
        },

        supports: function (feature) {
            /// <summary>
            /// Checks if an ORMMA feature is supported
            /// </summary>
            /// <param name="feature" type="String">the feature to check support for</param>
            /// <returns type="Boolean">true if feature is supported, false otherwise</returns>
            switch (feature) {
                case ORMMA_FEATURE_SCREEN:
                case ORMMA_FEATURE_ORIENTATION:
                case ORMMA_FEATURE_LEVEL1:
                case ORMMA_FEATURE_LEVEL2:
                case ORMMA_FEATURE_NETWORK:
                case ORMMA_FEATURE_INLINEVIDEO:
                    return true;
                case ORMMA_FEATURE_SHAKE:
                    return this._shakeCapability;
                case ORMMA_FEATURE_TILT:
                    return this._tiltCapability;
                case ORMMA_FEATURE_PHONE:
                case ORMMA_FEATURE_SMS:
                case ORMMA_FEATURE_EMAIL:
                case ORMMA_FEATURE_LOCATION:
                case ORMMA_FEATURE_HEADING:
                case ORMMA_FEATURE_CALENDAR:
                case ORMMA_FEATURE_AUDIO:
                case ORMMA_FEATURE_VIDEO:
                case ORMMA_FEATURE_LEVEL3:
                default:
                    return false;
            }
        },

        useCustomClose: function (flag) {
            /// <summary>
            /// if true the adControl will allow use of the full screen, if false adControl will 
            /// provide mechanism to close the ad (black bands)
            /// </summary>
            /// <param name="flag" type="boolean">if true don't priovide a close button/mechanism, otherwise provide close button</param>
            if (typeof (flag) === "boolean") {
                _logme("calling usecustomclose:" + flag);
                if (typeof (this._expandProperties) === "object") {
                    this._expandProperties.useCustomClose = flag;
                }
                _notify(MSG_TYPE_USECUSTOMCLOSE + ":" + flag);
            }
            else {
                reportError("useCustomClose", "parameter 'flag' is not a boolean value");
            }
        },

        // The functions below allow additional features not supported by the Ormma API.
        adAnchorReady: function () {
            /// <summary>
            /// MAPLE API
            /// called by the ad/renderer when it has finished rendering. This was added
            /// to help the WebView workarounds so that the AdvertisingWebBrowser knows 
            /// when to take a screenshot to use as the anchor image
            /// </summary>
            var adContainer = document.getElementById("msMainAdDiv");
            if (adContainer !== null) {
                adContainer.style.visibility = "visible";
            }

            _notify(MSG_TYPE_ADRENDERED);
        },

        adError: function (action, message) {
            /// <summary>
            /// MAPLE API
            /// reports to ad sdk that the ad has an error and should be invalidated/thrown away
            /// </summary>
            /// <param name="action" type="String">action that caused the error</param>
            /// <param name="message" type="String">details of the error</param>
            var a;
            if (typeof (action) === "string" && action !== null) {
                a = action;
            } else {
                a = "unknown action";
            }

            var m;
            if (typeof (message) === "string" && message !== null) {
                m = message;
            } else {
                m = "an unknown error occurred.";
            }

            var msg = JSON.stringify({ "action": a, "message": m });
            _notify(MSG_TYPE_ERROR + ":" + msg);
        },

        getLocale: function () {
            /// <summary>
            /// MAPLE API
            /// Gets the current locale of the ad, can be null/empty
            /// </summary>
            /// <returns type="String">the current locale of the ad</returns>
            if (this._locale !== null) {
                return this._locale;
            }
        },

        getSdkVersion: function () {
            ///	<summary>
            /// MAPLE API
            /// Returns version number of SDK
            ///	</summary>
            /// <returns type="String">version number of the SDK</returns>
            if (this._sdkInfo !== null) {
                return this._sdkInfo;
            }
        },

        hasListenerForClickEvent: function () {
            /// <summary>
            /// MAPLE API
            /// Returns true if there are any listeners subscribed to the "click" event, otherwise returns false.
            /// This is a workaround for detecting the renderer's support of the "click" event by the bootstrap 
            /// to allow/reject the renderer based on version required by SDK.
            /// </summary>
            if (typeof (_adClient.listeners[MAPLE_EVENT_CLICK]) !== "undefined"
                && _adClient.listeners[MAPLE_EVENT_CLICK] !== null
                && _adClient.listeners[MAPLE_EVENT_CLICK].length > 0) {
                return true;
            }
            else {
                return false;
            }
        },

        setUserEngaged: function (isEngaged) {
            /// <summary>
            /// MAPLE API
            /// Sets whether the user is current engaged with the ad, e.g. has playing a video or a game.
            /// </summary>
            /// <param name="isEngaged" type="Boolean">whether the user is engaged with ad</param>
            this._isUserEngaged = typeof (isEngaged) === "boolean" ? isEngaged : false;
            _logme("setting user engaged: " + this._isUserEngaged);
            _notify(MSG_TYPE_SETUSERENGAGED + ":engaged=" + this._isUserEngaged);
        },


        // The private functions
        _clicked: function () {
            ///	<summary>
            /// raises the clicked event, this will notify the ad renderer that a user
            /// has clicked on the ad. This will only be raised by the Xaml AdControl when
            /// the developer has set UseStaticAnchor=true
            ///	</summary>
            _fireEvent(MAPLE_EVENT_CLICK, null);
        },

        // These calls are be necessary for the C# control to send data and change events to the JS library.
        // They should not be called by the ad.
        _tiltChange: function (data) {
            /// <summary>
            /// Fires the tilt change event.
            /// </summary>
            this._lastTiltCoords = data;
            _fireEvent(ORMMA_EVENT_TILT_CHANGE, data);
        },

        _shake: function () {
            /// <summary>
            /// Fires the shake event.
            /// </summary>
            _fireEvent(ORMMA_EVENT_SHAKE);
        },

        _viewableChange: function (data) {
            /// <summary>
            /// Fires the viewable event.
            /// </summary>
            window.mraid._isViewable = data;
            _fireEvent(ORMMA_EVENT_VIEWABLE_CHANGE, data);
        },

        _viewableDataChange: function (data) {
            /// <summary>
            /// Fires the viewableDataChange event.
            /// </summary>
            _fireEvent(MAPLE_EVENT_VIEWABLEDATA_CHANGE, data);
        },

        _sendResponse: function (data) {
            /// <summary>
            /// Generates a response event.
            /// </summary>
            /// <param name="data" type="String">response data for event</param>
            _fireEvent(ORMMA_EVENT_RESPONSE, data);
        },

        _throwError: function (action, message) {
            /// <summary>
            /// Generates an error event.
            /// </summary>
            /// <param name="action" type="String">the action which caused the error</param>
            /// <param name="message" type="String">the error message</param>
            var data = { message: message, action: action };
            _fireEvent(ORMMA_EVENT_ERROR, data);
        },

        _init: function () {
            /// <summary>
            /// This function tells the ORMMA implementation to notify the ad that ORMMA is ready.
            /// </summary>
            _logme("Ormma is ready");

            if (this._state == ORMMA_STATE_LOADING) {
                Ormma._setState(ORMMA_STATE_DEFAULT);
            }

            _fireEvent(ORMMA_EVENT_READY, null);

            // ORMMA spec says to cal this window-scope function when ready.
            if (typeof (window.ORMMAReady) === "function") {
                _logme("Ormma calling ORMMAReady()");
                window.ORMMAReady();
            }
        },

        _setOrientation: function (orientation) {
            /// <summary>
            /// Helper method for setting the orientation by ad control
            /// </summary>
            /// <param name="orientation" type="Number">the orientation of the phone, 
            /// -1  - device orientation unknown
            /// 0   - portrait, 
            /// 90  - clockwise to landscape
            /// 180 - portrait upside down
            /// 270 - counter clockwise to landscape
            /// </param>  
            var oldOrientation = this._orientation;
            this._orientation = orientation;
            if (oldOrientation !== this._orientation) {
                _fireEvent(ORMMA_EVENT_ORIENTATION_CHANGE, this._orientation);
            }
        },

        _setPlacementType: function (placementType) {
            /// <summary>
            /// Helper method for setting the placement type by ad control
            /// </summary>
            /// <param name="placementType" type="String">the current placement type of the ad control</param>
            if (placementType === ORMMA_PLACEMENTTYPE_INLINE) {
                this._placementType = placementType;
            } else if (placementType === ORMMA_PLACEMENTTYPE_INTERSTITIAL) {
                this._placementType = placementType;
            }
        },

        _setState: function (state) {
            /// <summary>
            /// Helper method for setting the state by ad control
            /// </summary>
            /// <param name="state" type="String">the current state of the ad control</param>
            var oldState = this._state;
            this._state = state;
            if (oldState !== state) {
                _fireEvent(ORMMA_EVENT_STATE_CHANGE, this._state);
            }
        },

        _setLocale: function (locale) {
            /// <summary>
            /// Helper method for setting the locale by ad control
            /// </summary>
            /// <param name="locale" type="String">the current locale of the ad control</param>
            this._locale = locale;
        },

        _setSize: function (width, height) {
            /// <summary>
            /// Helper method for setting the size by ad control
            /// </summary>
            /// <param name="width" type="Number">the current state of the ad control</param>
            /// <param name="height" type="Number">the current state of the ad control</param>
            if (typeof (this._dimensions) === "undefined") {
                this._dimensions = {};
            }

            var currWidth = typeof (this._dimensions.width) === "undefined" ? 0 : this._dimensions.width;
            var currHeight = typeof (this._dimensions.height) === "undefined" ? 0 : this._dimensions.height;

            this._dimensions.width = width;
            this._dimensions.height = height;

            if (currWidth !== width || currHeight !== height) {
                _fireEvent(ORMMA_EVENT_SIZE_CHANGE, this._dimensions);
            }
        },

        _setSdkVersion: function (sdkVersion, client, runtimeType) {
            /// <summary>
            /// Helper method for setting the SDK version string
            /// </summary>
            /// <param name="sdkVersion" type="String">the sdk version string</param>
            /// <param name="client" type="String">the sdk client string</param>
            /// <param name="runtimeType" type="String">the type of runtime currently loaded</param>
            this._sdkInfo = {
                sdkVersion: sdkVersion,
                client: client,
                runtimeType: runtimeType,
                appName: navigator.appName,
                appVersion: navigator.appVersion,
                userAgent: navigator.userAgent,
                platform: navigator.platform
            };
        },

        _setCapability: function (capability, value) {
            /// <summary>
            /// Helper method for setting capabilities
            /// </summary>
            /// <param name="capability" type="String">the cabability to set</param>
            /// <param name="value" type="Boolean">true if the capability is available, false otherwise</param>

            if (typeof (value) !== "boolean") {
                return;
            }

            if (capability === ORMMA_FEATURE_TILT) {
                this._tiltCapability = value;
            } else if (capability === ORMMA_FEATURE_SHAKE) {
                this._shakeCapability = value;
            }
        },

        _setMaxSize: function (width, height) {
            /// <summary>
            /// Helper method for setting the size from c#
            /// </summary>
            /// <param name="width" type="String">the max width that can be used on resize</param>
            /// <param name="height" type="String">the max height that can be used on resize</param>
            if (typeof (this._maxSize) === "undefined") {
                this._maxSize = {};
            }

            this._maxSize.width = width;
            this._maxSize.height = height;
        },

        _setScreenSize: function (width, height) {
            /// <summary>
            /// Sets the device screen size
            /// </summary>
            /// <param name="width" type="Number">the width of the screen</param>
            /// <param name="height" type="Number">the height of the screen</param>
            if (this._screenSize === null || typeof (this._screenSize) === "undefined") {
                this._screenSize = {};
            }

            var currWidth = typeof (this._screenSize.width) === "undefined" ? 0 : this._screenSize.width;
            var currHeight = typeof (this._screenSize.height) === "undefined" ? 0 : this._screenSize.height;

            this._screenSize.width = width;
            this._screenSize.height = height;

            // Change expand properties to account for screen size. setExpandProperties checks for valid sizes, otherwise defaults to screen size 
            // We assume that if the expand properties match the previous screen size, that the new expand properties should adjust for the new screen size as well (eg. if device is rotated)
            if (this._expandProperties.width === currWidth) this._expandProperties.width = width;
            if (this._expandProperties.height === currHeight) this._expandProperties.height = height;

            this.setExpandProperties(this._expandProperties);

            if (currWidth !== this._screenSize.width || currHeight !== this._screenSize.height) {
                _fireEvent(ORMMA_EVENT_SCREEN_CHANGE, this._screenSize);
            }
        },

        _setNetwork: function (networkState) {
            /// <summary>
            /// Sets the network state
            /// </summary>
            /// <param name="networkState" type="String">new network state</param>

            _logme("setting network state: " + networkState);
            if (typeof (this._lastNetworkState) === "undefined") {
                this._lastNetworkState = "";
            }

            if (networkState !== this._lastNetworkState) {
                this._lastNetworkState = networkState;
                _fireEvent(ORMMA_EVENT_NETWORK_CHANGE, {
                    online: (networkState !== "offline"),
                    connection: networkState
                });
            }
        },

        /// Converts the msOrientation returned value to Ormma degrees.
        _msOrientationToOrmmaDegrees: function (msOrientation) {
            switch (msOrientation) {
                case this._msOrientationModes.landscapePrimary.ieProperty:
                    return this._msOrientationModes.landscapePrimary.orientationDegrees;
                case this._msOrientationModes.landscapeSecondary.ieProperty:
                    return this._msOrientationModes.landscapeSecondary.orientationDegrees;
                case this._msOrientationModes.portaitSecondary.ieProperty:
                    return this._msOrientationModes.portaitSecondary.orientationDegrees;
                case this._msOrientationModes.portaitPrimary.ieProperty:
                    return this._msOrientationModes.portaitPrimary.orientationDegrees;
                default:
                    return this._msOrientationModes.unknown.orientationDegrees;
            }
        }

    };

    function _logme(msg) {
        /// <summary>
        /// Logs a message.
        /// </summary>
        /// <param name="msg" type="String">the width of the screen</param>

        //window.external.notify("$log - " + msg);
   }

    function _isValidExpandPropertiesDimension(val) {
        /// <summary>
        /// Checks if the expand properties value is valid.
        /// </summary>
        /// <returns type="Boolean">true if item is valid, false otherwise
        /// </returns>
        if (typeof (val) === "undefined" || val === null || val <= 0) {
            return false;
        } else {
            return true;
        }
    }

    function _notify(msg) {
        /// <summary>
        /// Sends a message to the ad control.
        /// </summary>
        /// <param name="msg" type="String">The message to pass to the ad control.</param>

        // Embedded browsers (WebBrowser on Windows Phone and WebView in Windows 8) use the
        // notify mechanism for sending messages to the ad control.
        if (window.msAdSDK && typeof (window.msAdSDK.notify) === "function") {
            // Test hook to unit test
            window.msAdSDK.notify(msg);
        }
        else {
            window.external.notify(msg);
        }
    }

    function _setAppPrebidHeader(appId, appGuid, domain) {
        /// <summary>
        /// Helper method for setting the SDK version string
        /// </summary>
        /// <param name="appId" type="String">the appId version string</param>
        /// <param name="appGuid" type="String">the appGuid string</param>
        /// <param name="domain" type="String">the domain currently loaded</param>

        if (window.maple.app === undefined) {
            window.maple.app = {};
        }

        window.maple.app.appId = appId;
        window.maple.app.appGuid = appGuid;
        window.maple.app.domain = domain;
    }

    function _fireEvent(eventType, data) {
        /// <summary>
        /// Generic private function to throw events to listeners. In this implementation, all events are MessageEvents.
        /// </summary>
        /// <param name="eventType" type="String">The type of event being thrown.</param>
        /// <param name="data" type="Object">The data for the event (can be JSON or string depending on the event type).</param>
        _logme("raising event " + eventType + " with data " + data);
        
        if (_adClient.listeners[eventType] !== null && typeof (_adClient.listeners[eventType]) !== "undefined") {
            try {
                // Make a copy of the listeners array in case it is modified by the listeners
                // themselves (e.g. listener removes itself from list).
                var callbacks = _adClient.listeners[eventType].slice();
                var ix;
                switch (eventType) {
                    case ORMMA_EVENT_ERROR:
                        for (ix = 0; ix < callbacks.length; ix++) {
                            callbacks[ix](data.message, data.action);
                        }
                        break;
                    case ORMMA_EVENT_NETWORK_CHANGE:
                        for (ix = 0; ix < callbacks.length; ix++) {
                            callbacks[ix](data.online, data.connection);
                        }
                        break;
                    case ORMMA_EVENT_ORIENTATION_CHANGE:
                        for (ix = 0; ix < callbacks.length; ix++) {
                            callbacks[ix](data);
                        }
                        break;
                    case ORMMA_EVENT_READY:
                        for (ix = 0; ix < callbacks.length; ix++) {
                            callbacks[ix]();
                        }
                        break;
                    case ORMMA_EVENT_SCREEN_CHANGE:
                        for (ix = 0; ix < callbacks.length; ix++) {
                            callbacks[ix](data.width, data.height);
                        }
                        break;
                    case ORMMA_EVENT_SHAKE:
                        for (ix = 0; ix < callbacks.length; ix++) {
                            callbacks[ix]();
                        }
                        break;
                    case ORMMA_EVENT_SIZE_CHANGE:
                        for (ix = 0; ix < callbacks.length; ix++) {
                            callbacks[ix](data.width, data.height);
                        }
                        break;
                    case ORMMA_EVENT_STATE_CHANGE:
                        for (ix = 0; ix < callbacks.length; ix++) {
                            callbacks[ix](data);
                        }
                        break;
                    case ORMMA_EVENT_TILT_CHANGE:
                        for (ix = 0; ix < callbacks.length; ix++) {
                            callbacks[ix](data.x, data.y, data.z);
                        }
                        break;
                    case ORMMA_EVENT_VIEWABLE_CHANGE:
                        for (ix = 0; ix < callbacks.length; ix++) {
                            callbacks[ix](data);
                        }
                        break;
                    case ORMMA_EVENT_RESPONSE:
                        for (ix = 0; ix < callbacks.length; ix++) {
                            callbacks[ix](unescape(data.url), unescape(data.response));
                        }
                        break;
                    case MAPLE_EVENT_CLICK:
                        for (ix = 0; ix < callbacks.length; ix++) {
                            callbacks[ix]();
                        }
                        break;
                    case MAPLE_EVENT_VIEWABLEDATA_CHANGE:

                        var params = JSON.parse(data);
                        _setAppPrebidHeader(params[ADBROKER_PREBID_HEADER_APPID],
                                            params[ADBROKER_PREBID_HEADER_APPGUID],
                                            params[ADBROKER_PREBID_HEADER_DOMAIN]);
                        
                        for (ix = 0; ix < callbacks.length; ix++) {
                            callbacks[ix](data);
                        }
                        break;
                        // The rest are not supported.
                    case ORMMA_EVENT_HEADING_CHANGE:
                    case ORMMA_EVENT_KEYBOARD_CHANGE:
                    case ORMMA_EVENT_LOCATION_CHANGE:
                    default:
                        return false;
                }
            }
            catch (err) {
                _logme("exception thrown while firing event: " + err.message);
            }
        } else {
            _logme("no listeners for event " + eventType);
        }
    }

    window.mraid = window.MRAID = window.Mraid = {

        _isViewable: false,

        // MRAID 1.0 methods
        addEventListener: function (evt, callback) {

            if (this._mraidSupportedEvts === null) {
                this._mraidSupportedEvts = this._initEventList();
            }

            // If the event is supported in MRAID API then forward to ORMMA, otherwise ignore.
            if (this._mraidSupportedEvts[evt]) {
                window.ormma.addEventListener(evt, callback);
            }
        },
        removeEventListener: function (evt, callback) {

            if (this._mraidSupportedEvts === null) {
                this._mraidSupportedEvts = this._initEventList();
            }

            // If the event is supported in MRAID API then forward to ORMMA, otherwise ignore.
            if (this._mraidSupportedEvts[evt]) {
                window.ormma.removeEventListener(evt, callback);
            }
        },
        expand: function (url) {
            Object.getOwnPropertyNames(this);
            window.ormma.expand(url, false);
        },
        getState: function () {
            return window.ormma._state;
        },
        getVersion: function () {
            return MRAID_API_VERSION;
        },
        isViewable: function () {
            return window.mraid._isViewable;
        },

        // MRAID 1.0 events are "error", "ready", "stateChange" and "viewableChange"
        _mraidSupportedEvts: null,

        // Initializer for events supported in MRAID
        _initEventList: function () {
            var evtArray = [];
            evtArray[ORMMA_EVENT_ERROR] =
                evtArray[ORMMA_EVENT_READY] =
                evtArray[ORMMA_EVENT_STATE_CHANGE] =
                evtArray[MAPLE_EVENT_VIEWABLEDATA_CHANGE] =
                evtArray[ORMMA_EVENT_VIEWABLE_CHANGE] = true;
            return evtArray;
        },
        /******************************************************************************************************************/
        /*********************      Below are the MRAID implementation that are the same as ORMMA      ********************/
        /******************************************************************************************************************/
        supports: function (feature) {
            return window.ormma.supports(feature);
        },

        getSize: function () {
            return window.ormma.getSize();
        },

        open: function (url) {
            return window.ormma.open(url);
        },

        close: function () {
            return window.ormma.close();
        },

        getExpandProperties: function () {
            return window.ormma.getExpandProperties();
        },

        getPlacementType: function () {
            return window.ormma.getPlacementType();
        },

        setExpandProperties: function (properties) {
            return window.ormma.setExpandProperties(properties);
        },

        useCustomClose: function (flag) {
            return window.ormma.useCustomClose(flag);
        }
    };
})();
