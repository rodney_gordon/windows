﻿/*!
  Copyright (C) Microsoft. All rights reserved.
  This library is supported for use in Windows Store apps only.
  NOTE: please make sure this file is in sync with https://videoadssdk.azurewebsites.net/api/vpaid?code=oRMOeXB1sSJe12kLg/9JQRBZZM42xQ/dP1abFOwAq7tVZD4DMe13wQ==
*/
(function () {
    "use strict";

    var sdkVpaidVersionMajor = 2;
    var sdkVpaidVersionMinor = 0;
    var sdkVpaidVersion = sdkVpaidVersionMajor + "." + sdkVpaidVersionMinor;

    window.onerror = function (msg, url, lineNumber) {
        try {
            console.error(msg);
            if (typeof (_msVpaidImpl) === "function") {
                var debugMsg = "Url: " + url + " Line number: " + lineNumber;
                _msVpaidImpl.reportAdError("unhandled JS exception: " + msg, debugMsg);
            }
        }
        catch (e) {
        }
        return true;
    };

    window.onload = function () {
        if (typeof (getVPAIDAd) !== "function") {
            _msVpaidImpl.reportAdError("getVPAIDAd function not defined");
            return;
        }

        var ad = getVPAIDAd();

        if (!ad) {
            _msVpaidImpl.reportAdError("getVPAIDAd did not return an ad");
            return;
        }

        window._msVpaidInstance = new VpaidImpl(ad);
    };

    // Receives string message from outside WebView.
    window._msReceiveMessageFromAdSdk = function (msg) {
        VpaidImpl.log("message from SDK: " + msg);
        if (window._msVpaidInstance) {
            window._msVpaidInstance.receiveMessageFromAdSdk(msg);
        }
        if (window.Maple) {
            window.Maple.receiveMessageFromAdSdk(msg);
        }
    };

    var VpaidImpl = function (vpaidCreative) {
        VpaidImpl.log("instantiating VpaidImpl");

        this._creative = vpaidCreative;
        this._adParameters = null;

        if (!VpaidImpl.isValidVpaidCreative(vpaidCreative)) {
            VpaidImpl.reportAdError("ad is not a valid VPAID creative");
            return;
        }

        var adVpaidVersion = vpaidCreative.handshakeVersion(sdkVpaidVersion);
        if (!VpaidImpl.isVersionSupported(adVpaidVersion)) {
            VpaidImpl.reportAdError("ad requires VPAID version " + adVpaidVersion + ", sdk supports " + sdkVpaidVersion);
            return;
        }

        this._setupEventHandlers(this._creative);
    };

    VpaidImpl.prototype = {
        DEFAULT_DESIRED_BITRATE: 2000,

        initAd: function (width, height, viewMode, desiredBitrate, creativeData, environmentVars) {
            VpaidImpl.log("initializing vpaid ad");

            if (!environmentVars) {
                environmentVars = {
                    slot: document.querySelector("#msVpaidSlot"),
                    videoSlot: document.querySelector("#msVpaidVideo")
                };
            }

            this._creative.initAd(width, height, viewMode, desiredBitrate, creativeData, environmentVars);
        },

        receiveMessageFromAdSdk: function (msg) {
            if (!msg || typeof (msg) !== "string") {
                return;
            }

            var msgData = msg;
            var msgType = null;
            var msgContent = null;
            try {
                var colonIx = msgData.indexOf(":");
                if (colonIx < 0) {
                    msgType = msgData;
                } else {
                    msgType = msgData.substr(0, colonIx);
                    msgContent = msgData.substr(colonIx + 1);
                }

                if (msgType === "InitAd") {
                    this._receiveMessageInitAd(msgContent);
                } else if (msgType === "StartAd") {
                    this.startAd();
                } else if (msgType === "StopAd") {
                    this.stopAd();
                } else if (msgType === "PauseAd") {
                    this.pauseAd();
                } else if (msgType === "ResumeAd") {
                    this.resumeAd();
                } else if (msgType === "ResizeAd") {
                    var params = JSON.parse(msgContent);
                    this.resizeAd(params.width, params.height, params.viewMode);
                } else if (msgType === "SetAdVolume") {
                    // TODO read params from message
                    var volume = 50;
                    this.setAdVolume(volume);
                }
            }
            catch (err) {
                VpaidImpl.reportAdError("error handling message of type " + msgType);
            }
        },

        _receiveMessageInitAd: function (msg) {
            try {
                var initParams = JSON.parse(msg);

                var desiredBitrate = (typeof (initParams.desiredBitrate) === "number" ? initParams.desiredBitrate : this.DEFAULT_DESIRED_BITRATE);
                var width = (typeof (initParams.width) === "number" ? initParams.width : 0);
                var height = (typeof (initParams.height) === "number" ? initParams.height : 0);
                var viewMode = (typeof (initParams.viewMode) === "string" ? initParams.viewMode : "fullscreen");
                var creativeData = { AdParameters: initParams.adParameters };

                this.initAd(width, height, viewMode, desiredBitrate, creativeData, null);
            } catch (err) {
                VpaidImpl.reportAdError("unable to parse JSON for initAd");
            }
        },

        setAdVolume: function (val) {
            this._creative.setAdVolume(val);
        },

        startAd: function () {
            VpaidImpl.log("startAd");
            this._creative.startAd();
        },

        stopAd: function () {
            this._creative.stopAd();
        },

        resizeAd: function (width, height, viewMode) {
            VpaidImpl.log("resizing ad to " + width + " x " + height + ", " + viewMode);
            this._creative.resizeAd(width, height, viewMode);
        },

        pauseAd: function () {
            this._creative.pauseAd();
        },

        resumeAd: function () {
            this._creative.resumeAd();
        },

        // This function registers the callbacks of each of the events      
        _setupEventHandlers: function (creative) {
            if (creative) {
                VpaidImpl.log("adding event handlers");
                creative.subscribe(this._adStartedHandler, "AdStarted", this);
                creative.subscribe(this._adStoppedHandler, "AdStopped", this);
                creative.subscribe(this._adSkippedHandler, "AdSkipped", this);
                creative.subscribe(this._adLoadedHandler, "AdLoaded", this);
                creative.subscribe(this._adLinearChangeHandler, "AdLinearChange", this);
                creative.subscribe(this._adSizeChangeHandler, "AdSizeChange", this);
                creative.subscribe(this._adExpandedChangeHandler, "AdExpandedChange", this);
                creative.subscribe(this._adSkippableStateChangeHandler, "AdSkippableStateChange", this);
                creative.subscribe(this._adDurationChangeHandler, "AdDurationChange", this);
                creative.subscribe(this._adRemainingTimeChangeHandler, "AdRemainingTimeChange", this);
                creative.subscribe(this._adVolumeChangeHandler, "AdVolumeChange", this);
                creative.subscribe(this._adImpressionHandler, "AdImpression", this);
                creative.subscribe(this._adClickThruHandler, "AdClickThru", this);
                creative.subscribe(this._adInteractionHandler, "AdInteraction", this);
                creative.subscribe(this._adVideoStartHandler, "AdVideoStart", this);
                creative.subscribe(this._adVideoFirstQuartileHandler, "AdVideoFirstQuartile", this);
                creative.subscribe(this._adVideoMidpointHandler, "AdVideoMidpoint", this);
                creative.subscribe(this._adVideoThirdQuartileHandler, "AdVideoThirdQuartile", this);
                creative.subscribe(this._adVideoCompleteHandler, "AdVideoComplete", this);
                creative.subscribe(this._adUserAcceptInvitationHandler, "AdUserAcceptInvitation", this);
                creative.subscribe(this._adUserMinimizeHandler, "AdUserMinimize", this);
                creative.subscribe(this._adUserCloseHandler, "AdUserClose", this);
                creative.subscribe(this._adPausedHandler, "AdPaused", this);
                creative.subscribe(this._adPlayingHandler, "AdPlaying", this);
                creative.subscribe(this._adErrorHandler, "AdError", this);
                creative.subscribe(this._adLogHandler, "AdLog", this);
            }
        },

        // VPAID event handlers

        _adLoadedHandler: function () {
            VpaidImpl.log("ad has been loaded");
            VpaidImpl.sendMessageToAdSdk("AdLoaded");
        },

        _adPausedHandler: function () {
            VpaidImpl.log("ad paused");
            VpaidImpl.sendMessageToAdSdk("AdPaused");
        },

        _adPlayingHandler: function () {
            VpaidImpl.log("ad playing");
            VpaidImpl.sendMessageToAdSdk("AdPlaying");
        },

        _adErrorHandler: function (message) {

            if (typeof (message) !== "string") {

                var errorObj = message;
                if (errorObj !== null && typeof(errorObj) === "object"
                    && errorObj.message !== "undefined") {
                    message = "ad error message was: [object]: " + errorObj.message;

                } else {
                    message = "ad error message was: " + typeof (message);
                }
            }

            
            VpaidImpl.log("ad error: " + message);
            var params = { message: message };
            VpaidImpl.sendMessageToAdSdk("(VpaidError) AdError:" + JSON.stringify(params));
        },

        _adLogHandler: function (message) {
            if (typeof (message) !== "string") {
                var logObj = message;
                if (logObj !== null && typeof (logObj) === "object"
                    && logObj.message !== "undefined") {
                    message = "AdLog:: [object]: " + logObj.message;

                } else {
                    message = "AdLog::" + typeof (message);
                }
            }
            VpaidImpl.log("ad log: " + message);
            var params = { message: message };
            VpaidImpl.sendMessageToAdSdk("AdLog:" + JSON.stringify(params));
        },

        _adUserAcceptInvitationHandler: function () {
            VpaidImpl.log("AdUserAcceptInvitation");
            VpaidImpl.sendMessageToAdSdk("AdUserAcceptInvitation");
        },

        _adUserMinimizeHandler: function () {
            VpaidImpl.log("AdUserMinimize");
            VpaidImpl.sendMessageToAdSdk("AdUserMinimize");
        },

        _adUserCloseHandler: function () {
            VpaidImpl.log("AdUserClose");
            VpaidImpl.sendMessageToAdSdk("AdUserClose");
        },

        _adSkippableStateChangeHandler: function () {
            VpaidImpl.log("ad Skippable State Changed to: " + this._creative.getAdSkippableState());
            // TODO include skippableState in message to ad sdk
            VpaidImpl.sendMessageToAdSdk("AdSkippableStateChange");
        },

        _adExpandedChangeHandler: function () {
            VpaidImpl.log("ad Expanded Changed to: " + this._creative.getAdExpanded());
            VpaidImpl.sendMessageToAdSdk("AdExpanded");
        },

        _adSizeChangeHandler: function () {
            VpaidImpl.log("ad size changed to: w=" + this._creative.getAdWidth() + " h=" + this._creative.getAdHeight());
            var params = { width: this._creative.getAdWidth(), height: this._creative.getAdHeight() };
            VpaidImpl.sendMessageToAdSdk("AdSizeChange:" + JSON.stringify(params));
        },

        _adDurationChangeHandler: function () {
            VpaidImpl.log("ad duration changed to: " + this._creative.getAdDuration());
            var params = { duration: this._creative.getAdDuration() };
            VpaidImpl.sendMessageToAdSdk("AdDurationChange:" + JSON.stringify(params));
        },

        _adRemainingTimeChangeHandler: function () {
            var params = { remainingTime: this._creative.getAdRemainingTime() };
            VpaidImpl.sendMessageToAdSdk("AdRemainingTimeChange:" + JSON.stringify(params));
        },

        _adImpressionHandler: function () {
            VpaidImpl.log("ad impression");
            VpaidImpl.sendMessageToAdSdk("AdImpression");
        },

        _adClickThruHandler: function (url, id, playerHandles) {
            VpaidImpl.log("click-through");
            if (typeof (url) !== "string") {
                url = "";
            }
            if (typeof (id) !== "string") {
                id = "";
            }
            if (typeof (playerHandles) !== "boolean") {
                // If playerHandles is not a boolean, default to false unless equal to string "true".
                playerHandles = playerHandles === "true" ? true : false;
            }
            var params = { url: url, id: id, playerHandles: playerHandles };
            VpaidImpl.sendMessageToAdSdk("AdClickThru:" + JSON.stringify(params));
        },

        _adInteractionHandler: function (id) {
            VpaidImpl.log("ad interaction");
            VpaidImpl.sendMessageToAdSdk("AdInteraction");
        },

        _adVideoStartHandler: function () {
            VpaidImpl.log("video started");
            VpaidImpl.sendMessageToAdSdk("AdVideoStart");
        },

        _adVideoFirstQuartileHandler: function () {
            VpaidImpl.log("video 25% completed");
            VpaidImpl.sendMessageToAdSdk("AdVideoFirstQuartile");
        },

        _adVideoMidpointHandler: function () {
            VpaidImpl.log("video 50% completed");
            VpaidImpl.sendMessageToAdSdk("AdVideoMidpoint");
        },

        _adVideoThirdQuartileHandler: function () {
            VpaidImpl.log("video 75% completed");
            VpaidImpl.sendMessageToAdSdk("AdVideoThirdQuartile");
        },

        _adVideoCompleteHandler: function () {
            VpaidImpl.log("video completed");
            VpaidImpl.sendMessageToAdSdk("AdVideoComplete");
        },

        _adLinearChangeHandler: function () {
            VpaidImpl.log("ad linear has changed: " + this._creative.getAdLinear())
            VpaidImpl.sendMessageToAdSdk("AdLinearChange");
        },

        _adStartedHandler: function () {
            VpaidImpl.log("ad started");
            VpaidImpl.sendMessageToAdSdk("AdStarted");
        },

        _adStoppedHandler: function () {
            VpaidImpl.log("ad stopped");
            VpaidImpl.sendMessageToAdSdk("AdStopped");
        },

        _adSkippedHandler: function () {
            VpaidImpl.log("ad skipped");
            VpaidImpl.sendMessageToAdSdk("AdSkipped");
        },

        _adVolumeChangeHandler: function () {
            VpaidImpl.log("ad volume changed to - " + this._creative.getAdVolume());
            // TODO pass volume to ad sdk
            VpaidImpl.sendMessageToAdSdk("AdVolumeChange");
        },
    };

    VpaidImpl.MSG_TYPE_ERROR = "AdError";

    VpaidImpl.reportAdError = function (msg, debugMsg) {
        VpaidImpl.log("AD ERROR: " + msg);
        var params = { message: msg };
        if (debugMsg != null) {
            params.debugMsg = debugMsg;
        }
        VpaidImpl.sendMessageToAdSdk(VpaidImpl.MSG_TYPE_ERROR + ":" + JSON.stringify(params));
    };

    VpaidImpl.sendMessageToAdSdk = function (msg) {
        window.external.notify(msg);
        // Note: Replace the window.external.notify(msg) with webAllowedObject.invokeScript(msg) for Azure hosted url 
        // since urls cannot invoke script notify without giving access in packageManifest and the work around if using WebAllowedObject
    };

    VpaidImpl.isVersionSupported = function (adVpaidVersion) {
        if (!adVpaidVersion || typeof(adVpaidVersion) !== "string") {
            return false;
        }

        // Compare the major.minor version numbers.
        var adVer = adVpaidVersion.split(".").map(function (n) {
            return parseInt(n, 10);
        });
        var major = adVer.length > 0 ? adVer[0] : NaN;
        var minor = adVer.length > 1 ? adVer[1] : 0;

        if (isNaN(major) || isNaN(minor)) {
            return false;
        }

        if (major > sdkVpaidVersionMajor) {
            return false;
        }
        if (major === sdkVpaidVersionMajor && minor > sdkVpaidVersionMinor) {
            return false;
        }

        return true;
    };

    VpaidImpl.isValidVpaidCreative = function (vpaidCreative) {
        if (typeof(vpaidCreative) === "object" && vpaidCreative !== null &&
            vpaidCreative.handshakeVersion && typeof vpaidCreative.handshakeVersion === "function" &&
            vpaidCreative.initAd && typeof vpaidCreative.initAd === "function" &&
            vpaidCreative.startAd && typeof vpaidCreative.startAd === "function" &&
            vpaidCreative.stopAd && typeof vpaidCreative.stopAd === "function" &&
            vpaidCreative.skipAd && typeof vpaidCreative.skipAd === "function" &&
            vpaidCreative.resizeAd && typeof vpaidCreative.resizeAd === "function" &&
            vpaidCreative.pauseAd && typeof vpaidCreative.pauseAd === "function" &&
            vpaidCreative.resumeAd && typeof vpaidCreative.resumeAd === "function" &&
            vpaidCreative.expandAd && typeof vpaidCreative.expandAd === "function" &&
            vpaidCreative.collapseAd && typeof vpaidCreative.collapseAd === "function" &&
            vpaidCreative.subscribe && typeof vpaidCreative.subscribe === "function" &&
            vpaidCreative.unsubscribe && typeof vpaidCreative.unsubscribe === "function") {
            return true;
        }
        return false;
    };

    VpaidImpl.log = function (msg) {
        //console.log(msg);
    };

    window._msVpaidImpl = VpaidImpl;

    var MRAID_EVENT_VIEWABLE_CHANGE = "viewableChange";
    var MAPLE_EVENT_VIEWABLEDATA_CHANGE = "viewableDataChange";

    var MSG_TYPE_VIEWABLECHANGE = "ViewableChange";
    var MSG_TYPE_VIEWABLEDATACHANGE = "ViewableDataChange";
    var MSG_TYPE_SDKVERSION = "SdkVersion";

    var ADBROKER_PREBID_HEADER_APPID = "appId";
    var ADBROKER_PREBID_HEADER_APPGUID = "appGuid";
    var ADBROKER_PREBID_HEADER_DOMAIN = "domain";

    function MapleImpl() {
        this._listeners = [];
        this._adSize = { width: 0, height: 0 }; // default to 0x0 until SDK sets a value
        this._setSdkVersion("unknown", "unknown", "unknown");
        this._masterVolumeLevel = 0;
        this._masterVolumeMuted = false;
    }

    MapleImpl.prototype = {

        addEventListener: function (evt, callback) {
            /// <summary>
            /// Adds an listener for an event
            /// </summary>
            /// <param name="evt" type="String">The name of the event to listen for</param>
            /// <param name="callback" type="Function">Name of function or an anonymous function to execute when the event occurs</param>
            if (evt !== MAPLE_EVENT_VIEWABLEDATA_CHANGE &&
                evt !== MRAID_EVENT_VIEWABLE_CHANGE) {
                return;
            }

            if (!evt || typeof (callback) !== "function") {
                return;
            }

            if (this._listeners[evt] === null || typeof (this._listeners[evt]) === "undefined") {
                this._listeners[evt] = [];
            }

            VpaidImpl.log("adding listener for " + evt);
            this._listeners[evt].push(callback);

            if (evt === MRAID_EVENT_VIEWABLE_CHANGE) {
                VpaidImpl.sendMessageToAdSdk(MSG_TYPE_VIEWABLECHANGE + ":" + JSON.stringify({ listener: "start" }));
            } else if (evt === MAPLE_EVENT_VIEWABLEDATA_CHANGE) {
                VpaidImpl.sendMessageToAdSdk(MSG_TYPE_VIEWABLEDATACHANGE + ":" +JSON.stringify({ listener: "start" }));
            }
        },

        removeEventListener: function (evt, listener) {
            /// <summary>
            /// Use this method to unsubscribe a specific handler method from a specific event. Event listeners should
            /// always be removed when they are no longer useful to avoid errors. If no listener function is provided,
            /// then all functions listening to the event will be removed.
            /// </summary>
            /// <param name="evt" type="String">name of event to unsubscribe from</param>
            /// <param name="listener" type="Function">the function to unsubscribe</param>
            VpaidImpl.log("removing listener for " + evt);
            if (this._listeners && this._listeners[evt] !== null && typeof (this._listeners[evt]) !== "undefined") {
                if (listener === undefined || listener === null || listener === "") {
                    // Removes all listeners if no listener function has been specified.
                    this._listeners[evt].length = 0;
                }
                else {
                    var callbacks = this._listeners[evt];
                    for (var ix = 0; ix < callbacks.length; ix++) {
                        if (callbacks[ix] === listener) {
                            callbacks.splice(ix, 1);
                            break;
                        }
                    }
                }

                // Only send the stop message if no more client listeners are present
                if (this._listeners[evt] === null || typeof (this._listeners[evt]) === "undefined" || this._listeners[evt].length === 0) {
                    if (evt === MRAID_EVENT_VIEWABLE_CHANGE) {
                        VpaidImpl.log("stopping viewable changed event: " + evt + " (no more client listeners)");
                        VpaidImpl.sendMessageToAdSdk(MSG_TYPE_VIEWABLECHANGE + ":" + JSON.stringify({ listener: "stop" }));
                    } else if (evt === MAPLE_EVENT_VIEWABLEDATA_CHANGE) {
                        VpaidImpl.log("stopping viewable data changed event: " + evt + " (no more client listeners)");
                        VpaidImpl.sendMessageToAdSdk(MSG_TYPE_VIEWABLEDATACHANGE + ":" + JSON.stringify({ listener: "stop" }));
                    }
                }
            }
        },

        getSize: function () {
            /// <summary>
            /// Gets the current size of the ad
            /// </summary>
            /// <returns type="Object">the ad size in the format {width: 480, height: 800} or {} if not known</returns>
            if (typeof (this._adSize) === "undefined") {
                this._adSize = {};
            }
            return this._adSize;
        },

        getSdkVersion: function () {
            ///	<summary>
            /// MAPLE API
            /// Returns version number of SDK
            ///	</summary>
            /// <returns type="String">version number of the SDK</returns>
            if (!this._sdkInfo) {
                this._sdkInfo = {};
            }
            return this._sdkInfo;
        },

        receiveMessageFromAdSdk: function (msg) {
            if (!msg || typeof (msg) !== "string") {
                return;
            }

            var msgData = msg;
            var msgType = null;
            var msgContent = null;
            try {
                var colonIx = msgData.indexOf(":");
                if (colonIx < 0) {
                    msgType = msgData;
                } else {
                    msgType = msgData.substr(0, colonIx);
                    msgContent = msgData.substr(colonIx + 1);
                }

                if (msgType === MSG_TYPE_VIEWABLECHANGE) {
                    var isViewable = msgContent ? msgContent.toLowerCase() === "true" : false;
                    this._fireEvent(MRAID_EVENT_VIEWABLE_CHANGE, isViewable);
                } else if (msgType === MSG_TYPE_VIEWABLEDATACHANGE) {
                    var params = JSON.parse(msgContent);
                    this._setAppPrebidHeader(params[ADBROKER_PREBID_HEADER_APPID],
                                             params[ADBROKER_PREBID_HEADER_APPGUID],
                                             params[ADBROKER_PREBID_HEADER_DOMAIN]);

                    this._fireEvent(MAPLE_EVENT_VIEWABLEDATA_CHANGE, params);
                } else if (msgType === "ResizeAd") {// TODO make constants
                    var params = JSON.parse(msgContent);
                    this._setSize(params.width, params.height);
                } else if (msgType === MSG_TYPE_SDKVERSION) {
                    var params = JSON.parse(msgContent);
                    this._setSdkVersion(params.sdkVersion, params.client, params.runtimeType);
                    this._setAppPrebidHeader(params[ADBROKER_PREBID_HEADER_APPID],
                                             params[ADBROKER_PREBID_HEADER_APPGUID],
                                             params[ADBROKER_PREBID_HEADER_DOMAIN]);
                }
            }
            catch (err) {
                VpaidImpl.log("Maple error handling message of type: " + msgType + ": " + (err && err.message ? err.message : "unknown"));
                VpaidImpl.reportAdError("Maple error handling message of type " + msgType);
            }
        },

        _setSize: function (width, height) {
            /// <summary>
            /// Helper method for setting the size by ad control
            /// </summary>
            /// <param name="width" type="Number">the current state of the ad control</param>
            /// <param name="height" type="Number">the current state of the ad control</param>
            VpaidImpl.log("setting Maple size to " + width + " x " + height);
            this._adSize = {
                width: width,
                height: height
            };
        },

        _setSdkVersion: function (sdkVersion, client, runtimeType) {
            /// <summary>
            /// Helper method for setting the SDK version string
            /// </summary>
            /// <param name="sdkVersion" type="String">the sdk version string</param>
            /// <param name="client" type="String">the sdk client string</param>
            /// <param name="runtimeType" type="String">the type of runtime currently loaded</param>
            this._sdkInfo = {
                sdkVersion: sdkVersion,
                client: client,
                runtimeType: runtimeType,
                appName: navigator.appName,
                appVersion: navigator.appVersion,
                userAgent: navigator.userAgent,
                platform: navigator.platform
            };
            VpaidImpl.log("Ad SDK version: " + sdkVersion + ", " + client + ", " + runtimeType);
        },

        _setAppPrebidHeader: function (appId, appGuid, domain) {
            /// <summary>
            /// Helper method for setting the SDK version string
            /// </summary>
            /// <param name="sdkVersion" type="String">the sdk version string</param>
            /// <param name="client" type="String">the sdk client string</param>
            /// <param name="runtimeType" type="String">the type of runtime currently loaded</param>
            
            if (this.app === undefined)
                this.app = {};

            this.app.appId = appId;
            this.app.appGuid = appGuid;
            this.app.domain = domain;
        },

        _fireEvent: function (eventType, data) {
            /// <summary>
            /// Generic private function to throw events to listeners.
            /// </summary>
            /// <param name="eventType" type="String">The type of event being thrown.</param>
            /// <param name="data" type="Object">The data for the event (can be JSON or string depending on the event type).</param>
            VpaidImpl.log("raising event " + eventType + " with data " + data);

            if (this._listeners[eventType] !== null && typeof (this._listeners[eventType]) !== "undefined") {
                try {
                    // Make a copy of the listeners array in case it is modified by the listeners
                    // themselves (e.g. listener removes itself from list).
                    var callbacks = this._listeners[eventType].slice();
                    var ix;
                    switch (eventType) {
                        case MRAID_EVENT_VIEWABLE_CHANGE:
                        case MAPLE_EVENT_VIEWABLEDATA_CHANGE:
                            {
                                this._masterVolumeLevel = data.adMasterVolume;
                                this._masterVolumeMuted = data.adMasterVolumeMuted;
                                this._setAppPrebidHeader(data[ADBROKER_PREBID_HEADER_APPID],
                                                         data[ADBROKER_PREBID_HEADER_APPGUID],
                                                         data[ADBROKER_PREBID_HEADER_DOMAIN]);

                                for (ix = 0; ix < callbacks.length; ix++) {
                                    callbacks[ix](data);
                                }
                            }
                            break;
                        default:
                            return;
                    }
                }
                catch (err) {
                    VpaidImpl.log("exception thrown while firing event: " + err.message);
                }
            } else {
                VpaidImpl.log("no listeners for event " + eventType);
            }
        }

    };

    window.Maple = window.maple = window.MAPLE = new MapleImpl();

})();
