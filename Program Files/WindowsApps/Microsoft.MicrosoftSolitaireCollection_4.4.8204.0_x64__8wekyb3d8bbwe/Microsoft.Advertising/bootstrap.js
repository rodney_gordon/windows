﻿window.onerror = function (msg, url, lineNumber) {
    "use strict";
    try {
        // According to the WebView team, script error will not be thrown here. so we
        // needn't handle "mraid.js" script reference error here.
        console.error("[msg:" + msg + "][url:" + url + "][lineNumber:" + lineNumber + "]");
        window.external.notify("error:unhandled ad exception: " + msg);
    }
    catch (e) {
    }
    return true;
};

var localCompartment = null;
var adControlId = null;
var pointerDown;
var adParams = null;
var prmParams = null;
var appParams = null;
var MSG_TYPE_ADPARAMS = "adParams";
var MSG_TYPE_PRMPARAMS = "prmParams";
var MSG_TYPE_APPPARAMS = "appParams";
var MSG_TYPE_INIT = "init";
var MSG_TYPE_SETID = "setId";
var MSG_TYPE_ERROR = "error";

var MSG_TYPE_SETMAXSIZE = "setMaxSize";
var MSG_TYPE_SETSCREENSIZE = "setScreenSize";
var MSG_TYPE_SETSIZE = "setSize";
var MSG_TYPE_SETSTATE = "setState";
var MSG_TYPE_SETPLACEMENTTYPE = "setPlacementType";
var MSG_TYPE_WIREAPPEVENTS = "wireAppEvents";
var MSG_TYPE_START = "ormmaStart";
var MSG_TYPE_ORMMA_RESPONSE = "ormmaResponse";
var MSG_TYPE_FIRESHAKE = "fireShake";
var MSG_TYPE_UPDATETILTCOORDS = "updateTiltCoords";
var MSG_TYPE_UPDATEORIENTATION = "updateOrienation";
var MSG_TYPE_VIEWABLECHANGE = "viewableChange";
var MSG_TYPE_SETNETWORK = "setNetwork";
var MSG_TYPE_SETLOCALE = "setLocale";
var MSG_TYPE_SETSDKINFO = "setSdkInfo";
var MSG_TYPE_SETCAPABILITY = "setCapability";
var MSG_TYPE_SETFOCUS = "setFocus";
var MSG_TYPE_ONPOINTERDOWN = "MSPointerDown";
var MSG_TYPE_ONPOINTERUP = "MSPointerUp";
var MSG_TYPE_ONPOINTERMOVE = "MSPointerMove";
var MSG_TYPE_ONMOUSEWHEEL = "MSMouseWheel";
var MSG_TYPE_ONMANIPSTATECHANGED = "MSManipulationStateChanged";

// If updating this, make sure to also update in ad control.
var EVENT_TYPE_ENUM = {
    All: ~0,
    PointerDown: 1 << 0,
    PointerUp: 1 << 1,
    PointerMove: 1 << 2,
    // 1 << 3, was used by pointer hover but freed up due to TFS #864590
    MouseWheel: 1 << 4,
    ManipulationStateChanged: 1 << 5
    // We can only send a max of 1<<31 events this way.
};

var applicationEventHandlers = {
    onPointerDown: null,
    onPointerUp: null,
    onMouseWheel: null,
    onPointerMove: null,
    onManipulationStateChanged: null
};

/* receives string message from c# layer via invoke script and forwards to receiveMessage as event */
function receiveMessageString(data) {
    "use strict";
    var msg = new Object();
    msg.data = data;
    msg.source = "";
    receiveMessage(msg);
}

function receiveMessage(msg) {
    "use strict";
    if (localCompartment === null) {
        localCompartment = msg.source;
    }

    if (MSG_TYPE_ADPARAMS === msg.data.substr(0, MSG_TYPE_ADPARAMS.length)) {
        var toEval = msg.data.substr(MSG_TYPE_ADPARAMS.length + 1);
        if (toEval !== "") {
            try {
                adParams = JSON.parse(toEval);
            }
            catch (err) {
                reportAdError("Ad params JSON parse error: " + err.message);
            }
        }
    }
    else if (MSG_TYPE_PRMPARAMS === msg.data.substr(0, MSG_TYPE_PRMPARAMS.length)) {
        var toEval = msg.data.substr(MSG_TYPE_PRMPARAMS.length + 1);
        if (toEval !== "") {
            try {
                prmParams = JSON.parse(toEval);
            }
            catch (err) {
                reportAdError("Prm params JSON parse error: " + err.message);
            }
        }
    }
    else if (MSG_TYPE_APPPARAMS === msg.data.substr(0, MSG_TYPE_APPPARAMS.length)) {
        var toEval = msg.data.substr(MSG_TYPE_APPPARAMS.length + 1);
        if (toEval !== "") {
            try {
                appParams = JSON.parse(toEval);
            }
            catch (err) {
                reportAdError("App options JSON parse error: " + err.message);
            }
        }
    }
    else if (MSG_TYPE_SETID === msg.data.substr(0, MSG_TYPE_SETID.length)) {
        adControlId = msg.data.substr(MSG_TYPE_SETID.length + 1);
    }
    else if (MSG_TYPE_INIT === msg.data.substr(0, MSG_TYPE_INIT.length)) {

    }
        // Handle Ormma message
    else {
        if (typeof (Ormma) !== "undefined" && Ormma !== null) {

            var msgData = msg.data;
            var msgType = null;
            var msgContent = null;
            var colonIx = msgData.indexOf(":");
            if (colonIx < 0) {
                msgType = msgData;
            } else {
                msgType = msgData.substr(0, colonIx);
                msgContent = msgData.substr(colonIx + 1);
            }

            if (msgType === MSG_TYPE_SETSIZE) {
                var size = JSON.parse(msgContent);
                Ormma._setSize(size.width, size.height);
            } else if (msgType === MSG_TYPE_SETMAXSIZE) {
                var maxSize = JSON.parse(msgContent);
                Ormma._maxSize = maxSize;
            } else if (msgType === MSG_TYPE_SETSCREENSIZE) {
                var screenSize = JSON.parse(msgContent);
                Ormma._setScreenSize(screenSize.width, screenSize.height);
            } else if (msgType === MSG_TYPE_SETSTATE) {
                Ormma._setState(msgContent);
            } else if (msgType === MSG_TYPE_SETLOCALE) {
                Ormma._setLocale(msgContent);
            } else if (msgType === MSG_TYPE_SETSDKINFO) {
                var sdkInfo = JSON.parse(msgContent);
                Ormma._setSdkVersion(sdkInfo.sdkVersion, sdkInfo.client, sdkInfo.runtimeType);
            } else if (msgType === MSG_TYPE_SETCAPABILITY) {
                var capabilityInfo = JSON.parse(msgContent);
                Ormma._setCapability(capabilityInfo.capability, capabilityInfo.value);
            } else if (msgType === MSG_TYPE_SETPLACEMENTTYPE) {
                Ormma._setPlacementType(msgContent);
            } else if (msgType === MSG_TYPE_START) {
                Ormma._init(msgContent);
            } else if (msgType === MSG_TYPE_FIRESHAKE) {
                Ormma._shake();
            } else if (msgType === MSG_TYPE_ORMMA_RESPONSE) {
                Ormma._sendResponse(JSON.parse(msgContent));
            } else if (msgType === MSG_TYPE_UPDATETILTCOORDS) {
                var coords = JSON.parse(msgContent);
                Ormma._tiltChange(coords);
            } else if (msgType === MSG_TYPE_VIEWABLECHANGE) {
                var viewable = JSON.parse(msgContent);
                Ormma._viewableChange(viewable.viewable);
            } else if (msgType === MSG_TYPE_UPDATEORIENTATION) {
                var orienation = JSON.parse(msgContent);
                Ormma._setOrientation(orienation.orientation);
            } else if (msgType === MSG_TYPE_SETNETWORK) {
                Ormma._setNetwork(msgContent);
            } else if (msgType === MSG_TYPE_WIREAPPEVENTS) {
                var params = JSON.parse(msgContent);
                _wireApplicationEvents(params);
            } else if (msgType === MSG_TYPE_ERROR) {
                var errorInfo = JSON.parse(msgContent);
                Ormma._throwError(errorInfo.action, errorInfo.message);
            } else if (msgType === MSG_TYPE_SETFOCUS) {
                if (msgContent === "true") {
                    document.body.focus();
                }
            }
        }
    }
};

function reportAdError(msg) {
    "use strict";
    console.log(msg);
    postToLocal(MSG_TYPE_ERROR + ":" + msg);
};

function postToLocal(msg) {
    "use strict";
    if (localCompartment != null) {
        window.external.notify(msg);
    }
};

function _wireApplicationEvents(params) {
    "use strict";
    if (params === null || typeof (params) === "undefined") {
        return;
    }

    var eventMask = parseInt(params.events);

    // NOTE: these MUST be attached to window and using bubbling (false param to addEventListener), otherwise the renderer will not be
    //   able to intercept and cancel (stopImmediatePropagation()) on these events if needed!
    //   There renderer will attach to a more down level element (ie: window.document). Because these are in bubbling phase the renderer
    //   will be able to call stopImmediatePropagation and thus the event will stop bubbling at that point. Bubbling events execute
    //   on the lowest level element first.

    if ((eventMask & EVENT_TYPE_ENUM.PointerDown) !== 0 && typeof (applicationEventHandlers.onPointerDown) !== "function") {
        applicationEventHandlers.onPointerDown = function (evt) {
            if (evt.which === 2 && params.preventDefault) {
                evt.preventDefault(); // prevent the default windows handling of mouse wheel, hard code for appex...
            }

            pointerDown = true;
            postToLocal(MSG_TYPE_ONPOINTERDOWN + ":" + JSON.stringify({
                "clientX": evt.clientX,
                "clientY": evt.clientY,
                "pointerType": evt.pointerType,
                "which": evt.which
            }));
        };

        window.addEventListener("MSPointerDown", applicationEventHandlers.onPointerDown, false);
    } else if ((eventMask & EVENT_TYPE_ENUM.PointerDown) === 0 && typeof (applicationEventHandlers.onPointerDown) === "function") {
        window.removeEventListener("MSPointerDown", applicationEventHandlers.onPointerDown, false);
        applicationEventHandlers.onPointerDown = null;
    }

    if ((eventMask & EVENT_TYPE_ENUM.PointerUp) !== 0 && typeof (applicationEventHandlers.onPointerUp) !== "function") {
        applicationEventHandlers.onPointerUp = function (evt) {
            if (evt.type === "pointerout") {
                if (pointerDown) {
                    pointerDown = false;
                    postToLocal(MSG_TYPE_ONPOINTERUP);
                }
            }
            else {
                pointerDown = false;
                postToLocal(MSG_TYPE_ONPOINTERUP);
            }
        };

        window.addEventListener("MSPointerUp", applicationEventHandlers.onPointerUp, false);
        window.addEventListener("MSPointerCancel", applicationEventHandlers.onPointerUp, false);
        window.addEventListener("MSPointerOut", applicationEventHandlers.onPointerUp, false);
        window.addEventListener("MSLostPointerCapture", applicationEventHandlers.onPointerUp, false);
    } else if ((eventMask & EVENT_TYPE_ENUM.PointerUp) === 0 && typeof (applicationEventHandlers.onPointerUp) === "function") {
        window.removeEventListener("MSPointerUp", applicationEventHandlers.onPointerUp, false);
        window.removeEventListener("MSPointerCancel", applicationEventHandlers.onPointerUp, false);
        window.removeEventListener("MSPointerOut", applicationEventHandlers.onPointerUp, false);
        window.removeEventListener("MSLostPointerCapture", applicationEventHandlers.onPointerUp, false);
        applicationEventHandlers.onPointerUp = null;
    }

    if ((eventMask & EVENT_TYPE_ENUM.PointerMove) !== 0 && typeof (applicationEventHandlers.onPointerMove) !== "function") {
        applicationEventHandlers.onPointerMove = function (evt) {
            postToLocal(MSG_TYPE_ONPOINTERMOVE + ":" + JSON.stringify({
                "clientX": evt.clientX,
                "clientY": evt.clientY,
            }));
        };

        window.addEventListener("MSPointerMove", applicationEventHandlers.onPointerMove, false);
    } else if ((eventMask & EVENT_TYPE_ENUM.PointerMove) === 0 && typeof (applicationEventHandlers.onPointerMove) === "function") {
        window.removeEventListener("MSPointerMove", applicationEventHandlers.onPointerMove, false);
        applicationEventHandlers.onPointerMove = null;
    }

    if ((eventMask & EVENT_TYPE_ENUM.ManipulationStateChanged) !== 0 && typeof (applicationEventHandlers.onManipulationStateChanged) !== "function") {
        applicationEventHandlers.onManipulationStateChanged = function (evt) {
            postToLocal(MSG_TYPE_ONMANIPSTATECHANGED + ":" + JSON.stringify({
                "lastState": evt.lastState,
                "currentState": evt.currentState
            }));
        };

        window.addEventListener("MSManipulationStateChanged", applicationEventHandlers.onManipulationStateChanged, false);
    } else if ((eventMask & EVENT_TYPE_ENUM.ManipulationStateChanged) === 0 && typeof (applicationEventHandlers.onManipulationStateChanged) === "function") {
        window.removeEventListener("MSManipulationStateChanged", applicationEventHandlers.onManipulationStateChanged, false);
        applicationEventHandlers.onManipulationStateChanged = null;
    }

    if ((eventMask & EVENT_TYPE_ENUM.MouseWheel) !== 0 && typeof (applicationEventHandlers.onMouseWheel) !== "function") {
        applicationEventHandlers.onMouseWheel = function (evt) {
            if (params.preventDefault) {
                evt.preventDefault(); // prevent the default windows handling of mouse wheel, hard code for appex...
            }

            postToLocal(MSG_TYPE_ONMOUSEWHEEL + ":" + JSON.stringify({
                "clientX": evt.clientX,
                "clientY": evt.clientY,
                "ctrlKey": evt.ctrlKey,
                "wheelDelta": evt.wheelDelta
            }));
        };

        window.addEventListener("mousewheel", applicationEventHandlers.onMouseWheel, false);
    } else if ((eventMask & EVENT_TYPE_ENUM.MouseWheel) === 0 && typeof (applicationEventHandlers.onMouseWheel) === "function") {
        window.removeEventListener("mousewheel", applicationEventHandlers.onMouseWheel, false);
        applicationEventHandlers.onMouseWheel = null;
    }
};