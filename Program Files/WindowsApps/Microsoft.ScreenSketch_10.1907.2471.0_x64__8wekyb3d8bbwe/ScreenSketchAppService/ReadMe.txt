Note that the code files showing in the ScreenSketchAppService/AppCode folder
are only linked here, but are actually located in the app project folder.
To avoid collisions - all of the types in these files are compiled twice -
once in the app namespace, and once in the app service namespace.
This makes it easier to reuse the code without significant refactoring.
