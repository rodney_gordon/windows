# Localized	12/07/2019 11:39 AM (GMT)	303:6.40.20520 	WindowsPackageCab.Strings.psd1
# Localized WindowsPackageCab.Strings.psd1

ConvertFrom-StringData @'
###PSLOC
SourcePathDoesNotExist=Die Quelle ist nicht vorhanden: {0}
ConfigurationStarted=Die Konfiguration der WindowsPackageCab-Ressource wird gestartet.
ConfigurationFinished=Die Konfiguration der WindowsPackageCab-Ressource ist abgeschlossen.
FailedToAddPackage=Fehler beim Hinzufügen des Pakets von {0}
FailedToRemovePackage=Fehler beim Entfernen des Pakets {0}
SourcePathInvalid=Der Quellpfad ist NULL oder leer.
###PSLOC

'@
