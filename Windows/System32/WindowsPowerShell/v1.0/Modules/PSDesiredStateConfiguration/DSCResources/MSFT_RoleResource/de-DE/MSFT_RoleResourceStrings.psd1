# Localized	12/07/2019 11:49 AM (GMT)	303:6.40.20520 	MSFT_RoleResourceStrings.psd1
# Localized MSFT_RoleResource.psd1

ConvertFrom-StringData @'
###PSLOC
SetTargetResourceInstallwhatIfMessage=Es wird versucht, das Feature "{0}" zu installieren
SetTargetResourceUnInstallwhatIfMessage=Es wird versucht, das Feature "{0}" zu deinstallieren
FeatureNotFoundError=Das angeforderte Feature "{0}" wurde nicht auf dem Zielcomputer gefunden.
FeatureDiscoveryFailureError=Fehler beim Abrufen der angeforderten Informationen zum Feature "{0}" vom Zielcomputer. Platzhaltermuster werden im Featurenamen nicht unterstützt.
FeatureInstallationFailureError=Fehler beim Installieren des Features "{0}".
FeatureUnInstallationFailureError=Fehler beim Deinstallieren des Features "{0}".
QueryFeature=Feature "{0}" wird mit dem Server-Manager-Cmdlet "Get-WindowsFeature" abgefragt.
InstallFeature=Es wird versucht, das Feature "{0}" mit dem Server-Manager-Cmdlet "Add-WindowsFeature" zu installieren.
UninstallFeature=Es wird versucht, das Feature "{0}" mit dem Server-Manager-Cmdlet "Remove-WindowsFeature" zu deinstallieren.
RestartNeeded=Der Zielcomputer muss neu gestartet werden.
GetTargetResourceStartVerboseMessage=Ausführung der Get-Funktion für das Feature "{0}" starten.
GetTargetResourceEndVerboseMessage=Ausführung der Get-Funktion für das Feature "{0}" beenden.
SetTargetResourceStartVerboseMessage=Ausführung der Set-Funktion für das Feature "{0}" starten.
SetTargetResourceEndVerboseMessage=Ausführung der Set-Funktion für das Feature "{0}" beenden.
TestTargetResourceStartVerboseMessage=Ausführung der Test-Funktion für das Feature "{0}" starten.
TestTargetResourceEndVerboseMessage=Ausführung der Test-Funktion für das Feature "{0}" beenden.
ServerManagerModuleNotFoundDebugMessage=Das ServerManager-Modul ist nicht auf dem Computer installiert.
SkuNotSupported=Die Installation von Rollen und Features mit PowerShell Desired State Configuration wird nur für Server-SKUs unterstützt. Für Client-SKUs ist diese Funktion nicht verfügbar.
SourcePropertyNotSupportedDebugMessage=Die Quelleigenschaft in "MSFT_RoleResource" wird auf diesem Betriebssystem nicht unterstützt und wurde ignoriert.
EnableServerManagerPSHCmdletsFeature=Windows Server 2008R2 Core-Betriebssystem erkannt: Das Feature "ServerManager-PSH-Cmdlets" wurde deaktiviert.
UninstallSuccess=Die Funktion '{0}' wurde erfolgreich deinstalliert.
InstallSuccess=Das Feature {0} wurde erfolgreich installiert.
###PSLOC

'@
