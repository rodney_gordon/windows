# Localized	12/07/2019 11:38 AM (GMT)	303:6.40.20520 	RunAsHelper.strings.psd1
# Localized resources for RunAsHelper

ConvertFrom-StringData @'
###PSLOC
ErrorInvalidUserName="UserName" {0} ist ungültig.
UserCouldNotBeLoggedError=Der Benutzer konnte nicht angemeldet werden. Stellen Sie sicher, dass der Benutzer auf dem Computer über ein vorhandenes Profil verfügt und dass die richtigen Anmeldeinformationen angegeben wurden. Anmeldefehlernummer:
OpenProcessTokenError=Fehler beim Öffnen des Prozesstokens. Fehler-Nr.
PrivilegeLookingUpError=Fehler beim Suchen der Prozessberechtigung. Dies sollte nicht geschehen, wenn der DSC als LocalSystem ausgeführt wird. Fehler beim Suchen einer Berechtigung. Fehler-Nr.
TokenElevationError=Fehler bei Tokenerhöhung. Fehler-Nr.
DuplicateTokenError=Fehler aufgrund doppelter Token. Fehler-Nr.
CouldNotCreateProcessError=Der Prozess konnte nicht erstellt werden. Fehler beim Erstellen des Prozesses als Benutzer. Fehler-Nr.
WaitFailedError=Fehler beim Warten auf die Prozesserstellung. Benutzerfehler-Nr.
RetriveStatusError=Fehler beim Statusabruf. Fehler-Nr.
###PSLOC

'@
