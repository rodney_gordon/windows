# Localized	12/07/2019 11:38 AM (GMT)	303:6.40.20520 	PSDSCxMachine.strings.psd1
# Localized resources for MSFT_EnvironmentResource

ConvertFrom-StringData @'
###PSLOC
InvalidInputParam=Ungültige Eingabeparameter.
InvalidInputThrottle=Ungültige Eingabedrosselungsgrenze.
CheckRemoteState=Der Status von Remoteressource "{0}" wird überprüft...
RemoteResourceNotReady=Die Remoteressource "{0}" ist nicht bereit.
RemoteResourceNotReadyAndRetry=Die Remoteressource "{0}" ist nicht bereit. Der Vorgang wird nach {1} Sekunde(n) wiederholt.
RemoteResourceReady=Die Remoteressource "{0}" ist bereit.
RemoteConfigNotReady=Die Ressource "{0}"auf dem/den Computer(n) "{1}" ist nicht bereit.
RemoteConfigNotReadyWithRetry=Die Ressource "{0}"auf dem/den Computer(n) "{1}" ist nach {2} Wiederholungsversuchen mit einem Wiederholungsintervall von {3} Sekunde(n) noch nicht bereit.
NodeNameHasDuplicates=NodeName enthält doppelte Computernamen. Überprüfen Sie NodeName und die erwarteten MinimalNumberOfMachineInState-Parameter.
RemoteConnectivityFailure=Es kann keine Remotesitzung für Computer "{0}" erstellt werden.
###PSLOC
'@
