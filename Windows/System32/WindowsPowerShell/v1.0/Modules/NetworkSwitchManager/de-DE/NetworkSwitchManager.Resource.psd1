# Localized	12/07/2019 11:42 AM (GMT)	303:6.40.20520 	NetworkSwitchManager.Resource.psd1
#################################################################
#                                                               #
#   Module Name: NetworkSwitchManager.Resources.psd1            #
#                                                               #
#   Description: Network switch manager localized strings       #
#                                                               #
#   Copyright (c) Microsoft Corporation. All rights reserved.   #
#                                                               #
#################################################################

ConvertFrom-StringData @'
###PSLOC
ErrorMessageNoTarget='{0}' hat einen Fehler festgestellt. Stellen Sie sicher, dass Sie eine gültige CIM-Sitzung verwenden und dass das Ethernet-Switch-Profil ordnungsgemäß registriert ist. Wenn Sie Eingabeobjekte übergeben, müssen diese gültige Instanzen vom richtigen Typ sein. Weitere Informationen zur Fehlerursache finden Sie im Exception-Member dieses Fehlerdatensatzes.
ErrorMessageTarget='{0}' hat bei der Verarbeitung von '{1}' einen Fehler festgestellt. Stellen Sie sicher, dass Sie eine gültige CIM-Sitzung verwenden und dass das Ethernet-Switch-Profil ordnungsgemäß registriert ist. Wenn Sie Eingabeobjekte übergeben, müssen diese gültige Instanzen vom richtigen Typ sein. Weitere Informationen zur Fehlerursache finden Sie im Exception-Member dieses Fehlerdatensatzes.
WarningMessageNoTarget=Warnung für '{0}': {1}.
WarningMessageTarget=Warnung für '{0}' bei der Verarbeitung von '{1}': {2}.
UnknownError=Unbekannter Fehler. Fehlercode: {0}.
NoValidAssociatedSwitch=Das registrierte Ethernet-Switch-Profil ist keinem übereinstimmenden Switch zugeordnet. Registrieren Sie Ihren Switch in Übereinstimmung mit dem Ethernet-Switch-Profil.
NoValidAssociatedNamespace=Der Switch, der in Übereinstimmung mit dem Ethernet-Switch-Profil registriert wurde, ist keinem gültigen Namespace zugeordnet. Überprüfen Sie die Implementierung und Registrierung für den Switch.
NoValidRegisteredProfile=Das Ethernet-Switch-Profil wurde nicht in 'root/interop', 'interop', '/root/interop' oder '/interop' registriert. Registrieren Sie das Ethernet-Switch-Profil in einem dieser Namespaces.
NoMatchingInstance=Es wurden keine Instanzen gefunden, die mit den Suchkriterien übereinstimmen: {0} = {1}.
###PSLOC
'@
