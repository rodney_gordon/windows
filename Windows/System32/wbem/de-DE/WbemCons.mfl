// Copyright (c) 1997-2003 Microsoft Corporation, All Rights Reserved

instance of __namespace{ name="ms_407";};
#pragma namespace("ms_407")

[Description("Protokolliert Ereignisse in einer Textdatei.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407)] 
class LogFileEventConsumer : __EventConsumer
{
  [key,Description("Ein eindeutiger Bezeichner für diese Instanz") : Amended ToSubclass] string Name;
  [Description("Der vollständig qualifizierte Pfadname für die Protokolldatei. Falls die Datei nicht vorhanden ist, wird sie erstellt. Die Datei wird nicht erstellt, falls das Verzeichnis nicht vorhanden ist.") : Amended ToSubclass] string Filename;
  [Description("Meldung, die in die Protokolldatei eingefügt wird.") : Amended ToSubclass] string Text;
  [Description("Die maximal zugelassen Größe der Datei. Wenn die Datei die Größe überschreitet, wird sie aktiviert. Archivierte Dateien haben eine Erweiterung zwischen 0,001 und 0,999. Ein Wert von null bedeutet, dass die Datei nicht archiviert wird. ") : Amended ToSubclass] uint64 MaximumFileSize;
  [Description("Die Datei hat kein Unicode-Format, falls der Wert FALSE oder NULL ist.") : Amended ToSubclass] boolean IsUnicode;
};

[Description("Führt einen Befehl als Reaktion zu einem Ereignis aus. Weitere Informationen über vollständige Beschreibungen von Eigenschaften erhalten Sie in der Win32-SDK-Dokumentation für \"CreateProcess\".") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407)] 
class CommandLineEventConsumer : __EventConsumer
{
  [key,Description("Ein eindeutiger Bezeichner für diese Instanz") : Amended ToSubclass] string Name;
  [Description("Name des ausführbaren Moduls") : Amended ToSubclass] string ExecutablePath;
  [Description("Befehlszeilen-Zeichenfolge") : Amended ToSubclass] string CommandLineTemplate;
  [Description("Neuer Prozess erbt den Fehlermodus des aufrufenden Prozesses. ") : Amended ToSubclass] boolean UseDefaultErrorMode;
  [Description("Der neue Prozess hat eine neuen Konsole. Hinweis: Diese Eigenschaft wurde in Windows XP verworfen und wird nicht mehr verwendet.") : Amended ToSubclass] boolean CreateNewConsole;
  [Description("Der neue Prozess ist der Stammprozess einer neuen Prozessgruppe.") : Amended ToSubclass] boolean CreateNewProcessGroup;
  [Description("Der neue Prozess wird in einer privaten VDM (Virtual DOS Machine) ausgeführt.") : Amended ToSubclass] boolean CreateSeparateWowVdm;
  [Description("Falls die Option \"DefaultSeparateVDM \" im Windows-Abschnitt von WIN.INI TRUE ist, verursacht dieses Kennzeichen, dass die Funktion \"CreateProcess\" die Option außer Kraft setzt und den neuen Prozess in der freigegebenen VDM (Virtual DOS Machine) ausführt.") : Amended ToSubclass] boolean CreateSharedWowVdm;
  [Description("Wird zum Festlegen der Zeitplanprioritäten der Prozessthreads verwendet.") : Amended ToSubclass] sint32 Priority;
  [Description("Das vom Prozess verwendete Standardverzeichnis") : Amended ToSubclass] string WorkingDirectory;
  [Description("Nur der Name des Desktops oder der Name des Desktops und der Fensterstation für diesen Prozess. Hinweis: Diese Eigenschaft wurde in Windows XP verworfen und wird nicht mehr verwendet.") : Amended ToSubclass] string DesktopName;
  [Description("Dies ist der Titel in der Titelleiste für Konsolenprozesse, falls ein neues Konsolenfenster erstellt wird.") : Amended ToSubclass] string WindowTitle;
  [Description("Bestimmt die X-Koordinate des oberen linken Ecke des Fensters.") : Amended ToSubclass] uint32 XCoordinate;
  [Description("Bestimmt die Y-Koordinate des oberen linken Ecke des Fensters.") : Amended ToSubclass] uint32 YCoordinate;
  [Description("Bestimmt die Breite des Fensters.") : Amended ToSubclass] uint32 XSize;
  [Description("Bestimmt die Höhe des Fensters") : Amended ToSubclass] uint32 YSize;
  [Description("Bestimmt die Breite des Fenster für Konsolenprozesse in Zeichen.") : Amended ToSubclass] uint32 XNumCharacters;
  [Description("Bestimmt die Höhe des Fenster für Konsolenprozesse in Zeichen.") : Amended ToSubclass] uint32 YNumCharacters;
  [Description("Bestimmt den ursprünglichen Text und Hintergrundfarben für ein in einer Konsolenanwendung neu erstelltes Konsolenfenster.") : Amended ToSubclass] uint32 FillAttribute;
  [Description("\"ShowWindowCommand\" kann eine der in WINUSER.H definierten Konstanten \"SW_\" sein.") : Amended ToSubclass] uint32 ShowWindowCommand;
  [Description("Zeigt an, dass der Cursor sich zwei Sekunden nach Aufrufen von \"CreateProcess\" im Feedbackmodus befindet.") : Amended ToSubclass] boolean ForceOnFeedback;
  [Description("Zeigt an, dass der Feedbackcursor während des Prozessstarts nicht aktiv ist.") : Amended ToSubclass] boolean ForceOffFeedback;
  [Description("Zeigt an, dass der Prozess auf das Desktop zugreifen kann. ") : Amended ToSubclass] boolean RunInteractively;
  [description("Anzahl der Sekunden, in denen der untergeordnete Prozess ausgeführt werden darf. Falls der Wert null ist, wird der Prozess nicht beendet.") : Amended ToSubclass] uint32 KillTimeout;
};

[description("Protokolliert Ereignisse in das NT-Ereignisprotokoll. Vollständige Beschreibungen erhalten Sie in der Win32-SDK-Dokumentation.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407)] 
class NTEventLogEventConsumer : __EventConsumer
{
  [key,Description("Ein eindeutiger Bezeichner für diese Instanz") : Amended ToSubclass] string Name;
  [Description("UNC-Name (Universal Naming Convention) des Servers, auf dem dieser Vorgang ausgeführt werden soll. Falls der Wert NULL ist, wird der Vorgang auf dem lokalen Computer ausgeführt.") : Amended ToSubclass] string UNCServerName;
  [Description("Der Quellname muss ein Unterschlüssel eines Protokolldateieintrags unter dem Schlüssel \"EventLog\" in der Registrierung sein.") : Amended ToSubclass] string SourceName;
  [Description("Die Ereigniskennung legt die Meldung für das Ereignis fest, und zwar als einen Eintrag in der mit der Ereignisquelle assoziierten Meldungsdatei.") : Amended ToSubclass] uint32 EventID;
  [Description("Bestimmt den zu protokollierenden Ereignistyp.") : Amended ToSubclass] uint32 EventType;
  [Description("Bestimmt die Ereigniskategorie. Hierbei handelt es sich um Quellspezifische Informationen.") : Amended ToSubclass] uint16 Category;
  [Description("Anzahl der Zeichenfolgen, die in den Ereignistext eingefügt werden.") : Amended ToSubclass] uint32 NumberOfInsertionStrings;
  [Description("Zeichenfolgen, die in den Ereignistext eingefügt werden.") : Amended ToSubclass] string InsertionStringTemplates[];
  [Description("Die Eigenschaft \"NameOfRawDataProperty\" ist der Name für die Eigenschaft im Ereignis, das die \"UserSID\" in einer Zeichenfolge oder einem Array von Bytes enthält. Daraus entsteht der Paramater \"lpUserSid\" in der Funktion \"ReportEvent API\"."): Amended ToSubclass] string NameOfRawDataProperty;
  [Description("Die Eigenschaft \"NameOfUserSIDProperty\" ist der Name für die Eigenschaft im Ereignis, das ein Array von Bytes oder eine Zeichenfolge enthält. Daraus entsteht der Paramater \"lpRawData\" in der Funktion \"ReportEvent API\"."): Amended ToSubclass] string NameOfUserSIDProperty;
};
