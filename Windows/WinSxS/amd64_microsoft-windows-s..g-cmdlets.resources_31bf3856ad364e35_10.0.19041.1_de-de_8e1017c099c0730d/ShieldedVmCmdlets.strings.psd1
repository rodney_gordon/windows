# Localized	12/07/2019 11:40 AM (GMT)	303:6.40.20520 	ShieldedVmCmdlets.strings.psd1
ConvertFrom-StringData @'
    ###PSLOC
UnexpectedError=Unerwarteter Fehler
PathValidError="{0}" ist kein gültiger Pfad.
FileDoesNotExistError=Die Datei "{0}" ist nicht vorhanden.
FileLoadError={0} konnte nicht geladen werden.
CouldNotCreateFSK=Fehler beim Erstellen der Spezialisierungsdatendatei
FSKFileExtensionError=Die Dateierweiterung muss ".fsk" sein.
PDKFileExtensionError=Die Dateierweiterung muss '.pdk' sein.
FabricDataTypeError=SpecializationDataPairs muss Einträge vom Typ [string] enthalten.
VirtualMachineDoesNotExist=Der virtuelle Computer "{0}" wurde nicht gefunden.
ProvisioningJobNotFound=Es wurde kein übereinstimmender Bereitstellungsauftrag gefunden. Der Bereitstellungsauftrag wurde nach Abschluss u. U. entfernt.
CimTypeError=Die bereitgestellte CimInstance muss über einen erweiterten Typ von Msps_ProvisioningJob verfügen.
CimTypeErrorPDK=Die bereitgestellte Datei war keine gültige geschützte Datendatei.
'@
