# Localized	12/07/2019 11:47 AM (GMT)	303:6.40.20520 	CL_LocalizationData.psd1
ConvertFrom-StringData @'
###PSLOC
ID_PROG_MAIN_Initializing=Diagnose wird initialisiert...
ID_Check_Bluetooth=Bluetooth-Funktion wird gesucht
ID_PROG_DriverProblems=Bluetooth-Einstellungen werden überprüft
ID_RS_PROG_DriverProblems=Bluetooth-Adapter wird zurückgesetzt
ID_Check_Radio_Bluetooth=Es wird überprüft, ob Bluetooth aktiviert ist.
ID_TurnOn_Bluetooth=Bluetooth wird eingeschaltet
ID_EnableDevice=Gerät wird aktiviert
###PSLOC
'@
