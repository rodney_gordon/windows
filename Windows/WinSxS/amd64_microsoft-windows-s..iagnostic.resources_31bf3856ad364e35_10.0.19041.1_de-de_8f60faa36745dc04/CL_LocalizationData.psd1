# Localized	12/07/2019 11:56 AM (GMT)	303:6.40.20520 	CL_LocalizationData.psd1
ConvertFrom-StringData @'
###PSLOC
progress_ts_indexingService=Windows Search wird überprüft
progress_ts_indexerCrashing=Es wird auf vor kurzem aufgetretene Abstürze von Windows Search geprüft
progress_ts_filterHostCrashing=Es wird auf vor kurzem aufgetretene Filterhostabstürze von Windows Search geprüft.
progress_ts_protocolHostCrashing=Es wird auf vor kurzem aufgetretene Protokollhostabstürze von Windows Search geprüft.
progress_ts_indexingServiceForcedShutdown=Es wird auf erzwungene Herunterfahrereignisse geprüft
progress_ts_indexingServiceFailedRecovery=Es wird auf gescheiterte Wiederherstellungsereignisse geprüft
progress_ts_checkPermissions=Es wird auf ungültige Berechtigungen für Indizierungsverzeichnisse geprüft
progress_ts_searchApp=Windows Search wird überprüft ...
progress_rs_restorePermissions=Korrekte Berechtigungen für Indizierungsverzeichnisse werden wiederhergestellt
progress_rs_startIndexingService=Windows Search wird gestartet
progress_rs_restoreDefaults=Standardeinstellungen werden wiederhergestellt, und Windows Search wird neu gestartet
progress_rs_restoreSearchApp=Windows Search-Anwendung wird erneut gestartet
indexerEvent_name=Windows Search-Ereignisse
indexerEvent_description=Die Ereignisse, die auf einen Absturz von Windows Search hinweisen. Die Ereignisladung gibt möglicherweise Aufschluss über das fehlerhafte Modul.
filterHostEvent_name=Filterhostereignisse von Windows Search
filterHostEvent_description=Die Ereignisse, die auf einen Absturz im Filterhost von Windows Search hinweisen. Die Ereignisladung gibt möglicherweise Aufschluss über das fehlerhafte Modul.
protocolHostEvent_name=Protokollhostereignisse von Windows Search
protocolHostEvent_description=Die Ereignisse, die auf einen Absturz im Protokollhost von Windows Search hinweisen. Die Ereignisladung gibt möglicherweise Aufschluss über das fehlerhafte Modul.
dataDirectory_name=Verzeichnis
dataDirectory_description=Datenverzeichnis von Windows Search
dataDirectoryOwner_name=Besitzer des Datenverzeichnisses
dataDirectoryOwner_description=Besitzer des Windows Search-Datenverzeichnisses
dataDirectoryPermissions_name=Datenverzeichnisberechtigungen
dataDirectoryPermissions_description=Berechtigungen für Windows Search-Datenverzeichnis
problemType_name=Problemtyp
problemType_description=Vom Benutzer gemeldete Probleme
userProblem_name=Problem
userProblem_description=Benutzerdefiniertes Problem
error_information=Ausnahmeinformationen
error_function_name=Fehler beim Aufrufen von "{0}".
error_function_description=Ausnahme beim Aufrufen der Funktion "{0}".
error_file_name=Fehler beim Ausführen von "{0}".
error_file_description=Ausnahme beim Ausführen der Datei "{0}".
throw_invalidFileName=Ungültiger Dateiname
throw_noExecutableCommandSpecified=Es wurde kein ausführbarer Befehl angegeben.
throw_win32APIFailed=Fehler beim Aufrufen der Win32-API "{0}". Fehlercode: {1}
###PSLOC
'@
