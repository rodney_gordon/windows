# Localized	12/07/2019 08:26 AM (GMT)	303:6.40.20520 	TestDtc.psd1
ConvertFrom-StringData @'
       
###PSLOC start localizing

FirewallRuleEnabled="{0}: Die Firewallregel für '{1}' ist aktiviert."
FirewallRuleDisabled="{0}: Die Firewallregel für '{1}' ist deaktiviert. Dieser Computer kann nicht an Netzwerktransaktionen teilnehmen."
CmdletFailed="Fehler beim {0}-Cmdlet. Stellen Sie sicher, dass das {1}-Modul installiert ist."
InvalidLocalComputer="'{0}' ist kein gültiger lokaler Computername."
RPCEndpointMapper="RPC-Endpunktzuordnung"
DtcIncomingConnection="Eingehende DTC-Verbindungen"
DtcOutgoingConnection="Ausgehende DTC-Verbindungen"
MatchingDtcNotFound="Eine DTC-Instanz mit VirtualServerName '{0}' ist nicht vorhanden."
InboundDisabled="{0}: Eingehende Transaktionen sind nicht zulässig, und dieser Computer kann nicht an Netzwerktransaktionen teilnehmen."
OutboundDisabled="{0}: Ausgehende Transaktionen sind nicht zulässig, und dieser Computer kann nicht an Netzwerktransaktionen teilnehmen."
OSVersion="{0}-Betriebssystemversion: {1}."
OSQueryFailed="Fehler beim Abfragen des Betriebssystems von {0}."
VersionNotSupported="DTC-Tests für Windows-Versionen unter {0} werden bei diesem Cmdlet nicht unterstützt."
FailedToCreateCimSession="Fehler beim Erstellen einer CIM-Sitzung für '{0}'."
NotARemoteComputer="{0} ist kein Remotecomputer."
PingingSucceeded="Ping für Computer '{0}' von '{1}' wurde erfolgreich ausgeführt."
PingingFailed="Fehler beim Ausführen von Ping für Computer '{0}' von '{1}'."
SameCids="Die CID {0} für '{1}' und '{2}' ist identisch. Jeder Computer sollte über eine eindeutige CID verfügen."
DiagnosticTestPrompt="Bei diesem Diagnosetest wird versucht, eine Transaktionsverteilung zwischen '{0}' und '{1}' auszuführen. Dazu muss auf '{0}' ein TCP-Port geöffnet sein, damit ein Test-Ressourcen-Manager an Netzwerktransaktionen teilnehmen kann."
DefaultPortDescription="Der Standardport ist {0}. Sie können ihn mit dem ResourceManagerPort-Parameter ändern und den Test erneut ausführen. "
PortDescription="Sie haben {0} als 'ResourceManagerPort' angegeben."
FirewallRequest="Öffnen Sie Port {0} in der Firewall, um den Test fortzusetzen."
QueryText="Möchten Sie den Test fortsetzen?"
InvalidDefaultCluster="'{0}' ist nicht der Name des virtuellen Servers des auf diesem Computer konfigurierten Standard-DTCs. Sie können das Cmdlet 'Set-DtcClusterDefault' zum Konfigurieren des Standard-DTCs auf diesem Computer verwenden."
InvalidDefault="'{0}' ist nicht der Name des virtuellen Servers des auf diesem Computer konfigurierten Standard-DTCs. Sie können das Cmdlet 'Set-DtcDefault' zum Konfigurieren des Standard-DTCs auf diesem Computer verwenden."
NeedDtcSecurityFix="DTC-Sicherheitseinstellungen und -Firewalleinstellungen sollten korrigiert werden, um den Transaktionsverteilungstest abzuschließen."
StartResourceManagerFailed="Fehler beim Erstellen des Test-Ressourcen-Managers."
ResourceManagerStarted="Der Test-Ressourcen-Manager wurde gestartet."
PSSessionCreated="Eine neue PSSession für '{0}' wurde erstellt."
TransactionPropagated="Die Transaktion wurde unter Verwendung der {2}-Verteilung von '{0}' an '{1}' weitergegeben."
TransactionPropagationFailed="Fehler bei der Transaktionsverteilung von '{0}' an '{1}' unter Verwendung der {2}-Verteilung."
TestRMVerboseLog="Ausführliches Protokoll des Test-Ressourcen-Managers:"
TestRMWarningLog="Warnungsprotokoll des Test-Ressourcen-Managers:"
InvalidParameters="Mindestens der LocalComputerName-Parameter oder der RemoteComputerName-Parameter sollte angegeben werden."

###PSLOC
'@
