# Localized	12/07/2019 11:52 AM (GMT)	303:6.40.20520 	RS_DisableAddonLoadingTime.psd1
ConvertFrom-StringData @'
###PSLOC
Disable_loadTime_Slowaddon=Langsame Add-Ons für Internet Explorer deaktivieren
Resolver_name_slowAddOnDisabling=Langsame Add-Ons werden deaktiviert
regitemvalue_original=Ursprünglicher Wert
regitemvalue_reset=Neuer Wert
reg_path=Schlüssel
value_name=Wertname
add_onName=Name des Add-Ons:
add_onPublisher=Herausgeber:
add_onLoadTime=Durchschnittliche Ladezeit:
seconds=Sekunden
add_onPublisherDefaultValue=Unbekannter Herausgeber
add_onPublisherNotVerified=(Nicht überprüft)
###PSLOC
'@
