# Localized	12/07/2019 11:56 AM (GMT)	303:6.40.20520 	CL_LocalizationData.psd1
ConvertFrom-StringData @'
###PSLOC
ConsentRTFCalibrate=Es wird empfohlen, vor Verwendung der Spracherkennung Ihr Mikrofon einzurichten, damit wir Sie deutlich hören können. Möchten Sie das Mikrofon einrichten?
MicWizActivity=Starten des Mikrofoneinrichtungs-Assistenten
JackLocInfo=Der Anschluss für dieses Gerät befindet sich
NoJackInfoAvailable=Keine Buchseninformationen verfügbar
Rear=auf der Rückseite des Computers
Front=auf der Vorderseite des Computers
Left=auf der linken Seite des Computers
Right=auf der rechten Seite des Computers
Top=auf der Oberseite des Computers
Bottom=auf der Unterseite des Computers
RearSlide=im Schiebe- oder Klappfach auf der Rückseite des Computers
RiserCard=auf der Riser-Karte
InsideLid=an der Innenseite des Deckels eines Notebookcomputers
DriveBay=im Laufwerkschacht
HDMIConnector=im HDMI-Anschluss
OutSideLid=an der Außenseite des Deckels eines Notebookcomputers
ATAPIConnector=im ATAPI-Anschluss
NotDefault_Progress=Standardaudiogerät wird überprüft...
SetAsDefault_Progress=Ausgewähltes Audiogerät wird als Standard festgelegt...
TroublePage_DefaultDescription=Durch die Einrichtung Ihres Mikrofons kann sich die Sprachgenauigkeit verbessern, wenn der PC oder das Mikrofon kürzlich umpositioniert wurde. Möchten Sie das Mikrofon einrichten?
TroublePage_LegacyDevice_Description=Dieser PC wurde vor der Einführung von Windows 10 hergestellt. Ihr Mikrofon funktioniert möglicherweise nicht mit Cortana oder der Spracherkennung. Möchten Sie das Mikrofon trotzdem einrichten?
TroublePage_NotLegacyDevice_Description=Das mit diesem PC verwendete Mikrofon wurde nicht für Cortana entwickelt, sodass Cortana Sie möglicherweise nicht einwandfrei hören kann. Möchten Sie das Mikrofon trotzdem einrichten?
Calibration_Prompt=Lesen Sie die folgenden Sätze vor, um die Einrichtung des Mikrofons abzuschließen.
Calibration_Activity=Peter spricht mit seinem Computer. Er zieht diese Methode der Tastatureingabe und ganz besonders der Verwendung von Stift und Papier vor.
DiagReportTitle_AudioDeviceDetails=Audiogerätdetails
###PSLOC
'@
