# Localized	12/07/2019 11:54 AM (GMT)	303:6.40.20520 	PSDesiredStateConfiguration.Resource.psd1
# Localized	04/21/2015 09:07 AM (GMT)	303:4.80.0411 	PSDesiredStateConfiguration.Resource.psd1
# Localized PSDesiredStateConfigurationResource.psd1

ConvertFrom-StringData @'
###PSLOC
CheckSumFileExists=Die Datei "{0}" ist bereits vorhanden. Geben Sie zum Überschreiben der vorhandenen Prüfsummendateien den Parameter "-Force" an.
CreateChecksumFile=Prüfsummendatei "{0}" erstellen
OverwriteChecksumFile=Prüfsummendatei "{0}" überschreiben
OutpathConflict=(FEHLER) Das Verzeichnis "{0}" kann nicht erstellt werden. Eine Datei mit demselben Namen ist bereits vorhanden.
InvalidConfigPath=(FEHLER) Ungültiger Konfigurationspfad ({0}) angegeben.
InvalidOutpath=(FEHLER) Ungültiger Wert für "OutPath" ({0}) angegeben.
InvalidConfigurationName=Der ungültige Konfigurationsname '{0}' wurde angegeben. Standardnamen dürfen nur Zeichen (a-z, A-Z), Ziffern (0-9) und Unterstriche (_) enthalten. Der Name darf nicht NULL oder leer sein und sollte mit einem Buchstaben beginnen.
NoValidConfigFileFound=Keine gültigen Konfigurationsdateien (MOF, ZIP) gefunden.
InputFileNotExist=Die Datei "{0}" ist nicht vorhanden.
FileReadError=Fehler beim Lesen der Datei "{0}".
MatchingFileNotFound=Keine übereinstimmende Datei gefunden.
CertificateFileReadError=Fehler beim Lesen der Zertifikatdatei "{0}".
CertificateStoreReadError=Fehler beim Lesen des Zertifikatspeichers für {0}.
CannotCreateOutputPath=Ungültige Kombination aus Konfigurationsname und Ausgabepfad: {0}. Stellen Sie sicher, dass der Ausgabeparameter ein gültiges Pfadsegment darstellt.
ConflictingDuplicateResource=Zwischen den Ressourcen "{0}" und "{1}" in Knoten "{2}" wurde ein Konflikt erkannt. Die Ressourcen verfügen über identische Schlüsseleigenschaften, während die folgenden Nicht-Schlüsseleigenschaften Unterschiede aufweisen: "{3}". Die {4}-Werte stimmen nicht mit den {5}-Werten überein. Aktualisieren Sie diese Eigenschaftswerte, damit sie in beiden Fällen identisch sind.
ConfiguratonDataNeedAllNodes=Für den ConfigurationData-Parameter muss die AllNodes-Eigenschaft angegeben werden.
ConfiguratonDataAllNodesNeedHashtable=Bei der AllNodes-Eigenschaft des ConfigurationData-Parameters muss es sich um eine Auflistung handeln.
AllNodeNeedToBeHashtable=Alle Elemente von "AllNodes" müssen Hashtabellen sein und eine NodeName-Eigenschaft aufweisen.
DuplicatedNodeInConfigurationData=Die übergebenen Konfigurationsdaten enthalten doppelte Knotennamen ({0}).
EncryptedToPlaintextNotAllowed=Das Konvertieren und Speichern verschlüsselter Kennwörter im Nur-Text-Format wird nicht empfohlen. Weitere Informationen zum Sichern von Anmeldeinformationen in der MOF-Datei finden Sie im MSDN-Blog: https://go.microsoft.com/fwlink/?LinkId=393729.
DomainCredentialNotAllowed=Es wird davon abgeraten, Domänenanmeldeinformationen für den Knoten '{0}' zu verwenden. Um die Warnung zu unterdrücken, können Sie Ihren DSC-Konfigurationsdaten für Knoten '{0}' eine Eigenschaft namens 'PSDscAllowDomainUser' mit dem Wert '$true' hinzufügen.
NestedNodeNotAllowed=Die Definition des Knotens "{0}" innerhalb des aktuellen Knotens "{1}" ist nicht zulässig, weil Knotendefinitionen nicht geschachtelt werden können. Verschieben Sie die Definition für den Knoten "{0}" auf die oberste Ebene der Konfiguration "{2}".
FailToProcessNode=Während der Verarbeitung des Knotens "{0}" wurde eine Ausnahme ausgelöst: {1}
LocalHostNodeNotAllowed=Die Definition eines Knotens "localhost" in der Konfiguration "{0}" ist nicht zulässig, weil die Konfiguration mindestens eine Ressourcendefinition enthält, die keinem Knoten zugeordnet ist.
InvalidMOFDefinition=Ungültige MOF-Definition für Knoten "{0}": {1}
RequiredResourceNotFound=Die für "{1}" erforderliche Ressource "{0}" ist nicht vorhanden. Stellen Sie sicher, dass die erforderliche Ressource vorhanden und das Format des Namens korrekt ist.
ReferencedManagerNotFound=Der Download-Manager '{0}', auf den von '{1}' verwiesen wird, ist nicht vorhanden. Stellen Sie sicher, dass der betreffende Download-Manager vorhanden ist und ein geeignetes Namensformat aufweist.
ReferencedResourceSourceNotFound=Das Ressourcenrepository "{0}", auf das von "{1}" verwiesen wird, ist nicht vorhanden. Stellen Sie sicher, dass das betreffende Ressourcenrepository vorhanden ist und ein geeignetes Namensformat aufweist.
DependsOnLinkTooDeep=Der DependsOn-Link hat die maximale Tiefenbeschränkung "{0}" überschritten.
DependsOnLoopDetected=Ein DependsOn-Zirkelverweis ist vorhanden: "{0}". Stellen Sie sicher, dass keine Zirkelverweis vorhanden sind.
FailToProcessConfiguration=Kompilierungsfehler bei der Verarbeitung der Konfiguration "{0}". Überprüfen Sie die im Fehlerdatenstrom gemeldeten Fehler, und ändern Sie den Konfigurationscode entsprechend.
FailToProcessProperty=Fehler "{0}" beim Verarbeiten der {1}-Eigenschaft vom Typ "{2}": {3}
NodeNameIsRequired=Die Knotenverarbeitung wird übersprungen, weil der Knotenname leer ist.
ConvertValueToPropertyFailed="{0}" kann für die {2}-Eigenschaft nicht in den Typ "{1}" konvertiert werden.
ResourceNotFound=Der Begriff "{0}" wird nicht als {1}-Name erkannt.
GetDscResourceInputName=Der Wert des Get-DscResource-Eingabeparameters "{0}" ist "{1}".
ResourceNotMatched=Ressource "{0}" wird übersprungen, weil sie nicht mit dem angeforderten Namen übereinstimmt.
InitializingClassCache=Klassencache wird initialisiert
LoadingDefaultCimKeywords=CIM-Standardschlüsselwörter werden geladen
GettingModuleList=Modulliste wird abgerufen
CreatingResourceList=Ressourcenliste wird erstellt
CreatingResource=Ressource "{0}" wird erstellt.
SchemaFileForResource=Schemadateiname für Ressource "{0}"
UnsupportedReservedKeyword=Das Schlüsselwort "{0}" wird in dieser Sprachversion nicht unterstützt.
UnsupportedReservedProperty=Die Eigenschaft "{0}" wird in dieser Sprachversion nicht unterstützt.
MetaConfigurationHasMoreThanOneLocalConfigurationManager=Die Metakonfiguration für den Knoten '{0}' enthält mehrere Definitionen für 'LocalConfigurationManager'. Dies ist nicht zulässig.
MetaConfigurationSettingsMissing=Die Einstellungsdefinition für den Knoten "{0}" fehlt. Eine leere Standard-Einstellungsdefinition wird für den Knoten hinzugefügt.
ConflictInExclusiveResources=Die partiellen Konfigurationen "{0}" und "{1}" weisen einen Konflikt zwischen Deklarationen exklusiver Ressourcen auf.
ReferencedModuleNotExist=Das Modul "{0}", auf das verwiesen wird, ist auf dem Computer nicht vorhanden. Verwenden Sie Get-DscResource, um herauszufinden, was auf dem Computer vorhanden ist.
ReferencedResourceNotExist=Die Ressource "{0}", auf die verwiesen wird, ist auf dem Computer nicht vorhanden. Verwenden Sie Get-DscResource, um herauszufinden, was auf dem Computer vorhanden ist.
ReferencedModuleResourceNotExist=Das Modul\\die Ressource "{0}", auf das bzw. die verwiesen wird, ist auf dem Computer nicht vorhanden. Verwenden Sie Get-DscResource, um herauszufinden, was auf dem Computer vorhanden ist.
DuplicatedResourceInModules=Die Ressource "{0}", auf die verwiesen wird, ist in Modul "{1}" und Modul "{2}" auf dem Computer vorhanden. Stellen Sie sicher, dass sie nur in einem Modul vorhanden ist.
CannotConvertStringToBool=Der Wert "System.String" kann nicht in den Typ "System.Boolean" konvertiert werden. Boolesche Parameter akzeptieren nur boolesche Werte und Ziffern wie $True, $False bzw. 1 oder 0.
NoModulesPresent=Das System enthält keine Module mit der angegebenen Modulspezifikation.
ImportDscResourceWarningForInbuiltResource=Von der Konfiguration "{0}" wird mindestens eine integrierte Ressource heruntergeladen, ohne die zugehörigen Module explizit zu importieren. Fügen Sie Ihrer Konfiguration "Import-DscResource –ModuleName 'PSDesiredStateConfiguration'" hinzu, damit diese Meldung nicht mehr angezeigt wird.
PasswordTooLong=Fehler bei der Verschlüsselung eines Kennworts in Knoten "{0}". Wahrscheinlich ist das eingegebene Kennwort für die Verschlüsselung mithilfe des ausgewählten Zertifikats zu lang. Verwenden Sie ein kürzeres Kennwort, oder wählen Sie ein Zertifikat mit einem umfangreicheren Schlüssel aus.
HashtableElementTypeNotAllowed=Werte vom Typ "{0}" sind in Hashtabellen nicht zulässig. Unterstützte Typen: [String], [Char], [Int64], [UInt64], [Double], [Bool],[DateTime] und [ScriptBlock].
PullModeWithoutDownloadManager=Die Metakonfiguration wurde auf den Pullmodus festgelegt. Dazu muss ein DownloadManager angegeben werden.
PullModeWithoutConfigurationRepository=Die Metakonfiguration wurde auf den Pullmodus festgelegt. Dazu muss ein ConfigurationRepository angegeben werden.
DownloadManagerWithoutPullMode=Ein DownloadManager wurde angegeben, ohne den Aktualisierungsmodus auf PULL festzulegen.
ConfigurationRepositoryWithoutPullMode=Ein ConfigurationRepository wurde angegeben, ohne den Aktualisierungsmodus auf PULL festzulegen.
ReferencedPolicyNotDefined=Die referenzierte SignatureValidationPolicy {0} wurde nicht definiert. Definieren Sie einen SignatureValidation-Block mit dem gleichen Namen.
IncorrectSignatureValidationPolicyFormat=Der für SignatureValidationPolicy angegebene Wert hat ein falsches Format. Geben Sie den Wert im folgenden Format an: [SignatureValidation]<Name>.
###PSLOC
'@
