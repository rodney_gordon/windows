# Localized	12/07/2019 11:49 AM (GMT)	303:6.40.20520 	MSFT_RegistryResource.strings.psd1
# Localized resources for MSFT_EnvironmentResource

ConvertFrom-StringData @'
###PSLOC
ParameterValueInvalid=(FEHLER) Der Parameter "{0}" enthält den ungültigen Wert "{1}" für den Typ "{2}".
InvalidPSDriveSpecified=(FEHLER) Im Registrierungsschlüssel "{1}" ist das ungültige PowerShell-Laufwerk "{0}" angegeben.
InvalidRegistryHiveSpecified=(FEHLER) Im Registrierungsschlüssel "{0}" ist eine ungültige Registrierungsstruktur angegeben.
SetRegValueFailed=(FEHLER) Fehler beim Festlegen des Registrierungsschlüsselwerts "{0}" auf den Wert "{1}" vom Typ "{2}"
SetRegValueUnchanged=(UNVERÄNDERT) Keine Änderung am Registrierungsschlüsselwert "{0}" mit "{1}"
SetRegKeyUnchanged=(UNVERÄNDERT) Keine Änderung am Registrierungsschlüssel "{0}"
SetRegValueSucceeded=(FESTLEGEN) Festlegen des Registrierungsschlüsselwerts "{0}" auf "{1}" vom Typ "{2}"
SetRegKeySucceeded=(FESTLEGEN) Erstellen des Registrierungsschlüssels "{0}"
SetRegKeyFailed=(FEHLER) Fehler beim Erstellen des Registrierungsschlüssels "{0}"
RemoveRegKeyTreeFailed=(FEHLER) Der Registrierungsschlüssel "{0}" enthält Unterschlüssel. Entfernen ohne Force-Flag nicht möglich.
RemoveRegKeySucceeded=(ENTFERNEN) Der Registrierungsschlüssel "{0}" wurde entfernt.
RemoveRegKeyFailed=(FEHLER) Fehler beim Entfernen des Registrierungsschlüssels "{0}"
RemoveRegValueSucceeded=(ENTFERNEN) Der Registrierungsschlüsselwert "{0}" wurde entfernt.
RemoveRegValueFailed=(FEHLER) Fehler beim Entfernen des Registrierungsschlüsselwerts "{0}"
RegKeyDoesNotExist=Der Registrierungsschlüssel "{0}" ist nicht vorhanden.
RegKeyExists=Der Registrierungsschlüssel "{0}" ist vorhanden.
RegValueExists=Der Registrierungsschlüsselwert "{0}" mit dem Typ "{1}" und den Daten "{2}" wurde gefunden.
RegValueDoesNotExist=Der Registrierungsschlüsselwert "{0}" ist nicht vorhanden.
RegValueTypeMismatch=Der Registrierungsschlüssel "{0}" vom Typ "{1}" ist nicht vorhanden.
RegValueDataMismatch=Der Registrierungsschlüsselwert "{0}" vom Typ "{1}" enthält nicht die Daten "{2}".
DefaultValueDisplayName=(Standard)
###PSLOC
'@
