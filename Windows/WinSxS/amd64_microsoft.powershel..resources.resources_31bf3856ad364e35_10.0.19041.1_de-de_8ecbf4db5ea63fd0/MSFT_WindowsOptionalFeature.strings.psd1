# Localized	12/07/2019 11:51 AM (GMT)	303:6.40.20520 	MSFT_WindowsOptionalFeature.strings.psd1
# Localized resources for MSFT_WindowsOptionalFeature

ConvertFrom-StringData @'
###PSLOC
DismNotAvailable=Das PowerShell-Modul Dism konnte nicht importiert werden.
NotAClientSku=Die Ressource ist nur für Windows-Clients verfügbar.
ElevationRequired=Die Ressource muss als Administrator ausgeführt werden.
ValidatingPrerequisites=Voraussetzungen werden überprüft...
CouldNotCovertFeatureState=Der Featurezustand '{0}' konnte nicht in 'Aktivieren/Deaktivieren' konvertiert werden.
EnsureNotSupported=Der Wert '{0}' wird für die Ensure-Eigenschaft nicht unterstützt.
RestartNeeded=Der Zielcomputer muss erneut gestartet werden.
GetTargetResourceStartMessage=Ausführung der Get-Funktion für das Feature "{0}" starten.
GetTargetResourceEndMessage=Ausführung der Get-Funktion für das Feature "{0}" beenden.
SetTargetResourceStartMessage=Ausführung der Set-Funktion für das Feature "{0}" starten.
SetTargetResourceEndMessage=Ausführung der Set-Funktion für das Feature "{0}" beenden.
TestTargetResourceStartMessage=Ausführung der Test-Funktion für das Feature "{0}" starten.
TestTargetResourceEndMessage=Ausführung der Test-Funktion für das Feature "{0}" beenden.
FeatureInstalled=Das Feature '{0}' wurde installiert.
FeatureUninstalled=Das Feature '{0}' wurde deinstalliert.
###PSLOC

'@
