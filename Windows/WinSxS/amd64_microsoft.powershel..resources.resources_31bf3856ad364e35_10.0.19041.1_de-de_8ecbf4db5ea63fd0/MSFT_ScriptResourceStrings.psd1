# Localized	12/07/2019 11:49 AM (GMT)	303:6.40.20520 	MSFT_ScriptResourceStrings.psd1
# Localized MSFT_ScriptResourceStrings.psd1

ConvertFrom-StringData @'
###PSLOC
SetScriptWhatIfMessage="SetScript" wird mit den vom Benutzer angegebenen Anmeldeinformationen ausgeführt
InValidResultFromGetScriptError=Fehler beim Abrufen der Ergebnisse des Skripts im Hashtabellenformat.
InValidResultFromTestScriptError=Fehler beim Abrufen des Ergebnisses der Testskriptausführung. Ungültiges Ergebnis. Das Testskript muss "True" oder "False" zurückgeben.
ScriptBlockProviderScriptExecutionFailureError=Fehler beim Ausführen des Skripts.
GetTargetResourceStartVerboseMessage=Startet die Ausführung des Get-Skripts.
GetTargetResourceEndVerboseMessage=Beendet die Ausführung des Get-Skripts.
SetTargetResourceStartVerboseMessage=Startet die Ausführung des Set-Skripts.
SetTargetResourceEndVerboseMessage=Beendet die Ausführung des Set-Skripts.
TestTargetResourceStartVerboseMessage=Startet die Ausführung des Test-Skripts.
TestTargetResourceEndVerboseMessage=Beendet die Ausführung des Test-Skripts.
ExecutingScriptMessage=Ausführen des Skripts: {0}
ResourceNotAllowedWhenDeviceGuardIsEnabled=Wenn Device Guard aktiviert ist, wird die "Script"-Ressource aus dem Modul "PSDesiredStateConfiguration" nicht unterstützt. Bitte verwenden Sie die vom Modul "PSDscResources" aus dem PowerShell-Katalog veröffentlichte "Script"-Ressource.
WarningRunningScriptResourceInFullLanguageMode=Da Device Guard für den Modus "Überwachung" aktiviert ist, wird die "Script"-Ressource im FullLanguage-Modus ausgeführt.
###PSLOC

'@
