# Localized	12/07/2019 11:54 AM (GMT)	303:6.40.20520 	PackageProvider.psd1
# Localized PackageProvider.psd1

ConvertFrom-StringData @'
###PSLOC
InvalidIdentifyingNumber=Der angegebene IdentifyingNumber-Wert ({0}) ist keine gültige Guid.
InvalidPath=Das Format des angegebenen Path-Werts ({0}) ist ungültig. Gültige Formate sind lokale Pfade, UNC- und HTTP-Pfade.
InvalidNameOrId=Der angegebene Name ({0}) und die IdentifyingNumber ({1}) stimmen nicht mit dem Namen ({2}) und der IdentifyingNumber ({3}) in der MSI-Datei überein.
NeedsMoreInfo="Name" oder "ProductId" müssen angegeben sein.
InvalidBinaryType=Der angegebene Path-Wert ({0}) ist anscheinend kein Pfad einer EXE- oder MSI-Datei und wird daher nicht unterstützt.
CouldNotOpenLog=Der in "LogPath" angegebene Pfad ({0}) konnte nicht geöffnet werden.
CouldNotStartProcess=Der Prozess "{0}" konnte nicht gestartet werden.
UnexpectedReturnCode=Der Rückgabecode "{0}" wurde nicht erwartet. Die Konfiguration ist wahrscheinlich nicht korrekt.
PathDoesNotExist=Der in "Path" angegebene Pfad ({0}) wurde nicht gefunden.
CouldNotOpenDestFile=Die Datei "{0}" konnte nicht zum Schreiben geöffnet werden.
CouldNotGetHttpStream=Der {0}-Datenstrom für die Datei "{1}" konnte nicht abgerufen werden.
ErrorCopyingDataToFile=Fehler beim Schreiben der Inhalte von "{0}" in "{1}"
PackageConfigurationComplete=Paketkonfiguration abgeschlossen
PackageConfigurationStarting=Paketkonfiguration wird gestartet
InstalledPackage=Installiertes Paket
UninstalledPackage=Deinstalliertes Paket
NoChangeRequired=Das Paket befindet sich im gewünschten Zustand. Keine Aktion erforderlich.
RemoveExistingLogFile=Vorhandene Protokolldatei entfernen
CreateLogFile=Protokolldatei erstellen
MountSharePath=Freigabe zum Abrufen von Medien einbinden
DownloadHTTPFile=Medien über HTTP oder HTTPS herunterladen
StartingProcessMessage=Prozess "{0}" wird gestartet. Argumente: {1}
RemoveDownloadedFile=Heruntergeladene Datei entfernen
PackageInstalled=Das Paket wurde installiert.
PackageUninstalled=Das Paket wurde deinstalliert.
MachineRequiresReboot=Der Computer muss neu gestartet werden.
PackageDoesNotAppearInstalled=Das Paket "{0}" ist nicht installiert.
PackageAppearsInstalled=Das Paket "{0}" ist bereits installiert.
PostValidationError=Das Paket von {0} wurde installiert, der angegebene Wert für "ProductId" und/oder "Name" stimmt jedoch nicht mit den Paketdetails überein.
ValidateStandardArgumentsPathwasPath='Validate-StandardArguments', Pfad (Path) war '{0}'
TheurischemewasuriScheme=Das URI-Schema war '{0}'.
ThepathextensionwaspathExt=Die Pfaderweiterung war '{0}'.
ParsingProductIdasanidentifyingNumber='{0}' wird als ID-Nummer (identifyingNumber) analysiert.
ParsedProductIdasidentifyingNumber='{0}' wurde als '{1}' analysiert.
EnsureisEnsure=Sicherstellen, dass gleich '{0}'
productisproduct=Das Produkt "{0}" wurde gefunden.
productasbooleanis=Produkt als boolesch ist '{0}'
Creatingcachelocation=Cachespeicherort wird erstellt.
NeedtodownloadfilefromschemedestinationwillbedestName=Datei muss von '{0}' heruntergeladen werden, das Ziel ist '{1}'.
Creatingthedestinationcachefile=Die Zielcachedatei wird erstellt.
Creatingtheschemestream=Der '{0}'-Datenstrom wird erstellt.
Settingdefaultcredential=Die Standardanmeldeinformationen werden festgelegt.
Settingauthenticationlevel=Die Authentifizierungsebene wird festgelegt.
Ignoringbadcertificates=Fehlerhafte Zertifikate werden ignoriert.
Gettingtheschemeresponsestream=Der '{0}'-Antwortdatenstrom wird abgerufen.
ErrorOutString=Fehler: {0}
Copyingtheschemestreambytestothediskcache=Die Bytes des '{0}'-Datenstroms werden in den Datenträgercache kopiert.
Redirectingpackagepathtocachefilelocation=Der Paketpfad wird zur Speicherposition der Cachedatei umgeleitet.
ThebinaryisanEXE=Die Binärdatei ist eine EXE-Datei.
Userhasrequestedloggingneedtoattacheventhandlerstotheprocess=Benutzer hat Anmeldung angefordert. Dem Prozess müssen Ereignishandler zugewiesen werden.
StartingwithstartInfoFileNamestartInfoArguments='{0}' wird mit '{1}' gestartet.
###PSLOC

'@
