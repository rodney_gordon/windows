// Copyright (c) 1997-2003 Microsoft Corporation, All Rights Reserved

#pragma autorecover
#pragma classflags(64)
#pragma namespace("\\\\.\\Root\\CIMV2")
instance of __namespace{ name="ms_407";};
#pragma namespace("\\\\.\\Root\\CIMV2\\ms_407")

[AMENDMENT, LOCALE(0x0407) : ToInstance] 
class CIM_ManagedSystemElement
{
};

[AMENDMENT, LOCALE(0x0407) : ToInstance] 
class CIM_LogicalElement : CIM_ManagedSystemElement
{
};

[AMENDMENT, LOCALE(0x0407) : ToInstance] 
class CIM_LogicalFile : CIM_LogicalElement
{
};

[AMENDMENT, LOCALE(0x0407) : ToInstance] 
class CIM_DataFile : CIM_LogicalFile
{
};

[AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_NTEventlogFile : CIM_DataFile
{
  [Description("Die Eigenschaft \"LogFileName\" zeigt den Namen der Protokolldatei an.") : Amended ToSubclass] string LogfileName;
  [Description("Die Eigenschaft \"MaxFileSize\" zeigt die maximal zugelassene Protokolldateigröße in Bytes an. Falls die Datei die Maximalgröße überschreitet, wird deren Inhalt in eine andere Datei verschoben, und die primäre Datei geleert. Ein Wert gleich null bedeutet, dass keine Größenbeschränkung festgelegt wurde. ") : Amended ToSubclass] uint32 MaxFileSize;
  [Description("Die Anzahl der Einträge in der Protokolldatei. Dieser Wert wird durch Aufrufen der Win32-Funktion \"GetNumberOfEventLogRecords\" bestimmt.") : Amended ToSubclass] uint32 NumberOfRecords;
  [Description("Aktuelle Richtlinie bezüglich des Überschreiben dieser Protokolldatei des Windows NT/Windows 2000-Ereignisprotokolldienstes. Mögliche Werte: \n\"WhenNeeded\" - Dieser Wert korrespondiert mit OverWriteOutdated = 0.\n\"outDated\" - Dieser Wert korrespondiert mit OverWriteOutdated zwischen 1 und 365.\n\"Never\" - Dieser Wert korrespondiert mit OverWriteOutdated = 4294967295.\nDer Eigenschaftwert \"OverWriteOutDated\" (schreibbar) und der Eigenschaftwert \"OverWritePolicy\" (nicht schreibbar) sind voneinander abhängig.\nFalls der Eigenschaftwert \"OverWriteOutDated\" auf 0 gesetzt wird, wird der Eigenschaftwert \"OverWritePolicy\" auf \"WhenNeeded\" gesetzt. \nFalls der Eigenschaftwert \"OverWriteOutDated\" auf 1-365 gesetzt wird, wird der Eigenschaftwert \"OverWritePolicy\" auf \"outDated\" gesetzt. \nFalls der Eigenschaftwert \"OverWriteOutDated\" auf 4294967295 gesetzt wird, wird der Eigenschaftwert \"OverWritePolicy\" auf \"Never\" gesetzt.") : Amended ToSubclass,Values{"Bei Bedarf", "Veraltet", "Nie"} : Amended ToSubclass] string OverWritePolicy;
  [Description("Anzahl der Tage, in der das Ereignis überschrieben werden kann. Werte:\n0 = Jeder Eintrag kann, falls erforderlich, überschritten werden.1..365 = Ereignisse, die innerhalb eines Jahres (365 Tage) protokolliert wurden, können überschrieben werden.4294967295 = Ereignisse können nie überschrieben werden. \nDer Eigenschaftwert \"OverWriteOutDated\" (schreibbar) und der Eigenschaftwert \"OverWritePolicy\" (nicht schreibbar) sind voneinander abhängig.\nFalls der Eigenschaftwert \"OverWriteOutDated\" auf 0 gesetzt wird, wird der Eigenschaftwert \"OverWritePolicy\" auf \"henNeeded\" gesetzt. \nFalls der Eigenschaftwert \"OverWriteOutDated\" auf 1-365 gesetzt wird, wird der Eigenschaftwert \"OverWritePolicy\" auf \"outDated\" gesetzt. \nFalls der Eigenschaftwert \"OverWriteOutDated\" auf 4294967295 gesetzt wird, wird der Eigenschaftwert \"OverWritePolicy\" auf \"Never\" gesetzt.") : Amended ToSubclass,Units("Tage") : Amended ToSubclass] uint32 OverwriteOutDated;
  [Description("Die Eigenschaft \"Sources\" zeigt die Anwendungen an, die zum Protokollieren in dieser Protokolldatei registriert sind.") : Amended ToSubclass] string Sources[];
  [Description("Löscht das angegebene Ereignisprotokoll und bietet die Möglichkeit eine Kopie der aktuellen Protokolldatei in eine Sicherungsdatei zu speichern. Diese Methode gibt einen der folgenden ganzzahligen Werte zurück: \n0 - Erfolgreicher Abschluss.\n8 - Benutzer verfügt nicht über ausreichend Berechtigungen.\n21 - Ungültiger Parameter.\nAndere - Andere als oben aufgeführte ganzzahlige Werte finden Sie in der Win32-Fehlercodedokumentation.") : Amended ToSubclass,Values{"Erfolgreich", "Fehlende Berechtigung", "Ungültiger Parameter", "Andere"} : Amended ToSubclass] uint32 ClearEventlog([Description("Zeichenfolge, die den Namen der Datei bestimmt, in der eine aktuelle Kopie der Ereignisprotokolldatei gespeichert wird. Die Funktion schlägt fehlt, falls die Datei bereits vorhanden ist. ") : Amended ToSubclass,in] string ArchiveFileName);
  [Description("Speichert das angegebene Ereignisprotokoll in einer Sicherungsdatei. Die Methode gibt einen ganzzahligen Wert mit folgenden Bedeutungen zurück: \n0 - Erfolgreicher Abschluss\n8 - Unbekannter Fehler\n21 - Ungültiger Parameter\n183 - Der Archivdateiname ist bereits vorhanden. Datei kann nicht erstellt werden.\nAndere - Andere als oben aufgeführte ganzzahlige Werte finden Sie in der Win32-Fehlercodedokumentation.") : Amended ToSubclass,Values{"Erfolgreich", "Fehlende Berechtigung", "Ungültiger Parameter", "Der Archivdateiname ist bereits vorhanden.", "Andere"} : Amended ToSubclass] uint32 BackupEventlog([Description("Zeichenfolge, die den Namen der Sicherungsdatei angibt.") : Amended ToSubclass,in] string ArchiveFileName);
};

[DisplayName("NT-Protokollereignisse") : Amended,Description("Diese Klasse wird verwendet, um Instanzen aus dem NT-Ereignisprotokoll zu übersetzen.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_NTLogEvent
{
  [DisplayName("Eintragsnummer") : Amended,Key : ToInstance ToSubclass DisableOverride,Description("Identifiziert das Ereignis in der NT-Ereignisprotokolldatei spezifisch zur Protokolldatei, und wird zusammen mit dem Protokolldateinamen verwendet, um eine Instanz dieser Klasse eindeutig zu identifizieren.") : Amended ToSubclass] uint32 RecordNumber;
  [DisplayName("Protokolldatei") : Amended,Key : ToInstance ToSubclass DisableOverride,Description("Der Name der NT-Ereignisprotokolldatei wird mit \"RecordNumber\" verwendet, um eine Instanz dieser Klasse eindeutig zu identifizieren.") : Amended ToSubclass] string Logfile;
  [DisplayName("Ereignisbezeichner") : Amended,Description("Identifiziert das Ereignis spezifisch zur Quelle, die den Ereignisprotokolleintrag generiert hat, und zusammen mit \"SourceName\" verwendet wird, um einen NT-Ereignistyp eindeutig zu identifizieren.") : Amended ToSubclass] uint32 EventIdentifier;
  [DisplayName("Ereigniscode") : Amended,Description("Diese Eigenschaft enthält die niedrigen 16-Bits der Eigenschaft \"EventIdentifier\", um den in der NT-Ereignisanzeige angezeigten Wert anzupassen. Hinweis: Zwei Ereignisse aus der gleichen Quelle haben möglicherweise für diese Eigenschaft den gleichen Wert, aber unterschiedliche Werte für den Schweregrad und \"EventIdentifier\".") : Amended ToSubclass] uint16 EventCode;
  [DisplayName("Quellname") : Amended,Description("Die mit Null beendete Zeichenfolge in variabler Länge gibt den Namen der Quelle (Anwendung, Dienst, Treiber, Subsystem) an, die den Eintrag generiert hat. Diese Zeichenfolge wird mit \"EventIdentifier\" verwendet, um einen NT-Ereignistyp eindeutig zu identifizieren.") : Amended ToSubclass] string SourceName;
  [DisplayName("Typ") : Amended,Description("Gibt den Ereignistyp als aufgelistete Zeichenfolge an.") : Amended ToSubclass,Values{"Erfolgreich", "Fehler", "Warnung", "Informationen", "Überwachung erfolgreich", "Überwachungsfehler"} : Amended ToSubclass] string Type;
  [DisplayName("Kategorie") : Amended,Description("Gibt eine quellenspezifische Unterkategorie für das Ereignis an.") : Amended ToSubclass] uint16 Category;
  [DisplayName("Kategoriezeichenfolge") : Amended,Description("Gibt die quellenspezifische Übersetzung der Unterkategorie an.") : Amended ToSubclass] string CategoryString;
  [DisplayName("Erstellungszeit") : Amended,Description("Gibt den Zeitpunkt an, zu dem die Quelle das Ereignis generiert hat.") : Amended ToSubclass] datetime TimeGenerated;
  [DisplayName("Erstellungszeit") : Amended,Description("Gibt den Zeitpunkt an, zu dem das Ereignis in die Protokolldatei geschrieben wurde.") : Amended ToSubclass] datetime TimeWritten;
  [DisplayName("Lokaler Computer") : Amended,Description("Die mit Null beendete Zeichenfolge in variabler Länge gibt den Namen des Computers an, der das Ereignis generiert hat.") : Amended ToSubclass] string ComputerName;
  [DisplayName("Benutzername") : Amended,Description("Der Benutzername des Benutzers, der angemeldet war, als das Ereignis auftrat. Wenn der Benutzername nicht bestimmt werden kann, ist diese Eigenschaft Null.") : Amended ToSubclass] string User;
  [DisplayName("Meldung") : Amended,Description("Die im NT-Ereignisprotokoll angezeigte Ereignismeldung. Diese Standardmeldung enthält keine oder mehrere Einfügezeichenfolgen, die von der NT-Ereignisquelle angegeben werden. Die Einfügezeichenfolgen werden in einem vordefinierten Format in die Standardmeldung eingefügt. Wenn keine Einfügezeichenfolgen vorhanden sind, oder beim Einfügen ein Fehler auftritt, wird in diesem Feld nur die Standardmeldung angezeigt.") : Amended ToSubclass] string Message;
  [DisplayName("Einfügezeichenfolge") : Amended,Description("Die Berichtseinfügezeichenfolge des NT-Ereignisses.") : Amended ToSubclass] string InsertionStrings[];
  [DisplayName("Binärdaten") : Amended,Description("Die binären Berichtsdaten des NT-Ereignisses.") : Amended ToSubclass] Uint8 Data[];
  [Description("Die Eigenschaft \"Type\" gibt den Ereignistyp an.") : Amended ToSubclass,DisplayName("Ereignistyp") : Amended,Values{"Erfolgreich", "Fehler", "Warnung", "Informationen", "Sicherheitsüberwachung erfolgreich", "Sicherheitsüberwachungsfehler"} : Amended ToSubclass] uint8 EventType;
};

[Description("Die Klasse \"Win32_NTLogEventLog\" stellt eine Assoziation zwischen einem NT-Ereignisprotokoll und der Protokolldatei, die das Ereignis enthält, dar.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_NTLogEventLog
{
  [Description("Die Eigenschaft \"Log\" verweist auf die Protokolldatei, die das NT-Protokollereignis enthält.") : Amended ToSubclass,Key : ToInstance ToSubclass DisableOverride] Win32_NTEventlogFile Ref Log;
  [Description("Die Eigenschaft \"Record\" verweist auf ein NT-Protokollereignis.") : Amended ToSubclass,Key : ToInstance ToSubclass DisableOverride] Win32_NTLogEvent Ref Record;
};

[Description("Die Klasse \"Win32_NTLogEventUser\" stellt eine Assoziation zwischen einem NT-Protokollereignis und dem Benutzer, der zurzeit des protokollierten Ereignisses angemeldet war, dar. ") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_NTLogEventUser
{
  [Description("Die Eigenschaft \"User\" verweist auf den Benutzer, der zurzeit des protokollierten Ereignisses angemeldet war.") : Amended ToSubclass,Key : ToInstance ToSubclass DisableOverride] Win32_UserAccount Ref User;
  [Description("Die Eigenschaft \"Record\" verweist auf ein NT-Protokollereignis.") : Amended ToSubclass,Key : ToInstance ToSubclass DisableOverride] Win32_NTLogEvent Ref Record;
};

[Description("Die Klasse \"Win32_NTLogEventComputer\" stellt eine Assoziation zwischen einem NT-Protokollereignis und dem Computer, auf dem das Ereignis aufgetreten ist, dar.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_NTLogEventComputer
{
  [Description("Die Eigenschaft \"Computer\" verweist auf den Computer, auf dem das Ereignis aufgetreten ist.") : Amended ToSubclass,Key : ToInstance ToSubclass DisableOverride] Win32_ComputerSystem Ref Computer;
  [Description("Die Eigenschaft \"Record\" verweist auf ein NT-Protokollereignis.") : Amended ToSubclass,Key : ToInstance ToSubclass DisableOverride] Win32_NTLogEvent Ref Record;
};
