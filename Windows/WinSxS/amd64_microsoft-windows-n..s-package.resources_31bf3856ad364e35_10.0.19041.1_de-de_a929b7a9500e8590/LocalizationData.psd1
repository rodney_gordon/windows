# Localized	12/07/2019 11:54 AM (GMT)	303:6.40.20520 	LocalizationData.psd1
ConvertFrom-StringData @'

###PSLOC

progress_Diagnosing_NoDetails=Probleme werden gesucht...
progress_Diagnosing_SafeMode=Der Startmodus wird überprüft...
progress_Diagnosing_DPS=Die Ausführung des Netzwerkdiagnosediensts wird überprüft...
progress_Diagnosing_Initializing=Die Netzwerkdiagnose wird gestartet...
progress_Repairing=Die Reparatur wird ausgeführt...
progress_Vaildating_NoDetails=Die Behebung des Problems wird überprüft...
progress_Collecting_Logs=Ergebnisse werden gesammelt...
progress_Collecting_Config=Konfigurationsdetails werden gesammelt...
interaction_AllAdapters=Alle Netzwerkadapter
interaction_InvalidURL_Desc=Beispielsweise "http://www.microsoft.com".
interaction_InvalidURL_FormatError=Bei "{0}" scheint es sich nicht um eine gültige Adresse zu handeln. Geben Sie die Adresse in folgendem Format ein: http://www.microsoft.com
interaction_InvalidUNC_Desc=Zum Beispiel: \\\\Netzwerkname\\Ordner
interaction_InvalidUNC_FormatError=Bei "{0}" scheint es sich nicht um eine gültige Netzwerkadresse zu handeln. Geben Sie die Netzwerkadresse in folgendem Format ein: \\\\Netzwerkname\\Ordner
interaction_InvalidUNC_CharError="{0}" enthält ungültige Zeichen. Eine Netzwerkadresse darf folgende Zeichen nicht enthalten: / : * ? " < > |
interaction_InvalidURLOrUNC_Desc=Zum Beispiel: http://www.microsoft.com oder \\\\Netzwerkname\\Ordner.
interaction_InvalidURLOrUNC_FormatError=Bei "{0}" scheint es sich nicht um eine gültige Adresse oder um einen gültigen Netzwerkpfad zu handeln. Geben Sie die Adresse oder den Netzwerkpfad in folgendem Format ein:  http://www.microsoft.com oder \\\\Netzwerkname\\Ordner
interaction_InvalidExe_Desc=Klicken Sie auf "Durchsuchen", und wählen Sie dann eine Programmdatei aus. Die ausgewählte Datei muss über die Erweiterung ".exe" verfügen.
interaction_InvalidExe_FormatError="{0}" ist keine gültige EXE-Datei. Stellen Sie sicher, dass die EXE-Datei das folgende Format hat: "C:\\windows\\explorer.exe".
interaction_DefaultContinueButtonName=Überprüfen Sie, ob das Problem behoben wurde.
interaction_DefaultContinueButtonDescription=Klicken Sie hier, nachdem Sie die obigen Anweisungen befolgt haben.
interaction_DefaultLaunchButtonName=Anzeigen, wo die obige Aufgabe ausgeführt werden kann
interaction_Inbound_Exe=Anderen Computern das Herstellen einer Verbindung mit {0} ermöglichen
TraceFileReportName=Netzwerkdiagnoseprotokoll
OtherNetworkConfigReportName=Andere Netzwerkkonfiguration und Protokolle
HelperClassEventName=Diagnoseinformationen
HelperClassEventNameWithHCName=Diagnoseinformationen ({0})
HelperClassEventDesc=Detaillierte Informationen aus Netzwerkdiagnosemodulen
DefaultHelpTopicText=Weitere Informationen finden Sie über Windows-Hilfe und Support.
repair_StartDPS=Starten Sie den Diagnoserichtliniendienst.
repair_SetAutoDPS=Starten Sie den Diagnoserichtliniendienst automatisch.

###PSLOC

'@
