# Localized	12/07/2019 11:46 AM (GMT)	303:6.40.20520 	CL_LocalizationData.psd1
ConvertFrom-StringData @'
###PSLOC
id_collectfileprogressactivity=Dateien werden gesammelt. Dies kann einige Minuten dauern.
id_int_desc_collectdata_prog=Daten werden gesammelt. Dies kann einige Minuten dauern.
id_name_rc_appreadiness=AppReadiness-Ordner fehlt
id_name_rc_auinstall=Für automatische Updates erforderliche Ordner fehlen.
id_rc_prog_checkacl=Sicherheitseinstellungen werden überprüft...
id_progress_verifyaccount=Microsoft-Konto wird überprüft
id_rc_progress_gpsetting=Gruppenrichtlinieneinstellungen werden überprüft...
id_int_desc_tile=Kachelbenachrichtigungen sind deaktiviert.
id_int_desc_toast=Popupbenachrichtigungen sind deaktiviert.
id_int_desc_toastonlock=Popupbenachrichtigungen auf dem Sperrbildschirm sind deaktiviert.
id_int_desc_cloud=Benachrichtigungen des Netzwerks sind deaktiviert.
id_int_desc_tileshistory=Kachelbenachrichtigungen werden bei der Abmeldung gelöscht.
id_int_desc_uninstall=Benutzer können keine Apps von der Startseite deinstallieren.
id_progress_networkavail=Internetzugriff wird überprüft
id_name_rc_regapp=Die Windows Store-Konfiguration ist möglicherweise beschädigt.
id_name_rc_tempinetfolder=Der Pfad für temporäre Internetdateien hat sich geändert.
id_name_rc_wsreset=Der Windows Store-Cache ist möglicherweise beschädigt.
id_name_rs_appreadiness=AppReadiness-Ordner erstellen
id_name_rs_auinstall=Für automatische Updates erforderliche Ordner erstellen
id_rs_prog_resetacl=Sicherheitseinstellungen werden zurückgesetzt...
id_name_rs_kindassoc=Gängige Bild- und Videodateiformate registrieren
id_name_rs_regapp=Windows Store erneut registrieren
id_name_rs_tempinetfolder=Pfad für temporäre Internetdateien auf Standard zurücksetzen
id_name_rs_wsreset=Windows Store zurücksetzen und öffnen
###PSLOC
'@
