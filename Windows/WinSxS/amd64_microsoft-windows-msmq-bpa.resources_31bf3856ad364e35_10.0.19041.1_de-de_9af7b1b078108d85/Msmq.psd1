# Localized	12/07/2019 11:45 AM (GMT)	303:6.40.20520 	MsmqBpa.psd1
# 
# Only add new (name,value) pairs to the end of this table
# Do not remove, insert or re-arrange entries
#

ConvertFrom-StringData @'

###PSLOC start localizing
    #
    # Install on non-DC servers
    #
InstallOnNonDC_Title=MSMQ sollte auf einem Nichtdomänencontroller installiert sein.
InstallOnNonDC_Problem=MSMQ ist auf einem Domänencontroller installiert.
InstallOnNonDC_Impact=Bei starker Auslastung sind möglicherweise normale Domänenvorgänge betroffen.
InstallOnNonDC_Resolution=Installieren Sie MSMQ auf einem Nichtdomänencontroller.
InstallOnNonDC_Compliant=Bei der Best Practices Analyzer-Überprüfung für MSMQ wurde festgestellt, dass diese bewährte Methode eingehalten wird.

###PSLOC
'@
