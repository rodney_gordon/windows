# Localized	12/07/2019 11:55 AM (GMT)	303:6.40.20520 	RS_ResetDisplayIdleTimeout.psd1
ConvertFrom-StringData @'
###PSLOC
reset_DisplayIdleTimeout=Zurücksetzen des Leerlaufzeitlimits für die Anzeige
Report_name_DisplayIdleTimeout_AC_result=Das Ergebnis des Zurücksetzens des Leerlaufzeitlimits der Anzeige für Wechselstrom
Report_name_DisplayIdleTimeout_DC_result=Das Ergebnis des Zurücksetzens des Leerlaufzeitlimits der Anzeige für Gleichstrom
Report_name_DisplayIdleTimeout=Anpassung des Leerlaufzeitlimits für die Anzeige
AC_settingvalue_original=Ursprüngliche Wechselstromeinstellung
DC_settingvalue_original=Ursprüngliche Gleichstromeinstellung
AC_settingvalue_reset=Neue Wechselstromeinstellung
DC_settingvalue_reset=Neue Gleichstromeinstellung
###PSLOC
'@

