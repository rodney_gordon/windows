# Localized	12/07/2019 11:55 AM (GMT)	303:6.40.20520 	RS_Adjustwirelessadaptersettings.psd1
ConvertFrom-StringData @'
###PSLOC
reset_Wirelessadaptersettings=Zurücksetzen der Drahtlosadaptereinstellungen
Report_name_Wirelessadaptersettings_AC_result=Das Ergebnis des Zurücksetzens der Drahtlosadaptereinstellungen für Wechselstrom.
Report_name_Wirelessadaptersettings_DC_result=Das Ergebnis des Zurücksetzens der Drahtlosadaptereinstellungen für Gleichstrom.
Report_name_Wirelessadaptersettings=Anpassung der Drahtlosadaptereinstellung
AC_settingvalue_original=Ursprüngliche Wechselstromeinstellung
DC_settingvalue_original=Ursprüngliche Gleichstromeinstellung
AC_settingvalue_reset=Neue Wechselstromeinstellung
DC_settingvalue_reset=Neue Gleichstromeinstellung
###PSLOC
'@

