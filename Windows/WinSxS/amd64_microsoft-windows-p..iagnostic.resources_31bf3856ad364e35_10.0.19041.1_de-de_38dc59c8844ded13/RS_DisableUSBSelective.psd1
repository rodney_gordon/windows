# Localized	12/07/2019 11:55 AM (GMT)	303:6.40.20520 	RS_DisableUSBSelective.psd1
ConvertFrom-StringData @'
###PSLOC
reset_USBSelective=Selektives USB-Energiesparen wird aktiviert.
Report_name_USBSelective_AC_result=Das Ergebnis der Aktivierung des selektiven USB-Energiesparens für Wechselstrom.
Report_name_USBSelective_DC_result=Das Ergebnis der Aktivierung des selektiven USB-Energiesparens für Gleichstrom.
Report_name_USBSelective=Anpassung des selektiven USB-Energiesparens
AC_settingvalue_original=Ursprüngliche Wechselstromeinstellung
DC_settingvalue_original=Ursprüngliche Gleichstromeinstellung
AC_settingvalue_reset=Neue Wechselstromeinstellung
DC_settingvalue_reset=Neue Gleichstromeinstellung
###PSLOC
'@

