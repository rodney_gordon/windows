# Localized	12/07/2019 11:56 AM (GMT)	303:6.40.20520 	RS_ResetIdleSleepsetting.psd1
ConvertFrom-StringData @'
###PSLOC
reset_IdleSleepsetting=Zurücksetzen der Einstellung für den Energiesparmodus nach Leerlaufbetrieb
Report_name_IdleSleepsetting_AC_result=Das Ergebnis des Zurücksetzens der Einstellung für den Energiesparmodus nach Leerlaufbetrieb für Wechselstrom
Report_name_IdleSleepsetting_DC_result=Das Ergebnis des Zurücksetzens der Einstellung für den Energiesparmodus nach Leerlaufbetrieb für Gleichstrom
Report_name_IdleSleepsetting=Anpassung der Einstellung füe den Energiesparmodus nach Leerlaufbetrieb
AC_settingvalue_original=Ursprüngliche Wechselstromeinstellung
DC_settingvalue_original=Ursprüngliche Gleichstromeinstellung
AC_settingvalue_reset=Neue Wechselstromeinstellung
DC_settingvalue_reset=Neue Gleichstromeinstellung
###PSLOC
'@

