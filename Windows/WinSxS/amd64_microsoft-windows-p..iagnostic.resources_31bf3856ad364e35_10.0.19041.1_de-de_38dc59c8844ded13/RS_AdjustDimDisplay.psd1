# Localized	12/07/2019 11:55 AM (GMT)	303:6.40.20520 	RS_AdjustDimDisplay.psd1
ConvertFrom-StringData @'
###PSLOC
reset_DimDisplay=Abblendeinstellungen zurücksetzen
Report_name_DimDisplay_AC_result=Ergebnis der Zurücksetzung der Abblendeinstellungen für Wechselstrom
Report_name_DimDisplay_DC_result=Ergebnis der Zurücksetzung der Abblendeinstellungen für Gleichstrom
Report_name_DimDisplay=Anpassung des Zeitlimits für das Abblenden des Bildschirms
AC_settingvalue_original=Ursprüngliche Wechselstromeinstellung
DC_settingvalue_original=Ursprüngliche Gleichstromeinstellung
AC_settingvalue_reset=Neue Wechselstromeinstellung
DC_settingvalue_reset=Neue Gleichstromeinstellung
###PSLOC
'@
