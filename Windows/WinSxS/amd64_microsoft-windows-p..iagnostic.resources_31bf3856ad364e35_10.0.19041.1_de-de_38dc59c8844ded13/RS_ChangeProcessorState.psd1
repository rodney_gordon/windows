# Localized	12/07/2019 11:55 AM (GMT)	303:6.40.20520 	RS_ChangeProcessorState.psd1
ConvertFrom-StringData @'
###PSLOC
reset_MinProcessorState=Minimalen Leistungszustand des Prozessors zurücksetzen
Report_name_MinProcessorState_AC_result=Ergebnis der Zurücksetzung des minimalen Leistungszustands des Prozessors für Wechselstrom
Report_name_MinProcessorState_DC_result=Ergebnis der Zurücksetzung des minimalen Leistungszustands des Prozessors für Gleichstrom
Report_name_MinProcessorState=Anpassung des minimalen Leistungszustands des Prozessors
AC_settingvalue_original=Ursprüngliche Wechselstromeinstellung
DC_settingvalue_original=Ursprüngliche Gleichstromeinstellung
AC_settingvalue_reset=Neue Wechselstromeinstellung
DC_settingvalue_reset=Neue Gleichstromeinstellung
###PSLOC
'@
