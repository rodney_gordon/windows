# Localized	12/07/2019 11:47 AM (GMT)	303:6.40.20520 	ArchiveResources.psd1
# Localized ArchiveResources.psd1

ConvertFrom-StringData @'
###PSLOC
PathNotFoundError=Der Pfad "{0}" ist entweder nicht vorhanden oder entspricht keinem gültigen Dateisystempfad.
ExpandArchiveInValidDestinationPath=Der Pfad "{0}" ist kein gültiger Dateisystemverzeichnispfad.
InvalidZipFileExtensionError='{0}' ist kein unterstütztes Archivdateiformat. '{1}' ist das einzige unterstützte Archivdateiformat.
ArchiveFileIsReadOnly=Da das Attribut der Archivdatei "{0}" auf "ReadOnly" festgelegt ist, kann sie nicht aktualisiert werden. Wenn Sie beabsichtigen, die vorhandene Archivdatei zu aktualisieren, entfernen Sie das ReadOnly-Attribut von der Archivdatei, oder verwenden Sie den -Force-Parameter, um die Datei zu überschreiben und eine neue Archivdatei zu erstellen.
ZipFileExistError=Die Archivdatei "{0}" ist bereits vorhanden. Verwenden Sie den -Update-Parameter, um die vorhandene Archivdatei zu aktualisieren, oder den -Force-Parameter, um die vorhandene Archivdatei zu überschreiben.
DuplicatePathFoundError=Die Eingabe für den Parameter '{0}' enthält den doppelten Pfad '{1}'. Geben Sie einen eindeutigen Satz von Pfaden als Eingabe für den Parameter '{2}' an.
ArchiveFileIsEmpty=Die Archivdatei '{0}' ist leer.
CompressProgressBarText=Archivdatei '{0}' wird erstellt...
ExpandProgressBarText=Erweiterung der Archivdatei '{0}' wird ausgeführt...
AppendArchiveFileExtensionMessage=Der für den DestinationPath-Parameter angegebene Archivdateipfad '{0}' enthält keine ZIP-Erweiterung. Folglich ist die ZIP-Datei an die bereitgestellten DestinationPath-Pfad angehängt, und die Archivdatei würde unter '{1}' erstellt werden.
AddItemtoArchiveFile='{0}' wird hinzugefügt.
BadArchiveEntry=Ungültiger Archiveintrag "{0}".
CreateFileAtExpandedPath='{0}' wurde erstellt.
InvalidArchiveFilePathError=Der als Eingabe für den Parameter '{1}' angegebene Archivdateipfad '{0}' wird in mehrere Dateisystempfade aufgelöst. Geben Sie einen eindeutigen Pfad zum Parameter '{2}' an, unter dem die Archivdatei erstellt werden soll.
InvalidExpandedDirPathError=Der als Eingabe für den DestinationPath-Parameter angegebene Verzeichnispfad '{0}' wird in mehrere Dateisystempfade aufgelöst. Geben Sie einen eindeutigen Pfad zum Destination-Parameter an, unter dem der Inhalt der Archivdatei erweitert werden soll.
FileExistsError=Fehler beim Erstellen der Datei '{0}', während der Inhalt der Archivdatei '{1}' erweitert wird. Die Datei '{2}' ist bereits vorhanden. Verwenden Sie den Parameter '-Force', wenn Sie den Inhalt des bestehenden Verzeichnisses '{3}' beim Expandieren der Archivdatei überschreiben möchten.
DeleteArchiveFile=Die teilweise erstellte Archivdatei '{0}' wird gelöscht, weil sie unbrauchbar ist.
InvalidDestinationPath=Der Zielpfad '{0}' enthält keine Archivdatei mit einem gültigen Namen.
PreparingToCompressVerboseMessage=Komprimieren wird vorbereitet...
PreparingToExpandVerboseMessage=Erweitern wird vorbereitet...
###PSLOC
'@
