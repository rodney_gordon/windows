# Localized	12/07/2019 11:48 AM (GMT)	303:6.40.20520 	PackageManagementDscUtilities.strings.psd1
#########################################################################################
#
# Copyright (c) Microsoft Corporation.
#
# culture="en-US"
#
#########################################################################################
ConvertFrom-StringData @'
###PSLOC
InValidUri=Ungültiger URI: {0}. Beispiel für einen gültigen URI: https://www.powershellgallery.com/api/v2/
PathDoesNotExist=Der Pfad "{0}" ist nicht vorhanden.
VersionError=MinimumVersion muss kleiner als MaximumVersion sein. MinimumVersion oder MaximumVersion kann nicht mit RequiredVersion im selben Befehl verwendet werden.
UnexpectedArgument=Unerwarteter Argumenttyp: {0}
SourceNotFound=Die Quelle {0} wurde nicht gefunden. Stellen Sie sicher, dass sie registriert wurde.
CallingFunction="Rufen Sie die Funktion {0} auf".
###PSLOC
'@
