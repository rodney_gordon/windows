# Localized	12/07/2019 08:20 AM (GMT)	303:6.40.20520 	MSFT_GroupResource.strings.psd1
# Localized resources for MSFT_GroupResource

ConvertFrom-StringData @'
###PSLOC
GroupWithName=Gruppe: {0}
RemoveOperation=Entfernen
AddOperation=Hinzufügen
SetOperation=Festlegen
GroupCreated=Die Gruppe "{0}" wurde erfolgreich erstellt.
GroupUpdated=Die Eigenschaften der Gruppe "{0}" wurden erfolgreich aktualisiert.
GroupRemoved=Die Gruppe "{0}" wurde erfolgreich entfernt.
NoConfigurationRequired=Die Gruppe "{0}" ist auf diesem Knoten mit den gewünschten Eigenschaften vorhanden. Keine Aktion erforderlich.
NoConfigurationRequiredGroupDoesNotExist=Die Gruppe "{0}" ist auf diesem Knoten nicht vorhanden. Keine Aktion erforderlich.
CouldNotFindPrincipal=Es wurde kein Prinzipal mit dem angegebenen Namen [{0}] gefunden.
MembersAndIncludeExcludeConflict=Bei den Parametern {0} und {1} und/oder {2} tritt ein Konflikt auf. Der Parameter {0} darf nicht in Kombination mit den Parametern {1} und {2} verwendet werden.
MembersIsNull=Der Members-Parameterwert ist NULL. Der Parameter "{0}" muss angegeben werden, wenn weder "{1}" noch "{2}" bereitgestellt wird.
MembersIsEmpty=Der Members-Parameter ist leer. Es muss mindestens ein Gruppenmitglied angegeben werden.
MemberNotValid=Das Gruppenmitglied ist nicht vorhanden oder kann nicht aufgelöst werden: {0}.
IncludeAndExcludeConflict=Der Prinzipal "{0}" ist sowohl im Parameterwert "{1}" als auch im Parameterwert "{2}" enthalten. In den Prinzipalwerten "{1}" und "{2}" darf nicht der gleiche Prinzipal verwendet werden.
IncludeAndExcludeAreEmpty=MembersToInclude und MembersToExclude sind entweder beide NULL oder leer. Mindestens ein Mitglied muss in einem dieser Parameter angegeben werden.
InvalidGroupName=Der Name "{0}" kann nicht verwendet werden. Namen dürfen nicht ausschließlich aus Punkten und/oder Leerzeichen bestehen oder die folgenden Zeichen enthalten: {1}
GroupExists=Eine Gruppe mit dem Namen "{0}" ist vorhanden.
GroupDoesNotExist=Es ist keine Gruppe mit dem Namen "{0}" vorhanden.
PropertyMismatch=Für die {0}-Eigenschaft wird der Wert {1} erwartet, der Wert ist jedoch {2}.
MembersNumberMismatch={0}-Eigenschaft. Die angegebene Anzahl eindeutiger Gruppenmitglieder ({1}) stimmt nicht mit der tatsächlichen Anzahl von Gruppenmitgliedern ({2}) überein.
MembersMemberMismatch=Für mindestens ein Mitglied ({0}) im angegebenen {1}-Parameter liegt keine Übereinstimmung in der vorhandenen Gruppe "{2}" vor.
MemberToExcludeMatch=Für mindestens ein Mitglied ({0}) im angegebenen {1}-Parameter liegt eine Übereinstimmung in der vorhandenen Gruppe "{2}" vor.
ResolvingLocalAccount={0} wird als lokales Konto aufgelöst.
ResolvingDomainAccount={0} wird in der Domäne "{1}" aufgelöst.
ResolvingDomainAccountWithTrust={0} wird mit Domänenvertrauensstellung aufgelöst.
DomainCredentialsRequired=Zum Auflösen des Domänenkontos "{0}" sind Anmeldeinformationen erforderlich.
UnableToResolveAccount=Das Konto '{0}' konnte nicht aufgelöst werden. Fehlermeldung: {1} (Fehlercode={2})
###PSLOC

'@
