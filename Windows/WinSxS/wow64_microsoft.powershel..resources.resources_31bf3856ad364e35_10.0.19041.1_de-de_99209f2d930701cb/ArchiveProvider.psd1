# Localized	12/07/2019 08:16 AM (GMT)	303:6.40.20520 	ArchiveProvider.psd1
# Localized ArchiveProvider.psd1

ConvertFrom-StringData @'
###PSLOC
InvalidChecksumArgsMessage=Die Angabe einer Prüfsumme ist nicht sinnvoll, wenn keine Inhaltsprüfung (Validate-Parameter) angefordert wird.
InvalidDestinationDirectory=Das angegebene Zielverzeichnis "{0}" ist nicht vorhanden oder kein Verzeichnis.
InvalidSourcePath=Die angegebene Quelldatei "{0}" ist nicht vorhanden oder keine Datei.
InvalidNetSourcePath=Die angegebene Quelldatei "{0}" ist kein gültiger .NET-Quellpfad.
ErrorOpeningExistingFile=Fehler beim Öffnen der Datei "{0}" auf dem Datenträger. Details können Sie der internen Ausnahme entnehmen.
ErrorOpeningArchiveFile=Fehler beim Öffnen der Archivdatei "{0}". Details können Sie der internen Ausnahme entnehmen.
ItemExistsButIsWrongType=Das benannte Element ({0}) ist vorhanden, jedoch nicht vom erwarteten Typ, und "Force" wurde nicht angegeben.
ItemExistsButIsIncorrect=Es wurde festgestellt, dass die Zieldatei "{0}" nicht der Quelldatei entspricht, "Force" jedoch nicht angegeben wurde. Der Vorgang kann nicht fortgesetzt werden.
ErrorCopyingToOutstream=Fehler beim Kopieren der archivierten Datei nach "{0}"
PackageUninstalled=Das Archiv unter "{0}" wurde vom Zielcomputer "{1}" entfernt.
PackageInstalled=Das Archiv unter "{0}" wurde auf dem Zielcomputer "{1}" entpackt.
ConfigurationStarted=Die Konfiguration von "MSFT_ArchiveResource" wird gestartet.
ConfigurationFinished=Die Konfiguration von "MSFT_ArchiveResource" wurde abgeschlossen.
MakeDirectory=Verzeichnis "{0}" erstellen
RemoveFileAndRecreateAsDirectory=Vorhandene Datei "{0}" entfernen und durch ein gleichnamiges Verzeichnis ersetzen
RemoveFile=Datei "{0}" entfernen
RemoveDirectory=Verzeichnis "{0}" entfernen
UnzipFile=Archivierte Datei nach "{0}" entzippen
DestMissingOrIncorrectTypeReason=Die Zieldatei "{0}" war nicht vorhanden oder keine Datei.
DestHasIncorrectHashvalue=Die Zieldatei "{0}" ist vorhanden, ihre Prüfsumme stimmte jedoch nicht mit der ursprünglichen Datei überein.
DestShouldNotBeThereReason=Die Zieldatei "{0}" ist vorhanden, sollte jedoch nicht vorhanden sein.
UsingKeyToRetrieveHashValue='{0}' wird verwendet, um einen Hashwert abzurufen.
Nocachevaluefound=Kein Cachewert gefunden
Cachevaluefoundreturning=Cachewert gefunden, es wird '{0}' zurückgegeben.
CacheCorrupt=Es wurde ein Cache gefunden, der jedoch nicht geladen werden konnte. Der Cache wird ignoriert.
Usingtmpkeytosavehashvalue=Es wird {0} {1} verwendet, um den Hashwert zu speichern.
AbouttocachevalueInputObject=Darauf wartend, den Wert '{0}' zwischenspeichern zu können
InUpdateCache=In 'Update-Cache'
AddingentryFullNameasacacheentry='{0}' wird als Cacheeintrag hinzugefügt.
UpdatingCacheObject='CacheObject' wird aktualisiert.
Placednewcacheentry=Neuer Cacheeintrag wurde platziert.
NormalizeChecksumreturningChecksum='Normalize-Checksum' gibt '{0}' zurück.
PathPathisalreadyaccessiableNomountneeded.=Der Pfad '{0}' ist bereits verfügbar. Es ist kein Einbinden erforderlich.
Pathpathisnotavalidatenetpath=Der Pfad '{0}' ist kein gültiger Netzpfad.
createpsdrivewithPathpath='psdrive' mit Pfad '{0}' erstellen...
CannotaccessPathPathwithgivenCredential=Mit den angegebenen Anmeldeinformationen kann nicht auf den Pfad '{0}' zugegriffen werden.
Abouttovalidatestandardarguments=Darauf wartend, die Standardargumente überprüfen zu können
Goingforcacheentries=Mit Cacheeinträgen beginnen
Thecachewasuptodateusingcachetosatisfyrequests=Der Cache war aktuell. Die Anforderungen werden über den Cache erfüllt.
Abouttoopenthezipfile=Darauf wartend, die ZIP-Datei öffnen zu können
Cacheupdatedwithentries=Der Cache wurde mit {0} Einträgen aktualisiert.
Processing='{0}' wird verarbeitet.
InTestTargetResourcedestexistsnotusingchecksumscontinuing=In 'Test-TargetResource': '{0}' ist vorhanden, Prüfsummen werden nicht verwendet, Vorgang wird fortgesetzt.
Notperformingchecksumthefileondiskhasthesamewritetimeasthelasttimeweverifieditscontents=Prüfsumme wird nicht ausgeführt. Die Datei auf dem Datenträger hat dieselbe Schreibzugriffszeit wie bei der letzten Überprüfung ihres Inhalts.
destexistsandthehashmatcheseven='{0}' ist vorhanden, und der Hash stimmt überein, obwohl 'LastModifiedTime' nicht übereinstimmte. Der Cache wird aktualisiert.
InTestTargetResourcedestexistsandtheselectedtimestampChecksummatched=In 'Test-TargetResource': '{0}' ist vorhanden, und der ausgewählte Zeitstempel '{1}' stimmte überein.
RemovePSDriveonRootpsdriveRoot=PowerShell-Laufwerk (PSDrive) aus Stamm '{0}' entfernen
RemovingDir='{0}' wird entfernt.
Hashesofexistingandzipfilesmatchremoving=Die Hashes der vorhandenen und der ZIP-Dateien stimmen überein. Entfernen wird ausgeführt.
HashdidnotmatchfilehasbeenmodifiedsinceitwasextractedLeaving=Der Hash stimmte nicht überein. Die Datei wurde seit ihrem letzten Extrahieren geändert. Der Vorgang wird beendet.
InSetTargetResourceexistsselectedtimestampmatched=In 'Set-TargetResource': '{0}' ist vorhanden, und der ausgewählte Zeitstempel '{1}' stimmte überein. Entfernen wird ausgeführt.
InSetTargetResourceexistsdtheselectedtimestampnotmatchg=In 'Set-TargetResource': '{0}' ist vorhanden, und der ausgewählte Zeitstempel '{1}' stimmte nicht überein. Die Funktion wird beendet.
existingappearstobeanemptydirectoryRemovingit='{0}' scheint ein leeres Verzeichnis zu sein. Es wird entfernt.
LastWriteTimemtcheswhatwehaverecordnotreexaminingchecksum='LastWriteTime' von '{0}' stimmt mit dem gespeicherten Wert überein. '{1}' wird nicht erneut ausgewertet.
FoundfatdestwheregoingtoplaceoneandhashmatchedContinuing=In '{0}', wo eine Datei abgelegt werden soll, wurde eine gefunden, und der Hash stimmt überein. Der Vorgang wird fortgesetzt.
FoundfileatdestwhereweweregoingtoplaceoneandhashdidntmatchItwillbeoverwritten=In '$dest', wo eine Datei abgelegt werden soll, wurde eine gefunden, aber der Hash stimmt nicht überein. Die Datei wird überschrieben.
FoundfileatdestwhereweweregoingtoplaceoneanddoesnotmatchthesourcebutForcewasnotspecifiedErroring=In '{0}', wo eine Datei abgelegt werden soll, wurde eine gefunden, aber sie stimmt nicht mit der Quelldatei überein, und 'Force' ist nicht angegeben. Fehlerbedingung wird generiert.
InSetTargetResourcedestexistsandtheselectedtimestamp$ChecksumdidnotmatchForcewasspecifiedwewilloverwrite="In 'Set-TargetResource': '{0}' ist vorhanden, aber der angegebene Zeitstempel '{1}' stimmte nicht überein. Da 'Force' angegeben ist, wird überschrieben.
FoundafileatdestandtimestampChecksumdoesnotmatchthesourcebutForcewasnotspecifiedErroring=In '{0} ' wurde eine Datei gefunden, aber der Zeitstempel '{1}' stimmt nicht mit der Quelle überein, und 'Force' ist nicht angegeben. Fehlerbedingung wird generiert.
FoundadirectoryatdestwhereafileshouldbeRemoving=In '{0}' wurde ein Verzeichnis gefunden, wo eine Datei sein sollte. Entfernen wird ausgeführt.
FounddirectoryatdestwhereafileshouldbeandForcewasnotspecifiedErroring=In '{0}' wurde ein Verzeichnis gefunden, wo eine Datei sein sollte, und 'Force' ist nicht angegeben. Fehlerbedingung wird generiert.
Writingtofiledest=Es wird in die Datei '{0}' geschrieben.
RemovePSDriveonRootdriveRoot=PowerShell-Laufwerk (PSDrive) aus Stamm '{0}' entfernen
Updatingcache=Der Cache wird aktualisiert.
FolderDirdoesnotexist=Den Ordner '{0}' gibt es nicht.
Examiningdirectorytoseeifitshouldberemoved='{0}' wird untersucht, um festzustellen, ob es entfernt werden soll.
InSetTargetResourcedestexistsandtheselectedtimestampChecksummatchedwillleaveit=In 'Set-TargetResource': '{0}' ist vorhanden, und der ausgewählte Zeitstempel '{1}' stimmt überein. Die Funktion wird beendet.
###PSLOC

'@
