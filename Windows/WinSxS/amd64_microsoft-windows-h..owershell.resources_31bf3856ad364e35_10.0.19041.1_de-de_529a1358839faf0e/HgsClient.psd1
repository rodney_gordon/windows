# Localized	12/07/2019 11:56 AM (GMT)	303:6.40.20520 	HgsClient.psd1
#/*++
#
#    Copyright (c) Microsoft Corporation.  All rights reserved.
#
#    Abstract:
#
#        String table for the Host Guardian Client (HGS) PowerShell
#
#--*/

#
# Data table for the format strings
#
ConvertFrom-StringData @"
    ###PSLOC - Localizable string
ErrorSetHostKeyAlreadySet=Ein Hostschlüssel wurde bereits festgelegt. Um einen neuen Schlüssel anzugeben, führen Sie zuerst Remove-HgsClientHostKey aus.
ErrorSetHostKeyMissingPrivateKey=Es wurde ein Zertifikat mit dem Fingerabdruck {0} gefunden, der private Schlüssel konnte jedoch nicht verwendet werden.
ErrorSetHostKeyThumbprintInvalid=Ein Zertifikat mit Fingerabdruck {0} wurde nicht gefunden.
ErrorGetHostKeyNotSet=Es wurde kein Hostschlüssel festgelegt. Verwenden Sie Set-HgsClientHostKey, um einen Hostschlüssel festzulegen.
ErrorGetHostKeyNotFound=Das Hostschlüssel-Zertifikat mit dem Fingerabdruck "{0}" wurde nicht gefunden. Installieren Sie das Zertifikat in LocalMachine\\My erneut, oder verwenden Sie "Remove-HgsClientHostKey" und "Set-HgsClientHostKey", um einen neuen Hostschlüssel zu konfigurieren.
WarningRemoveHostKeyProvided=Das Zertifikat mit dem Fingerabdruck {0} wurde nicht entfernt, da es nicht von Set-HgsClientHostKey generiert wurde.
    ###PSLOC - End of localizable string
"@
