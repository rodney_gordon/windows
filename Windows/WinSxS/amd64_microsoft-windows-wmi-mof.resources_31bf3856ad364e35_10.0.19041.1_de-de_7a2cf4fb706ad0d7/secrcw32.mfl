// Copyright (c) 1997-2003 Microsoft Corporation, All Rights Reserved

#pragma autorecover
#pragma classflags(64)
#pragma namespace("\\\\.\\root\\cimv2")
instance of __namespace{ name="ms_407";};
#pragma namespace("\\\\.\\root\\cimv2\\ms_407")

[description("Eine beliebige SID kann nicht aufgelistet werden.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_SID
{
  [Description("SID im Zeichenkettenformat") : Amended ToSubclass,Key : ToInstance ToSubclass DisableOverride] string SID;
  [Description("SID im binären Format") : Amended ToSubclass] uint8 BinaryRepresentation[];
  [Description("Der Name des Kontos, dem die SID zugeordnet ist.") : Amended ToSubclass] string AccountName;
  [Description("Der Domäne des Kontos, dem die SID zugeordnet ist.") : Amended ToSubclass] string ReferencedDomainName;
  [Description("Die Eigenschaft \"SidLength\" gibt die SID-Länge in Bytes an.") : Amended ToSubclass,Units("Bytes") : Amended ToSubclass] uint32 SidLength;
};

[description("Die SID eines Kontos") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_AccountSID
{
  [Description("Konto") : Amended ToSubclass,Key : ToInstance ToSubclass DisableOverride] Win32_Account Ref Element;
  [Description("Die SID des Kontos") : Amended ToSubclass,Key : ToInstance ToSubclass DisableOverride] Win32_SID Ref Setting;
};

[description("Stellt die Sicherheitseinstellungen für ein verwaltetes Element dar.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_SecuritySetting : CIM_Setting
{
  [description("Vererbungsbezogene Kennzeichen. Siehe SECURITY_DESCRIPTOR_CONTROL.") : Amended ToSubclass] uint32 ControlFlags;
  [description("Ermittelt eine strukturelle Darstellung der Sicherheitsbeschreibung des Objekts.") : Amended ToSubclass] uint32 GetSecurityDescriptor([OUT] Win32_SecurityDescriptor Descriptor);
  [description("Legt die Sicherheitsbeschreibung in der angegebenen Struktur fest.") : Amended ToSubclass] uint32 SetSecurityDescriptor([IN] Win32_SecurityDescriptor Descriptor);
};

[description("Ordnet einem Objekt seine Sicherheitseinstellungen zu.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_SecuritySettingOfObject : CIM_ElementSetting
{
  [Description("Objekt mit Sicherheitseinstellungen") : Amended ToSubclass] CIM_LogicalElement Ref Element;
  [Description("Die Sicherheitseinstellungen des Objekts.") : Amended ToSubclass] Win32_SecuritySetting Ref Setting;
};

[description("Beziehung zwischen den Sicherheitseinstellungen und dem Besitzer eines Objekts.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_SecuritySettingOwner
{
  [Description("Die Sicherheitseinstellungen eines Objekts.") : Amended ToSubclass,key : ToInstance ToSubclass DisableOverride] Win32_SecuritySetting Ref SecuritySetting;
  [Description("Die Win32-SID des Objektbesitzers.") : Amended ToSubclass,key : ToInstance ToSubclass DisableOverride] Win32_SID Ref Owner;
};

[description("Beziehung zwischen der Sicherheit und der Gruppe eines Objekts.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_SecuritySettingGroup
{
  [Description("Die Sicherheitseinstellungen eines Objekts.") : Amended ToSubclass,key : ToInstance ToSubclass DisableOverride] Win32_SecuritySetting Ref SecuritySetting;
  [Description("Die Win32-SID der Objektgruppe.") : Amended ToSubclass,key : ToInstance ToSubclass DisableOverride] Win32_SID Ref Group;
};

[description("Gibt die einem Vertrauensnehmer erteilten und verweigerten Rechte für ein Objekt an. Erstellt nach EXPLICIT_ACCESS.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_SecuritySettingAccess
{
  [Description("Die Sicherheitseinstellungen eines Objekts.") : Amended ToSubclass,key : ToInstance ToSubclass DisableOverride] Win32_SecuritySetting Ref SecuritySetting;
  [Description("Die Win32_SID des Vertrauensnehmers für diesen Zugriffseintrag.") : Amended ToSubclass,key : ToInstance ToSubclass DisableOverride] Win32_SID Ref Trustee;
  [Description("Der für den Vertrauensnehmer angegebene Zugriffstyp.") : Amended ToSubclass,Values{"Festlegen", "Verweigern"} : Amended ToSubclass] uint32 Type;
  [Description("Bitkennzeichen, die die Vererbungsmethode der Zugriffsrechte angeben.") : Amended ToSubclass] uint32 Inheritance;
  [Description("Bitkennzeichen, die angeben, welche Berechtigungen betroffen sind.") : Amended ToSubclass] uint32 AccessMask;
  [Description("Die GUID des Objekttyps, dem die Sicherheitseinstellungen zugewiesen sind.") : Amended ToSubclass] string GuidObjectType;
  [Description("Die GUID des Objekttyps von dem dieses Objekt erbt.") : Amended ToSubclass] string GuidInheritedObjectType;
};

[description("Gibt die Vertrauensnehmerüberwachung für ein Objekt an. Erstellt nach EXPLICIT_ACCESS.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_SecuritySettingAuditing
{
  [Description("Die Sicherheitseinstellungen eines Objekts.") : Amended ToSubclass,key : ToInstance ToSubclass DisableOverride] Win32_SecuritySetting Ref SecuritySetting;
  [Description("Die Win32-SID des Vertrauensnehmers für den Überwachungseintrag.") : Amended ToSubclass,key : ToInstance ToSubclass DisableOverride] Win32_SID Ref Trustee;
  [Description("Der für den Vertrauensnehmer angegebene Zugriffstyp.") : Amended ToSubclass,Values{"Überwachung erfolgreich", "Überwachungsfehler"} : Amended ToSubclass] uint32 Type;
  [Description("Bitkennzeichen, die die Vererbungsmethode der Überwachungsrichtlinien angeben.") : Amended ToSubclass] uint32 Inheritance;
  [Description("Bitkennzeichen, die angeben, welche Vorgänge überwacht werden.") : Amended ToSubclass] uint32 AuditedAccessMask;
  [Description("Die GUID des Objekttyps, dem die Sicherheitseinstellungen zugewiesen sind.") : Amended ToSubclass] string GuidObjectType;
  [Description("Die GUID des Objekttyps von dem dieses Objekt erbt.") : Amended ToSubclass] string GuidInheritedObjectType;
};

[description("Gibt einen Vertrauensnehmer an. Es kann ein Name oder eine SID verwendet werden.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_Trustee : __Trustee
{
  [Description("Die SID des Vertrauensnehmers.") : Amended ToSubclass] uint8 SID[];
  [Description("Die SID des Vertrauensnehmers im Zeichenfolgenformat (z. B. S-1-1-0)") : Amended ToSubclass] string SIDString;
  [Description("Der Namensteil des Kontos.") : Amended ToSubclass] string Name;
  [Description("Der Domänenteil des Kontos.") : Amended ToSubclass] string Domain;
  [Description("Die Länge der SID in Bytes.") : Amended ToSubclass] uint32 SidLength;
};

[description("Gibt einen ACE-Eintrag an.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_ACE : __ACE
{
  [Description("Der Vertrauensnehmer des ACE-Eintrags.") : Amended ToSubclass] Win32_Trustee Trustee;
  [Description("Der Typ des ACE-Eintrags.") : Amended ToSubclass,Values{"Zugriff erlaubt", "Zugriff verweigert", "Überwachung"} : Amended ToSubclass] uint32 AceType;
  [Description("Bitkennzeichen, die die Vererbung von ACE angeben.") : Amended ToSubclass] uint32 AceFlags;
  [Description("Bitkennzeichen, die die dem Vertrauensnehmer erteilten/verweigerten Rechte darstellen.") : Amended ToSubclass] uint32 AccessMask;
  [Description("Die dem Objekttyp mit diesen Rechten zugeordnete GUID.") : Amended ToSubclass] string GuidObjectType;
  [Description("Die dem übergeordneten Objekt mit diesen Rechten zugeordnete GUID.") : Amended ToSubclass] string GuidInheritedObjectType;
};

[description("Strukturelle Darstellung von einer Sicherheitsbeschreibung.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_SecurityDescriptor : __SecurityDescriptor
{
  [Description("Der Vertrauensnehmer, der den Objektbesitzer darstellt.") : Amended ToSubclass] Win32_Trustee Owner;
  [Description("Der Vertrauensnehmer, der die Objektgruppe darstellt.") : Amended ToSubclass] Win32_Trustee Group;
  [Description("Win32_ACE-Einträge die den Zugriff auf das Objekt festlegen.") : Amended ToSubclass] Win32_ACE DACL[];
  [Description("Win32_ACE-Einträge die angeben, für welche Benutzer/Gruppen Überwachungsinformationen zusammengestellt werden.") : Amended ToSubclass] Win32_ACE SACL[];
  [Description("Bitkennzeichen mit Informationen über den Inhalt und das Format der Beschreibung.") : Amended ToSubclass] uint32 ControlFlags;
};

[Description("Die \"Win32_SecurityDescriptorHelper\"-Klasse stellt die grundlegende Funktion für das Konvertieren einer Sicherheitsbeschreibung zwischen drei unterschiedlichen Darstellungen bereit:    1) __SecurityDescriptor   2) SDDL - Zeichenfolgendarstellung einer Sicherheitsbeschreibung   3) Binäre Darstellung einer Sicherheitsbeschreibung") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_SecurityDescriptorHelper
{
  [Description("Diese Methode konvertiert eine \"__SecurityDescriptor\"-Instanz in das SDDL-Zeichenfolgenformat.") : Amended ToSubclass] uint32 Win32SDToSDDL([in] __SecurityDescriptor Descriptor,[out] string SDDL);
  [Description("Diese Methode konvertiert eine \"__SecurityDescriptor\"-Instanz in eine Sicherheitsbeschreibung im binären Blob-Format.") : Amended ToSubclass] uint32 Win32SDToBinarySD([in] __SecurityDescriptor Descriptor,[out] uint8 BinarySD[]);
  [Description("Diese Methode konvertiert eine Sicherheitsbeschreibung im SDDL-Zeichenfolgenformat in eine \"__SecurityDescriptor\"-Instanz.") : Amended ToSubclass] uint32 SDDLToWin32SD([in] string SDDL,[out] __SecurityDescriptor Descriptor);
  [Description("Diese Methode konvertiert eine Sicherheitsbeschreibung im SDDL-Zeichenfolgenformat in eine Sicherheitsbeschreibung im binären Blob-Format.") : Amended ToSubclass] uint32 SDDLToBinarySD([in] string SDDL,[out] uint8 BinarySD[]);
  [Description("Diese Methode konvertiert eine Sicherheitsbeschreibung im binären Blob-Format in eine \"__SecurityDescriptor\"-Instanz.") : Amended ToSubclass] uint32 BinarySDToWin32SD([in] uint8 BinarySD[],[out] __SecurityDescriptor Descriptor);
  [Description("Diese Methode konvertiert eine Sicherheitsbeschreibung im binären Blob-Format in eine Sicherheitsbeschreibung im SDDL-Zeichenfolgenformat.") : Amended ToSubclass] uint32 BinarySDToSDDL([in] uint8 BinarySD[],[out] string SDDL);
};

[Description("Sicherheitseinstellungen für eine logische Datei") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_LogicalFileSecuritySetting : Win32_SecuritySetting
{
  [key : ToInstance ToSubclass DisableOverride,Description("Der vollständige Pfadname der Datei oder des Verzeichnisses.") : Amended ToSubclass] string Path;
  [description("Gibt an, ob der Aufrufer über Berechtigungen für das Objekt verfügt.  ") : Amended ToSubclass] boolean OwnerPermissions;
  [description("Ermittelt eine strukturelle Darstellung der Sicherheitsbeschreibung des Objekts.\nDie Methode gibt einen ganzzahligen Wert mit folgenden Bedeutungen zurück: \n0 - Erfolgreicher Abschluss\n2 - Der Benutzer ist nicht berechtigt, auf die angeforderten Informationen zuzugreifen.\n8 - Unbekannter Fehler\n9 - Der Benutzer verfügt nicht über die erforderliche Berechtigungen\n21 - Der angegebene Parameter ist ungültig.\nAndere - Andere als oben aufgeführte ganzzahlige Werte finden Sie in der Win32-Fehlercodedokumentation.") : Amended ToSubclass,Values{"Erfolgreich", "Zugriff verweigert", "Unbekannter Fehler", "Fehlende Berechtigung", "Ungültiger Parameter", "Andere"} : Amended ToSubclass] uint32 GetSecurityDescriptor([out] Win32_SecurityDescriptor Descriptor);
  [description("Setzt die Sicherheitsbeschreibung in die angegebene Struktur.\nDie Methode gibt einen ganzzahligen Wert mit folgenden Bedeutungen zurück: \n0 - Erfolgreicher Abschluss\n2 - Der Benutzer ist nicht berechtigt, auf die angeforderten Informationen zuzugreifen.\n8 - Unbekannter Fehler\n9 - Der Benutzer verfügt nicht über die erforderliche Berechtigungen\n21 - Der angegebene Parameter ist ungültig.\nAndere - Andere als oben aufgeführte ganzzahlige Werte finden Sie in der Win32-Fehlercodedokumentation.") : Amended ToSubclass,Values{"Erfolgreich", "Zugriff verweigert", "Unbekannter Fehler", "Fehlende Berechtigung", "Ungültiger Parameter", "Andere"} : Amended ToSubclass] uint32 SetSecurityDescriptor([in] Win32_SecurityDescriptor Descriptor);
};

[Description("Die Sicherheitseinstellungen einer Datei oder eines Verzeichnisobjekts.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_SecuritySettingOfLogicalFile : Win32_SecuritySettingOfObject
{
  [Description("Die Datei oder das Verzeichnis") : Amended ToSubclass,key : ToInstance ToSubclass DisableOverride] CIM_LogicalFile Ref Element;
  [Description("Die Sicherheitseinstellungen der Datei oder des Verzeichnisses.") : Amended ToSubclass,key : ToInstance ToSubclass DisableOverride] Win32_LogicalFileSecuritySetting Ref Setting;
};

[Description("Beziehung zwischen den Sicherheitseinstellungen einer Datei oder eines Verzeichnisses und ihrem bzw. seinem Besitzer.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_LogicalFileOwner : Win32_SecuritySettingOwner
{
  [Description("Die Sicherheitseinstellungen des Datei-/Verzeichnisobjekts können nicht aufgelistet werden.") : Amended ToSubclass] Win32_LogicalFileSecuritySetting Ref SecuritySetting;
  [Description("Der Besitzer des Datei-/Verzeichnisobjekts.") : Amended ToSubclass] Win32_SID Ref Owner;
};

[Description("Beziehung zwischen den Sicherheitseinstellungen einer Datei oder eines Verzeichnisses und ihrer bzw. seiner Gruppe.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_LogicalFileGroup : Win32_SecuritySettingGroup
{
  [Description("Die Sicherheitseinstellungen des Datei-/Verzeichnisobjekts können nicht aufgelistet werden.") : Amended ToSubclass] Win32_LogicalFileSecuritySetting Ref SecuritySetting;
  [Description("Die Gruppe des Datei-/Verzeichnisobjekts.") : Amended ToSubclass] Win32_SID Ref Group;
};

[Description("Die Beziehung zwischen einer Datei/einem Verzeichnis und einem DACL-Mitglied.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_LogicalFileAccess : Win32_SecuritySettingAccess
{
  [Description("Die Sicherheitseinstellungen des Datei-/Verzeichnisobjekts können nicht aufgelistet werden.") : Amended ToSubclass] Win32_LogicalFileSecuritySetting Ref SecuritySetting;
  [Description("Ein Eintrag in der Objekt-DACL.") : Amended ToSubclass] Win32_SID Ref Trustee;
};

[Description("Die Beziehung zwischen einer Datei/einem Verzeichnis und einem SACL-Mitglied.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_LogicalFileAuditing : Win32_SecuritySettingAuditing
{
  [Description("Die Sicherheitseinstellungen des Datei-/Verzeichnisobjekts können nicht aufgelistet werden.") : Amended ToSubclass] Win32_LogicalFileSecuritySetting Ref SecuritySetting;
  [Description("Ein Eintrag in der Objekt-SACL.") : Amended ToSubclass] Win32_SID Ref Trustee;
};

[Description("Sicherheitseinstellungen für eine logische Datei") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_LogicalShareSecuritySetting : Win32_SecuritySetting
{
  [key : ToInstance ToSubclass DisableOverride,Description("Freigabename") : Amended ToSubclass] string Name;
  [description("Ermittelt eine strukturelle Darstellung der Sicherheitsbeschreibung des Objekts.\nDie Methode gibt einen ganzzahligen Wert mit folgenden Bedeutungen zurück: \n0 - Erfolgreicher Abschluss\n2 - Der Benutzer ist nicht berechtigt, auf die angeforderten Informationen zuzugreifen.\n8 - Unbekannter Fehler\n9 - Der Benutzer verfügt nicht über die erforderliche Berechtigungen\n21 - Der angegebene Parameter ist ungültig.\nAndere - Andere als oben aufgeführte ganzzahlige Werte finden Sie in der Win32-Fehlercodedokumentation.") : Amended ToSubclass,Values{"Erfolgreich", "Zugriff verweigert", "Unbekannter Fehler", "Fehlende Berechtigung", "Ungültiger Parameter", "Andere"} : Amended ToSubclass] uint32 GetSecurityDescriptor([out] Win32_SecurityDescriptor Descriptor);
  [description("Setzt die Sicherheitsbeschreibung in die angegebene Struktur.\nDie Methode gibt einen ganzzahligen Wert mit folgenden Bedeutungen zurück: \n0 - Erfolgreicher Abschluss\n2 - Der Benutzer ist nicht berechtigt, auf die angeforderten Informationen zuzugreifen.\n8 - Unbekannter Fehler\n9 - Der Benutzer verfügt nicht über die erforderliche Berechtigungen\n21 - Der angegebene Parameter ist ungültig.\nAndere - Andere als oben aufgeführte ganzzahlige Werte finden Sie in der Win32-Fehlercodedokumentation.") : Amended ToSubclass,Values{"Erfolgreich", "Zugriff verweigert", "Unbekannter Fehler", "Fehlende Berechtigung", "Ungültiger Parameter", "Andere"} : Amended ToSubclass] uint32 SetSecurityDescriptor([in] Win32_SecurityDescriptor Descriptor);
};

[Description("Die Sicherheitseinstellungen eines freigegebenen Objekts.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_SecuritySettingOfLogicalShare : Win32_SecuritySettingOfObject
{
  [Description("Die Freigabe.") : Amended ToSubclass,key : ToInstance ToSubclass DisableOverride] Win32_Share Ref Element;
  [Description("Die Sicherheitseinstellungen der Freigabe.") : Amended ToSubclass,key : ToInstance ToSubclass DisableOverride] Win32_LogicalShareSecuritySetting Ref Setting;
};

[Description("Die Beziehung zwischen einer Freigabe und einem DACL-Mitglied.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_LogicalShareAccess : Win32_SecuritySettingAccess
{
  [Description("Die Sicherheitseinstellungen des freigegebenen Objekts.") : Amended ToSubclass] Win32_LogicalShareSecuritySetting Ref SecuritySetting;
  [Description("Ein Eintrag in der Objekt-DACL.") : Amended ToSubclass] Win32_SID Ref Trustee;
};

[Description("Die Beziehung zwischen einer Freigabe und einem SACL-Mitglied.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_LogicalShareAuditing : Win32_SecuritySettingAuditing
{
  [Description("Die Sicherheitseinstellungen des freigegebenen Objekts.") : Amended ToSubclass] Win32_LogicalShareSecuritySetting Ref SecuritySetting;
  [Description("Ein Eintrag in der Objekt-SACL.") : Amended ToSubclass] Win32_SID Ref Trustee;
};

[Description("Die Klasse \"Win32_DCOMApplicationLaunchAllowedSetting\" stellt die Beziehung zwischen der \"Win32_DCOMApplication\" und der Benutzer-SID dar, die diese starten kann.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_DCOMApplicationLaunchAllowedSetting
{
  [Key : ToInstance ToSubclass DisableOverride,Description("Die Referenz \"Element\" stellt die Funktion von Win32_DCOMApplication dar.") : Amended ToSubclass] Win32_DCOMApplication Ref Element;
  [Key : ToInstance ToSubclass DisableOverride,Description("Die Referenz \"Setting\" stellt die Funktion eines Benutzers dar, der eine unter Win32_DCOMApplication gruppierte Komponente starten kann.") : Amended ToSubclass] Win32_SID Ref Setting;
};

[Description("Die Klasse \"Win32_DCOMApplicationAccessAllowedSetting\" stellt die Beziehung zwischen der \"Win32_DCOMApplication\" und der Benutzer-SID dar, die darauf zugreifen kann.") : Amended ToSubclass,AMENDMENT, LOCALE(0x0407) : ToInstance] 
class Win32_DCOMApplicationAccessAllowedSetting
{
  [Key : ToInstance ToSubclass DisableOverride,Description("Die Referenz \"Element\" stellt die Funktion von Win32_DCOMApplication dar.") : Amended ToSubclass] Win32_DCOMApplication Ref Element;
  [Key : ToInstance ToSubclass DisableOverride,Description("Die Referenz \"Setting\" stellt die Funktion eines Benutzers dar, der auf eine unter Win32_DCOMApplication gruppierte Komponente zugreifen kann.") : Amended ToSubclass] Win32_SID Ref Setting;
};
