# Localized	12/07/2019 11:48 AM (GMT)	303:6.40.20520 	MSFT_PackageManagementSource.strings.psd1
#########################################################################################
#
# Copyright (c) Microsoft Corporation.
#
#########################################################################################
ConvertFrom-StringData @'
###PSLOC
StartGetPackageSource=Aufruf von Get-packageSource {0} starten
StartRegisterPackageSource=Aufruf von Register-Packagesource {0} starten
StartUnRegisterPackageSource=Aufruf von UnRegister-Packagesource {0} starten
PackageSourceFound=Die Paketquelle {0} wurde gefunden.
PackageSourceNotFound=Der Paketquelle {0} wurde nicht gefunden.
RegisteredSuccess=Die Paketquelle {0} wurde erfolgreich registriert.
UnRegisteredSuccess=Die Registrierung der Paketquelle {0} wurde erfolgreich aufgehoben.
RegisterFailed=Fehler beim Registrieren der Paketquelle {0}. Meldung: {1}
UnRegisterFailed=Fehler beim Registrieren der Paketquelle {0}. Meldung: {1}
InDesiredState=Die Ressource {0} weist den gewünschten Zustand auf. Der erforderliche Ensure-Wert ist {1} und der tatsächliche Ensure-Wert {2}.
NotInDesiredState=Die Ressource {0} weist nicht den gewünschten Zustand auf. Der erforderliche Ensure-Wert ist {1} und der tatsächliche Ensure-Wert {2}.
NotInDesiredStateDuetoLocationMismatch=Die Ressource {0} weist nicht den gewünschten Zustand auf. Der erforderliche Speicherort ist {1} und der registrierte {2}.
NotInDesiredStateDuetoPolicyMismatch=Die Ressource {0} weist nicht den gewünschten Zustand auf. Die erforderliche Installationsrichtlinie ist {1} und die registrierte {2}.
InstallationPolicyWarning=Registrierung von {0} am Quellspeicherort "{1}" mit Richtlinie {2} starten
###PSLOC

'@

