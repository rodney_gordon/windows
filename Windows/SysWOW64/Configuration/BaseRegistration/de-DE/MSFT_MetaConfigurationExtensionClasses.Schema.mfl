#pragma namespace("\\\\.\\root\\default")
instance of __namespace{ name="ms_407";};
#pragma namespace("\\\\.\\root\\default\\ms_407")

[Description("Die Webdownload-Manager-Klasse, die von 'OMI_ConfigurationDownloadManager' erbt.") : Amended,AMENDMENT, LOCALE(0x0407)] 
class MSFT_WebDownloadManager : OMI_ConfigurationDownloadManager
{
  [Description("Die Zeichenfolgen-URL des Speicherorts des Download-Managers.") : Amended] string ServerURL;
  [Description("Die zum Auffinden des Zertifikats verwendete Zertifikat-ID.") : Amended] string CertificateID;
  [Description("Gibt an, ob der Berichts-Manager unsichere Verbindungen über HTTP verwenden kann.") : Amended] boolean AllowUnsecureConnection; 
  [Description("Registrierungsschlüssel für die Registrierung beim Pull-Server") : Amended ToSubclass] string RegistrationKey;
  [Description("Gruppe von Konfigurationsnamen für die Registrierung beim Pull-Server") : Amended] string ConfigurationNames[];
  [Description("Die Zeichenfolgen-URL des Proxyservers.") : Amended] string ProxyURL;
  [Description("Anmeldeinformationen für den Zugriff auf den Proxyserver") : Amended] MSFT_Credential ProxyCredential;
};

[Description("Die Dateikonfigurationsdownload-Manager-Klasse, die von 'OMI_ConfigurationDownloadManager' erbt.") : Amended,AMENDMENT, LOCALE(0x0407)] 
class MSFT_FileDownloadManager : OMI_ConfigurationDownloadManager
{
  [Description("Die UNC-Pfadzeichenfolge mit dem Speicherort des Download-Managers.") : Amended] string SourcePath;
  [Description("Standardanmeldeinformationen für den Zugriff auf den Dateispeicherort.") : Amended] string Credential;
};


[Description("Die Webressourcenmodul-Manager-Klasse, die von 'OMI_ResourceModuleManager' erbt.") : Amended,AMENDMENT, LOCALE(0x0407)] 
class MSFT_WebResourceManager : OMI_ResourceModuleManager
{
  [Description("Die Zeichenfolgen-URL des Speicherorts des Ressourcenmodul-Managers.") : Amended] string ServerURL;
  [Description("Die zum Auffinden des Zertifikats verwendete Zertifikat-ID.") : Amended] string CertificateID;
  [Description ("Eine boolesche Variable, die unsichere Verbindungen zulässt." ) : Amended] boolean AllowUnsecureConnection;
  [Description("Registrierungsschlüssel für die Registrierung beim Ressourcenrepositoryweb") : Amended ToSubclass] string RegistrationKey;
  [Description("Die Zeichenfolgen-URL des Proxyservers.") : Amended] string ProxyURL;
  [Description("Anmeldeinformationen für den Zugriff auf den Proxyserver") : Amended] MSFT_Credential ProxyCredential;
};

[Description("Die Dateiressourcenmodul-Manager-Klasse, die von der OMI_ResourceModuleManager-Klasse erbt.") : Amended,AMENDMENT, LOCALE(0x0407)] 
class MSFT_FileResourceManager : OMI_ResourceModuleManager
{
  [Description("Die UNC-Pfadzeichenfolge des Dateiressourcen-Managers.") : Amended] string SourcePath;
  [Description("Standardanmeldeinformationen für den Ressourcenzugriff.") : Amended] string Credential;
};

[Description("Die Webberichts-Manager-Klasse, die von der OMI_ReportManager-Klasse erbt.") : Amended,AMENDMENT, LOCALE(0x0407)] 
class MSFT_WebReportManager : OMI_ReportManager
{
  [Description("Die URL-Zeichenfolge mit dem Speicherort des Berichts-Managers.") : Amended] string ServerURL;
  [Description ("Die Zertifikat-ID, mit der das Zertifikat für sichere Verbindungen gesucht wird." ) : Amended] string  CertificateID;
  [Description("Gibt an, ob der Berichts-Manager unsichere Verbindungen über HTTP verwenden kann.") : Amended] boolean AllowUnsecureConnection; 
  [Description("Registrierungsschlüssel für die Registrierung beim Berichtsserver") : Amended ToSubclass] string RegistrationKey;
  [Description("Die Zeichenfolgen-URL des Proxyservers.") : Amended] string ProxyURL;
  [Description("Anmeldeinformationen für den Zugriff auf den Proxyserver") : Amended] MSFT_Credential ProxyCredential;
};

[Description("Stellt eine Klasse für die partielle Konfiguration dar.") : Amended,AMENDMENT, LOCALE(0x0407)] 
class MSFT_PartialConfiguration : OMI_MetaConfigurationResource
{
  [Description("Die Beschreibung der partiellen Konfiguration.") : Amended] String Description;
  [Description("Definiert die Ressourcen, die für die betreffende partielle Konfiguration exklusiv sind.") : Amended] String ExclusiveResources[];
  [Description("Die Quelle des Konfigurationsrepositorys, die von der partiellen Konfiguration verwendet wird.") : Amended] String ConfigurationSource;
  [Description("Eine Abhängigkeitsvariable, die angibt, welche partielle Konfiguration zuvor angewendet werden muss.") : Amended] String DependsOn[];
  [Description("Der Modus zum Aktualisieren des Servers. Gültige Werte sind: Pull, Push und Disabled.") : Amended] string RefreshMode;
};

[Description("Einstellungen des lokalen Konfigurations-Managers.") : Amended,AMENDMENT, LOCALE(0x0407)] 
class MSFT_DSCMetaConfigurationV2
{
  [Description("Das Zeitintervall zwischen aufeinander folgenden Ausführungen, um die Konfiguration erneut anzuwenden und so den gewünschten Zustand zu erhalten.") : Amended] uint32 ConfigurationModeFrequencyMins;
  [Description("Knoten neu starten, falls erforderlich.") : Amended] boolean RebootNodeIfNeeded;
  [Description("Der Modus für den Server zum Übernehmen der Konfiguration.") : Amended] string ConfigurationMode;
  [Description("Der Modus zum Aktualisieren des Servers. Gültige Werte sind: Pull, Push und Disabled.") : Amended] string RefreshMode;
  [Description("Die Aktion nach dem Neustart des Servers. Gültige Werte sind ContinueConfiguration und StopConfiguration.") : Amended] string ActionAfterReboot;
  [Description("Die zum Abrufen der Konfiguration vom Pull-Server verwendete Konfigurations-ID.") : Amended] string ConfigurationID;
  [Description("Das Zeitintervall zwischen aufeinander folgenden Ausführungen, um die Aktion vom Server abzurufen.") : Amended] uint32 RefreshFrequencyMins;
  [Description("Module beim Download vom Pull-Server überschreiben.") : Amended] boolean AllowModuleOverwrite;
  [Description("Debugmodus. Gültige Werte sind: None, ForceModuleImport, ResourceScriptBreakAll oder All.") : Amended] string DebugMode[];
  [Description("Die aktuelle Version des lokalen Konfigurations-Managers.") : Amended] string LCMVersion;
  [Description("Kompatible Versionen des aktuellen lokalen Konfigurations-Managers.") : Amended] string LCMCompatibleVersions[];
  [Description("Der aktuelle Zustand des lokalen Konfigurations-Managers.") : Amended] string LCMState;
  [Description("Zustandsdetails des lokalen Konfigurations-Managers.") : Amended] string LCMStateDetail;
  [Description("Ein Array der Objekte des Konfigurationsdownload-Managers, die Speicherortinformationen zum Herunterladen von Konfigurationen enthalten.") : Amended] string ConfigurationDownloadManagers[];
  [Description("Ein Array der Ressourcenmodul-Manager, die auf einen Speicherort zum Herunterladen fehlender DSC-Ressourcen verweisen.") : Amended] string ResourceModuleManagers[];
  [Description("Ein Array der Berichts-Manager, die auf einen Speicherort verweisen, der die Generierung von DSC-Berichten unterstützen kann.") : Amended] string ReportManagers[];
  [Description("Ein Array der partiellen Konfigurationen, die zur Anwendung gekennzeichnet sind.") : Amended] string PartialConfigurations[];  
  [Description("Die Anzahl der Tage, die der Konfigurationsstatusverlauf beibehalten wird.") : Amended] uint32 StatusRetentionTimeInDays;
  [Description("AgentId des aktuellen DSC-Agents.") : Amended] string AgentId;
  [Description("Aktuelle Richtlinie für die Signaturüberprüfung") : Amended] string SignatureValidationPolicy;
  [Description ("Die Signaturüberprüfungsoptionen des Knotens") : Amended] string  SignatureValidations[];   
  [Description ("Die maximale Modulgröße in MB, die heruntergeladen werden kann.") : Amended] uint32  MaximumDownloadSizeMB; 
};
