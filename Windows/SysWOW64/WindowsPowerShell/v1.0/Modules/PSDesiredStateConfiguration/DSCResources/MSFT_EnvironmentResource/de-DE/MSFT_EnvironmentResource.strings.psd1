# Localized	12/07/2019 08:18 AM (GMT)	303:6.40.20520 	MSFT_EnvironmentResource.strings.psd1
# Localized resources for MSFT_EnvironmentResource

ConvertFrom-StringData @'
###PSLOC
EnvVarCreated=(ERSTELLEN) Umgebungsvariable "{0}" mit dem Wert "{1}"
EnvVarSetError=(FEHLER) Fehler beim Festlegen der Umgebungsvariablen "{0}" auf den Wert "{1}"
EnvVarPathSetError=(FEHLER) Fehler beim Hinzufügen des Pfads "{0}" zur Umgebungsvariablen "{1}" mit dem Wert "{2}"
EnvVarRemoveError=(FEHLER) Fehler beim Entfernen der Umgebungsvariablen "{0}" mit dem Wert "{1}"
EnvVarPathRemoveError=(FEHLER) Fehler beim Entfernen des Pfads "{0}" aus der Variablen "{1}" mit dem Wert "{2}"
EnvVarUnchanged=(UNVERÄNDERT) Umgebungsvariable "{0}" mit dem Wert "{1}"
EnvVarUpdated=(UPDATE) Umgebungsvariable "{0}" von Wert "{1}" auf Wert "{2}"
EnvVarPathUnchanged=(UNVERÄNDERT) Pfadumgebungsvariable "{0}" mit dem Wert "{1}"
EnvVarPathUpdated=(UPDATE) Umgebungsvariable "{0}" von Wert "{1}" auf Wert "{2}"
EnvVarNotFound=(NICHT GEFUNDEN) Umgebungsvariable "{0}"
EnvVarFound=(GEFUNDEN) Umgebungsvariable "{0}" mit dem Wert "{1}
EnvVarFoundWithMisMatchingValue=(ABWEICHUNG GEFUNDEN) Die Umgebungsvariable "{0}" mit dem Wert "{1}" stimmte nicht mit dem angegebenen Wert "{2}" überein.
EnvVarRemoved=(ENTFERNEN) Umgebungsvariable "{0}"
###PSLOC
'@
