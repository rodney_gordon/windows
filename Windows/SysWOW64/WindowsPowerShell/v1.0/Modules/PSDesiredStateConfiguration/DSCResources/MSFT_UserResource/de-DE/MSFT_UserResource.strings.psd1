# Localized	12/07/2019 08:25 AM (GMT)	303:6.40.20520 	MSFT_UserResource.strings.psd1
# Localized resources for MSFT_UserResource

ConvertFrom-StringData @'
###PSLOC
UserWithName=Benutzer: {0}
RemoveOperation=Entfernen
AddOperation=Hinzufügen
SetOperation=Festlegen
ConfigurationStarted=Die Konfiguration des Benutzers "{0}" wurde gestartet.
ConfigurationCompleted=Die Konfiguration des Benutzers "{0}" wurde erfolgreich abgeschlossen.
UserCreated=Der Benutzer "{0}" wurde erfolgreich erstellt.
UserUpdated=Die Eigenschaften des Benutzers "{0}" wurden erfolgreich aktualisiert.
UserRemoved=Der Benutzer "{0}" wurde erfolgreich entfernt.
NoConfigurationRequired=Der Benutzer "{0}" ist auf diesem Knoten mit den gewünschten Eigenschaften vorhanden. Keine Aktion erforderlich.
NoConfigurationRequiredUserDoesNotExist=Der Benutzer "{0}" ist auf diesem Knoten nicht vorhanden. Keine Aktion erforderlich.
InvalidUserName=Der Name "{0}" kann nicht verwendet werden. Namen dürfen nicht ausschließlich aus Punkten und/oder Leerzeichen bestehen oder die folgenden Zeichen enthalten: {1}
UserExists=Ein Benutzer mit dem Namen "{0}" ist vorhanden.
UserDoesNotExist=Es ist kein Benutzer mit dem Namen "{0}" vorhanden.
PropertyMismatch=Für die {0}-Eigenschaft wird der Wert {1} erwartet, der Wert ist jedoch {2}.
PasswordPropertyMismatch=Der Wert der {0}-Eigenschaft stimmt nicht überein.
AllUserPropertisMatch=Alle Eigenschaften von {0} {1} stimmen überein.
ConnectionError=Möglicher Verbindungsfehler beim Versuch, die System.DirectoryServices-API zu verwenden.
MultipleMatches=Mögliche Ausnahme aufgrund mehrerer Übereinstimmungen, als versucht wurde, die System.DirectoryServices-API zu verwenden.
###PSLOC

'@
