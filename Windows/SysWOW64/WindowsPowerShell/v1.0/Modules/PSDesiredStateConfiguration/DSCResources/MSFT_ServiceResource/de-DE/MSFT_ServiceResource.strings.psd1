# Localized	12/07/2019 08:24 AM (GMT)	303:6.40.20520 	MSFT_ServiceResource.strings.psd1
# Localized resources for MSFT_UserResource

ConvertFrom-StringData @'
###PSLOC
ServiceNotFound=Der Dienst "{0}" wurde nicht gefunden.
CannotStartAndDisable=Es ist nicht möglich, einen Dienst zu starten und zu deaktivieren.
CannotStopServiceSetToStartAutomatically=Es ist nicht möglich, einen Dienst zu beenden und auf automatischen Start festzulegen.
ServiceAlreadyStarted=Der Dienst "{0}" wurde bereits gestartet. Keine Aktion erforderlich.
ServiceStarted=Der Dienst "{0}" wurde gestartet.
ServiceStopped=Der Dienst "{0}" wurde beendet.
ErrorStartingService=Fehler beim Starten des Diensts "{0}". Überprüfen Sie den für den Dienst angegebenen Pfad "{1}". Meldung: {2}
OnlyOneParameterCanBeSpecified=Es darf nur einer der folgenden Parameter angegeben werden: {0}, {1}.
StartServiceWhatIf=Dienst starten
ServiceAlreadyStopped=Der Dienst "{0}" wurde bereits beendet. Keine Aktion erforderlich.
ErrorStoppingService=Fehler beim Beenden des Diensts "{0}". Meldung: {1}
ErrorRetrievingServiceInformation=Fehler beim Abrufen der Informationen für den Dienst "{0}". Meldung: {1}
ErrorSettingServiceCredential=Fehler beim Festlegen der Anmeldeinformationen für den Dienst "{0}". Meldung: '{1}'
SetCredentialWhatIf=Anmeldeinformationen festlegen
SetStartupTypeWhatIf=Starttyp festlegen
ErrorSettingServiceStartupType=Fehler beim Festlegen des Starttyps für den Dienst "{0}". Meldung: {1}
TestUserNameMismatch=Der Benutzername für den Dienst "{0}" ist "{1}". Der Name stimmt nicht mit "{2}" überein.
TestStartupTypeMismatch=Der Starttyp für den Dienst "{0}" ist "{1}". Der Typ stimmt nicht mit "{2}" überein.
MethodFailed=Fehler bei der {0}-Methode von "{1}". Fehlercode: "{2}".
ErrorChangingProperty=Fehler beim Ändern der {0}-Eigenschaft. Meldung: {1}
ErrorSetingLogOnAsServiceRightsForUser=Fehler beim Gewähren der Berechtigung für {0} zum Anmelden als Dienst. Meldung: {1}.
CannotOpenPolicyErrorMessage=Der Richtlinien-Manager kann nicht geöffnet werden.
UserNameTooLongErrorMessage=Der Benutzername ist zu lang.
CannotLookupNamesErrorMessage=Fehler beim Suchen des Benutzernamens
CannotOpenAccountErrorMessage=Fehler beim Öffnen der Richtlinie für den Benutzer
CannotCreateAccountAccessErrorMessage=Fehler beim Erstellen der Richtlinie für den Benutzer
CannotGetAccountAccessErrorMessage=Fehler beim Abrufen der Benutzerrichtlinienberechtigungen
CannotSetAccountAccessErrorMessage=Fehler beim Festlegen der Benutzerrichtlinienberechtigungen
BinaryPathNotSpecified=Geben Sie den Pfad zur ausführbaren Datei an, wenn Sie versuchen, einen neuen Dienst zu erstellen.
ServiceAlreadyExists=Der zu erstellende Dienst '{0}' ist bereits vorhanden.
ServiceExistsSamePath=Der zu erstellende Dienst '{0}' ist bereits unter dem Pfad '{1}' vorhanden.
ServiceNotExists=Der Dienst '{0}' ist nicht vorhanden. Geben Sie den Pfad zur ausführbaren Datei an, um einen neuen Dienst zu erstellen.
ErrorDeletingService=Fehler beim Löschen von Dienst '{0}'.
ServiceDeletedSuccessfully=Der Dienst '{0}' wurde erfolgreich gelöscht.
TryDeleteAgain=Es wird zwei Sekunden auf das Löschen des Diensts gewartet.
WritePropertiesIgnored=Der Dienst '{0}' ist bereits vorhanden. Schreibzugriffseigenschaften wie 'Status', 'DisplayName', 'Description', 'Dependencies' werden für vorhandene Dienste ignoriert.
###PSLOC

'@

