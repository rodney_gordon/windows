# Localized	12/07/2019 08:23 AM (GMT)	303:6.40.20520 	MSFT_ProcessResource.strings.psd1
# Localized resources for MSFT_UserResource

ConvertFrom-StringData @'
###PSLOC
FileNotFound=Die Datei wurde nicht im Umgebungspfad gefunden.
AbsolutePathOrFileName=Ein absoluter Pfad oder Dateiname wurde erwartet.
InvalidArgument=Ungültiges Argument: "{0}" mit Wert "{1}".
InvalidArgumentAndMessage={0} {1}
ProcessStarted=Der Prozessauswahlpfad "{0}" wurde gestartet.
ProcessesStopped=Der Prozessauswahlpfad "{0}" mit IDs ({1}) wurde beendet.
ProcessAlreadyStarted=Der Prozessauswahlpfad "{0}" ist aktiv. Keine Aktion erforderlich.
ProcessAlreadyStopped=Der Prozessauswahlpfad "{0}" ist nicht aktiv. Keine Aktion erforderlich.
ErrorStopping=Fehler beim Beenden des Prozessauswahlpfads "{0}" mit IDs "({1})". Meldung: {2}.
ErrorStarting=Fehler beim Starten des Prozessauswahlpfads "{0}". Meldung: {1}.
StartingProcessWhatif=Start-Process
ProcessNotFound=Prozessauswahlpfad "{0}" nicht gefunden
PathShouldBeAbsolute=Der Pfad muss absolut sein.
PathShouldExist=Der Pfad muss vorhanden sein.
ParameterShouldNotBeSpecified=Der {0}-Parameter darf nicht angegeben werden.
FailureWaitingForProcessesToStart=Fehler beim Warten auf den Prozessstart.
FailureWaitingForProcessesToStop=Fehler beim Warten auf das Prozessende.
ErrorParametersNotSupportedWithCredential="StandardOutputPath", "StandardInputPath" oder "WorkingDirectory" können nicht angegeben werden, wenn versucht wird, einen Prozess in einem Benutzerkontext auszuführen.
VerboseInProcessHandle=In Prozesshandle {0}
ErrorInvalidUserName="UserName" {0} ist ungültig.
ErrorRunAsCredentialParameterNotSupported=Der PsDscRunAsCredential-Parameter wird von der Prozessressource nicht unterstützt. Verwenden Sie den Credential-Parameter, um den Prozess mit dem Benutzer "{0}" zu starten.
ErrorCredentialParameterNotSupportedWithRunAsCredential=Der PsDscRunAsCredential-Parameter wird von der Prozessressource nicht unterstützt und kann nicht mit dem Credential-Parameter verwendet werden. Verwenden Sie nur den Credential-Parameter und nicht den PsDscRunAsCredential-Parameter, um den Prozess mit dem Benutzer "{0}" zu starten.
###PSLOC
'@
