# Localized	12/07/2019 08:14 AM (GMT)	303:6.40.20520 	Microsoft.PowerShell.ODataUtilsStrings.psd1
# Localized PSODataUtils.psd1

ConvertFrom-StringData @'
###PSLOC
SelectedAdapter=DOT-Quellentnahme '{0}'.
ArchitectureNotSupported=Das Modul wird in der Prozessorarchitektur ({0}) nicht unterstützt.
ArguementNullError=Fehler beim Generieren des Proxys als '{0}'. Er verweist auf $null in '{1}'.
EmptyMetadata=Die Lesemetadaten waren leer. URL: {0}.
InvalidEndpointAddress=Ungültige Endpunktadresse ({0}). Beim Zugriff auf diese Endpunktadresse wurde eine Webantwort mit dem Statuscode "{1}" empfangen.
NoEntitySets=Die Metadaten von URI "{0}" enthalten keine Entitätenmengen. Es werden keine Ausgabedaten geschrieben.
NoEntityTypes=Die Metadaten von URI "{0}" enthalten keine Entitätstypen. Es werden keine Ausgabedaten geschrieben.
MetadataUriDoesNotExist=Die für URI '{0}' angegebenen Metadaten sind nicht vorhanden. Es wird keine Ausgabe geschrieben.
InValidIdentifierInMetadata=Die für den URI "{0}" angegebenen Metadaten enthalten den ungültigen Bezeichner "{1}". Nur gültige C#-Bezeichner werden während der Proxyerstellung in generierten komplexen Typen unterstützt.
InValidMetadata=Fehler beim Verarbeiten der für URI "{0}" angegebenen Metadaten. Es wird keine Ausgabe geschrieben.
InValidXmlInMetadata=Die für den URI '{0}' angegebenen Metadaten enthalten ungültigen XML-Code. Es werden keine Ausgabedaten geschrieben.
ODataVersionNotFound=Die für den URI '{0}' angegebenen Metadaten enthalten keine OData-Version. Es werden keine Ausgabedaten geschrieben.
ODataVersionNotSupported=Die in den Metadaten für den URI '{1}' angegebene OData-Version '{0}' wird nicht unterstützt. Nur OData-Versionen zwischen '{2}' und '{3}' werden während der Proxygenerierung von '{4}' unterstützt. Es werden keine Ausgabedaten geschrieben.
InValidSchemaNamespace=Die für URI "{0}" angegebenen Metadaten sind ungültig. NULL- oder Empty-Werte werden für das Namespace-Attribut im Schema nicht unterstützt.
InValidSchemaNamespaceConflictWithClassName=Die bei URI "{0}" angegebenen Metadaten enthalten den ungültigen Namespacenamen {1}, der einen Konflikt mit einem anderen Typnamen verursacht. {1} wird in {2} geändert, um einen Kompilierungsfehler zu vermeiden.
InValidSchemaNamespaceContainsInvalidChars=Die bei URI "{0}" angegebenen Metadaten enthalten den ungültigen Namespacenamen {1} mit einer Kombination aus Punkten und Ziffern, die in .NET nicht zulässig ist. {1} wird in {2} geändert, um einen Kompilierungsfehler zu vermeiden.
InValidUri=URI '{0}' ist ungültig. Es wird keine Ausgabe geschrieben.
RedfishNotEnabled=Redfish wird von dieser Version von Microsoft.PowerShell.ODataUtils nicht unterstützt. Führen Sie "update-module Microsoft.PowerShell.ODataUtils" aus, um Unterstützung für Redfish zu erhalten.
EntitySetUndefinedType=Die Metadaten von URI "{0}" enthalten keinen Typ für die Entitätenmenge "{1}". Es werden keine Ausgabedaten geschrieben.
XmlWriterInitializationError=Fehler beim Initiieren von XmlWriter für das Schreiben des CDXML-Moduls '{0}'.
EmptySchema=Der Knoten 'Edmx.DataServices.Schema' darf nicht NULL sein.
VerboseReadingMetadata=Metadaten werden aus dem URI {0} gelesen.
VerboseParsingMetadata=Metadaten werden analysiert...
VerboseVerifyingMetadata=Metadaten werden überprüft...
VerboseSavingModule=Das Ausgabemodul wird unter Pfad '{0}' gespeichert.
VerboseSavedCDXML=Das CDXML-Modul für '{0}' wurde in '{1}' gespeichert.
VerboseSavedServiceActions=Das CDXML-Modul für Dienstaktionen wurde unter '{0}' gespeichert.
VerboseSavedModuleManifest=Das Modulmanifest wurde unter '{0}' gespeichert.
AssociationNotFound=Die Zuordnung '{0}' wurde in 'Metadata.Associations' nicht gefunden.
TooManyMatchingAssociationTypes={0} {1} Zuordnungen wurden in 'Metadata.Associations' gefunden, obwohl nur eine erwartet wurde.
ZeroMatchingAssociationTypes=Die Navigationseigenschaft '{0}' wurde für die Zuordnung '{1}' nicht gefunden.
WrongCountEntitySet=Es wurde ein EntitySet für EntityType '{0}' erwartet, aber {1} war(en) vorhanden.
EntityNameConflictError=Die Proxyerstellung wird nicht unterstützt, wenn mehrere EntitySets demselben EntityType zugeordnet sind. Die Metadaten bei URI '{0}'enthalten die EntitySets '{1}' und '{2}', die demselben EntityType '{3}' zugeordnet sind.
VerboseSavedTypeDefinationModule=Das Typdefinitionsmodul '{0}' wurde unter '{1}' gespeichert.
VerboseAddingTypeDefinationToGeneratedModule=Die Typdefinition für '{0}' wird dem Modul '{1}' hinzugefügt.
OutputPathNotFound=Es wurde kein Teil das Pfads '{0}' gefunden.
ModuleAlreadyExistsAndForceParameterIsNotSpecified=Das Verzeichnis '{0}' ist bereits vorhanden. Verwenden Sie den Parameter '-Force', wenn das Verzeichnis und die Dateien im Verzeichnis überschrieben werden sollen.
InvalidOutputModulePath=Im Pfad '{0}', der für den Parameter '-OutputModule' angegeben ist, fehlt der Modulname.
OutputModulePathIsNotUnique=Der Pfad '{0}', der im Parameter '-OutputModule' angegeben ist, ergibt ausgewertet mehrere Pfade im Dateisystem. Geben Sie für den Parameter '-OutputModule' einen eindeutigen Dateisystempfad an.
OutputModulePathIsNotFileSystemPath=Der Pfad '{0}', der im Parameter '-OutputModule' angegeben ist, ergibt kein Dateisystem. Geben Sie für den Parameter '-OutputModule' einen eindeutigen Dateisystempfad an.
SkipEntitySetProxyCreation=Das Erstellen des CDXML-Moduls für die Entitätenmenge '{0}' wurde übersprungen, weil deren Entitätstyp '{1}' die Eigenschaft '{2}' enthält, die mit einer der Standardeigenschaften der generierten Cmdlets kollidiert.
EntitySetProxyCreationWithWarning=Das CDXML-Modul für die Entitätenmenge '{0}' wurde erfolgreich erstellt, enthält aber die Eigenschaft '{1}' im Entitätstyp '{2}', die mit einer der Standardeigenschaften der generierten Cmdlets kollidiert.
SkipEntitySetConflictCommandCreation=Das Erstellen des CDXML-Moduls für die Entitätenmenge '{0}' wurde übersprungen, weil der exportierte Befehl '{1}' zu einem Konflikt mit dem Eingangsbefehl führt.
EntitySetConflictCommandCreationWithWarning=Das CDXML-Modul für die Entitätenmenge '{0}' wurde erfolgreich erstellt, enthält aber einen '{1}'-Befehl, der mit dem Eingangsbefehl kollidiert.
SkipConflictServiceActionCommandCreation=Das Erstellen des CDXML-Moduls für die Dienstaktion '{0}' wurde übersprungen, weil der exportierte Befehl '{1}' zu einem Konflikt mit dem Eingangsbefehl führt.
ConflictServiceActionCommandCreationWithWarning=Das CDXML-Modul für die Dienstaktion '{0}' wurde erfolgreich erstellt, enthält aber einen '{1}'-Befehl, der mit dem Eingangsbefehl kollidiert.
AllowUnsecureConnectionMessage=Das Cmdlet '{0}' versucht, über den URI '{1}' eine unsichere Verbindung mit dem OData-Endpunkt herzustellen. Geben Sie einen sicheren URI für den Parameter '-{2}' an, oder verwenden Sie den Parameter '-AllowUnsecureConnection', wenn Sie den aktuellen URI verwenden möchten.
ProgressBarMessage=Der Proxy für den OData-Endpunkt in der URI '{0}' wird erstellt.
###PSLOC

'@
