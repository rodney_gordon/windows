# Localized	12/07/2019 11:52 AM (GMT)	303:6.40.20520 	IESecurity_TroubleShooter.psd1
ConvertFrom-StringData @'
###PSLOC
Check_PhishingFilter=Einstellungen des SmartScreen-Filters werden überprüft...
Check_Blockpopups=Popupblocker wird überprüft...
Check_IEsecuritysettings=Sicherheitseinstellungen von Internet Explorer werden überprüft...
Report_name_IEsettings=Sicherheitseinstellungen von Internet Explorer
Report_description_IEsettings=Die Liste mit den Sicherheitseinstellungen für Internet Explorer-Zonen.
keys=Zonenname
zone_name=Zonenname
protected_mode=Geschützter Modus
security_level=Sicherheitsstufe
non_default=Nicht standardmäßig
default=Standard
Message_IESecurityLevels=Die Sicherheitszoneneinstellungen werden durch eine Gruppenrichtlinie gesteuert und können nicht geändert werden.
Report_name_IESecurityLevels=Sicherheitszoneneinstellungen können nicht geändert werden.
Message_Phishingfilter=Der SmartScreen-Filter wird durch eine Gruppenrichtlinie gesteuert und kann nicht aktiviert oder deaktiviert werden.
Report_name_Phishingfilter=SmartScreen-Filtereinstellungen können nicht geändert werden.
Message_Popupblocker=Der Popupblocker wird durch eine Gruppenrichtlinie gesteuert und kann nicht aktiviert oder deaktiviert werden.
Report_name_Popupblocker=Popupblockereinstellungen können nicht geändert werden.
###PSLOC
'@
