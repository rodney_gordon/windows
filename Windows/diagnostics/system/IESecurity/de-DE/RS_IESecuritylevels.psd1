# Localized	12/07/2019 11:53 AM (GMT)	303:6.40.20520 	RS_IEsecuritylevels.psd1
ConvertFrom-StringData @'
###PSLOC
Reset_securitysettings=Dient zum Zurücksetzen der Einstellungen für alle Zonen auf die Standardeinstellungen.
Report_name_resetIEsettings=Sicherheitseinstellungen
Report_description_resetIEsettings=Sicherheitszonen, für die die Sicherheitseinstellungen auf die sicheren Standardwerte festgelegt sind.
Report_name_resetIEprotectedmode=Geschützter Modus
Report_description_resetIEprotectedmode=Sicherheitszonen, für die der geschützte Modus auf die sicheren Standardwerte festgelegt ist.
keys=Zonenname
###PSLOC
'@
