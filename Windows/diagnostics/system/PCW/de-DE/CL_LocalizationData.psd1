# Localized	12/07/2019 11:54 AM (GMT)	303:6.40.20520 	CL_LocalizationData.psd1
# Localized	01/04/2013 11:32 AM (GMT)	303:4.80.0411 	CL_LocalizationData.psd1
ConvertFrom-StringData @'
###PSLOC
Program_Choice_NOTLISTED=Nicht aufgeführt
Version_Choice_DEFAULT=Keine
Version_Choice_WIN8RTM=Windows 8
Version_Choice_WIN7RTM=Windows 7
Version_Choice_WINVISTA2=Windows Vista (Service Pack 2)
Version_Choice_WINXPSP3=Windows XP (Service Pack 3)
Version_Choice_MSIAUTO=Versionsprüfung überspringen
Version_Choice_UNKNOWN=Nicht bekannt
VersionD_Choice_ALL     =  
Display_Choice_DEFAULT=Normal
Display_Choice_256COLOR=256 Farben (8-Bit)
Display_Choice_16BITCOLOR=65.536 Farben (16-Bit)
Display_Choice_640x480=Auflösung 640x480
Display_Choice_HIGHDPIAWARE=Skalierung bei hohem DPI-Wert
Access_Choice_DEFAULT=Normal
Access_Choice_ADMIN=Als Administrator ausführen
Emulation_Choice_DEFAULT=Normal
Emulation_Choice_SAFE=Sichere Emulation
ProblemN_Choice_VERSION=Das Programm war in früheren Versionen von Windows funktionsfähig, kann nun aber nicht mehr installiert oder ausgeführt werden.
ProblemD_Choice_VERSION=Beispiel: Das Setupprogramm wird nicht gestartet.
ProblemN_Choice_DISPLAY=Das Programm wird zwar geöffnet, aber nicht ordnungsgemäß angezeigt.
ProblemD_Choice_DISPLAY=Beispiel: falsche Farben, Größe oder Auflösung
ProblemN_Choice_ACCESS=Für das Programm sind zusätzliche Berechtigungen erforderlich.
ProblemD_Choice_ACCESS=Beispiel: Fehler aufgrund verweigerten Zugriffs werden angezeigt, oder zur Ausführung des Programms werden Administratorrechte angefordert.
ProblemN_Choice_UNKNOWN=Mein Problem ist nicht aufgeführt
ProblemD_Choice_UNKNOWN =  
DisplayN_Choice_256COLOR=Fehlermeldung: Das Programm muss im 256-Farben-Modus (8-Bit) ausgeführt werden
DisplayD_Choice_256COLOR  =   
DisplayN_Choice_16BITCOLOR=Fehlermeldung: Das Programm muss im 65.536-Farben-Modus (16-Bit) ausgeführt werden
DisplayD_Choice_16BITCOLOR  =   
DisplayN_Choice_640x480=Programm startet in kleinem Fenster (640x480 Pixel), kein Wechsel zur Vollbildschirmanzeige möglich.
DisplayD_Choice_640x480   =   
DisplayN_Choice_HIGHDPIAWARE=Programm wird mit den Einstellungen für große Skalen nicht richtig angezeigt
DisplayD_Choice_HIGHDPIAWARE =   
DisplayN_Choice_UNKNOWN=Mein Problem ist nicht aufgeführt
DisplayD_Choice_UNKNOWN =   
Text_FILE_UNSPECIFIED=Nicht angegeben
Text_FILE_INVALID=Am angegebenen Pfad befindet sich kein gültiges Programm. Wählen Sie ein gültiges Programm (mit der Endung ".exe" oder ".msi") aus.
Text_FILE_PROTECTED=Am angegebenen Pfad befindet sich ein Programm, das von dieser Problembehandlung nicht geändert werden kann. Wählen Sie ein anderes Programm aus.
Text_Activity_TROUBLESHOOTING=Problembehandlung
Text_Activity_SAVING=Einstellungen werden gespeichert
Text_Status_SEARCHING=Suchen nach Programmen...
Text_Status_GENERATING=Bericht wird generiert...
Text_Version_Title=Windows-Kompatibilitätsmodus:
Text_Display_Title=Anzeigeeinstellungen:
Text_Access_Title=Benutzerkontensteuerung:
Text_Emulation_Title=Emulationseinstellungen:
Text_Display_Warning=Die Anzeigeeinstellungen können sichtbare Auswirkungen auf die Darstellung anderer Programme haben.  Schließen Sie das Programm nach dem Testen, um die Darstellung wiederherzustellen.
Text_Report_CompatName=Kompatibilitätsmodus
Text_Report_CompatDesc=Angewendete Kompatibilitätsmodi:
Text_Report_SolutionYes=Benutzerüberprüfung: Fehlerkorrektur war erfolgreich
Text_Report_SolutionNo=Benutzerüberprüfung: Fehlerkorrektur war nicht erfolgreich
Throw_NO_MODE=Es wurde kein Modus angegeben.
Throw_INVALID_PATH=Es wurde ein ungültiger Zielpfad angegeben.
Throw_NO_PATH=Es wurde kein Pfad angegeben.
###PSLOC
'@
