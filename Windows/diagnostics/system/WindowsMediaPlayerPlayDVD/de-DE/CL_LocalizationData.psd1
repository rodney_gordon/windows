# Localized	12/07/2019 11:56 AM (GMT)	303:6.40.20520 	CL_LocalizationData.psd1
ConvertFrom-StringData @'
###PSLOC
Troubleshoot_Title=Problembehandlung
Troubleshoot_DetectDVDDevice=DVD-Gerät wird überprüft...
Troubleshoot_DetectDVDvideoDecoder=Videodecoder für DVD-Wiedergabe wird überprüft...
Troubleshoot_DetectDVDAudioDecoder=Audiodecoder für DVD-Wiedergabe wird überprüft...
Resolution_Title=Auflösung
CDRomDeviceName=Name
PnPDeviceID=ID
Status=Status
Report_Name_CDRomDeviceInformation=DVD-/CD-ROM-Laufwerke
Report_Description_CDRomDeviceInformation=Informationen zu den DVD/CD-ROM-Laufwerken
###PSLOC
'@
