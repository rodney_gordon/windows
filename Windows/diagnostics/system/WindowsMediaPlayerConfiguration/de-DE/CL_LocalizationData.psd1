# Localized	12/07/2019 11:56 AM (GMT)	303:6.40.20520 	CL_LocalizationData.psd1
ConvertFrom-StringData @'
###PSLOC
Troubleshoot_Title=Problembehandlung
Troubleshoot_DetectNetworkCache=Netzwerkcache von Windows Media Player wird überprüft...
Resolution_Title=Auflösung
Resolution_ResetConfiguration=Alle Konfigurationseinstellungen werden zurückgesetzt...
Resolution_ResetNetworkCache=Netzwerkcachekonfigurationen werden zurückgesetzt...
BinaryPath=Binärpfad
Version=Version
Report_Name_WMPInformation=Analysierte Version
Report_Description_WMPInformation=Windows Media Player-Informationen
Report_Name_NetWorkCachedFile=Zwischengespeicherte Konfiguration
Report_Description_NetWorkCachedFile=Informationen zur zwischengespeicherten Windows Media Player-Netzwerkkonfiguration
###PSLOC
'@
