# Localized	12/07/2019 11:56 AM (GMT)	303:6.40.20520 	CL_LocalizationData.psd1
ConvertFrom-StringData @'
###PSLOC

id_name_rc_datastoreprogress=Windows Update-Datenbank wird neu erstellt...
id_status_stop_service=%servicename%-Dienst wird beendet
id_clear_service=%servicename%-Warteschlange wird bereinigt
id_status_start_service=%servicename%-Dienst wird gestartet
ID_CollectFileProgressActivity=Dateien werden gesammelt. Dies kann einige Minuten dauern.
ID_ERROR_MSG_SERVICE=Problem mit %ServiceName%
ID_pending_restart=Auf ausstehenden Neustart prüfen
ID_pending_update=Auf ausstehende Updates prüfen
ID_pending_updates_status=Dies kann einige Minuten dauern...
ID_dism_check=Systemdateien werden überprüft und repariert.
ID_WAAS_MEDIC_ISSUE_FOUND=Problem gefunden von:
ID_WAAS_MEDIC_ISSUE_FIXED=Probleme behoben von:
###PSLOC
'@

