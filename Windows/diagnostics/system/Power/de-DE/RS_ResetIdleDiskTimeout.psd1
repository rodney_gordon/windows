# Localized	12/07/2019 11:55 AM (GMT)	303:6.40.20520 	RS_ResetIdleDiskTimeout.psd1
ConvertFrom-StringData @'
###PSLOC
reset_DiskIdleTimeout=Zurücksetzen des Leerlaufzeitlimits für den Datenträger
Report_name_DiskIdleTimeout_AC_result=Das Ergebnis des Leerlaufzeitlimits für den Datenträger für Wechselstrom
Report_name_DiskIdleTimeout_DC_result=Das Ergebnis des Zurücksetzens des Leerlaufzeitlimits für den Datenträger für Gleichstrom
Report_name_DiskIdleTimeout=Anpassung des Leerlaufzeitlimits für Datenträger
AC_settingvalue_original=Ursprüngliche Wechselstromeinstellung
DC_settingvalue_original=Ursprüngliche Gleichstromeinstellung
AC_settingvalue_reset=Neue Wechselstromeinstellung
DC_settingvalue_reset=Neue Gleichstromeinstellung
###PSLOC
'@

