# Localized	12/07/2019 11:55 AM (GMT)	303:6.40.20520 	RS_AdjustScreenBrightness.psd1
ConvertFrom-StringData @'
###PSLOC
reset_ScreenBrightness=Bildschirmhelligkeit wird zurückgesetzt.
Report_name_ScreenBrightness_DC_result=Das Ergebnis des Zurücksetzens der Bildschirmhelligkeit für Gleichstrom.
Report_name_ScreenBrightness=Anpassung der Bildschirmhelligkeit
AC_settingvalue_original=Ursprüngliche Wechselstromeinstellung
DC_settingvalue_original=Ursprüngliche Gleichstromeinstellung
AC_settingvalue_reset=Neue Wechselstromeinstellung
DC_settingvalue_reset=Neue Gleichstromeinstellung
###PSLOC
'@

