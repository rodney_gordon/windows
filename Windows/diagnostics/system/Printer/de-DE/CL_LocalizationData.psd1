# Localized	12/07/2019 11:56 AM (GMT)	303:6.40.20520 	CL_LocalizationData.psd1
ConvertFrom-StringData @'
###PSLOC
progress_ts_spoolerService=Spoolerdienst wird überprüft...
progress_rs_setSpoolerStartMode=Druckspoolerdienst wird für den automatischen Start festgelegt...
progress_rs_startSpoolerService=Spoolerdienst wird gestartet...
progress_ts_defaultPrinter=Standarddrucker wird gesucht...
progress_rs_wrongDefaultPrinter=Standarddrucker wird festgelegt...
progress_ts_noPrinterInstalled=Installierte Drucker werden gesucht...
progress_rs_noPrinterInstalled=Drucker wird installiert...
progress_ts_printJobsStuck=Druckwarteschlange wird überprüft...
progress_rs_cancelAllJobs=Alle Druckaufträge werden abgebrochen...
progress_rs_ProcessCurrentPrintJobs=Aktuelle Druckaufträge werden verarbeitet...
progress_rs_restartSpoolerService=Spoolerdienst wird neu gestartet...
progress_rs_deletePrintJobs=Druckaufträge werden gelöscht...
progress_ts_spoolerCrashing=Es wird auf Spoolerdienstfehler geprüft...
progress_rs_spoolerCrashing=Spoolerdienstabstürze werden behoben...
progress_ts_printerTurnedOff=Druckerenergiestatus wird überprüft...
progress_ts_outOfToner=Druckerpatrone wird überprüft...
progress_ts_outOfPaper=Papierschacht wird überprüft...
progress_ts_paperJam=Es wird auf Papierstau geprüft...
progress_ts_cannotConnect=Netzwerkdruckerkonnektivität wird überprüft...
progress_ts_printerDriver=Es wird nach Druckertreiberupdates gesucht...
progress_rs_printerDriver=Druckertreiber werden aktualisiert...
progress_ts_printerDriverError=Druckertreiberfehler werden gesucht...
deletedFiles_name=Gelöschte Druckdateien
cannotDeletedFiles_name=Verbleibende Druckdateien
cannotDeletedFiles_description=Einige Druckdateien konnten nicht entfernt werden. Durch manuelles Löschen dieser Dateien wird das Druckerproblem möglicherweise behoben.
printJobs_name=Druckaufträge
printJobs_printerName=Druckername
printJobs_userName=Benutzername
printJobs_status=Status des Druckauftrags
defaultPrinter_name=Geänderter Standarddrucker
defaultPrinter_oldDefaultPrinter=Alter Standarddrucker
defaultPrinter_newDefaultPrinter=Neuer Standarddrucker
newPrinter_printerName=Druckername
newPrinter_name=Neuer installierter Drucker
spoolerEvent_name=Spoolerdienstereignisse
spoolerEvent_description=Die Ereignisse, die auf einen Absturz des Spoolers hinweisen. Die Ereignisladung gibt möglicherweise Aufschluss über das fehlerhafte Modul.
printerPort_name=TCP/IP-Druckeranschluss
printerPort_portName=Anschlussname
printerPort_portNumber=Anschlussnummer
fileName=Dateiname
error_information=Ausnahmeinformationen
error_function_name=Fehler beim Aufrufen von "{0}".
error_function_description=Ausnahme beim Aufruf von Funktion "{0}".
error_file_name=Fehler beim Ausführen von "{0}".
error_file_description=Ausnahme bei der Ausführung von Datei "{0}".
throw_invalidFileName=Ungültiger Dateiname
throw_noPrinterDeviceIDSpecified=Es wurde keine Drucker-ID angegeben.
throw_noPrinterNameSpecified=Kein Druckername angegeben.
throw_win32APIFailed=Fehler beim Aufrufen von win32 API "{0}". Fehlercode: "{1}".
throw_invalidPrinterName=Ungültiger Druckername
###PSLOC
'@
