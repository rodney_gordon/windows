# Localized	12/07/2019 11:51 AM (GMT)	303:6.40.20520 	IEBrowseWeb_TroubleShooter.psd1
ConvertFrom-StringData @'
###PSLOC
Check_DefectiveAddon=Es wird nach fehlerhaften Add-Ons gesucht...
Check_tempfilecachesize=Die Größe des temporären Dateicaches wird überprüft...
Check_Pagecachesyncsettings=Die Seitencache-Synchronisierungseinstellungen werden überprüft...
Check_IEserverconnections=Die Serververbindungseinstellungen für Internet Explorer werden überprüft...
Report_name_defectiveaddon=Letzte Internet Explorer-Fehler
Report_description_defectiveaddon=Liste der Add-Ons, aufgrund deren Internet Explorer in den vergangenen sieben Tagen nicht mehr reagierte
Report_name_crashedIEAddon=Letzte Fehler in Internet Explorer
Report_description_crashedIEAddon=Ereignisinformationen zur Anzahl nicht erfolgter Antworten von Internet Explorer in den vergangenen sieben Tagen.
Report_name_detectedIEAddon=Alle Add-Ons für Internet Explorer wurden installiert.
Report_description_detectedIEAddon=Liste aller installierten Add-Ons für Internet Explorer.
file_description=Dateibeschreibung
file_version=Dateiversion
product_name=Produktname
date_modified=Änderungsdatum
full_path=Vollständiger Pfad
Message_IEAddon=Die Add-Ons für Internet Explorer werden durch die Gruppenrichtlinie gesteuert. Sie können weder aktiviert noch deaktiviert werden.
Report_name_IEAddon=Add-Ons für Internet Explorer können weder aktiviert noch deaktiviert werden.
Report_name_LoadTimeAddon=Ladezeiten für Internet Explorer-Add-Ons
Report_description_LoadTimeAddon=Liste der Add-Ons für Internet Explorer mit ihren durchschnittlichen Ladezeiten.
load_time=Ladezeit
###PSLOC
'@
